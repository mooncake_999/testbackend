﻿using MyEnergi.Data.Entity;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.Interfaces
{
    public interface IDeleteDeviceRequestEmailSender
    {
        Task SendDeleteDeviceRequestMail(User user, Device device);
    }
}
