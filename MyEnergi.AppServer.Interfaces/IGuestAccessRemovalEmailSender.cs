﻿using System.Threading.Tasks;

namespace MyEnergi.AppServer.Interfaces
{
    public interface IGuestAccessRemovalEmailSender
    {
        Task SendGuestAccessRemovalEmail(string guestEmail, string guestFirstName, string hubSerialNo);
    }
}
