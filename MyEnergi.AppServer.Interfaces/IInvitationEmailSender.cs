﻿using MyEnergi.AppServer.Model.Email;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.Interfaces
{
    public interface IInvitationEmailSender
    {
        MailConfigurationData SetConfigurationData(bool appAccess, IEnumerable<string> features);
        
        Task SendInvitationEmail(Guid invitationGuid, User owner, string guestMail, string firstName,
            MailConfigurationData dataConfig);
    }
}
