﻿using MyEnergi.AppServer.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.Interfaces
{
    public interface IOneSignal
    {
        string SetResponse(List<PropertiesData> objList, string heading, string content);
        Task PushNotification(string json, string serialNo);
    }
}
