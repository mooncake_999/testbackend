﻿using System.Threading.Tasks;

namespace MyEnergi.AppServer.Interfaces
{
    public interface IRegistrationEmailSender
    {
        Task SendRegistrationMail(string userCognitoId, string filePath, string sender, string mailAddress,
            string mailSubject);
    }
}
