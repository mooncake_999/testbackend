﻿using System;

namespace MyEnergi.AppServer.Model
{
    public class DeviceData
    {
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string SerialNumber { get; set; }
        public string Firmware { get; set; }
        public DateTime LastActiveTime { get; set; }
        public string HeaterOutput1 { get; set; }
        public string HeaterOutput2 { get; set; }
        public string TimeZoneRegion { get; set; }
    }
}
