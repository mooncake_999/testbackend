﻿namespace MyEnergi.AppServer.Model.Email
{
    public class EmailMessage
	{
		public EmailMessage()
		{
			ToAddress = new EmailAddress();
			FromAddress = new EmailAddress();
		}
        
		public EmailAddress ToAddress { get; set; }
		public EmailAddress FromAddress { get; set; }
		public string Subject { get; set; }
		public string Content { get; set; }
		public string CTA { get; set; }
		public string CTAApp { get; set; }
	}
}
