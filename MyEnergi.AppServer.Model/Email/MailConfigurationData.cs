﻿namespace MyEnergi.AppServer.Model.Email
{
    public class MailConfigurationData
    {
        public string FilePath { get; set; }
        public string Sender { get; set; }
        public string MailAddress { get; set; }
        public string MailSubject { get; set; }
        public string MailCTA { get; set; }
        public string MailCTAApp { get; set; }
        public string ToMailAddress { get; set; }
        public string ToName { get; set; }
    }
}

