﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyEnergi.AppServer.Model
{
    public class EnergyAPIPriceData
    {
        public string GridSupplyPoint { get; set; }
        public float ValueWithoutVAT { get; set; }
        public float ValueWithVAT { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Currency { get; set; }

    }
}
