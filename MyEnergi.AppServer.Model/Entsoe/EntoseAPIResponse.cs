﻿using System.Collections.Generic;

namespace MyEnergi.AppServer.Model.Entsoe
{
    public class EntoseAPIResponse
    {
        public string Currency { get; set; }
        public string MeasureUnit { get; set; }
        
        public List<EntsoePriceData> Prices { get; set; }
    }
}
