﻿using System;

namespace MyEnergi.AppServer.Model.Entsoe
{
    public class EntsoePriceData
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        
        public int Hour { get; set; }
        public float Price { get; set; }
    }
}
