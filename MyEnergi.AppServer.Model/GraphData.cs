﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyEnergi.AppServer.Model
{
    public class GraphData
    {
        public DateTime QueryDate { get; set; }
        public int Imported { get; set; }
        public int Exported { get; set; }
        public int Generated { get; set; }
        public int Consumed { get; set; }
        public int H1d { get; set; }
        public int H2d { get; set; }
        public int H3d { get; set; }
        public int H1b { get; set; }
        public int H2b { get; set; }
        public int H3b { get; set; }
    }
}
