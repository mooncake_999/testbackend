﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyEnergi.AppServer.Model
{
    public class Headings
    {
        [JsonProperty("en")]
        public string En { get; set; }
    }

    public class Contents
    {
        [JsonProperty("en")]
        public string En { get; set; }
    }

    public class OneSignalRequest
    {
        [JsonProperty("app_id")]
        public string AppId { get; set; }

        [JsonProperty("include_external_user_ids")]
        public List<string> IncludeExternalUserIds { get; set; }

        [JsonProperty("channel_for_external_user_ids")]
        public string ChannelForExternalUserIds { get; set; }

        [JsonProperty("headings")]
        public Headings Headings { get; set; }

        [JsonProperty("contents")]
        public Contents Contents { get; set; }
    }
}
