﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyEnergi.AppServer.Model
{
    public class PropertiesData
    {
        public string Id { get; set; }
        public bool EnableAll { get; set; }
        public long LastActive { get; set; }
    }
}
