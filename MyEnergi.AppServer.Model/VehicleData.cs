﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.Model
{
    public class VehicleData
    {
        public int RefId { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int? AvailabilityDateFrom { get; set; }
        public int? AvailabilityDateTo { get; set; }
        public int? ManufacturingYear { get; set; }
        public float? BatterySize { get; set; }
        public int? ManufacturerOfficialEVRange { get; set; }
        public int? RealEVRange { get; set; }
        public string VehicleNickName { get; set; }
        public float? ChargeRate { get; set; }
    }
}
