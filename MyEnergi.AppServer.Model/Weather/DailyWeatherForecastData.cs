﻿namespace MyEnergi.AppServer.Model.Weather
{
    public class DailyWeatherForecastData : WeatherForecastData
    {
        public string DateToDisplay { get; set; }
        
        public double? DailyMinTemp { get; set; }
        
        public double? DailyMaxTemp { get; set; }
    }
}
