﻿namespace MyEnergi.AppServer.Model.Weather
{
    public class HourlyWeatherForecastData : WeatherForecastData
    {
        public int HourOfTheDay => DateTime.Hour;

        public double? SolarRadiation { get; set; }
    }
}
