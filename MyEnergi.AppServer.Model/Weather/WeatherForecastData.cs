﻿using System;

namespace MyEnergi.AppServer.Model.Weather
{
    public class WeatherForecastData
    {
        public DateTime DateTime { get; set; }

        public string WindDirection { get; set; }
        public double? WindSpeed { get; set; }

        public string WeatherIconCode { get; set; }
        public string WeatherDescription { get; set; }

        public double? Temperature { get; set; }
    }
}
