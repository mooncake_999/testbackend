﻿using MyEnergi.AppServer.Model;
using System.Net.Http;

namespace MyEnergi.AppServer
{
    public abstract class ApiCaller
    {
        protected abstract string GenerateApiUrl();
        protected abstract ApiCallResponse DeserializeJson(HttpResponseMessage response);

        public abstract ApiCallResponse GetResult();
    }
}
