﻿using MyEnergi.AppServer.Model;
using Serilog;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyEnergi.AppServer
{
    public abstract class AppServerApiCaller : ApiCaller
    {
        private readonly string _userName;
        private readonly string _password;
        internal string HostServer;
        private string _logUrl;
        private int _noOfTries;

        internal AppServerApiCaller(string userName, string password)
        {
            _userName = userName;
            _password = password;
        }

        public override ApiCallResponse GetResult()
        {
            _noOfTries = 0;
            var answer = DetermineHubsHostServer();

            var result = new ApiCallResponse
            {
                HttpStatusCode = answer.StatusCode,
                HttpMessage = HandleResponseMessage(answer) ?? answer.ReasonPhrase
            };

            if (answer.IsSuccessStatusCode)
            {
                var desResult = DeserializeJson(answer);
                result.Status = desResult.Status;
                result.StatusText = desResult.StatusText;
                result.Content = desResult.Content;
                result.HostServer = HostServer;
            }
            else
            {
                Log.Warning(
                    $"The call to {_logUrl} for hub {_userName} has returned: {answer.StatusCode},  {answer.ReasonPhrase}.");
            }

            return result;
        }

        private HttpResponseMessage GetApiResponse(string url = null)
        {
            var uri = new Uri(url ?? GenerateApiUrl());
            _logUrl = uri.AbsoluteUri;

            var credCache = new CredentialCache
            {
                {
                    new Uri(uri.GetLeftPart(UriPartial.Authority)),
                    "Digest",
                    new NetworkCredential(_userName, _password)
                }
            };

            HttpResponseMessage response;
            try
            {
                using var httpClient = new HttpClient(new HttpClientHandler { Credentials = credCache })
                {
                    Timeout = TimeSpan.FromMinutes(5)
                };
                Log.Information($"A call to {uri} is made, for hub {_userName}");
                response = httpClient.GetAsync(uri).Result;
            }
            catch (TaskCanceledException ex)
            {
                Log.Warning($"Task cancellation was requested. {ex}");
                throw;
            }
            catch (Exception e)
            {
                if (e.InnerException is HttpRequestException)
                {
                    Log.Warning($"There was a host server {HostServer} issue when calling {url} for hub {_userName}. {e}. Retrying.");
                    if (_noOfTries < 3)
                    {
                        HostServer = null;
                        _noOfTries++;
                        return _ = DetermineHubsHostServer();
                    }

                    Log.Warning($"The maximum number of tries to call {url} has been exceeded for hub {_userName}.");
                    throw;
                }
                
                Log.Warning($"There was a timeout issue when calling {url} for hub {_userName}. {e}");
                throw;
            }

            return response;
        }

        private HttpResponseMessage DetermineHubsHostServer()
        {
            HttpResponseMessage answer;
            string newHost;

            if (string.IsNullOrEmpty(HostServer))
            {
                answer = GetApiResponse("https://director.myenergi.net");
                HostServer = answer.Headers.GetValues("X_MYENERGI-asn").FirstOrDefault();
            }

            var noOfTries = 0;
            do
            {
                newHost = HostServer;
                answer = GetApiResponse();
                noOfTries++;
                var hasHeader = answer.Headers.TryGetValues("X_MYENERGI-asn", out var headerVal);
                HostServer = hasHeader ? headerVal.FirstOrDefault() : HostServer;
            } while (noOfTries < 5 && answer.StatusCode == HttpStatusCode.Unauthorized && HostServer != newHost);

            return answer;
        }
        private string HandleResponseMessage(HttpResponseMessage answer)
        {
            var message = string.Empty;

            switch (answer.StatusCode)
            {
                case HttpStatusCode.Forbidden:
                    message = "Access denied.";
                    break;
                case HttpStatusCode.Unauthorized:
                    message = "Invalid serial number or password.";
                    break;
                case HttpStatusCode.InternalServerError:
                    message = "The server encountered a problem.";
                    break;
                case HttpStatusCode.NotImplemented:
                    message = "The server doesn't support the requested function.";
                    break;
            }

            return string.IsNullOrEmpty(message) ? null : message;
        }
    }
}
