﻿using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Linq;
using System.Net.Http;

namespace MyEnergi.AppServer
{
    public class DeviceBoostTime : AppServerApiCaller
    {
        private readonly string _deviceType;        //Z from Zappi, E from Eddi etc
        private readonly string _deviceSNo;         //12312312
        private readonly string _chargingSlot;      //11,12,13,14; 21,22,23,24 where first number means device output(for zappi only car but for eddi heater 1 and 2) and second number means charge slot
        private readonly string _chargingFrom;      //HHMM format (ex: 4:30 PM = 1630)
        private readonly string _chargeTime;        //HMM format, how much time will charge(2 h and 30 min = 230) works up till 9 h and min must be 0,15,30,45
        private readonly string _chargeDays;        //string of 7 digits each representing day of week (ex: just tuesday = 0100000, monday& friday = 1000100)

        public DeviceBoostTime(string hostServer, string deviceType, string deviceSNo,
            string chargingSlot, string chargingFrom, string chargeTime, string chargeDays, string serialNo, string password) : base(serialNo, password)
        {
            HostServer = hostServer;
            _deviceType = deviceType;
            _deviceSNo = deviceSNo;
            _chargingSlot = chargingSlot;
            _chargingFrom = chargingFrom;
            _chargeTime = chargeTime;
            _chargeDays = chargeDays;
        }
        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            Log.Information($"Calling {response?.RequestMessage?.RequestUri}");
            var result = response.Content.ReadAsStringAsync().Result;
            JObject allData = null;
            int? callStatus = null;
            try
            {
                allData = (JObject)JsonConvert.DeserializeObject(result);
                callStatus = allData?["status"]?.Value<int>();
                if (callStatus != null)
                    Log.Error($"Call to {response?.RequestMessage?.RequestUri} got a response status of {callStatus.Value}");
                else
                    Log.Information($"Call to {response?.RequestMessage?.RequestUri} was successfull");
            }
            catch (Exception ex)
            {
                Log.Error($"Deserializing response from {response?.RequestMessage?.RequestUri} threw error {ex.Message}");
            }

            if (allData == null) return new ApiCallResponse { Status = -1 };

            return new ApiCallResponse
            {
                Status = callStatus ?? 0,
                StatusText = allData?["statustext"]?.Value<string>() ?? ""
            };
        }

        protected override string GenerateApiUrl()
        {
            var url = $"https://{HostServer}/cgi-boost-time-{_deviceType.ToUpper().First()}{_deviceSNo}-{_chargingSlot}-{_chargingFrom}-{_chargeTime}-0{_chargeDays}";
            return url;
        }
    }
}
