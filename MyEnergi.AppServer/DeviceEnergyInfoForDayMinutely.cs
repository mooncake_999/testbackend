﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace MyEnergi.AppServer
{
    public class DeviceEnergyInfoForDayMinutely : AppServerApiCaller
    {
        private readonly string _deviceType;
        private readonly string _deviceSNo;
        private readonly DateTime _queryDate;
        private readonly int _minutesInterval;

        public DeviceEnergyInfoForDayMinutely(ILogger log, string serialNo, string password, string hostServer, string deviceType, string deviceSNo, DateTime queryDate, int minutesInterval = 0) : base(serialNo, password)
        {
            Log.Logger = log;
            HostServer = hostServer;
            _deviceType = deviceType;
            _deviceSNo = deviceSNo;
            _queryDate = queryDate;
            _minutesInterval = minutesInterval;
        }

        protected override string GenerateApiUrl()
        {
            return _minutesInterval == 0
                ? $"https://{HostServer}/cgi-jday-{_deviceType.ToUpper().First()}{_deviceSNo}-{_queryDate.Year}-{_queryDate.Month}-{_queryDate.Day}"
                : $"https://{HostServer}/cgi-jday-{_deviceType.ToUpper().First()}{_deviceSNo}-{_queryDate.Year}-{_queryDate.Month}-{_queryDate.Day}-{_queryDate.Hour}-{_queryDate.Minute}-{_minutesInterval}";
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            try
            {
                var allData = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                if (allData == null || allData.Value<JObject>().Properties().First().Name == "status")
                    return new ApiCallResponse
                    {
                        Status = -1,
                        Content = new List<object>()
                    };

                var deviceData = allData[$"U{_deviceSNo}"].Value<JArray>();

                var devicesInfo = new List<GraphData>();

                foreach (var data in deviceData)
                {
                    devicesInfo.Add(new GraphData
                    {
                        QueryDate = new DateTime(_queryDate.Year, _queryDate.Month, _queryDate.Day, data["hr"]?.Value<int>() ?? 0, data["min"]?.Value<int>() ?? 0, 0),
                        Imported = data["imp"]?.Value<int>() ?? 0,
                        Exported = (data["exp"]?.Value<int>() ?? 0) * -1,
                        Generated = data["gep"]?.Value<int>() ?? 0,
                        Consumed = (data["gep"]?.Value<int>() ?? 0) + ((data["exp"]?.Value<int>() ?? 0) * -1),
                        H1d = data["h1d"]?.Value<int>() ?? 0,
                        H2d = data["h2d"]?.Value<int>() ?? 0,
                        H3d = data["h3d"]?.Value<int>() ?? 0,
                        H1b = data["h1b"]?.Value<int>() ?? 0,
                        H2b = data["h2b"]?.Value<int>() ?? 0,
                        H3b = data["h3b"]?.Value<int>() ?? 0
                    });
                }

                return new ApiCallResponse
                {
                    Content = devicesInfo
                };
            }
            catch (Exception e)
            {
                Log.Warning($"Data deserialization went wrong. {e}");
                var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new ApiCallResponse
                {
                    Status = data?["status"]?.Value<int>() ?? -1,
                    StatusText = data?["statustext"]?.Value<string>() ?? ""
                };
            }
        }
    }
}
