﻿using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace MyEnergi.AppServer
{
    public class EddiHeaterPriority : AppServerApiCaller
    {
        private readonly string _deviceSNo;

        public EddiHeaterPriority(string hostServer, string deviceSNo, string hubSerialNo, string password) : base(hubSerialNo, password)
        {
            HostServer = hostServer;
            _deviceSNo = deviceSNo;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            var result = response.Content.ReadAsStringAsync().Result;

            var allData = (JObject)JsonConvert.DeserializeObject(result);
            if (allData == null) return new ApiCallResponse { Status = -1 };

            return new ApiCallResponse
            {
                Content = allData?["hpri"]?.Value<int>()
            };

        }

        protected override string GenerateApiUrl()
        {
            var url = $"https://{HostServer}/cgi-set-heater-priority-E{_deviceSNo}";
            return url;
        }
    }
}
