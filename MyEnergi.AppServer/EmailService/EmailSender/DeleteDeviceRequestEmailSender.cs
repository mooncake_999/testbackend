﻿using HandlebarsDotNet;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.AppServer.Model.Email;
using MyEnergi.Data.Entity;
using System.IO;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.EmailService.EmailSender
{
    public class DeleteDeviceRequestEmailSender : EmailService, IDeleteDeviceRequestEmailSender
    {
        public DeleteDeviceRequestEmailSender(IConfiguration configuration) :
            base(configuration)
        {
        }

        public async Task SendDeleteDeviceRequestMail(User user, Device device)
        {
            var dataConfig = SetConfigurationData();

            var email = new EmailMessage();
            const string address = "daniela.keszeg@deleteagency.com";
            email.ToAddress = new EmailAddress { Address = address, Name = dataConfig.ToName };
            email.FromAddress = new EmailAddress { Address = dataConfig.MailAddress, Name = dataConfig.Sender };
            email.Subject = dataConfig.MailSubject;
            email.Content = CreateEmailContent(user, dataConfig, device);

            await ExecuteOperationsToSendEmail(email);
        }

        private MailConfigurationData SetConfigurationData()
        {
            var sender = Configuration["EmailConfigurationDeviceDeleted:MailSender"];
            var mailSubject = Configuration["EmailConfigurationDeviceDeleted:MailSubject"];
            var mailAddress = Configuration["EmailConfigurationDeviceDeleted:MailAddress"];
            var toMailAddress = Configuration["EmailConfigurationDeviceDeleted:ToMailAddress"];
            var toName = Configuration["EmailConfigurationDeviceDeleted:ToName"];
            var filePath = Path.Combine(Directory.GetCurrentDirectory(),
                Configuration["EmailConfigurationDeviceDeleted:Path"]);

            return new MailConfigurationData
            {
                FilePath = filePath,
                Sender = sender,
                MailAddress = mailAddress,
                MailSubject = mailSubject,
                ToMailAddress = toMailAddress,
                ToName = toName
            };
        }

        private string CreateEmailContent(User owner, MailConfigurationData dataConfig, Device device)
        {
            var source = File.ReadAllText(dataConfig.FilePath);
            var doc = new HtmlDocument();
            doc.LoadHtml(source);

            using (var writer = new StringWriter())
            {
                doc.Save(writer);
                source = writer.ToString();
            }

            var data = new
            {
                first_name = owner.UserDetails?.FirstName,
                email_address = owner.Email,
                hub_serial = device.Hub?.SerialNo ?? "There is no Hub attached to this device.",
                device_name = device.DeviceType,
                device_serial = device.SerialNumber
            };

            var template = Handlebars.Compile(source);
            var result = template(data);
            return result;
        }
    }
}
