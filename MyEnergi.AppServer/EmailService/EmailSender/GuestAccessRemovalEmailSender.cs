﻿using HandlebarsDotNet;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.AppServer.Model.Email;
using System.IO;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.EmailService.EmailSender
{
    public class GuestAccessRemovalEmailSender : EmailService, IGuestAccessRemovalEmailSender
    {
        public GuestAccessRemovalEmailSender(IConfiguration configuration) :
            base(configuration)
        {
        }

        public async Task SendGuestAccessRemovalEmail(string guestEmail, string guestFirstName, string hubSerialNo)
        {
            var dataConfig = GetConfigurationData();

            var email = new EmailMessage
            {
                ToAddress = new EmailAddress
                {
                    Address = guestEmail,
                    Name = guestFirstName
                },
                FromAddress = new EmailAddress
                {
                    Address = dataConfig.MailAddress,
                    Name = dataConfig.Sender
                },
                Subject = dataConfig.MailSubject,
                Content = CreateEmailContent(dataConfig, guestFirstName, hubSerialNo)
            };

            await ExecuteOperationsToSendEmail(email);
        }

        private MailConfigurationData GetConfigurationData()
        {
            var sender = Configuration["EmailConfiguration:MailSender"];
            var mailAddress = Configuration["EmailConfiguration:MailAddress"];
            var mailSubject = Configuration["GuestHubAccessRemovalEmailConfiguration:MailSubject"];
            var filePath = Path.Combine(Directory.GetCurrentDirectory(),
                Configuration["GuestHubAccessRemovalEmailConfiguration:Template"]);

            return new MailConfigurationData
            {
                FilePath = filePath,
                Sender = sender,
                MailAddress = mailAddress,
                MailSubject = mailSubject,
            };
        }

        private string CreateEmailContent(MailConfigurationData dataConfig, string guestFirstName, string hubSerialNo)
        {
            var source = File.ReadAllText(dataConfig.FilePath);
            var doc = new HtmlDocument();
            doc.LoadHtml(source);

            using (var writer = new StringWriter())
            {
                doc.Save(writer);
                source = writer.ToString();
            }

            var data = new
            {
                guest_first_name = guestFirstName,
                hub_serial = hubSerialNo,
            };

            var template = Handlebars.Compile(source);
            var result = template(data);
            return result;
        }
    }
}
