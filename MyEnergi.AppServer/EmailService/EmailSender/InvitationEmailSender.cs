﻿using HandlebarsDotNet;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.AppServer.Model.Email;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.EmailService.EmailSender
{
    public class InvitationEmailSender : EmailService, IInvitationEmailSender
    {
        public InvitationEmailSender(IConfiguration configuration) :
            base(configuration)
        {
        }

        public MailConfigurationData SetConfigurationData(bool appAccess, IEnumerable<string> features)
        {
            var sender = Configuration["InvitationEmailConfiguration:MailSender"];
            var mailAddress = Configuration["InvitationEmailConfiguration:MailAddress"];
            var mailSubject = string.Empty;
            var mailCta = string.Empty;
            var filePath = string.Empty;
            var mailCtaApp = string.Empty;

            switch (appAccess, features.Any())
            {
                case (true, false):
                    mailSubject = Configuration["InvitationEmailConfiguration:MailSubjectApp"];
                    mailCtaApp = Configuration["InvitationEmailConfiguration:MailCTAApp"];
                    filePath = Path.Combine(Directory.GetCurrentDirectory(),
                        Configuration["InvitationMailTemplate:AppAccessPath"]);
                    break;
                case (true, true):
                    mailSubject = Configuration["InvitationEmailConfiguration:MailSubjectAppAA"];
                    mailCta = Configuration["InvitationEmailConfiguration:MailCTA"];
                    filePath = Path.Combine(Directory.GetCurrentDirectory(),
                        Configuration["InvitationMailTemplate:AppAndAccountAccessPath"]);
                    mailCtaApp = Configuration["InvitationEmailConfiguration:MailCTAApp"];
                    break;
                case (false, true):
                    mailSubject = Configuration["InvitationEmailConfiguration:MailSubjectAA"];
                    mailCta = Configuration["InvitationEmailConfiguration:MailCTA"];
                    filePath = Path.Combine(Directory.GetCurrentDirectory(),
                        Configuration["InvitationMailTemplate:AccountAccessPath"]);
                    break;
            }

            return new MailConfigurationData
            {
                FilePath = filePath,
                Sender = sender,
                MailAddress = mailAddress,
                MailSubject = mailSubject,
                MailCTA = mailCta,
                MailCTAApp = mailCtaApp
            };
        }

        public async Task SendInvitationEmail(Guid invitationGuid, User owner, string guestMail, string firstName,
            MailConfigurationData dataConfig)
        {
            var email = new EmailMessage
            {
                ToAddress = new EmailAddress { Address = guestMail, Name = firstName },
                FromAddress = new EmailAddress { Address = dataConfig.MailAddress, Name = dataConfig.Sender },
                Subject = dataConfig.MailSubject,
                CTA = dataConfig.MailCTA,
                CTAApp = dataConfig.MailCTAApp,
                Content = CreateEmailContent(invitationGuid, owner, firstName, dataConfig)
            };

            await ExecuteOperationsToSendEmail(email);
        }

        private string CreateEmailContent(Guid invitationGuid, User owner, string userFirstName,
            MailConfigurationData dataConfig)
        {
            var source = File.ReadAllText(dataConfig.FilePath);
            var doc = new HtmlDocument();
            doc.LoadHtml(source);

            using (var writer = new StringWriter())
            {
                doc.Save(writer);
                source = writer.ToString();
            }

            var data = new
            {
                email_code = invitationGuid,
                first_name_invited = string.IsNullOrEmpty(userFirstName) ? "-" : userFirstName,
                first_name_owner = owner.UserDetails.FirstName,
                cta = dataConfig.MailCTA,
                cta_app = dataConfig.MailCTAApp
            };

            var template = Handlebars.Compile(source);
            var result = template(data);
            return result;
        }
    }
}
