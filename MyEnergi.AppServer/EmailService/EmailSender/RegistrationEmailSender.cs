﻿using HandlebarsDotNet;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.AppServer.Model.Email;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.EmailService.EmailSender
{
    public class RegistrationEmailSender : EmailService, IRegistrationEmailSender
    {
        private readonly IUserRepository _userRepository;
        private readonly IHubRepository _hubRepository;
        private readonly IDeviceRepository _deviceRepository;

        public RegistrationEmailSender(IConfiguration configuration, IUserRepository userRepository,
            IHubRepository hubRepository, IDeviceRepository deviceRepository) :
            base(configuration)
        {
            _userRepository = userRepository;
            _hubRepository = hubRepository;
            _deviceRepository = deviceRepository;
        }

        public async Task SendRegistrationMail(string userCognitoId, string filePath, string sender, string mailAddress,
            string mailSubject)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            if (user == null)
            {
                throw new ArgumentNullException($"User with id:{userCognitoId} does not exist!");
            }

            var email = new EmailMessage
            {
                ToAddress = new EmailAddress
                {
                    Address = user.Email,
                    Name = user.UserDetails.FirstName + " " + user.UserDetails.LastName
                },
                FromAddress = new EmailAddress { Name = sender, Address = mailAddress },
                Subject = mailSubject,
                Content = await CreateEmailContent(user, filePath)
            };

            await ExecuteOperationsToSendEmail(email);
        }

        private async Task<string> CreateEmailContent(User user, string filePath)
        {
            var source = await File.ReadAllTextAsync(filePath);
            var hubs = (await _hubRepository.GetHubByUserId(user.CognitoId)).ToList();
            var devices = (await _deviceRepository.GetDevicesByUserId(user.CognitoId)).ToList();
            var addresses = new List<Address>();
            addresses.AddRange(devices.Select(s => s.Address));
            addresses.AddRange(hubs.Select(s => s.Address));
            addresses = addresses.Distinct().ToList();
            var doc = new HtmlDocument();
            doc.LoadHtml(source);
            var deviceSection = doc.DocumentNode.SelectNodes("//div").FirstOrDefault(s => s.Id == "deviceSection");
            var addressHtml = doc.DocumentNode.SelectNodes("//div").FirstOrDefault(s => s.Id == "deviceAddress");
            var deviceHtml = doc.DocumentNode.SelectNodes("//table").FirstOrDefault(s => s.Id == "deviceDetails");
            deviceSection.RemoveChild(addressHtml);
            deviceSection.RemoveChild(deviceHtml);

            foreach (var address in addresses)
            {
                if (addresses.Count > 1)
                    deviceSection.AppendChild(ParseAddress(address, addressHtml.Clone()));

                var addressHubs = hubs.Where(h => h.Address.Id == address.Id).ToList();
                foreach (var hub in addressHubs)
                {
                    deviceSection.AppendChild(ParseHub(hub, deviceHtml.Clone()));
                    var addressDevices = devices.Where(d => d.Hub != null && d.Hub.Id == hub.Id).ToList();
                    foreach (var device in addressDevices)
                    {
                        deviceSection.AppendChild(ParseDevice(device, deviceHtml.Clone()));
                    }
                }

                var devicesWithoutHub = devices.Where(d => d.Hub == null).ToList();
                foreach (var device in devicesWithoutHub)
                {
                    deviceSection.AppendChild(ParseDevice(device, deviceHtml.Clone()));
                }
            }

            await using (var writer = new StringWriter())
            {
                doc.Save(writer);
                source = writer.ToString();
            }

            var data = new
            {
                first_name = string.IsNullOrEmpty(user.UserDetails.FirstName) ? "-" : user.UserDetails.FirstName,
                user_name = user.UserDetails.FirstName + " " + user.UserDetails.LastName,
                user_email = string.IsNullOrEmpty(user.Email) ? "-" : user.Email,
                user_mobile = string.IsNullOrEmpty(user.UserDetails.MobileNumber)
                    ? "-"
                    : user.UserDetails.MobileCode + user.UserDetails.MobileNumber,
                user_landline = string.IsNullOrEmpty(user.UserDetails.LandlineNumber)
                    ? "-"
                    : user.UserDetails.LandlineCode + user.UserDetails.LandlineNumber,
                user_address = string.IsNullOrEmpty(addresses.First().AddressLine1)
                    ? "-"
                    : addresses.First().AddressLine1,
                user_city = string.IsNullOrEmpty(addresses.First().City) ? "-" : addresses.First().City,
                user_region = string.IsNullOrEmpty(addresses.First().Region) ? "-" : addresses.First().Region,
                user_postalcode = string.IsNullOrEmpty(addresses.First().PostalCode)
                    ? "-"
                    : addresses.First().PostalCode,
                user_country = string.IsNullOrEmpty(addresses.First().Country) ? "-" : addresses.First().Country,
            };

            var template = Handlebars.Compile(source);
            var result = template(data);
            return result;
        }

        private string GetInstaller(InstallationFeedback installation)
        {
            if (installation == null)
                return "-";
            if (string.IsNullOrEmpty(installation.Installer))
                return installation.InstalledBy;
            return installation.Installer.Trim().ToLower() == "other"
                ? installation.InstallerName
                : installation.Installer;
        }

        private HtmlNode ParseHub(Hub hub, HtmlNode deviceHtml)
        {
            var deviceData = new
            {
                device_name = "hub",
                device_serial = string.IsNullOrEmpty(hub.SerialNo) ? "-" : hub.SerialNo,
                device_installation_date = hub.Installation == null ? "-" :
                    hub.Installation.InstallationDate == null ? "-" :
                    hub.Installation.InstallationDate.Value.ToShortDateString(),
                device_installer = GetInstaller(hub.Installation)
            };
            var deviceTemplate = Handlebars.Compile(deviceHtml.InnerHtml);
            var parsedHtml = deviceTemplate(deviceData);
            deviceHtml.InnerHtml = parsedHtml;
            return deviceHtml;
        }

        private HtmlNode ParseAddress(Address address, HtmlNode addressHtml)
        {
            var addressData = new
            {
                user_address = string.IsNullOrEmpty(address.AddressLine1) ? "-" : address.AddressLine1,
                user_city = string.IsNullOrEmpty(address.City) ? "-" : address.City,
                user_region = string.IsNullOrEmpty(address.Region) ? "-" : address.Region,
                user_postalcode = string.IsNullOrEmpty(address.PostalCode) ? "-" : address.PostalCode,
                user_country = string.IsNullOrEmpty(address.Country) ? "-" : address.Country,
            };
            var deviceTemplate = Handlebars.Compile(addressHtml.InnerHtml);
            var parsedHtml = deviceTemplate(addressData);
            addressHtml.InnerHtml = parsedHtml;
            return addressHtml;
        }

        private HtmlNode ParseDevice(Device device, HtmlNode deviceHtml)
        {
            var deviceData = new
            {
                device_name = string.IsNullOrEmpty(device.DeviceType) ? "-" : device.DeviceType,
                device_serial = string.IsNullOrEmpty(device.SerialNumber) ? "-" : device.SerialNumber,
                device_installation_date = device.Installation == null ? "-" :
                    device.Installation.InstallationDate == null ? "-" :
                    device.Installation.InstallationDate.Value.ToShortDateString(),
                device_installer = GetInstaller(device.Installation)
            };
            var deviceTemplate = Handlebars.Compile(deviceHtml.InnerHtml);
            var parsedHtml = deviceTemplate(deviceData);
            deviceHtml.InnerHtml = parsedHtml;
            return deviceHtml;
        }
    }
}
