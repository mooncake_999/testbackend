﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;
using MyEnergi.AppServer.Model.Email;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.EmailService
{
    public class EmailService
    {
        protected readonly IConfiguration Configuration;
        
        private readonly string _smtpServer;
        private readonly int _smtpPort;
        private readonly string _smtpUsername;
        private readonly string _smtpPassword;

        protected EmailService(IConfiguration configuration)
        {
            Configuration = configuration;
            _smtpServer = configuration["EmailConfiguration:SmtpServer"];
            _smtpPort = int.Parse(configuration["EmailConfiguration:SmtpPort"]);
            _smtpUsername = configuration["EmailConfiguration:SmtpUsername"];
            _smtpPassword = configuration["EmailConfiguration:SmtpPassword"];
        }

        public async Task ExecuteOperationsToSendEmail(EmailMessage emailMessage)
        {
            var message = GetMimeMessage(emailMessage);

            await SendEmail(message);
        }

        private MimeMessage GetMimeMessage(EmailMessage emailMessage)
        {
            return new MimeMessage
            {
                Subject = emailMessage.Subject,
                To =
                {
                    new MailboxAddress(emailMessage.ToAddress.Name,  emailMessage.ToAddress.Address)
                },
                From =
                {
                    new MailboxAddress(emailMessage.FromAddress.Name, emailMessage.FromAddress.Address)
                },
                Body = new TextPart(TextFormat.Html)
                {
                    Text = emailMessage.Content
                }
            };
        }

        private async Task SendEmail(MimeMessage message)
        {
            using var emailClient = new SmtpClient();

            await emailClient.ConnectAsync(_smtpServer, _smtpPort, SecureSocketOptions.StartTls);
            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
            await emailClient.AuthenticateAsync(_smtpUsername, _smtpPassword);
            await emailClient.SendAsync(message);
            await emailClient.DisconnectAsync(true);
        }
    }
}

