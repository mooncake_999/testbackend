﻿using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace MyEnergi.AppServer
{
    public class EnergyProviderApiCaller
    {
        public List<EnergyAPIPriceData> GetEnergyPrices(string gridSupplyPoint, string energyTariff)
        {
            List<EnergyAPIPriceData> prices = new List<EnergyAPIPriceData>();

            string gsp = gridSupplyPoint;
            string tarrifCode = GetEnergyTarrifCode(energyTariff);
            if (gsp == null || tarrifCode == null)
                return prices;

            var url = $"https://api.octopus.energy/v1/products/{tarrifCode}/electricity-tariffs/E-1R-{tarrifCode}-{gsp}/standard-unit-rates/";

            var priceData = GetResultsFromAPICall(url);
            if (priceData != null)
            {
                foreach (var price in priceData)
                {
                    prices.Add(new EnergyAPIPriceData()
                    {
                        GridSupplyPoint = gsp,
                        ValueWithoutVAT = price["value_exc_vat"].Value<float>(),
                        ValueWithVAT = price["value_inc_vat"].Value<float>(),
                        ValidFrom = price["valid_from"].Value<DateTime>(),
                        ValidTo = price["valid_to"].Value<DateTime>(),
                        Currency="p"
                    });
                }
            }

            return prices;
        }

        private string GetEnergyTarrifCode(string energyTariff)
        {
            var tarrifUrl = "https://api.octopus.energy/v1/products";
            var results = GetResultsFromAPICall(tarrifUrl);
            if (results != null)
            {
                foreach (var result in results)
                {
                    var displayName = result?["display_name"]?.Value<string>();
                    if (displayName != null && displayName.ToUpper().Trim() == energyTariff.ToUpper().Trim())
                        return result?["code"]?.Value<string>();
                }
            }
            return null;
        }

        public string GetGridSupplyPoint(string postalCode)
        {
            var gspUrl = $"https://api.octopus.energy/v1/industry/grid-supply-points/?postcode={postalCode}";
            var results = GetResultsFromAPICall(gspUrl).FirstOrDefault();
            return results?["group_id"]?.Value<string>().Last().ToString().Trim();
        }

        private JArray GetResultsFromAPICall(string url)
        {
            var uri = new Uri(url);

            var httpClient = new HttpClient();
            var answer = httpClient.GetAsync(uri).Result;

            if (answer.IsSuccessStatusCode)
            {
                var allData = (JObject)JsonConvert.DeserializeObject(answer.Content.ReadAsStringAsync().Result);

                return allData[$"results"].Value<JArray>();
            }
            return null;
        }
    }
}
