﻿using MyEnergi.AppServer.Model;
using MyEnergi.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;

namespace MyEnergi.AppServer
{
    public class ExistingHubStatus : AppServerApiCaller
    {
        private readonly string _specificDevice;

        public ExistingHubStatus(string hostServer, string serialNo, string password, string deviceType = "", string deviceSNo = "*") : base(serialNo, password)
        {
            _specificDevice = deviceSNo == "*" ? deviceSNo : deviceType.First() + deviceSNo;
            HostServer = hostServer;
        }

        protected override string GenerateApiUrl()
        {
            var url = $"https://{HostServer}/cgi-jstatus-{_specificDevice}";

            return url;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            try
            {
                var data = (JArray) JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                if (data == null) return new ApiCallResponse {Status = -1};

                var hubFirmwareVersion = string.Empty;
                var devices = new List<DeviceData>();
                var deviceTypes = Enum.GetNames(typeof(Enumerators.DeviceType))
                    .Select(s => s.ToLower());

                var queryableData = data.AsQueryable();
                foreach (var deviceType in deviceTypes)
                {
                    var jToken = queryableData
                        .FirstOrDefault(jt => jt[deviceType] != null);
                    if (jToken != null)
                    {
                        devices.AddRange(jToken[deviceType].Select(d => GetDeviceData(d, deviceType)));
                    }
                }

                var fwvJToken = queryableData
                    .FirstOrDefault(jt => jt["fwv"] != null);
                if (fwvJToken != null)
                {
                    hubFirmwareVersion = fwvJToken["fwv"].Value<string>();
                }

                var hubFirmwareAndDevicesTuple = new Tuple<string, List<DeviceData>>(hubFirmwareVersion, devices);

                return new ApiCallResponse {Content = hubFirmwareAndDevicesTuple};
            }
            catch (JsonException)
            {
                var data = (JObject) JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new ApiCallResponse
                {
                    Status = data?["status"].Value<int>() ?? -1,
                    StatusText = data?["statustext"].Value<string>() ?? "Something went wrong."
                };
            }
        }

        private DeviceData GetDeviceData(JToken device, string deviceType)
        {
            return new DeviceData
            {
                DeviceType = deviceType,
                SerialNumber = device["sno"].Value<string>(),
                Firmware = device?["fwv"]?.Value<string>(),
                HeaterOutput1 = device?["ht1"]?.Value<string>() == "None" ? null : device?["ht1"]?.Value<string>(),
                HeaterOutput2 = device?["ht2"]?.Value<string>() == "None" ? null : device?["ht2"]?.Value<string>(),
                TimeZoneRegion = CalculateTimeZone(device),
                LastActiveTime = DateTime.ParseExact($"{device["dat"].Value<string>()} {device["tim"].Value<string>()}", "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture)
            };
        }

        private string CalculateTimeZone(JToken device)
        {
            if (device == null)
                return null;
            var timeZone = device["tz"]?.Value<int>() ?? default;
            var tz = timeZone switch
            {
                0 => "Europe/London",
                1 => "Europe/Dublin",
                2 => "Europe/Lisbon",
                3 => "Europe/Berlin",
                4 => "Europe/Helsinki",
                5 => "Asia/Dubai",
                6 => "Australia/Perth",
                7 => "Australia/Darwin",
                8 => "Australia/Adelaide",
                9 => "Australia/Sydney",
                10 => "Pacific/Auckland",
                11 => "Pacific/Chatham",
                _ => "Europe/London",
            };
            return tz;
        }
    }
}
