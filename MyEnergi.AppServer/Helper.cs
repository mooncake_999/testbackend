﻿using System;
using System.Linq;
using TimeZoneConverter;

namespace MyEnergi.AppServer
{
    public static class Helper
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime ConvertTimestampToDate(long timestamp)
        {
            var dtDateTime = Epoch;
            dtDateTime = dtDateTime.AddMilliseconds(timestamp);
            return dtDateTime;
        }     
    }
}
