﻿using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace MyEnergi.AppServer
{
    // TODO: Not used for now
    public class HubAlternateServerName : AppServerApiCaller
    {
        public HubAlternateServerName(string hostServer, string serialNo, string password) : base(serialNo, password)
        {
            HostServer = hostServer;
        }

        protected override string GenerateApiUrl()
        {
            var url = $"https://{HostServer}/cgi-jstatus-*";

            return url;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            try
            {
                var data = (JArray)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                if (data == null) return new ApiCallResponse { Status = -1 };

                var alternateServer = data[3]["asn"].Value<string>();

                return new ApiCallResponse { Content = alternateServer };
            }
            catch (JsonException)
            {
                var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new ApiCallResponse
                {
                    Status = data?["status"].Value<int>() ?? -1,
                    StatusText = data?["statustext"].Value<string>() ?? "Something went wrong."
                };
            }
        }
    }
}
