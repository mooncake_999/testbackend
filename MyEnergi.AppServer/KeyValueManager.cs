﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyEnergi.AppServer
{
    public class KeyValueManager : AppServerApiCaller
    {
        private readonly string _serialNo;
        private readonly string _key;
        private readonly string _value;

        public KeyValueManager(string hostServer, string serialNo, string password, string key = null, string value = null) : base(serialNo, password)
        {
            HostServer = hostServer;
            _serialNo = serialNo;
            _key = key;
            _value = value;
        }

        protected override string GenerateApiUrl()
        {
            var url = string.Empty;
            if (_key == null)
                url = $"https://{HostServer}/cgi-get-app-key-";
            else if (_value == null)
                url = $"https://{HostServer}/cgi-get-app-key-{_key}";
            else
                url = $"https://{HostServer}/cgi-set-app-key-{_key}={_value}";

            return url;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            var result = response.Content.ReadAsStringAsync().Result;

            try
            {
                var allData = (JObject)JsonConvert.DeserializeObject(result);
                if (allData == null) return new ApiCallResponse { Status = -1 };

                string key = allData.ContainsKey($"H{_serialNo}") ? $"H{_serialNo}" : allData.ContainsKey("H") ? "H" : string.Empty;
                var hubsData = allData[key]?.Value<JArray>();
                if (hubsData == null) return new ApiCallResponse { Status = -1 };

                object value = null;
                if (hubsData.HasValues)
                {
                    if (_key == null)
                    {
                        Dictionary<string, string> deviceNicknames = new Dictionary<string, string>();
                        foreach (var data in hubsData)
                        {
                            if (!deviceNicknames.ContainsKey(data["key"].Value<string>().ToUpper()))
                                deviceNicknames.Add(data["key"].Value<string>().ToUpper(), data["val"]?.Value<string>());
                        }
                        value = deviceNicknames;
                    }
                    else
                        value = hubsData.SingleOrDefault(k => k["key"].Value<string>() == _key)["val"]?.Value<string>();
                }

                return new ApiCallResponse
                {
                    Content = value
                };
            }
            catch (JsonException)
            {
                var data = (JObject)JsonConvert.DeserializeObject(result);

                return new ApiCallResponse
                {
                    Status = data?["status"].Value<int>() ?? -1,
                    StatusText = data?["statustext"].Value<string>() ?? ""
                };
            }
        }
    }
}
