﻿using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MyEnergi.AppServer.Loaders
{
    public class ElectricVehicleLoader : ApiCaller
    {
        private readonly string _url;

        public ElectricVehicleLoader(string url)
        {
            _url = url;
        }

        public override ApiCallResponse GetResult()
        {
            var url = GenerateApiUrl();

            using var client = new HttpClient { BaseAddress = new Uri(url) };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var answer = client.GetAsync(string.Empty).Result;

            var result = new ApiCallResponse
            {
                HttpStatusCode = answer.StatusCode,
                HttpMessage = answer.ReasonPhrase
            };

            if (answer.IsSuccessStatusCode)
            {
                var desResult = DeserializeJson(answer);
                result.Status = desResult.Status;
                result.StatusText = desResult.StatusText;
                result.Content = desResult.Content;
            }
            return result;
        }

        protected override string GenerateApiUrl()
        {
            return _url;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            try
            {
                //var content = JArray.Parse(response.Content.ReadAsStringAsync().Result);
                //JsonConvert.DeserializeObject<IList<VehicleLookupData>>(JsonConvert.DeserializeObject<string>(content))
                var data = (JArray)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                //var data = (JArray)JsonConvert.DeserializeObject(JsonConvert.DeserializeObject<string>(response.Content.ReadAsStringAsync().Result));
                var vehiclesData = new List<VehicleData>();

                if (data == null) return new ApiCallResponse { Status = -1 };

                foreach (var vehicle in data.Where(d => d["Availability_Status"]!.Value<int>() == 0 || d["Availability_Status"]!.Value<int>() == 1))
                {
                    var availabilityStatus = vehicle["Availability_Status"]!.Value<int>();
                    var availabilityDateFrom = string.IsNullOrEmpty(vehicle["Availability_Date_From"]!.Value<string>())
                        ? (int?)null
                        : int.Parse(vehicle["Availability_Date_From"].Value<string>().Substring(3));

                    var availabilityDateTo = string.IsNullOrEmpty(vehicle["Availability_Date_To"]!.Value<string>())
                        ? (int?)null
                        : int.Parse(vehicle["Availability_Date_To"].Value<string>().Substring(3));
                    var batterySize = vehicle["Battery_Capacity_Useable"]!.Value<float?>();
                    vehiclesData.Add(new VehicleData
                    {
                        RefId = (vehicle["Vehicle_ID"] ?? 0).Value<int>(),
                        AvailabilityDateFrom = availabilityDateFrom,
                        AvailabilityDateTo = availabilityDateTo,
                        RealEVRange = vehicle["Range_Real_WCmb"]!.Value<int?>(),
                        ManufacturerOfficialEVRange = vehicle["Range_Real"]!.Value<int?>(),
                        Model = $"{vehicle["Vehicle_Model"]!.Value<string>()} " +
                                $"{vehicle["Vehicle_Model_Version"]!.Value<string>()} " +
                                $"({availabilityDateFrom}-{(availabilityStatus == 0 ? availabilityDateTo.ToString() : string.Empty)})",
                        BatterySize = batterySize,
                        Manufacturer = vehicle["Vehicle_Make"]!.Value<string>(),
                        ManufacturingYear = null,
                        ChargeRate = vehicle["Charge_Standard_Table"]?["230V32A1X"]?["Charge_Power"]!.Value<float?>()
                    });
                }
                return new ApiCallResponse { Content = vehiclesData };
            }
            catch (JsonException)
            {
                var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new ApiCallResponse
                {
                    Status = data?["status"].Value<int>() ?? -1,
                    StatusText = data?["statustext"].Value<string>() ?? "Something went wrong."
                };
            }
        }
    }
}
