﻿using MyEnergi.AppServer.Model;
using MyEnergi.AppServer.Model.Entsoe;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MyEnergi.AppServer.Loaders
{
    public class EntsoePricesLoader
    {
        private readonly string _url;

        public EntsoePricesLoader(string url)
        {
            _url = url;
        }
        
        public async Task<ApiCallResponse> GetResult()
        {
            using var client = new HttpClient();

            var xmlMediaType = new MediaTypeWithQualityHeaderValue("application/xml");
            client.DefaultRequestHeaders.Accept.Add(xmlMediaType);

            var response = await client.GetAsync(_url);

            var result = new ApiCallResponse
            {
                HttpStatusCode = response.StatusCode,
                HttpMessage = response.ReasonPhrase
            };

            if (response.IsSuccessStatusCode)
            {
                var desResult = await DeserializeXml(response);
                result.Status = desResult.Status;
                result.StatusText = desResult.StatusText;
                result.Content = desResult.Content;
            }
            
            return result;
        }

        private async Task<ApiCallResponse> DeserializeXml(HttpResponseMessage response)
        {
            try
            {
                var responseString = await response.Content.ReadAsStringAsync();
                var xml = XElement.Parse(responseString);

                var descendants = xml.Descendants().ToList();

                var currency = descendants.FirstOrDefault(node => node.Name.LocalName == "currency_Unit.name")?.Value;
                var measureUnit = descendants.FirstOrDefault(node => node.Name.LocalName == "price_Measure_Unit.name")?.Value;
                
                var timeIntervalDescendants = descendants.FirstOrDefault(node => node.Name.LocalName == "timeInterval")?.Descendants().ToList();
                var startTimeIntervalString = timeIntervalDescendants.FirstOrDefault(node => node.Name.LocalName == "start")?.Value;
                
                var startTimeInterval = !string.IsNullOrEmpty(startTimeIntervalString) ? 
                    DateTime.ParseExact(startTimeIntervalString, "yyyy-MM-ddTHH:mmZ", CultureInfo.InvariantCulture, 
                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal)
                    : (DateTime?)null;

                var points = descendants.Where(node => node.Name.LocalName == "Point");
                
                var prices = new List<EntsoePriceData>();
                var minutes = 0;
                foreach (var point in points)
                {
                    var pointDescendants = point.Descendants().ToList();
                    var hour = pointDescendants.FirstOrDefault(node => node.Name.LocalName == "position")?.Value;
                    var price = pointDescendants.FirstOrDefault(node => node.Name.LocalName == "price.amount")?.Value;

                    for (int i = 0; i<=1; i++)
                    {
                        var priceData = new EntsoePriceData
                        {
                            StartTime = startTimeInterval.Value.AddMinutes(minutes),
                            EndTime = startTimeInterval.Value.AddMinutes(minutes + 30),
                            Hour = int.Parse(hour ?? string.Empty),
                            Price = float.Parse(price ?? string.Empty)
                        };

                        minutes = minutes + 30;

                        prices.Add(priceData);
                    }
                }

                var entsoeResponse = new EntoseAPIResponse
                {
                    Currency = currency,
                    MeasureUnit = measureUnit,
                    Prices = prices
                };

                return new ApiCallResponse
                {
                    Content = entsoeResponse
                };
            }
            catch (Exception ex)
            {
                Log.Warning($"Entsoe API Call exception: {ex.Message}");
                throw new Exception(ex.Message);
            }
        }
    }
}
