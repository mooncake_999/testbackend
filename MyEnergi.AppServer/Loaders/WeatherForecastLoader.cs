﻿using MyEnergi.AppServer.Model;
using MyEnergi.AppServer.Model.Weather;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MyEnergi.AppServer.Loaders
{
    public class WeatherForecastLoader : ApiCaller
    {
        private readonly string _url;

        public WeatherForecastLoader(string url)
        {
            _url = url;
        }

        protected override string GenerateApiUrl()
        {
            return _url;
        }

        public override ApiCallResponse GetResult()
        {
            var url = GenerateApiUrl();

            using var client = new HttpClient { BaseAddress = new Uri(url) };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var answer = client.GetAsync(string.Empty).Result;

            var result = new ApiCallResponse
            {
                HttpStatusCode = answer.StatusCode,
                HttpMessage = answer.ReasonPhrase
            };

            if (answer.IsSuccessStatusCode)
            {
                var desResult = DeserializeJson(answer);
                result.Status = desResult.Status;
                result.StatusText = desResult.StatusText;
                result.Content = desResult.Content;
            }
            return result;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            try
            {
                var result = response.Content.ReadAsStringAsync().Result;
                var jObject = JObject.Parse(result);
                var allData = jObject["data"];

                if (allData == null)
                {
                    return new ApiCallResponse { Status = -1 };
                }

                if (_url.Contains("daily"))
                {
                    var dailyWeatherForecast = allData
                        .Select(GetDailyWeatherResponse).ToList();

                    return new ApiCallResponse { Content = dailyWeatherForecast };
                }

                var hourlyWeatherForecast = allData
                    .Select(GetHourlyWeatherResponse).ToList();

                return new ApiCallResponse { Content = hourlyWeatherForecast };
            }
            catch (Exception ex)
            {
                Log.Warning($"WeatherbitAPI Call exception: {ex.Message}");
                var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new ApiCallResponse
                {
                    Status = data?["status"]?.Value<int>() ?? -1,
                    StatusText = data?["statustext"]?.Value<string>() ?? "Something went wrong."
                };
            }
        }

        private DailyWeatherForecastData GetDailyWeatherResponse(JToken data)
        {
            return new DailyWeatherForecastData
            {
                DateTime = GetDateTime(data["valid_date"]?.Value<string>()),

                WindDirection = data["wind_cdir"]?.Value<string>(),
                WindSpeed = data["wind_spd"]?.Value<double>(),

                DailyMinTemp = data["min_temp"]?.Value<double>(),
                DailyMaxTemp = data["max_temp"]?.Value<double>(),
                Temperature = data["temp"]?.Value<double>(),

                WeatherIconCode = data["weather"]?["code"]?.Value<string>(),
                WeatherDescription = data["weather"]?["description"]?.Value<string>(),
            };
        }

        private HourlyWeatherForecastData GetHourlyWeatherResponse(JToken data)
        {
            return new HourlyWeatherForecastData
            {
                WindDirection = data["wind_cdir"]?.Value<string>(),
                WindSpeed = data["wind_spd"]?.Value<double>(),

                SolarRadiation = data["solar_rad"]?.Value<double>(),

                Temperature = data["temp"]?.Value<double>(),

                WeatherIconCode = data["weather"]?["code"]?.Value<string>(),
                WeatherDescription = data["weather"]?["description"]?.Value<string>(),

                DateTime = GetDateTime(data["datetime"]?.Value<string>())
            };
        }

        private static DateTime GetDateTime(string dateTime)
        {
            var splitValues = dateTime.Split('-', ':'); // DateTime string format: 2021-01-29:19"

            var year = int.Parse(splitValues[0]);
            var month = int.Parse(splitValues[1]);
            var day = int.Parse(splitValues[2]);
            var hour = 0;
            if (splitValues.Length > 3)
            {
                hour = int.Parse(splitValues[3]);
            }

            return new DateTime(year, month, day, hour, 0, 0);
        }
    }
}
