﻿using System.Net.Http;
using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyEnergi.AppServer
{
    public class NewHubRegister : AppServerApiCaller
    {
        private readonly string _registrationCode;
        private readonly string _password;
        private readonly string _confirmationPassword;

        public NewHubRegister(string hostServer, string serialNo, string registrationCode, string password, string confirmationPassword) : base(serialNo, registrationCode)
        {
            HostServer = hostServer;
            _registrationCode = registrationCode;
            _password = password;
            _confirmationPassword = confirmationPassword;
        }

        protected override string GenerateApiUrl()
        {
            var url = string.IsNullOrEmpty(_password) || string.IsNullOrEmpty(_confirmationPassword)
                ? $"https://{HostServer}/cgi-jregister-"
                : $"https://{HostServer}/cgi-jregister-{_registrationCode}&{_password}&{_confirmationPassword}";

            return url;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

            return new ApiCallResponse
            {
                Status = data?["status"].Value<int>() ?? -1,
                StatusText = data?["statustext"].Value<string>() ?? ""
            };
        }
    }
}
