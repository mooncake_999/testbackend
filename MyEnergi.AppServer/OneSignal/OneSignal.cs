﻿using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyEnergi.AppServer.OneSignal
{
    public class OneSignal : IOneSignal
    {
        private readonly string _apiKey;
        private readonly string _url;
        private readonly string _appId;
        public OneSignal(IConfiguration configuration)
        {
            _apiKey = configuration["OneSignal:ApiKey"];
            _url = configuration["OneSignal:OneSignalUrl"];
            _appId = configuration["OneSignal:AppId"];
        }

        public string SetResponse(List<PropertiesData> objList, string heading, string content)
        {
            var ids = objList.Select(x => x.Id).ToList();

            var res = new OneSignalRequest()
            {
                AppId = _appId,
                IncludeExternalUserIds = ids,
                ChannelForExternalUserIds = "push",
                Headings = new Headings
                {
                    En = heading
                },
                Contents = new Contents
                {
                    En = content
                }
            };
            var jsonResult = JsonConvert.SerializeObject(res);
            return jsonResult;
        }

        public async Task PushNotification(string json, string serialNo)
        {
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            using var client = new HttpClient(new HttpClientHandler())
            {
                Timeout = TimeSpan.FromMinutes(5)
            };

            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", _apiKey);

            HttpResponseMessage response = await client.PostAsync(_url, data);
            if (!response.IsSuccessStatusCode)
            {
                Log.Information($"The call to {_url} was not successful for hub with SerialNo: {serialNo}.");
            }
        }
    }
}
