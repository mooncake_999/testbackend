﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using MyEnergi.AppServer.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyEnergi.AppServer
{
    public class PushNotification : AppServerApiCaller
    {
        private readonly string _serialNo;
        public PushNotification(string hostServer, string serialNo, string password) : base(serialNo, password)
        {
            HostServer = hostServer;
            _serialNo = serialNo;
        }

        protected override string GenerateApiUrl()
        {
            var url = $"https://{HostServer}/cgi-get-app-key-";

            return url;
        }

        protected override ApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

            if (data == null) return new ApiCallResponse { Status = -1 };

            string key = data.ContainsKey($"H{_serialNo}") ? $"H{_serialNo}" : data.ContainsKey("H") ? "H" : string.Empty;
            var dataI = data[key]?.Value<JArray>();

            List<PropertiesData> objList = new List<PropertiesData>();
            List<PropertiesData> objListFiltered = new List<PropertiesData>();


            foreach (var pair in dataI.Where(d => d["key"].Value<string>().StartsWith("pn")))
            {
                JToken token = pair["val"];
                if (token != null)
                {
                    if (pair["val"].Value<string>().Any() && pair["val"].Value<string>().FirstOrDefault() == '{' && pair["val"].Value<string>().LastOrDefault() == '}')
                    {
                        objList.Add(JsonConvert.DeserializeObject<PropertiesData>(pair["val"].Value<string>()));
                    }
                }
            }
            objListFiltered = objList.Where(x => x.EnableAll == true && DateTime.Compare(Helper.ConvertTimestampToDate(x.LastActive), DateTime.Today.AddMonths(-3)) >= 0).ToList();

            return new ApiCallResponse { Content = objListFiltered };
        }
    }
}
