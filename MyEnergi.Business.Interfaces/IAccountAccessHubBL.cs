﻿using MyEnergi.Business.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IAccountAccessHubBL
    {
        Task<ICollection<object>> GetHubs(string cognitoId);

        Task TransferHubOwnership(string hubSerialNo, string email, string ownerCognitoId);

        Task<IEnumerable<object>> MigrateHubs(IEnumerable<MigrateHubData> hubs, string requestUserCognitoId);
        
        Task<ResponseModel> RegisterNewHub(string hubSerialNo, string hubRegistrationCode, string userCognitoId);
    }
}
