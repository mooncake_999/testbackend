﻿using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IAccountAccessShareBL
    {
        Task RejectInvitation(string userCognitoId, string serialNo);
        
        Task<object> AcceptInvitation(string userCognitoId, string serialNo);
        
        Task RevokeShare(string userCognitoId, string email, string serialNo);

        Task ShareHub(string ownerCognitoId, string serialNo, string email);

        InvitationCreationData GetInvitationCreationData(string ownerCognitoId, InvitationDetails invitationDetails, IList<Device> devices, IList<AppFeature> appFeatures, Enumerators.InvitationStatus invitationStatus, User guestUser = null, string hubSerialNo = null, DateTime? startDate = null, DateTime? endDate = null, DateTime? expirationDate = null);
        
        Task<Guid> CreateInvitation(InvitationCreationData invitationCreationData);

        Task ExecuteOperationsToSuspendThePermissions(Invitation invitation);

        bool CheckThatInvitationIsNotNullAndNotDeleted(Invitation invitation);

        Task UpdateInvitationStatusToAccepted(Invitation invitation, InvitationStatus invitationAccepted);

        Task ExecuteOperationsToMakeAnInvitationWithGivenStatus(Hub hub, User guestUser, string guestEmail,
            string ownerCognitoId, Enumerators.InvitationStatus invitationStatus);

        Task ExecuteOperationsToRejectTheInvitation(Invitation invitation);
    }
}
