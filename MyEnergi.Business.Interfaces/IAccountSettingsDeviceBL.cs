﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IAccountSettingsDeviceBL
    {
        Task<List<object>> GetAllDeviceStatus(string userId);
        Task<List<object>> GetDeviceStatusOfHub(string userId, Guid hubId);
        Task<IEnumerable<object>> GetPhantomDeviceStatus(string userId);
        Task<List<DeviceData>> GetNewDevices(string userId);
    }
}