﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IAccountSettingsShareBL
    {
        Task DeleteInvitation(string ownerCognitoId, Guid invitationId);

        Task EditPermissions(string ownerCognitoId, UserPermissionData requestData);

        Task<ICollection<InvitationData>> GetInvitations(string ownerCognitoId);

        Task<object> GetUserPermissions(string ownerCognitoId, Guid invitationId);

        Task<object> GetGuestPermissions(string guestCognitoId, Guid invitationGuid);

        Task SuspendAccess(string ownerCognitoId, Guid invitationId);

        Task ResendInvitationEmail(string ownerCognitoId, ResendInvitationEmailData request);

        Task AcceptInvitation(string ownerCognitoId, Guid invitationId);

        Task RejectInvitation(string ownerCognitoId, Guid invitationId);

        Task<object> GetSharedFeatures(string userCognitoId);

        Task<List<object>> GetUserAccounts(string userCognitoId);

        Task SetSelectedAccount(string userCognitoId, Guid invitationId);

        Task<string> GetInvitationOwnerCognitoId(string currentUserCognitoId);

        Task<Guid> SendInvitation(string userCognitoId, UserPermissionData userPermissionData);
    }
}
