﻿using MyEnergi.Business.Model.Models;
using System;
using System.Threading.Tasks;
using ApiCallResponse = MyEnergi.AppServer.Model.ApiCallResponse;

namespace MyEnergi.Business.Interfaces
{
    public interface IApiCallerHelper
    {
        Task<ApiCallResponse> GetKeyValueResult(HubData hub, bool isPswDecrypted = false, string key = null, string value = null);
        Task<ApiCallResponse> GetExistingHubStatusResult(HubData hub, bool isPswDecrypted = false);
        Task<ApiCallResponse> GetHubRegisterResult(HubData hub);
        Task<ApiCallResponse> GetEddiHeaterResult(HubData hub, string deviceSNo);
        Task<ApiCallResponse> GetDeviceEnergyInfoResult(HubData hub, string deviceType, string deviceSNo, DateTime queryDate, int minutesInterval = 0);
        ApiCallResponse GetDeviceBoostTimeResult(HubData hub, string deviceType, string deviceSNo,
            string chargingSlot, string chargingFrom, string chargeTime, string chargeDays);
        Task<ApiCallResponse> GetKVPairResult(Guid hubId, string hostServer, string serialNo, string password);
    }
}