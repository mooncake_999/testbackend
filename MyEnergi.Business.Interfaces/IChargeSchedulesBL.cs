﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IChargeSchedulesBL
    {
        Task<List<ChargeSchedulesResponseData>> GetAllSchedules(string userCognitoId);
        Task<ChargeScheduleRequestData> SaveSchedules(ChargeScheduleRequestData chargeSchedule, string userCognitoId);
        Task SetIsActiveSchedule(Guid deviceId, string scheduleType, bool isActive, string userCognitoId);
    }
}
