﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyEnergi.Data.Entity;

namespace MyEnergi.Business.Interfaces
{
    public interface IDeviceBL
    {
        Task AddNewDevice(List<DeviceData> deviceData, string userCognitoId, bool addedWithoutHub);

        Task<List<DeviceFeedbackData>> GetDevicesWithInstallers(string userId);

        Task<List<DeviceFeedbackData>> GetDevicesWithInstallers(List<DeviceData> devices);

        Task<List<DeviceData>> GetDevicesByUserId(string userId);

        Task<List<DeviceData>> GetDevicesByHubExceptHarvi(Guid hubId);

        Task<ResponseModel> EditDevice(DeviceData device, string userCognitoId);

        Task<DeviceData> GetDeviceBySerialNo(string serialNo);

        Task<DeviceData> GetDeviceById(Guid id, string userCognitoId);

        Task<List<DeviceData>> GetUserAddedNewDevices(string userId);

        Task<List<DeviceData>> GetGuestDevicesData(string userCognitoId);

        Task UpdateDevicesNickname(string userCognitoId);

        Task UpdateDeviceInfo();

        Task UpdateFirmware(Guid deviceId, string firmware);

        Task UpdateOutputs(Guid deviceId, string out1, string out2);

        Task DeleteDevice(Guid deviceId, string userCognitoId);
        
        Task<List<DeviceData>> GetDevicesByUserIdIsDeleted(string userId);

        Task ExecuteOperationsToRemoveUserFeaturePermissions(IEnumerable<Device> devices);
    }
}
