﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IDualTariffEnergyProviderBL
    {
        Task SaveDualTariffEnergyPrices(string userCognitoId, List<DualTariffRequestData> dualTariffs);

        Task<List<DualTariffRequestData>> GetDualTariffEnergyPrices(Guid hubId, string userCognitoId);

        Task GenerateDualTariff(int timeToRun);
    }
}
