﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IEnergyProviderBL
    {
        Task SaveEnergyProviders(IList<EnergyProviderCSV> providers);
        Task SaveEnergySetupList(string userCognitoId, List<EnergyFormRequestData> energySetupList);
        Task<List<EnergyProviderData>> GetUsedEnergyProviders(bool enableEntsoe);
        Task<bool> SaveEnergyPrices(string userCognitoId, bool manualCall);
        Task<bool> SaveEntsoeEnergyPrices();
        Task<List<EnergyFormRequestData>> GetEnergySetups(string userCognitoId);
        Task<List<AddressData>> GetUserEnergyAddresses(string userCognitoId);
        Task<GraphEnergyPriceListData> GetBestPriceIntervalsForGraphs(Guid hubId, string userCognitoId);
        Task ProgramDevicesOnDemand(Guid deviceId, string userCognitoId);
        Task ValidateEnergySetupRemoval(Guid energySetupId, string userCognitoId);
        Task GenerateOctopus();
    }
}
