﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IGraphBL
    {
        Task<IList<DeviceEnergyInfo>> GetSpecificDataForGraph(Guid hubId, long startTimestamp, string timeZoneRegion);
        Task<IList<DeviceEnergyInfo>> GetSpecificDataForGraphDST(Guid hubId, long startTimestamp, string timeZoneRegion);
        Task<IList<DeviceEnergyInfo>> GetSpecificDataForGraphDSTSpring(Guid hubId, long startTimestamp, string timeZoneRegion);
        IList<DeviceEnergyInfo> GetActiveDataForGraph(List<DeviceData> devices, DateTime lastUpdate, int count = 0);
        IList<object> ProcessDataForDeviceGraph(IList<DeviceEnergyInfo> info, string timeZoneRegion);
        IList<object> ProcessDataForTotalGraph(IList<DeviceEnergyInfo> info, string timeZoneRegion);
    }
}