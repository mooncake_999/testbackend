﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IHubBL
    {
        Task<ResponseModel> RegisterNewHub(HubData hubData, string userCognitoId);
        Task AddNewHub(HubData hubData, string userCognitoId);
        Task PatchEncryptHubPsw();
        Task<List<HubData>> GetHubsByUserId(string userId);
        Task<ResponseModel> EditHub(HubData hub, string userCognitoId);
        Task RemoveHub(HubData hub, string userCognitoId);
        Task<HubData> GetHubById(Guid hubId, string userCognitoId);
        Task<HubData> GetHubBySerialNo(string serialNo);
        Task<List<HubData>> GetUserAddedNewHubs(string userId);
        Task UpdateHostServer(Guid hubId, string hostServer);
        Task<object> GetHubSerialNoPasswordByCognitoId(string userCognitoId);
        Task<object> GetUserHubsAndDevices(string userCognitoId);
        Task<List<DeviceFeedbackData>> GetHubsWithInstallers(List<HubData> hubs);
        Task DeleteHub(Guid hubId, string userCognitoId);
        Task<object> GenerateAPIKey(Guid hubId, string userCognitoId);
        Task<object> CheckIsAPIKeyAndLastUpdate(List<Guid> hubId, string userCognitoId);
    }
}