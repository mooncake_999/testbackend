﻿using MyEnergi.Business.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IInstallerBL
    {
        Task SaveInstallers(IList<InstallerData> installers);
        Task<InstallerData> GetInstallerByCode(string code);
        Task<IList<InstallerData>> GetInstallersByCountry(string country);
        Task AddNewInstallationFeedback(IList<InstallationFeedbackData> feedbackDatas, string userCognitoId);
    }
}