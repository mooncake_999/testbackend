﻿using MyEnergi.Business.Model.Models.Stripe;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IStripePaymentBL
    {
        Task<string> CreateSession(string userCognitoId, ExtendedWarrantyPaymentRequestData request);
        
        Task<bool> GetPaymentStatus(string userCognitoId, string sessionId);
    }
}
