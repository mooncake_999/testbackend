﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IUserBL
    {
        Task SaveUserDetails(string userCognitoId, UserDetailsData userDetailsData);
        
        Task SaveUser(UserRegistrationData data);
        
        Task<UserPersonalDetailsData> GetPersonalDetails(string userCognitoId);
        
        Task<IList<AddressData>> GetUsersAddresses(string userCognitoId);

        Task EditUserPreferences(UserRegistrationData data, string userCognitoId);

        Task EditUserMail(UserRegistrationData data, string userCognitoId);
        
        Task ValidateThatUserExists(string userCognitoId);
        Task<int> ValidateThatUserExistsAndGetId(string userCognitoId);
    }
}
