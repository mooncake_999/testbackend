﻿using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Entity;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IUserPreferencesBL
    {
        Task<UserPreferenceData> GetPreference(string userCognitoId);

        Task SaveUserPreferences(UserPreferenceData userPreferenceData, User user);
    }
}
