﻿using MyEnergi.Business.Model.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IVehicleBL
    {
        Task UpdateVehiclesData(IList<VehicleData> vehicleData);
        
        Task<IList<string>> GetAllManufacturers();
        
        Task<IList<KeyValuePair<Guid, string>>> GetVehicleByManufacturer(string manufacturer);
        
        Task<IList<VehicleData>> GetVehicleDetails(Guid vehicleId);

        Task<List<VehicleData>> GetUserVehicles(string userCognitoId);

        Task<List<VehicleData>> GetAllVehicles();
    }
}
