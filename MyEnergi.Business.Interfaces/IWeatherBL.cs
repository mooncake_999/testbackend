﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business.Interfaces
{
    public interface IWeatherBL
    {
        Task<ICollection<object>> GetForecastData(string userCognitoId);
    }
}
