﻿using System;
using System.Globalization;
using AutoMapper;
using MyEnergi.Business.Model.Helpers;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Data.Entity;
using static MyEnergi.Common.Enumerators;
using DeviceEnergyInfo = MyEnergi.Data.Entity.RedisModel.DeviceEnergyInfo;

namespace MyEnergi.Business.Model.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserDetails, UserPersonalDetailsData>().ForMember(x => x.DateOfBirth, opt => opt.MapFrom(m => m.DateOfBirth != null ? m.DateOfBirth.Value.ToShortDateString() : string.Empty));
            CreateMap<UserPersonalDetailsData, UserDetails>().ForMember(x => x.DateOfBirth, opt => opt.MapFrom(m => !string.IsNullOrEmpty(m.DateOfBirth) ? DateTime.ParseExact(m.DateOfBirth, "dd-MM-yyyy", CultureInfo.InvariantCulture) : (DateTime?)null))
                                                             .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<VehicleData, Vehicle>().ForMember(x => x.Id, opt => opt.Ignore())
                                             .ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                             .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<Vehicle, VehicleData>().ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid));
            CreateMap<VehicleData, UserVehicle>().ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                                 .ForMember(x => x.Id, opt => opt.Ignore())
                                                 .ForMember(x => x.Condition, opt => opt.MapFrom(o => o.Condition.ToString()))
                                                 .ForMember(x => x.Ownership, opt => opt.MapFrom(o => o.Ownership.ToString()))
                                                 .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<UserVehicle, VehicleData>().ForMember(x => x.Id, opt => opt.Ignore())
                                                 .ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid))
                                                 .ForMember(x => x.Condition, opt => opt.MapFrom(o => Enum.Parse<VehicleCondition>(o.Condition)))
                                                 .ForMember(x => x.Ownership, opt => opt.MapFrom(o => Enum.Parse<VehicleOwnership>(o.Ownership)));
            CreateMap<User, UserRegistrationData>().ForMember(x => x.UserCognitoId, opt => opt.MapFrom(m => m.CognitoId))
                                                    .ForMember(x => x.Email, opt => opt.MapFrom(m => m.Email));
            CreateMap<UserRegistrationData, User>().ForMember(x => x.CognitoId, opt => opt.MapFrom(m => m.UserCognitoId))
                                                    .ForMember(x => x.Email, opt => opt.MapFrom(m => m.Email))
                                                    .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<HubData, Hub>().ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                     .ForMember(x => x.Id, opt => opt.Ignore())
                                     .ForMember(dest => dest.RegistrationStartDate, opt => opt.MapFrom(o => DateTime.Now))
                                     .ForMember(dest => dest.AppPassword, opt => opt.MapFrom(o => KMSCipher.Encrypt(o.Password).Result))
                                     .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<Hub, HubData>().ForMember(dest => dest.ConfirmPassword, opt => opt.MapFrom(o => o.AppPassword))
                                     .ForMember(dest => dest.Password, opt => opt.MapFrom(o => o.AppPassword))
                                     .ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid));
            CreateMap<Address, AddressData>().ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid));
            CreateMap<AddressData, Address>().ForMember(x => x.Id, opt => opt.Ignore())
                                             .ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                             .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<EnergyProviderData, EnergyProvider>().ForMember(x => x.IsEV, opt => opt.MapFrom(m => m.IsEV == "Y" ? true : false))
                                                            .ForMember(x => x.IsFlexible, opt => opt.MapFrom(m => m.IsFlexible == "Y" ? true : false))
                                                            .ForMember(x => x.IsGreen, opt => opt.MapFrom(m => m.IsGreen == "Y" ? true : false))
                                                            .ForMember(x => x.IsEconomy, opt => opt.MapFrom(m => m.IsEconomy == "Y" ? true : false))
                                                            .ForMember(x => x.Provider, opt => opt.MapFrom(m => m.EnergyProvider.Trim()))
                                                            .ForMember(x => x.Tariff, opt => opt.MapFrom(m => m.TariffName.Trim()))
                                                            .ForMember(x => x.Guid, opt => opt.Ignore())
                                                            .ForMember(x => x.InternalReference, opt => opt.MapFrom(m => string.IsNullOrEmpty(m.InternalReference.Trim()) ? null : m.InternalReference.Trim()))
                                                            .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<EnergyProvider, EnergyProviderData>().ForMember(x => x.EnergyProvider, opt => opt.MapFrom(m => m.Provider))
                                                            .ForMember(x => x.TariffName, opt => opt.MapFrom(m => m.Tariff))
                                                            .ForMember(x => x.IsEV, opt => opt.MapFrom(m => m.IsEV.ToString()))
                                                            .ForMember(x => x.IsFlexible, opt => opt.MapFrom(m => m.IsFlexible.ToString()))
                                                            .ForMember(x => x.IsEconomy, opt => opt.MapFrom(m => m.IsEconomy.ToString()))
                                                            .ForMember(x => x.IsGreen, opt => opt.MapFrom(m => m.IsGreen.ToString()));
            CreateMap<EnergyProviderCSV, EnergyProvider>().ForMember(x => x.IsEV, opt => opt.MapFrom(m => m.IsEV == "Y" ? true : false))
                                                            .ForMember(x => x.IsFlexible, opt => opt.MapFrom(m => m.IsFlexible == "Y" ? true : false))
                                                            .ForMember(x => x.IsGreen, opt => opt.MapFrom(m => m.IsGreen == "Y" ? true : false))
                                                            .ForMember(x => x.IsEconomy, opt => opt.MapFrom(m => m.IsEconomy == "Y" ? true : false))
                                                            .ForMember(x => x.Provider, opt => opt.MapFrom(m => m.EnergyProvider.Trim()))
                                                            .ForMember(x => x.Tariff, opt => opt.MapFrom(m => m.TariffName.Trim()))
                                                            .ForMember(x => x.IsActive, opt => opt.MapFrom(m => m.IsActive == "Y" ? true: false))
                                                            .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow))
                                                            .ForMember(x => x.InternalReference, opt => opt.MapFrom(m => string.IsNullOrEmpty(m.InternalReference.Trim()) ? null : m.InternalReference.Trim()));
            CreateMap<EnergySetupData, EnergySetup>().ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                                     .ForMember(x => x.Id, opt => opt.Ignore())
                                                     .ForMember(x => x.TariffExpirationDate, opt => opt.MapFrom(m => !string.IsNullOrEmpty(m.TariffExpirationDate) ? DateTime.ParseExact(m.TariffExpirationDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : (DateTime?)null))
                                                     .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<EnergySetup, EnergySetupData>().ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid))
                                                     .ForMember(x => x.TariffExpirationDate, opt => opt.MapFrom(m => m.TariffExpirationDate != null ? m.TariffExpirationDate.Value.ToShortDateString() : string.Empty))
                                                     .ForMember(x => x.EnergyProviderId, opt => opt.MapFrom(m => m.EnergyProv == null? default(Guid): m.EnergyProv.Guid))
                                                     .ForMember(x => x.EnergyProvider, opt => opt.MapFrom(m => m.EnergyProv == null ? string.Empty : m.EnergyProv.Provider))
                                                     .ForMember(x => x.EnergyTariff, opt => opt.MapFrom(m => m.EnergyProv == null ? string.Empty : m.EnergyProv.Tariff))
                                                     .ForMember(x => x.IsEconomy, opt => opt.MapFrom(m => m.EnergyProv == null ? default(bool) : m.EnergyProv.IsEconomy))
                                                     .ForMember(x => x.IsActive, opt => opt.MapFrom(m => m.EnergyProv == null? default(bool) : m.EnergyProv.IsActive));
            CreateMap<DeviceData, Device>().ForMember(x => x.Id, opt => opt.Ignore())
                                           .ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                           .ForMember(dest => dest.RegistrationStartDate, opt => opt.MapFrom(o => DateTime.Now))
                                           .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<Device, DeviceData>().ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid));
            CreateMap<InstallationFeedbackData, InstallationFeedback>().ForMember(x => x.InstallationDate, opt => opt.MapFrom(m => !string.IsNullOrEmpty(m.InstallationDate) ? DateTime.ParseExact(m.InstallationDate, "dd-MM-yyyy", CultureInfo.InvariantCulture) : (DateTime?)null))
                                                                       .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<InstallationFeedback, InstallationFeedbackData>().ForMember(x => x.InstallationDate, opt => opt.MapFrom(m => m.InstallationDate != null ? m.InstallationDate.Value.ToShortDateString() : string.Empty));
            CreateMap<Installer, InstallerData>();
            CreateMap<InstallerData, Installer>().ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<DeviceData, AppServer.Model.DeviceData>();
            CreateMap<MyEnergi.AppServer.Model.DeviceData, DeviceData>();
            CreateMap<Device, AppServer.Model.DeviceData>();
            CreateMap<AppServer.Model.DeviceData, Device>();
            CreateMap<AppServer.Model.ApiCallResponse, ApiCallResponse>();
            CreateMap<AppServer.Model.EnergyAPIPriceData, EnergyPrice>();
            CreateMap<AppServer.Model.VehicleData, VehicleData>();
            CreateMap<ChargeScheduleData, DeviceChargesSetup>().ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                                               .ForMember(x => x.SingleChargeDay, opt => opt.MapFrom(m => !string.IsNullOrEmpty(m.SingleChargeDay) ? DateTime.ParseExact(m.SingleChargeDay, "dd-MM-yyyy", CultureInfo.InvariantCulture) : (DateTime?)null))
                                                               .ForMember(x => x.LastUpdated, opt => opt.MapFrom(o => DateTime.Now))
                                                               .ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<DeviceChargesSetup, ChargeScheduleData>().ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid))
                                                               .ForMember(x => x.SingleChargeDay, opt => opt.MapFrom(m => m.SingleChargeDay != null ? m.SingleChargeDay.Value.ToShortDateString() : string.Empty));
            CreateMap<EnergyPriceData, EnergyPrice>().ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<EnergyPrice, EnergyPriceData>();
            CreateMap<DeviceEnergyInfo, MyEnergi.Business.Model.Models.DeviceEnergyInfo>();
            CreateMap<MyEnergi.AppServer.Model.GraphData, MyEnergi.Business.Model.Models.DeviceEnergyInfo>().ForMember(x => x.Timestamp, opt => opt.MapFrom(o => Helper.ConvertToTimestamp(o.QueryDate)));
            CreateMap<MyEnergi.Business.Model.Models.DeviceEnergyInfo, MyEnergi.Business.Model.Models.DeviceEnergyInfo>();
            CreateMap<UserPreferenceData, UserPreference>().ForMember(x => x.Id, opt => opt.Ignore())
                                                           .ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id))
                                                           .ForMember(x => x.LastUpdate, opt => opt.MapFrom(m => DateTime.UtcNow));
            CreateMap<UserPreference, UserPreferenceData>().ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid));
            //CreateMap<DualTariffEnergyPrice, DualTariffRequestData>().ForMember(x => x.Days,
            //opt => opt.MapFrom(m => m.Days.Split(',', StringSplitOptions.RemoveEmptyEntries)));
            //CreateMap<DualTariffRequestData, DualTariffEnergyPrice>().ForMember(x => x.Days, opt => opt.MapFrom(m => m.Days.ToString()));

            CreateMap<AppFeature, AppFeatureData>()
                .ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid));
            CreateMap<AppFeatureData, AppFeature>()
                .ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id));

            CreateMap<Invitation, InvitationData>()
                .ForMember(x => x.Id, opt => opt.MapFrom(m => m.Guid))
                .ForMember(x => x.InvitationStatus, opt => opt.MapFrom(m => m.InvitationStatus.Status))
                .ForMember(x => x.Email, opt => opt.MapFrom(m => m.InvitationDetails.Email))
                .ForMember(x => x.FirstName, opt => opt.MapFrom(m => m.InvitationDetails.FirstName))
                .ForMember(x => x.LastName, opt => opt.MapFrom(m => m.InvitationDetails.LastName));

            CreateMap<InvitationData, Invitation>()
                .ForMember(x => x.Guid, opt => opt.MapFrom(m => m.Id));
        }
    }
}
