﻿using System;
using System.Linq;
using TimeZoneConverter;

namespace MyEnergi.Business.Model.Helpers
{
    public static class Helper
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime ConvertTimestampToDate(long timestamp)
        {
            var dtDateTime = Epoch;
            dtDateTime = dtDateTime.AddMilliseconds(timestamp);
            return dtDateTime;
        }

        public static long ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalMilliseconds;
        }

        public static long AdjustUtcToZone(long timestamp, string timeZoneRegion)
        {
            var date = ConvertTimestampToDate(timestamp);

            ConvertTimeZoneToHourMinute(timeZoneRegion, out var hourDiff, out var minuteDiff, date);

            var adjustedDate = date.AddHours(hourDiff).AddMinutes(minuteDiff);

            return ConvertToTimestamp(adjustedDate);
        }

        public static long AdjustZoneToUtc(long timestamp, string timeZoneRegion)
        {
            var date = ConvertTimestampToDate(timestamp);

            ConvertTimeZoneToHourMinute(timeZoneRegion, out var hourDiff, out var minuteDiff, date);

            var adjustedDate = date.AddHours(hourDiff * -1).AddMinutes(minuteDiff * -1);

            return ConvertToTimestamp(adjustedDate);
        }

        public static double ConvertJoulesToKwh(long joules)
        {
            return Math.Round(joules / (double)(1000 * 60), 6); // kW minute
        }

        public static void ConvertTimeZoneToHourMinute(string timezone, out int hour, out int minute, DateTime? dateToFindTZ=null)
        {
            try
            {
                TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(TZConvert.IanaToWindows(timezone));
                // offset contains DST value
                var offset = timeZoneInfo.GetUtcOffset(dateToFindTZ ?? DateTime.UtcNow);
                hour = offset.Hours;
                minute = offset.Minutes;
            }
            catch(Exception)
            {
                // Default UTC time 
                hour = 0;
                minute = 0;
            }
        }

        public static DateTime GetDateTimeMinutesOnly(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0);
        }

        public static DateTime GetDateTimeHoursOnly(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, 0, 0);
        }
        public static void ConvertTimeFromStringToMinutes(string fromTime, string toTime, out int totalFromMinutes,
            out int totalToMinutes)
        {
            string[] fromTimeSplit = fromTime.Split(':');
            string[] toTimeSplit = toTime.Split(':');

            int fromHour = Int32.Parse(fromTimeSplit.First());
            int fromMinutes = Int32.Parse(fromTimeSplit.Last());
            int toHour = Int32.Parse(toTimeSplit.First());
            int toMinutes = Int32.Parse(toTimeSplit.Last());

            totalFromMinutes = fromHour * 60 + fromMinutes;
            totalToMinutes = toHour * 60 + toMinutes;
        }
    }
}
