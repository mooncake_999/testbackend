﻿using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models.AccountAccess
{
    public class MigrateHubsRequest
    {
        public List<MigrateHubData> Hubs { get; set; }
    }
}
