﻿namespace MyEnergi.Business.Model.Models.AccountAccess
{
    public class RegisterNewHubRequest
    {
        public string SerialNo { get; set; }
        public string RegistrationCode { get; set; }
    }
}
