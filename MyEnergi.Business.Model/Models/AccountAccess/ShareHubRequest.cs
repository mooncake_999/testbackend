﻿namespace MyEnergi.Business.Model.Models.AccountAccess
{
    public class ShareHubRequest
    {
        public string SerialNo { get; set; }
        public string Email { get; set; }
    }
}
