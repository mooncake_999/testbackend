﻿namespace MyEnergi.Business.Model.Models.AccountAccess
{
    public class TransferOwnershipRequest
    {
        public string SerialNo { get; set; }
        
        public string Email { get; set; }
    }
}
