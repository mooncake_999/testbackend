﻿using System.Net;

namespace MyEnergi.Business.Model.Models
{
    public class ApiCallResponse
    {
        public HttpStatusCode HttpStatusCode { get; set; }
        public string HttpMessage { get; set; }
        public int Status { get; set; }
        public string StatusText { get; set; }
        public object Content { get; set; }
        public string HostServer { get; set; }
    }
}
