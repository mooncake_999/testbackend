﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class AppFeatureData
    {
        public Guid Id { get; set; }
        public string Feature { get; set; }
    }
}
