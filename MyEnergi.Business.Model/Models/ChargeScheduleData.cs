﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class ChargeScheduleData
    {
        public Guid Id { get; set; }
        public string ScheduleType { get; set; }
        public string ScheduleNickName { get; set; }
        public string ChargingOutputName { get; set; }
        public int? ChargeAmountMinutes { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string DaysOfWeek { get; set; }
        public decimal? PriceBelow { get; set; }
        public string SingleChargeDay { get; set; }
        public DateTime? NextChargeTimeFrom { get; set; }
        public DateTime? NextChargeTimeTo { get; set; }
        public bool IsActive { get; set; }

    }
}
