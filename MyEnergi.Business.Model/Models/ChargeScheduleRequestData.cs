﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class ChargeScheduleRequestData
    {
        public Guid DeviceId { get; set; }
        public List<ChargeScheduleData> ChargeSchedules { get; set; }
        public string ScheduleType { get; set; }
        public bool IsActive { get; set; }
    }
}
