﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class ChargeSchedulesResponseData
    {
        public Guid HubId { get; set; }
        public string HubNickName { get; set; }
        public string EnergySetupProvider { get; set; }
        public string EnergySetupTarrif { get; set; }
        public bool? IsEconomy { get; set; }
        public bool IsGuestUser { get; set; }

        public List<DevicesForChargeSchedules> Devices { get; set; }
        public ChargeSchedulesResponseData()
        {
            Devices = new List<DevicesForChargeSchedules>();
        }
    }
}
