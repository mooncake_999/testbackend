﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class CheckApiKeyData
    {
        public List<Guid> HubIds { get; set; }
    }
}
