﻿using System;
using MyEnergi.Data.Entity;
using DeviceStatus = MyEnergi.Common.Enumerators.DeviceStatus;

namespace MyEnergi.Business.Model.Models
{
    public class DeviceData : IEquatable<DeviceData>
    {
        public Guid Id { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string SerialNumber { get; set; }
        public AddressData Address { get; set; }
        public HubData Hub { get; set; }
        public string Firmware { get; set; }

        public string InstallerName { get; set; }
        public DateTime InstallationDate { get; set; }
        public DeviceStatus Status { get; set; }
        public DateTime LastActiveTime { get; set; }
        public string HeaterOutput1 { get; set; }
        public string HeaterOutput2 { get; set; }
        public string TimeZoneRegion { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsGuestUser { get; set; }
        public bool Equals(DeviceData other)
        {
            if (SerialNumber == other.SerialNumber)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hashSerialNo = SerialNumber == null ? 0 : SerialNumber.GetHashCode();

            return hashSerialNo;
        }
    }
}
