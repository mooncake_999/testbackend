﻿using MyEnergi.Common;
using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class DeviceFeedbackData
    {
        public Guid Id { get; set; }
        public string DeviceType { get; set; }
        public string DeviceSerialNumber { get; set; }
        public IList<InstallerData> Installers { get; set; }
        public HubData Hub { get; set; }
        public Enumerators.DeviceStatus Status { get; set; }

        public DeviceFeedbackData()
        {
            Installers = new List<InstallerData>();
        }
    }
}
