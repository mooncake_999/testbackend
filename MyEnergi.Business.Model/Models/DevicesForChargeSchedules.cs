﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class DevicesForChargeSchedules
    {
        public Guid DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public List<ChargeScheduleData> ScheduleCharges { get; set; }
        public bool ScheduleChargesActive { get; set; }
        public List<ChargeScheduleData> BudgetCharges { get; set; }
        public bool BudgetChargesActive { get; set; }
        public List<ChargeScheduleData> SingleCharges { get; set; }
        public bool SingleChargesActive { get; set; }
        public bool IsGuestUser { get; set; }

        public DateTime? DeviceNextChargeTimeFrom { get; set; }
        public DateTime? DeviceNextChargeTimeTo { get; set; }
        public DateTime? ScheduleNextChargeTimeFrom { get; set; }
        public DateTime? ScheduleNextChargeTimeTo { get; set; }
        public DateTime? SingleNextChargeTimeFrom { get; set; }
        public DateTime? SingleNextChargeTimeTo { get; set; }
        public DateTime? BudgetNextChargeTimeFrom { get; set; }
        public DateTime? BudgetNextChargeTimeTo { get; set; }

        public DevicesForChargeSchedules()
        {
            ScheduleCharges = new List<ChargeScheduleData>();
            BudgetCharges = new List<ChargeScheduleData>();
            SingleCharges = new List<ChargeScheduleData>();
        }
    }
}