﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class DualTariffRequestData
    {   
        public List<int> Days { get; set; }
        public List<Tariff> Tariffs { get; set; }
        public Guid EnergySetupId { get; set; }
    }

    public class Tariff
    {
        public Guid Id { get; set; }
        public int FromMinutes { get; set; }
        public int ToMinutes { get; set; }
        public decimal Price { get; set; }
    }
}
