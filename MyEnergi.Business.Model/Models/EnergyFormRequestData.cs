﻿namespace MyEnergi.Business.Model.Models
{
    public class EnergyFormRequestData
    {
        public AddressData Address { get; set; }
        public EnergySetupData EnergySetup {get;set;}
    }
}
