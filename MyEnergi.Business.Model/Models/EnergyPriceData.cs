﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class EnergyPriceData
    {
        public string GridSupplyPoint { get; set; }
        public decimal ValueWithoutVAT { get; set; }
        public decimal ValueWithVAT { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public bool AddedToCalculations { get; set; }
        public bool AlreadyProgrammedToOtherDevice { get; set; }

    }
}
