﻿namespace MyEnergi.Business.Model.Models
{
    public class EnergyProviderCSV
    {
        public string EnergyProvider { get; set; }
        public string TariffName { get; set; }
        public string IsFlexible { get; set; }
        public string IsGreen { get; set; }
        public string IsEV { get; set; }
        public string IsEconomy { get; set; }
        public string Country { get; set; }
        public string InternalReference { get; set; }
        public string IsActive { get; set; }
    }
}
