﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class EnergyProviderData
    {
        public string EnergyProvider { get; set; }
        public string TariffName { get; set; }
        public string IsFlexible { get; set; }
        public string IsGreen { get; set; }
        public string IsEV { get; set; }
        public string IsEconomy { get; set; }
        public string Country { get; set; }
        public string InternalReference { get; set; }
        public Guid Guid { get; set; }
    }
}
