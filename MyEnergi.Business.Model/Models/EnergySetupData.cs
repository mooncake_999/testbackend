﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class EnergySetupData
    {
        public Guid Id { get; set; }
        public string EnergyProvider { get; set; }
        public string EnergyTariff { get; set; }
        public bool IsEconomy { get; set; }
        public bool IsActive { get; set; }

        public string TariffExpirationDate { get; set; }
        public bool SolarPanels { get; set; }
        public bool WindTurbine { get; set; }
        public bool Hydroelectric { get; set; }
        public string OtherGeneration { get; set; }
        public decimal? MaxPowerOutput { get; set; }
        public bool AirSourceHeatPump { get; set; }
        public bool GroundSourceHeatPump { get; set; }
        public bool ElectricStorageHeating { get; set; }
        public string OtherElectricHeating { get; set; }
        public Guid EnergyProviderId { get; set; }
    }
}
