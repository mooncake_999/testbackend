﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
   public class GraphEnergyPriceListData
    {
        public List<EnergyPriceData> energyPrices { get; set; }
        public DateTime? FromInterval1 { get; set; }
        public DateTime? ToInterval1 { get; set; }
        public DateTime? FromInterval2 { get; set; }
        public DateTime? ToInterval2 { get; set; }
        public string Currency { get; set; }

    }
}
