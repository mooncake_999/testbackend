﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class GraphSpecificDataRequest
    {
        public Guid HubId { get; set; }
        public List<long> DayTimestampsUtc { get; set; }
    }
}
