﻿using System;
using DeviceStatus = MyEnergi.Common.Enumerators.DeviceStatus;

namespace MyEnergi.Business.Model.Models
{
    public class HubData
    {
        public Guid Id { get; set; }
        public AddressData Address { get; set; }
        public string SerialNo { get; set; }
        public string RegistrationCode { get; set; }
        public string NickName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public DeviceStatus Status { get; set; }
        public string Firmware { get; set; }
        public string TimeZoneRegion { get; set; }
        public string HostServer { get; set; }
        public bool IsGuestUser { get; set; }
        public string OwnerEmail { get; set; }
    }
}
