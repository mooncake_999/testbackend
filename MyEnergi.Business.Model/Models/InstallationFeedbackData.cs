﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class InstallationFeedbackData
    {
        public string InstalledBy { get; set; }
        public string Installer { get; set; }
        public string InstallerCode { get; set; }
        public string InstallerName { get; set; }
        public string InstallerWebsite { get; set; }
        public string InstallerPhoneNumber { get; set; }
        public string InstallationDate { get; set; }
        public string QualityOfInstall { get; set; }
        public string Feedback { get; set; }
        public bool ShareFeedback { get; set; }
        public bool PrizeDrawOptIn { get; set; }
        public bool SurveyOptIn { get; set; }
        public bool RecievedGrant { get; set; }
        public Guid? HubId { get; set; }
        public Guid? DeviceId { get; set; }
        public bool IsNew { get; set; }
    }
}
