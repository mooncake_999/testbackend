﻿namespace MyEnergi.Business.Model.Models
{
    public class InstallerData
    {
        public string InstallerName { get; set; }
        public string InstallerCode { get; set; }
        public string Country { get; set; }
    }
}
