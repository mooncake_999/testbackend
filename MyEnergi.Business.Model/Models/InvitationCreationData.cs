﻿using System;
using System.Collections.Generic;
using MyEnergi.Common;
using MyEnergi.Data.Entity;

namespace MyEnergi.Business.Model.Models
{
    public class InvitationCreationData
    {
        public string OwnerCognitoId { get; set; }

        public InvitationDetails InvitationDetails { get; set; }

        public string SerialNo { get; set; }
        
        public User GuestUser { get; set; }
            
        public IList<Device> Devices { get; set; }
        
        public IList<AppFeature> Features { get; set; }
        
        public Enumerators.InvitationStatus InvitationStatus { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        
        public InvitationCreationData()
        {
            Devices = new List<Device>();
            Features = new List<AppFeature>();
        }
    }
}
