﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class InvitationData
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string InvitationStatus { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
