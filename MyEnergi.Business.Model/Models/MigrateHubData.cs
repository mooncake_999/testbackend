﻿namespace MyEnergi.Business.Model.Models
{
    public class MigrateHubData
    {
        public string SerialNo { get; set; }

        public string HubPassword { get; set; }
    }
}
