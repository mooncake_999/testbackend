﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class ResendInvitationEmailData
    {
        public Guid InvitationId { get; set; }
        
        public string Email { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
