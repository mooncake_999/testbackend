﻿using MyEnergi.Common;

namespace MyEnergi.Business.Model.Models.Stripe
{
    public class ExtendedWarrantyPaymentRequestData
    {
        public Enumerators.PaymentMethod PaymentMethod { get; set; } = Enumerators.PaymentMethod.Card;

        public Enumerators.Currency Currency { get; set; } = Enumerators.Currency.Gbp;

        public long Price { get; set; } = 2000; // 2000 means 20.00 GBP

        public string ProductName { get; set; } = "Extended Warranty";

        public int Quantity { get; set; } = 1;

        public string SuccessUrl { get; set; } = "https://example.com/success";

        public string CancelUrl { get; set; } = "https://example.com/cancel";
    }
}
