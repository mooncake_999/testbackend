﻿namespace MyEnergi.Business.Model.Models
{
    public class UserDetailsData : UserPersonalDetailsData
    {
        public UserPreferenceData UserPreference { get; set; }
    }
}
