﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyEnergi.Business.Model.Models
{
    public class UserPermissionData
    {
        public Guid InvitationId { get; set; }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        [EmailAddress]
        public string Email { get; set; }

        public bool AdministratorAccess { get; set; }
        public bool AppAccess { get; set; }
        public bool AllDevicesAccess { get; set; }
        
        public IEnumerable<string> Features { get; set; }
        public IEnumerable<Guid> DevicesIds { get; set; }
        
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public UserPermissionData()
        {
            Features = new List<string>();
            DevicesIds = new List<Guid>();
        }
    }
}
