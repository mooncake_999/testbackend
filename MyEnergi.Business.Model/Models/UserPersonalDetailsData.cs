﻿using System.Collections.Generic;

namespace MyEnergi.Business.Model.Models
{
    public class UserPersonalDetailsData
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileCode { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineCode { get; set; }
        public string LandlineNumber { get; set; }
        public string DateOfBirth { get; set; }
        public IList<AddressData> Address { get; set; }
        public IList<VehicleData> UserVehicles { get; set; }

        public UserPersonalDetailsData()
        {
            UserVehicles = new List<VehicleData>();
            Address = new List<AddressData>();
        }
    }
}
