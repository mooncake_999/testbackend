﻿using System;

namespace MyEnergi.Business.Model.Models
{
    public class UserPreferenceData
    {
        public Guid Id { get; set; }
        public string DateFormat { get; set; }
        public bool IsMetric { get; set; }
        public string Language { get; set; }
        public string Currency { get; set; }
    }
}
