﻿namespace MyEnergi.Business.Model.Models
{
    public class UserRegistrationData
    {
        public string Email { get; set; }
        public string UserCognitoId { get; set; }
        public bool PrivacyAcceptance { get; set; }
        public bool MarketingOptIn { get; set; }
        public bool ThirdPartyMarketingOptIn { get; set; }
        public string Password { get; set; }
    }
}
