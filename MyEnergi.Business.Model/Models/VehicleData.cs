﻿using System;
using static MyEnergi.Common.Enumerators;

namespace MyEnergi.Business.Model.Models
{
    public class VehicleData
    {
        public Guid Id { get; set; }
        public int RefId { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int? AvailabilityDateFrom { get; set; }
        public int? AvailabilityDateTo { get; set; }
        public int? ManufacturingYear { get; set; }
        public float? BatterySize { get; set; }
        public int? ManufacturerOfficialEVRange { get; set; }
        public int? RealEVRange { get; set; }
        public string VehicleNickName { get; set; }
        public float? ChargeRate { get; set; }
        public VehicleCondition Condition { get; set; }
        public VehicleOwnership Ownership { get; set; }
    }
}
