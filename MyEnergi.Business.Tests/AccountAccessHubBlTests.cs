﻿using AutoMapper;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.AutoMapper;
using MyEnergi.Common;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class AccountAccessHubBlTests
    {
        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IAddressRepository> _addressRepositoryMock;
        private Mock<IHubRepository> _hubRepositoryMock;
        private Mock<IDeviceRepository> _deviceRepositoryMock;
        private Mock<IInvitationRepository> _invitationRepositoryMock;
        private Mock<IUserFeaturePermissionRepository> _userFeaturePermissionRepositoryMock;
        private Mock<IUserFeaturePermissionHistoryRepository> _userFeaturePermissionHistoryRepositoryMock;

        private Mock<IAccountAccessShareBL> _invitationAccountAccessBlMock;
        private Mock<IHubBL> _hubBlMock;

        private Mock<IApiCallerHelper> _apiCallerHelperMock;
        private IMapper _mapper;

        private string _cognitoId;

        [SetUp]
        public void AccountAccessHubBlTests_SetUp()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _addressRepositoryMock = new Mock<IAddressRepository>();
            _hubRepositoryMock = new Mock<IHubRepository>();
            _deviceRepositoryMock = new Mock<IDeviceRepository>();
            _invitationRepositoryMock = new Mock<IInvitationRepository>();
            _userFeaturePermissionRepositoryMock = new Mock<IUserFeaturePermissionRepository>();
            _userFeaturePermissionHistoryRepositoryMock = new Mock<IUserFeaturePermissionHistoryRepository>();

            _invitationAccountAccessBlMock = new Mock<IAccountAccessShareBL>();
            _hubBlMock = new Mock<IHubBL>();

            _apiCallerHelperMock = new Mock<IApiCallerHelper>();

            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            _mapper = mappingConfig.CreateMapper();

            _cognitoId = MockConstants.CognitoIdFromToken;
        }

        [Test]
        public void AccountAccessHubBlTests_GetHubs_HappyFlow()
        {
            // Arrange
            var hubs = HubMockFactory.GetListOfHubsForUser();
            var firstHub = hubs.First();
            var secondHub = hubs.ElementAt(1);
            
            _hubRepositoryMock
                .Setup(rep => rep.GetHubByUserId(_cognitoId))
                .Returns(Task.FromResult(new List<Hub> { firstHub }.AsEnumerable()));

            var ownerUser = UserMockFactory.GetNewFullUser();

            _userRepositoryMock
                .Setup(rep => rep.GetUserByCognitoId(_cognitoId))
                .Returns(Task.FromResult(ownerUser));

            var guestUser = UserMockFactory.GetNewFullUser("88fb8e83-489d-4bad-b308-10191d961f21");
            var devicesForHubOne = DeviceMockFactory.GetListOfNewDevices(hub: firstHub);
            var devicesForHubTwo = DeviceMockFactory.GetListOfNewDevices(hub: secondHub);
            
            var invitation1 = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(ownerUser, guestUser,
                Enumerators.InvitationStatus.Accepted, devicesForHubOne);
            var invitation2 = InvitationMockFactory.GetNewFullInvitationForNotExistingGuestUser(ownerUser,
                "guest2@yopmail.com", "Gion", "Guest2", Enumerators.InvitationStatus.Pending, devicesForHubOne);
            var invitation3 = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(guestUser, ownerUser,
                Enumerators.InvitationStatus.Accepted, devicesForHubTwo);

            _invitationRepositoryMock
                .Setup(rep => rep.GetOwnerUserSendInvitations(_cognitoId))
                .Returns(Task.FromResult(new List<Invitation> {invitation1, invitation2}.AsEnumerable()));

            _invitationRepositoryMock
                .Setup(rep => rep.GetGuestUserInvitations(_cognitoId))
                .Returns(Task.FromResult(new List<Invitation> {invitation3}.AsEnumerable()));
            
            var accountAccessHub =
                new AccountAccessHubBL(_userRepositoryMock.Object, null, _hubRepositoryMock.Object, null,
                    _invitationRepositoryMock.Object, null,
                    null, null, null, null, _mapper);

            // Act
            var result = accountAccessHub.GetHubs(_cognitoId).Result;

            var userIsOwnerHubResult = result.First();
            var userIsGuestHubResult = result.ElementAt(1);

            var firstHubPassword = userIsOwnerHubResult.GetType().GetProperty("hubPassword")?.GetValue(userIsOwnerHubResult);
            var firstHubOwner = userIsOwnerHubResult.GetType().GetProperty("owner")?.GetValue(userIsOwnerHubResult);
            var firstHubName = userIsOwnerHubResult.GetType().GetProperty("hubName")?.GetValue(userIsOwnerHubResult);
            var firstHubRelation = userIsOwnerHubResult.GetType().GetProperty("relation")?.GetValue(userIsOwnerHubResult);
            var firstHubSerialNo = userIsOwnerHubResult.GetType().GetProperty("serialNo")?.GetValue(userIsOwnerHubResult);
            var firstHubShares = (List<object>)userIsOwnerHubResult.GetType().GetProperty("shares")?.GetValue(userIsOwnerHubResult);

            var secondHubPassword = userIsGuestHubResult.GetType().GetProperty("hubPassword")?.GetValue(userIsGuestHubResult);
            var secondHubRelation = userIsGuestHubResult.GetType().GetProperty("relation")?.GetValue(userIsGuestHubResult);
            var secondHubSerialNo = userIsGuestHubResult.GetType().GetProperty("serialNo")?.GetValue(userIsGuestHubResult);
            var secondHubName = userIsGuestHubResult.GetType().GetProperty("hubName")?.GetValue(userIsGuestHubResult);
            var secondHubFeatures = (IEnumerable<string>)userIsGuestHubResult.GetType().GetProperty("features")?.GetValue(userIsGuestHubResult);

            // Assert
            Assert.IsNotNull(firstHubPassword);
            Assert.IsNotNull(firstHubOwner);
            Assert.AreEqual("Tisbury", firstHubName);
            Assert.AreEqual(Enumerators.UserRole.Owner.ToString(), firstHubRelation);
            Assert.AreEqual("10365623", firstHubSerialNo);
            Assert.AreEqual(2, firstHubShares.Count);

            Assert.IsNotNull(secondHubPassword);
            Assert.AreEqual("garage", secondHubName);
            Assert.AreEqual(Enumerators.UserRole.Guest.ToString(), secondHubRelation);
            Assert.AreEqual("10159297", secondHubSerialNo);
            Assert.AreEqual(3, secondHubFeatures.Count());
        }

        [TearDown]
        public void AccountAccessHubBlTests_TearDown()
        {
            _userRepositoryMock = null;
            _addressRepositoryMock = null;
            _hubRepositoryMock = null;
            _deviceRepositoryMock = null;
            _invitationAccountAccessBlMock = null;
            _userFeaturePermissionRepositoryMock = null;
            _userFeaturePermissionHistoryRepositoryMock = null;

            _invitationAccountAccessBlMock = null;
            _hubBlMock = null;

            _apiCallerHelperMock = null;
            _mapper = null;
        }
    }
}
