﻿using Moq;
using MyEnergi.Data.Interfaces;
using NUnit.Framework;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class AccountAccessShareBlTests
    {
        private Mock<IInvitationRepository> _invitationRepositoryMock;
        private Mock<IAppFeatureRepository> _appFeatureRepositoryMock;
        private Mock<IDeviceRepository> _deviceRepositoryMock;
        private Mock<IUserRepository> _userRepositoryMock;

        [SetUp]
        public void AccountAccessShareBl_SetUp()
        {
            _invitationRepositoryMock = new Mock<IInvitationRepository>();
            _appFeatureRepositoryMock = new Mock<IAppFeatureRepository>();
            _deviceRepositoryMock = new Mock<IDeviceRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();
        }

        [TearDown]
        public void AccountAccessShareBl_TearDown()
        {
            _invitationRepositoryMock = null;
            _appFeatureRepositoryMock = null;
            _deviceRepositoryMock = null;
        }
    }
}
