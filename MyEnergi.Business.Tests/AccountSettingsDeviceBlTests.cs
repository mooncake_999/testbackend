﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.AutoMapper;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DeviceData = MyEnergi.AppServer.Model.DeviceData;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class AccountSettingsDeviceBlTests
    {
        private Mock<IHubBL> _hubBlMock;
        private Mock<IUserBL> _userBlMock;
        private Mock<IDeviceBL> _deviceBlMock;
        private Mock<IAccountAccessShareBL> _accountAccesShareMock;

        private Mock<IHubRepository> _hubRepositoryMock;
        private Mock<IDeviceRepository> _deviceRepositoryMock;
        private Mock<IAppFeatureRepository> _appFeatureRepositoryMock;
        private Mock<IInvitationRepository> _invitationRepositoryMock;
        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IAddressRepository> _addressRepositoryMock;
        private Mock<IInvitationStatusRepository> _invitationStatusRepositoryMock;
        private Mock<IUserFeaturePermissionRepository> _userFeaturePermissionRepositoryMock;
        private Mock<IUserFeaturePermissionHistoryRepository> _userFeaturePermissionHistoryRepositoryMock;

        private Mock<IConfiguration> _configuration;

        private Mock<IApiCallerHelper> _apiCallerHelperMock;
        private IMapper _mapper;

        [SetUp]
        public void AccountSettingsDeviceBl_SetUp()
        {
            _hubBlMock = new Mock<IHubBL>();
            _userBlMock = new Mock<IUserBL>();
            _deviceBlMock = new Mock<IDeviceBL>();
            _accountAccesShareMock = new Mock<IAccountAccessShareBL>();

            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            _mapper = mappingConfig.CreateMapper();

            _hubRepositoryMock = new Mock<IHubRepository>();
            _appFeatureRepositoryMock = new Mock<IAppFeatureRepository>();
            _invitationRepositoryMock = new Mock<IInvitationRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _addressRepositoryMock = new Mock<IAddressRepository>();
                
            _invitationStatusRepositoryMock = new Mock<IInvitationStatusRepository>();
            _userFeaturePermissionRepositoryMock = new Mock<IUserFeaturePermissionRepository>();
            _userFeaturePermissionHistoryRepositoryMock = new Mock<IUserFeaturePermissionHistoryRepository>();
            _deviceRepositoryMock = new Mock<IDeviceRepository>();

            _configuration = new Mock<IConfiguration>();
            _apiCallerHelperMock = new Mock<IApiCallerHelper>();
        }

        [Test]
        public void AccountSettingsDeviceBl_GetNewDevices_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var devices = DeviceMockFactory.GetListOfNewDevices().AsEnumerable();

            _deviceRepositoryMock
                .Setup(rep => rep.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devices));

            var devicesData = _mapper.Map<List<Model.Models.DeviceData>>(devices);
            _deviceBlMock
                .Setup(bl => bl.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devicesData));

            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, null, null, null, null, null, null, _mapper, null);

            var accountSettingsBl =
                new AccountSettingsDeviceBL(null, hubBl, _deviceBlMock.Object, _apiCallerHelperMock.Object, null);

            // Act
            var resultDevices = accountSettingsBl.GetNewDevices(user.CognitoId).Result;

            var resultDevice1 = resultDevices.First();
            var resultDevice2 = resultDevices.ElementAt(1);
            var resultDevice3 = resultDevices.ElementAt(2);

            // Assert
            Assert.AreEqual(3, resultDevices.Count);

            Assert.AreEqual("zappi", resultDevice1.DeviceType);
            Assert.AreEqual("3560S3.103", resultDevice1.Firmware);
            Assert.AreEqual("12057779", resultDevice1.SerialNumber);

            Assert.AreEqual("harvi", resultDevice2.DeviceType);
            Assert.AreEqual("3300S3.101", resultDevice2.Firmware);
            Assert.AreEqual("11100003", resultDevice2.SerialNumber);

            Assert.AreEqual("eddi", resultDevice3.DeviceType);
            Assert.AreEqual("3200S2.093", resultDevice3.Firmware);
            Assert.AreEqual("12067512", resultDevice3.SerialNumber);
        }

        [Test]
        public void AccountSettingsDeviceBl_GetDeviceStatusOfHub_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var hub = HubMockFactory.GetNewFullHub();
            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(user, hub);
            var address = hub.Address;

            GetDeviceStatusOfHub_CreateMockSetupForHubBL(address, devices, hub);
            var hubBl = new HubBL(_hubRepositoryMock.Object, null, _userRepositoryMock.Object, null, null, null, null, null,
                _mapper, null);

            GetDeviceStatusOfHub_CreateMockSetupForAccountSettingsBL(hub, devices);

            var devicesData = _mapper.Map<List<Model.Models.DeviceData>>(devices);
            _deviceBlMock
                .Setup(rep => rep.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devicesData));
            
            _deviceBlMock
                .Setup(bl => bl.GetDevicesByUserIdIsDeleted(user.CognitoId))
                .Returns(Task.FromResult(devicesData));
            
            var accountSettingsBl = new AccountSettingsDeviceBL(null, hubBl, _deviceBlMock.Object,
                _apiCallerHelperMock.Object, _mapper);

            // Act
            var resultDevices = accountSettingsBl.GetDeviceStatusOfHub(user.CognitoId, user.Guid).Result;

            var resultDeviceObj1 = resultDevices.First();
            var resultDeviceObj2 = resultDevices.ElementAt(1);
            var resultDeviceObj3 = resultDevices.ElementAt(2);

            var resultDevice1 = (Model.Models.DeviceData)resultDeviceObj1.GetType().GetProperty("Device")
                ?.GetValue(resultDeviceObj1, null);
            var resultDevice2 = (Model.Models.DeviceData)resultDeviceObj2.GetType().GetProperty("Device")
                ?.GetValue(resultDeviceObj2, null);
            var resultDevice3 = (Model.Models.DeviceData)resultDeviceObj3.GetType().GetProperty("Device")
                ?.GetValue(resultDeviceObj3, null);

            // Assert
            Assert.AreEqual(3, resultDevices.Count);
            Assert.AreEqual("zappi", resultDevice1.DeviceType);
            Assert.AreEqual("3560S3.103", resultDevice1.Firmware);
            Assert.AreEqual("12057779", resultDevice1.SerialNumber);

            Assert.AreEqual("harvi", resultDevice2.DeviceType);
            Assert.AreEqual("3300S3.101", resultDevice2.Firmware);
            Assert.AreEqual("11100003", resultDevice2.SerialNumber);

            Assert.AreEqual("eddi", resultDevice3.DeviceType);
            Assert.AreEqual("3200S2.093", resultDevice3.Firmware);
            Assert.AreEqual("12067512", resultDevice3.SerialNumber);
        }

        private void GetDeviceStatusOfHub_CreateMockSetupForAccountSettingsBL(Hub hub, IList<Device> devices)
        {
            var apiCallResponse = new AppServer.Model.ApiCallResponse
            {
                HttpStatusCode = HttpStatusCode.OK,
                Status = 0,
                Content = new Tuple<string, List<DeviceData>>(hub.Firmware, _mapper.Map<List<DeviceData>>(devices))
            };

            _apiCallerHelperMock
                .Setup(api => api.GetExistingHubStatusResult(It.IsAny<HubData>(), false))
                .Returns(Task.FromResult(apiCallResponse));

            foreach (var device in devices)
            {
                _deviceRepositoryMock
                    .Setup(rep => rep.GetDeviceBySerialNo(device.SerialNumber))
                    .Returns(Task.FromResult(device));

                _apiCallerHelperMock
                    .Setup(api => api.GetKeyValueResult(It.IsAny<HubData>(), false,
                        device.DeviceType.ToUpper().First() + device.SerialNumber + 1, null))
                    .Returns(Task.FromResult(new AppServer.Model.ApiCallResponse
                    {
                        Content = device.HeaterOutput1
                    }));

                _apiCallerHelperMock
                    .Setup(api => api.GetKeyValueResult(It.IsAny<HubData>(), false,
                        device.DeviceType.ToUpper().First() + device.SerialNumber + 2, null))
                    .Returns(Task.FromResult(new AppServer.Model.ApiCallResponse
                    {
                        Content = device.HeaterOutput2
                    }));
            }
        }

        private void GetDeviceStatusOfHub_CreateMockSetupForHubBL(Address address, IEnumerable<Device> devices, Hub hub)
        {
            _addressRepositoryMock
                .Setup(rep => rep.GetAddressByIdAndUserId(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(Task.FromResult(address));

            _deviceRepositoryMock
                .Setup(rep => rep.GetDevicesByUserId(It.IsAny<string>()))
                .Returns(Task.FromResult(devices));

            _hubRepositoryMock
                .Setup(rep => rep.GetHubByIdAndUser(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(Task.FromResult(hub));

            _deviceRepositoryMock
                .Setup(rep => rep.GetDeviceById(It.IsAny<Guid>()))
                .Returns(Task.FromResult(DeviceMockFactory.GetEmptyDevice()));
        }

        [Test]
        public void AccountSettingsDeviceBl_GetPhantomDeviceStatus_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(user, null).AsEnumerable();

            _deviceRepositoryMock
                .Setup(rep => rep.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devices));

            _userRepositoryMock
                .Setup(rep => rep.GetUserByCognitoId(user.CognitoId))
                .Returns(Task.FromResult(user));

            var hubBl = new HubBL(_hubRepositoryMock.Object, null, _userRepositoryMock.Object, null, null, null, null,
                null, _mapper, null);
            var userBl = new UserBL(_userRepositoryMock.Object, _addressRepositoryMock.Object, null, null, _mapper, null);
            var devicesData = _mapper.Map<List<Model.Models.DeviceData>>(devices);
            _deviceBlMock.Setup(bl => bl.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devicesData));

            var accountSettingsBl = new AccountSettingsDeviceBL(userBl, hubBl, _deviceBlMock.Object,
                _apiCallerHelperMock.Object, _mapper);

            // Act
            var phantomDevicesStatus =
                accountSettingsBl.GetPhantomDeviceStatus(user.CognitoId).Result.ToList();

            var mainAddressLine = user.Addresses.First().AddressLine1;
            var resultDevice1 = (Model.Models.DeviceData)phantomDevicesStatus.First();
            var resultDevice2 = (Model.Models.DeviceData)phantomDevicesStatus.ElementAt(1);
            var resultDevice3 = (Model.Models.DeviceData)phantomDevicesStatus.ElementAt(2);

            // Assert
            Assert.AreEqual(3, phantomDevicesStatus.Count());
            Assert.AreEqual(null, resultDevice1.Hub);
            Assert.AreEqual(mainAddressLine, resultDevice1.Address.AddressLine1);
            Assert.AreEqual("zappi", resultDevice1.DeviceType);
            Assert.AreEqual("3560S3.103", resultDevice1.Firmware);
            Assert.AreEqual("12057779", resultDevice1.SerialNumber);

            Assert.AreEqual(null, resultDevice2.Hub);
            Assert.AreEqual(mainAddressLine, resultDevice2.Address.AddressLine1);
            Assert.AreEqual("harvi", resultDevice2.DeviceType);
            Assert.AreEqual("3300S3.101", resultDevice2.Firmware);
            Assert.AreEqual("11100003", resultDevice2.SerialNumber);

            Assert.AreEqual(null, resultDevice3.Hub);
            Assert.AreEqual(mainAddressLine, resultDevice3.Address.AddressLine1);
            Assert.AreEqual("eddi", resultDevice3.DeviceType);
            Assert.AreEqual("3200S2.093", resultDevice3.Firmware);
            Assert.AreEqual("12067512", resultDevice3.SerialNumber);
        }

        [Test]
        public void AccountSettingsDeviceBl_GetAllDeviceStatus_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var mainAddress = user.Addresses.First();
            var hub = HubMockFactory.GetNewFullHub(user);
            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(user, hub);
            var phantomDevice = DeviceMockFactory.GetPhantomDevice(mainAddress);
            devices.Add(phantomDevice);

            GetAllDeviceStatus_CreateMockSetup(user, hub, devices);

            var hubBl = new HubBL(_hubRepositoryMock.Object, null, _userRepositoryMock.Object, _addressRepositoryMock.Object, null, _userFeaturePermissionRepositoryMock.Object, null,
                null, _mapper, null);
            var userBl = new UserBL(_userRepositoryMock.Object, null, null, null, _mapper, null);

            var devicesData = _mapper.Map<List<Model.Models.DeviceData>>(devices);
            _deviceBlMock
                .Setup(bl => bl.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devicesData));

            _deviceBlMock
                .Setup(bl => bl.GetDevicesByUserIdIsDeleted(user.CognitoId))
                .Returns(Task.FromResult(devicesData));

            var accountSettingsBl = new AccountSettingsDeviceBL(userBl, hubBl, _deviceBlMock.Object,
                _apiCallerHelperMock.Object, _mapper);

            // Act
            var resultList = accountSettingsBl.GetAllDeviceStatus(user.CognitoId).Result;

            var result1 = resultList.First();

            var addressDataResult = (AddressData)result1.GetType().GetProperty("Address")?.GetValue(result1, null);

            var hubsFromResult1 = (List<object>)result1.GetType().GetProperty("Hubs")?.GetValue(result1, null);
            var phantomDevicesFromResult1 =
                (List<object>)result1.GetType().GetProperty("PhantomDevices")?.GetValue(result1, null);

            var hubResultObject = hubsFromResult1.First();
            var phantomDeviceDataResult = (Model.Models.DeviceData)phantomDevicesFromResult1.First();

            var hubDataResult = (HubData)hubResultObject.GetType().GetProperty("Hub")?.GetValue(hubResultObject, null);
            var devicesObject =
                (List<object>)hubResultObject.GetType().GetProperty("Devices")?.GetValue(hubResultObject, null);

            var resultDevicesData = devicesObject.Select(o =>
                (Model.Models.DeviceData)o.GetType().GetProperty("Device")?.GetValue(o, null)).ToList();
            var firstDeviceDataResult = resultDevicesData.First();

            // Assert
            Assert.AreEqual("Broadhead House", addressDataResult.AddressLine1);
            Assert.AreEqual("United Kingdom", addressDataResult.Country);
            Assert.AreEqual("SA73 1PE", addressDataResult.PostalCode);

            Assert.AreEqual(null, phantomDeviceDataResult.Hub);
            Assert.AreEqual("zappi", phantomDeviceDataResult.DeviceType);

            Assert.AreEqual(addressDataResult.AddressLine1, hubDataResult.Address.AddressLine1);
            Assert.AreEqual("3401S3051", hubDataResult.Firmware);
            Assert.AreEqual("10159297", hubDataResult.SerialNo);
            Assert.AreEqual("s7.myenergi.net", hubDataResult.HostServer);

            Assert.AreEqual(4, resultDevicesData.Count);
            Assert.AreEqual(4, resultDevicesData.Count(d => !string.IsNullOrEmpty(d.SerialNumber)));
            Assert.AreEqual("12057779", firstDeviceDataResult.SerialNumber);
            Assert.AreEqual(hubDataResult.Address.AddressLine1, firstDeviceDataResult.Address.AddressLine1);
        }

        private void GetAllDeviceStatus_CreateMockSetup(User user, Hub hub, IEnumerable<Device> devices)
        {
            _userRepositoryMock
                .Setup(rep => rep.GetUserByCognitoId(user.CognitoId))
                .Returns(Task.FromResult(user));

            var hubs = new List<Hub> {hub};
            _hubRepositoryMock
                .Setup(rep => rep.GetHubByUserId(user.CognitoId))
                .Returns(Task.FromResult(hubs.AsEnumerable()));

            foreach (var device in devices)
            {
                _deviceRepositoryMock
                    .Setup(rep => rep.GetDeviceById(device.Guid))
                    .Returns(Task.FromResult(device));

                var devicesData = _mapper.Map<Model.Models.DeviceData>(device);
                _deviceBlMock
                    .Setup(bl => bl.GetDeviceBySerialNo(device.SerialNumber))
                    .Returns(Task.FromResult(devicesData));
            }

            _deviceRepositoryMock
                .Setup(rep => rep.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devices));

            foreach (var address in user.Addresses)
            {
                _addressRepositoryMock
                    .Setup(rep => rep.GetAddressByIdAndUserId(address.Guid, user.CognitoId))
                    .Returns(Task.FromResult(address));
            }

            GetDeviceStatusOfHub_CreateMockSetupForAccountSettingsBL(hub, devices.ToList());
        }

        [Test]
        public void AccountSettingsDeviceBl_CheckIfInvitationExists_Returns_True_When_Invitation_IsNotNull()
        {
            // Arrange
            var ownerUser = UserMockFactory.GetNewFullUser();
            var guestUser = UserMockFactory.GetNewFullUser(Guid.NewGuid().ToString());

            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(ownerUser, null);

            var invitation = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(ownerUser, guestUser,
                Enumerators.InvitationStatus.Pending, devices);

            var requestData = new UserPermissionData
            {
                Email = "testEmail_1@yopmail.com"
            };

            _invitationRepositoryMock
                .Setup(rep => rep.GetOwnerInvitationForHub(ownerUser.CognitoId, requestData.Email))
                .Returns(Task.FromResult(invitation));

            _accountAccesShareMock
                .Setup(bl => bl.CheckThatInvitationIsNotNullAndNotDeleted(invitation))
                .Returns(true);

            var accountSettingsBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null,
                _accountAccesShareMock.Object,
                null, null);

            // Act
            var result = accountSettingsBl.CheckIfInvitationExists(ownerUser.CognitoId, requestData.Email).Result;

            // Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public void AccountSettingsDeviceBl_CheckIfInvitationExists_Returns_False_When_Invitation_IsNull()
        {
            // Arrange
            _accountAccesShareMock
                .Setup(bl => bl.CheckThatInvitationIsNotNullAndNotDeleted(It.IsAny<Invitation>()))
                .Returns(false);

            var accountSettingsBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null,
                _accountAccesShareMock.Object,
                null, null);

            // Act
            var result = accountSettingsBl.CheckIfInvitationExists("cognitoId", "email").Result;

            // Assert
            Assert.AreEqual(false, result);
        }

        [TearDown]
        public void AccountSettingsDeviceBl_TearDown()
        {
            _hubBlMock = null;
            _userBlMock = null;
            _accountAccesShareMock = null;

            _hubRepositoryMock = null;
            _appFeatureRepositoryMock = null;
            _invitationRepositoryMock = null;
            _userRepositoryMock = null;
            _invitationStatusRepositoryMock = null;
            _userFeaturePermissionRepositoryMock = null;
            _userFeaturePermissionHistoryRepositoryMock = null;
            _deviceRepositoryMock = null;

            _configuration = null;

            _apiCallerHelperMock = null;
            _mapper = null;
        }
    }
}
