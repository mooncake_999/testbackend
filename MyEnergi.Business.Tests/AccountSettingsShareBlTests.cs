﻿using Microsoft.EntityFrameworkCore.Query;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class AccountSettingsShareBlTests
    {
        private Mock<IInvitationRepository> _invitationRepositoryMock;
        private Mock<IAppFeatureRepository> _appFeatureRepositoryMock;
        private Mock<IDeviceRepository> _deviceRepositoryMock;
        private Mock<IUserRepository> _userRepositoryMock;

        private Mock<IAccountAccessShareBL> _accountAccessShareBlMock;
        
        [SetUp]
        public void AccountSettingsShareBl_SetUp()
        {
            _invitationRepositoryMock = new Mock<IInvitationRepository>();
            _appFeatureRepositoryMock = new Mock<IAppFeatureRepository>();
            _deviceRepositoryMock = new Mock<IDeviceRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();

            _accountAccessShareBlMock = new Mock<IAccountAccessShareBL>();
        }

        [Test]
        public void AccountSettingsShareBl_CreateNewInvitation_HappyFlow()
        {
            // Arrange
            var ownerUser = UserMockFactory.GetNewFullUser();
            var guestUser = UserMockFactory.GetNewFullUser(Guid.NewGuid().ToString());

            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(ownerUser, null);

            var invitation = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(ownerUser, guestUser,
                Enumerators.InvitationStatus.Pending, devices);

            var appFeatures = AppFeatureMockFactory.GetAppFeatures();
            var appFeatureNames = appFeatures.Select(apf => apf.Feature).ToList();

            var requestData = new UserPermissionData
            {
                InvitationId = Guid.NewGuid(),
                FirstName = guestUser.UserDetails.FirstName,
                LastName = guestUser.UserDetails.LastName,
                Email = guestUser.Email,
                AdministratorAccess = false,
                AppAccess = true,
                AllDevicesAccess = true,
                Features = appFeatureNames,
                DevicesIds = devices.Select(d => d.Guid),
                StartDate = null,
                EndDate = null
            };

            CreateRepositoriesMockSetup_For_CreateNewInvitation(ownerUser, guestUser, invitation, requestData, devices,
                appFeatures);
            AccountAccessShareBlMockSetup_For_CreateNewInvitation(invitation, guestUser, ownerUser, devices, appFeatures,
                requestData);

            var invitationAccountSettingsBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null,
                null, _userRepositoryMock.Object,
                _deviceRepositoryMock.Object, _appFeatureRepositoryMock.Object, _accountAccessShareBlMock.Object, null, null);

            // Act
            var invitationGuidResult = invitationAccountSettingsBl.CreateNewInvitation(ownerUser.CognitoId, requestData).Result;

            // Assert
            Assert.AreEqual(invitation.Guid, invitationGuidResult);
        }

        private void CreateRepositoriesMockSetup_For_CreateNewInvitation(User ownerUser, User guestUser,
            Invitation invitation,
            UserPermissionData requestData, IEnumerable<Device> devices, IEnumerable<AppFeature> appFeatures)
        {
            _userRepositoryMock
                .Setup(rep => rep.GetUserByEmail(requestData.Email))
                .Returns(Task.FromResult(guestUser));

            _invitationRepositoryMock
                .Setup(rep => rep.GetOwnerInvitationForHub(ownerUser.CognitoId, guestUser.Email))
                .Returns(Task.FromResult(invitation));

            _deviceRepositoryMock
                .Setup(rep => rep.GetBy(d => requestData.DevicesIds.Contains(d.Guid),
                    It.IsAny<Func<IQueryable<Device>, IIncludableQueryable<Device, object>>>()))
                .Returns(Task.FromResult(devices));

            _deviceRepositoryMock
                .Setup(rep => rep.GetDevicesByUserId(ownerUser.CognitoId))
                .Returns(Task.FromResult(devices));

            _appFeatureRepositoryMock
                .Setup(rep => rep.GetAllAppFeaturesByName(requestData.Features))
                .Returns(Task.FromResult(appFeatures));
        }

        private void AccountAccessShareBlMockSetup_For_CreateNewInvitation(Invitation invitation, User guestUser,
            User ownerUser,
            IList<Device> devices, IList<AppFeature> appFeatures, UserPermissionData requestData)
        {
            _accountAccessShareBlMock
                .Setup(bl => bl.CheckThatInvitationIsNotNullAndNotDeleted(invitation))
                .Returns(false);

            var invitationDetails = InvitationDetailsMockFactory.GetNewFullInvitationDetails(guestUser.Email,
                guestUser.UserDetails.FirstName, guestUser.UserDetails.LastName);

            var invitationCreationData = new InvitationCreationData
            {
                OwnerCognitoId = ownerUser.CognitoId,
                InvitationDetails = invitationDetails,
                SerialNo = "14042854",
                GuestUser = guestUser,
                Devices = devices,
                Features = appFeatures,
                InvitationStatus = Enumerators.InvitationStatus.Pending
            };

            _accountAccessShareBlMock
                .Setup(bl => bl.GetInvitationCreationData(ownerUser.CognitoId, It.IsAny<InvitationDetails>(), devices,
                    appFeatures, Enumerators.InvitationStatus.Pending, It.IsAny<User>(), It.IsAny<string>(),
                    requestData.StartDate, requestData.EndDate, It.IsAny<DateTime?>()))
                .Returns(invitationCreationData);

            _accountAccessShareBlMock
                .Setup(bl => bl.CreateInvitation(invitationCreationData))
                .Returns(Task.FromResult(invitation.Guid));
        }

        [Test]
        public void AccountSettingsShareBl_CreateNewInvitation_ThrowsException_From_ValidateDeviceIds()
        {
            // Arrange
            var ownerUser = UserMockFactory.GetNewFullUser();
            var guestUser = UserMockFactory.GetNewFullUser(Guid.NewGuid().ToString());

            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(ownerUser, null);

            var invitation = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(ownerUser, guestUser,
                Enumerators.InvitationStatus.Pending, devices);

            var appFeatures = AppFeatureMockFactory.GetAppFeatures();
            var appFeatureNames = appFeatures.Select(apf => apf.Feature).ToList();

            var requestData = new UserPermissionData
            {
                InvitationId = Guid.NewGuid(),
                FirstName = guestUser.UserDetails.FirstName,
                LastName = guestUser.UserDetails.LastName,
                Email = guestUser.Email,
                AdministratorAccess = false,
                AppAccess = true,
                AllDevicesAccess = true,
                Features = appFeatureNames,
                DevicesIds = devices.Select(d => d.Guid),
                StartDate = null,
                EndDate = null
            };

            _invitationRepositoryMock
                .Setup(rep => rep.GetOwnerInvitationForHub(ownerUser.CognitoId, requestData.Email))
                .Returns(Task.FromResult(invitation));

            _accountAccessShareBlMock
                .Setup(bl => bl.CheckThatInvitationIsNotNullAndNotDeleted(invitation))
                .Returns(false);

            _deviceRepositoryMock
                .Setup(rep => rep.GetDevicesByUserId(ownerUser.CognitoId))
                .Returns(Task.FromResult(new List<Device>().AsEnumerable()));

            var invitationAccountSettingsBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null,
                null, null,
                _deviceRepositoryMock.Object, _appFeatureRepositoryMock.Object, _accountAccessShareBlMock.Object, null, null);

            // Act
            var exception = Assert.ThrowsAsync<BadRequestException>(() =>
                    invitationAccountSettingsBl.CreateNewInvitation(ownerUser.CognitoId, requestData));

            // Assert
            Assert.AreEqual(exception.Message, "Invalid device ids");
        }

        [Test]
        public void AccountSettingsShareBl_CreateNewInvitation_ThrowsException_When_InvitationExists()
        {
            // Arrange
            var ownerUser = UserMockFactory.GetNewFullUser();
            var guestUser = UserMockFactory.GetNewFullUser(Guid.NewGuid().ToString());

            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(ownerUser, null);

            var invitation = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(ownerUser, guestUser,
                Enumerators.InvitationStatus.Pending, devices);

            var requestData = new UserPermissionData
            {
                Email = "testEmail_1@yopmail.com"
            };

            _invitationRepositoryMock
                .Setup(rep => rep.GetOwnerInvitationForHub(ownerUser.CognitoId, requestData.Email))
                .Returns(Task.FromResult(invitation));

            _accountAccessShareBlMock
                .Setup(bl => bl.CheckThatInvitationIsNotNullAndNotDeleted(invitation))
                .Returns(true);

            // Act
            var invitationAccountSettingsBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null,
                _accountAccessShareBlMock.Object,
                null, null);

            // Assert
            var exception = Assert.ThrowsAsync<BadRequestException>(() =>
                    invitationAccountSettingsBl.CreateNewInvitation(ownerUser.CognitoId, requestData));

            Assert.AreEqual(exception.Message, "Invitation already exists.");
        }

        [Test]
        public void AccountSettingsShareBl_GetUserAccounts_HappyFlow()
        {
            // Arrange
            var currentUser = UserMockFactory.GetNewFullUser();
            var guestUser = UserMockFactory.GetNewFullUser("ac445c87-a1f7-4da7-9a0b-2ae87cb0c2aa");
            var devices = DeviceMockFactory.GetListOfNewDevices();

            var invitation = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(currentUser, guestUser,
                Enumerators.InvitationStatus.Accepted, devices);

            var invitations = new List<Invitation> { invitation };
            var userCognitoId = guestUser.CognitoId;

            _invitationRepositoryMock
                .Setup(rep => rep.GetBy(i => i.GuestUser != null &&
                                             i.GuestUser.CognitoId.Equals(userCognitoId) &&
                                             i.InvitationDetails.AdministratorAccess &&
                                             i.InvitationStatus.Status.Equals(Enumerators.InvitationStatus.Accepted
                                                 .ToString()),
                    It.IsAny<Func<IQueryable<Invitation>, IIncludableQueryable<Invitation, object>>>()))
                .Returns(Task.FromResult(invitations.AsEnumerable()));

            var accountSettingsShareBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null,
                null, null, null);

            // Act
            var userAccounts = accountSettingsShareBl.GetUserAccounts(guestUser.CognitoId).Result;

            var firstUserAccount = userAccounts.First();

            var email = (string)firstUserAccount.GetType().GetProperty("Email")?.GetValue(firstUserAccount);
            var firstName =
                (string)firstUserAccount.GetType().GetProperty("FirstName")?.GetValue(firstUserAccount);
            var isGuestUser = (bool)firstUserAccount.GetType().GetProperty("IsGuestUser")?.GetValue(firstUserAccount);
            var lastName =
                (string)firstUserAccount.GetType().GetProperty("LastName")?.GetValue(firstUserAccount);

            var secondUserAccount = userAccounts.ElementAt(1);

            var secondAccountIsGuestUser =
                (bool)secondUserAccount.GetType().GetProperty("IsGuestUser")?.GetValue(secondUserAccount);
            var invitationId =
                (Guid)secondUserAccount.GetType().GetProperty("InvitationId")?.GetValue(secondUserAccount);
            var isSelectedAccount = (bool)secondUserAccount.GetType().GetProperty("IsSelectedAccount")
                ?.GetValue(secondUserAccount);

            // Assert
            Assert.AreEqual(2, userAccounts.Count);
            Assert.AreEqual(false, isGuestUser);
            Assert.IsNotEmpty(email);
            Assert.IsNotEmpty(firstName);
            Assert.IsNotEmpty(lastName);

            Assert.AreEqual(true, secondAccountIsGuestUser);
            Assert.AreEqual(false, isSelectedAccount);
            Assert.IsNotEmpty(invitationId.ToString());
        }

        [Test]
        public void AccountSettingsShareBl_GetUserAccounts_HappyFlow_ReturnsEmptyList_When_Invitation_DoesNotExist()
        {
            // Arrange
            var accountSettingsShareBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null,
                null, null, null);

            // Act
            var userAccounts = accountSettingsShareBl.GetUserAccounts("ac445c87-a1f7-4da7-9a0b-2ae87cb0c2aa").Result;

            // Assert
            Assert.IsEmpty(userAccounts);
        }

        [Test]
        public void AccountSettingsShareBl_GetInvitationOwnerCognitoId_HappyFlow_With_Default_GuestId()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();

            var accountSettingsShareBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null, null, null, null);

            // Act
            var resultCognitoId = accountSettingsShareBl.GetInvitationOwnerCognitoId(user.CognitoId).Result;

            // Assert
            Assert.AreEqual(user.CognitoId, resultCognitoId);
        }

        [Test]
        public void AccountSettingsShareBL_GetInvitationOwnerCognitoId_HappyFlow_With_GuestId()
        {
            // Arrange
            var currentUser = UserMockFactory.GetNewFullUser();
            var guestUser = UserMockFactory.GetNewFullUser("ac445c87-a1f7-4da7-9a0b-2ae87cb0c2aa");
            var devices = DeviceMockFactory.GetListOfNewDevices(currentUser);

            var invitation = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(currentUser, guestUser,
                Enumerators.InvitationStatus.Accepted, devices);
            var invitations = new List<Invitation> { invitation };

            var invitationOwnerGuid = guestUser.Guid;
            var currentUserCognitoId = currentUser.CognitoId;

            _invitationRepositoryMock
                .Setup(rep => rep.GetBy(i =>
                        i.GuestUser != null && i.GuestUser.CognitoId.Equals(currentUserCognitoId) &&
                        i.OwnerUser.Guid.Equals(invitationOwnerGuid) &&
                        i.InvitationDetails.AdministratorAccess &&
                        i.InvitationStatus.Status.Equals(Enumerators.InvitationStatus.Accepted.ToString()),
                    It.IsAny<Func<IQueryable<Invitation>, IIncludableQueryable<Invitation, object>>>()))
                .Returns(Task.FromResult(invitations.AsEnumerable()));

            _userRepositoryMock
                .Setup(rep => rep
                    .GetFirstOrDefaultBy(u => u.Guid.Equals(invitationOwnerGuid), It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>>()))
                .Returns(Task.FromResult(currentUser));

            var accountSettingsShareBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                _userRepositoryMock.Object, null, null, null, null, null);

            // Act
            var resultCognitoId = accountSettingsShareBl.GetInvitationOwnerCognitoId(currentUser.CognitoId).Result;

            // Assert
            Assert.AreEqual(currentUser.CognitoId, resultCognitoId);
        }

        [Test]
        public void AccountSettingsShareBL_SetSelectedAccount_HappyFlow()
        {
            // Arrange
            var currentUser = UserMockFactory.GetNewFullUser();
            var guestUser = UserMockFactory.GetNewFullUser("ac445c87-a1f7-4da7-9a0b-2ae87cb0c2aa");
            var devices = DeviceMockFactory.GetListOfNewDevices(currentUser);

            var invitation = InvitationMockFactory.GetNewFullInvitationForExistingGuestUser(currentUser, guestUser,
                Enumerators.InvitationStatus.Accepted, devices);
            var invitations = new List<Invitation> {invitation};

            var currentUserCognitoId = currentUser.CognitoId;

            _invitationRepositoryMock
                .Setup(rep => rep.GetBy(i =>
                        i.GuestUser != null &&
                        i.GuestUser.CognitoId.Equals(currentUserCognitoId) &&
                        i.InvitationDetails.AdministratorAccess &&
                        i.InvitationStatus.Status.Equals(Enumerators.InvitationStatus.Accepted.ToString()),
                    It.IsAny<Func<IQueryable<Invitation>, IIncludableQueryable<Invitation, object>>>()))
                .Returns(Task.FromResult(invitations.AsEnumerable()));

            var accountSettingsShareBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null, null, null, null);

            // Act
            accountSettingsShareBl.SetSelectedAccount(currentUserCognitoId, invitation.Guid).Wait();

            // Assert
            Assert.AreEqual(true, invitation.IsSelectedAccount);
        }

        [Test]
        public void AccountSettingsShareBL_SetSelectedAccount_Throws_BadRequestException()
        {
            // Arrange
            var accountSettingsShareBl = new AccountSettingsShareBL(_invitationRepositoryMock.Object, null, null, null,
                null, null, null, null, null, null);

            // Act & Assert
            var exception = Assert.ThrowsAsync<BadRequestException>(() =>
                accountSettingsShareBl.SetSelectedAccount("ac445c87-a1f7-4da7-9a0b-2ae87cb0c2aa", Guid.NewGuid()));

            Assert.AreEqual("There is no account to be selected!", exception.Message);
        }

        [TearDown]
        public void AccountSettingsShareBl_TearDown()
        {
            _invitationRepositoryMock = null;
            _appFeatureRepositoryMock = null;
            _deviceRepositoryMock = null;

            _accountAccessShareBlMock = null;
        }
    }
}
