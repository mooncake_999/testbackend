﻿using Microsoft.Extensions.Configuration;
using Moq;
using MyEnergi.AppServer.EmailService.EmailSender;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.Business.Interfaces;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class DeviceBlTests
    {
        private Mock<IDeviceBL> _deviceBlMock;
        private Mock<IHubRepository> _hubRepositoryMock;
        private Mock<IDeviceRepository> _deviceRepositoryMock;
        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IDeviceHistoryRepository> _deviceHistoryRepositoryMock;
        private Mock<IHubHistoryRepository> _hubHistoryRepositoryMock;
        private Mock<IUserFeaturePermissionRepository> _userFeaturePermissionRepository;
        private Mock<IUserFeaturePermissionHistoryRepository> _userFeaturePermissionHistoryRepository;
            
        private Mock<IConfiguration> _configurationMock;
        private IDeleteDeviceRequestEmailSender _deleteDeviceRequestEmailSender;

        [SetUp]
        public void DeviceBlTests_SetUp()
        {
            _deviceBlMock = new Mock<IDeviceBL>();
            _hubRepositoryMock = new Mock<IHubRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _deviceRepositoryMock = new Mock<IDeviceRepository>();
            _deviceHistoryRepositoryMock = new Mock<IDeviceHistoryRepository>();
            _hubHistoryRepositoryMock = new Mock<IHubHistoryRepository>();
            _userFeaturePermissionRepository = new Mock<IUserFeaturePermissionRepository>();
            _userFeaturePermissionHistoryRepository = new Moq.Mock<IUserFeaturePermissionHistoryRepository>();
            
            _configurationMock = new Mock<IConfiguration>();

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfigurationDeviceDeleted:MailSender")])
                .Returns("myenergi Customer Account");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfigurationDeviceDeleted:MailSubject")])
                .Returns("Request to delete device");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfigurationDeviceDeleted:MailAddress")])
                .Returns("noreply@myenergi.com");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfigurationDeviceDeleted:ToMailAddress")])
                .Returns("support@myenergi.com");
            
            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfigurationDeviceDeleted:ToName")])
                .Returns("myenergi Support");


            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfiguration:SmtpServer")])
                .Returns("smtp.office365.com");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfiguration:SmtpPort")])
                .Returns("587");
            
            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfiguration:SmtpUsername")])
                .Returns("noreply@myenergi.com");
            
            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfiguration:SmtpPassword")])
                .Returns("h1tAzD2g6mI2zb1S");

            var baseDirectory = AppContext.BaseDirectory.Substring(0,
                AppContext.BaseDirectory.IndexOf("MyEnergi", StringComparison.Ordinal));
            
            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfigurationDeviceDeleted:Path")])
                .Returns(baseDirectory + "MyEnergi\\Documents\\deleteDevice.html");

            _deleteDeviceRequestEmailSender = new DeleteDeviceRequestEmailSender(_configurationMock.Object);
        }

        [Test]
        public void DeviceBlTests_DeleteDevice_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var devices = DeviceMockFactory.GetListOfNewDevices(user);
            var hub = HubMockFactory.GetNewFullHub();

            foreach (var dev in devices)
            {
                dev.Hub = hub;
            }

            _deviceRepositoryMock
                .Setup(rep => rep.GetDeviceByIdAndUserId(devices[0].Guid, user.CognitoId))
                .Returns(Task.FromResult(devices[0]));
            _userRepositoryMock
                .Setup(rep => rep.GetUserByCognitoId(user.CognitoId))
                .Returns(Task.FromResult(user));

            _deviceHistoryRepositoryMock
                .Setup(rep => rep.GetDeviceHistoryBySerial(It.IsAny<string>()))
                .Returns(Task.FromResult(DeviceMockFactory.GetEmptyDeviceHistory()));

            var deviceBl = new DeviceBL(_userRepositoryMock.Object, _hubRepositoryMock.Object,
                _deviceRepositoryMock.Object, null, _userFeaturePermissionRepository.Object, _userFeaturePermissionHistoryRepository.Object, null, null, null, null, _deleteDeviceRequestEmailSender,
                null, _deviceHistoryRepositoryMock.Object, null);
            
            // Act
            Assert.DoesNotThrowAsync(() => deviceBl.DeleteDevice(devices[0].Guid, user.CognitoId));
            Assert.AreEqual(true, devices[0].IsDeleted);
            Assert.AreEqual(false, devices[1].IsDeleted);
            Assert.AreEqual(null, devices[0].Address);
            Assert.AreEqual(user.Addresses.First(), devices[1].Address);
        }


        [Test]
        public void DeviceBlTests_DeleteHub_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var devices = DeviceMockFactory.GetListOfNewDevices(user);
            var hub = HubMockFactory.GetNewFullHub();

            foreach (var dev in devices)
            {
                dev.Hub = hub;
            }

            _hubRepositoryMock
                .Setup(rep => rep.GetHubByIdAndUser(hub.Guid, user.CognitoId))
                .Returns(Task.FromResult(hub));

            _userRepositoryMock
                .Setup(rep => rep.GetUserByCognitoId(user.CognitoId))
                .Returns(Task.FromResult(user));

            _hubHistoryRepositoryMock
                .Setup(rep => rep.GetHubHistory(It.IsAny<string>()))
                .Returns(Task.FromResult(HubMockFactory.GetEmptyHubHistory()));

            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, null, null, null, null, _deviceBlMock.Object, null,
                _hubHistoryRepositoryMock.Object);

            // Act
            Assert.DoesNotThrow(() => { hubBl.DeleteHub(hub.Guid, user.CognitoId).Wait(); });
        }

        [TearDown]
        public void DeviceBl_TearDown()
        {
            _hubRepositoryMock = null;
            _userRepositoryMock = null;
            _deviceRepositoryMock = null;
            _configurationMock = null;
            _deleteDeviceRequestEmailSender = null;
        }
    }
}
