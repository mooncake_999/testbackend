﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.AutoMapper;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using MyEnergi.Data.Entity;
using Moq;
using System.Linq;
using MimeKit;
using MyEnergi.Business.Model.Models;
using DeviceData = MyEnergi.AppServer.Model.DeviceData;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class HubBlTests
    {
        private Mock<IDeviceBL> _deviceBlMock;
        private Mock<AccountSettingsDeviceBL> _accountSettingsDeviceMock;
        private Mock<IApiCallerHelper> _apiCallerHelperMock;
        private Mock<IHubRepository> _hubRepositoryMock;
        private Mock<IDeviceRepository> _deviceRepositoryMock;
        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IAddressRepository> _addressRepoMock;
        private IMapper _mapper;
        private const string CognitoId = MockConstants.CognitoIdFromToken;

        [SetUp]
        public void HubBl_SetUp()
        {
            _deviceBlMock = new Mock<IDeviceBL>();
            _accountSettingsDeviceMock = new Mock<AccountSettingsDeviceBL>();
            _apiCallerHelperMock = new Mock<IApiCallerHelper>();
            _hubRepositoryMock = new Mock<IHubRepository>();
            _userRepositoryMock = new Mock<IUserRepository>();
            _deviceRepositoryMock = new Mock<IDeviceRepository>();
            _addressRepoMock = new Mock<IAddressRepository>();
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            _mapper = mappingConfig.CreateMapper();

        }

        [Test]
        public void HubBlTests_RegisterNewHub_HappyFlow()
        {
            // Arrange
            var hubData = HubMockFactory.GetHubData();
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var hub = HubMockFactory.GetWellbankFullHub();
            var apiResponse = MockHelper.GetOkApiCallResponseHubNickname("Wellbank");
            var deviceData = DeviceMockFactory.GetDeviceData();

            var apiResponseExisting =
                MockHelper.GetOkApiCallResponseTuple(new Tuple<string, List<DeviceData>>("3401S3051", deviceData));

            _apiCallerHelperMock
                .Setup(rep => rep.GetHubRegisterResult(hubData))
                .Returns(Task.FromResult(apiResponse));

            _apiCallerHelperMock
                .Setup(rep => rep.GetExistingHubStatusResult(hubData, true))
                .Returns(Task.FromResult(apiResponseExisting));

            _apiCallerHelperMock
                .Setup(rep => rep.GetKeyValueResult(hubData, It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(apiResponse));

            _deviceBlMock
                .Setup(bl => bl.AddNewDevice(It.IsAny<List<Model.Models.DeviceData>>(), user.CognitoId, false));
            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNoIsDeleted(hub.SerialNo))
                .Returns(Task.FromResult(hub));
            _addressRepoMock
                .Setup(a => a.GetAddressByIdAndUserId(hubData.Address.Id, user.CognitoId))
                .Returns(Task.FromResult(hub.Address));

            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNo(hub.SerialNo))
                .Returns(Task.FromResult((Hub)null));

            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var hubResponse = hubBl.RegisterNewHub(hubData, user.CognitoId).Result;

            // Act
            Assert.DoesNotThrow(() => { hubBl.RegisterNewHub(hubData, user.CognitoId).Wait(); });
            Assert.AreEqual("New hub successfully added.", hubResponse.Message);
            Assert.AreEqual(true, hubResponse.Status);
        }

        [Test]
        public void HubBlTests_RegisterNewHub_ExceptionFlow()
        {
            // Arrange
            var hubData = HubMockFactory.GetHubData();
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var hub = HubMockFactory.GetWellbankFullHub();
            var apiResponse = MockHelper.GetOkApiCallResponseHubNickname(hubData.NickName);
            var deviceData = DeviceMockFactory.GetDeviceData();

            var apiResponseExisting =
                MockHelper.GetOkApiCallResponseTuple(new Tuple<string, List<DeviceData>>("3401S3051", deviceData));

            _apiCallerHelperMock
                .Setup(rep => rep.GetHubRegisterResult(hubData))
                .Returns(Task.FromResult(apiResponse));

            _apiCallerHelperMock
                .Setup(rep => rep.GetExistingHubStatusResult(hubData, true))
                .Returns(Task.FromResult(apiResponseExisting));

            _apiCallerHelperMock
                .Setup(rep => rep.GetKeyValueResult(hubData, It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(apiResponse));

            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNoIsDeleted(hub.SerialNo))
                .Returns(Task.FromResult(hub));
            _addressRepoMock
                .Setup(a => a.GetAddressByIdAndUserId(hubData.Address.Id, user.CognitoId))
                .Returns(Task.FromResult(hub.Address));

            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNo(hub.SerialNo))
                .Returns(Task.FromResult(hub));


            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var exception = Assert.ThrowsAsync<Exception>(() => hubBl.RegisterNewHub(hubData,user.CognitoId));

            // Assert
            Assert.AreEqual("A hub with this serial number already exists in the database.", exception.Message);
            Assert.AreEqual(true, exception is Exception);

        }

        [Test]
        public void HubBlTests_EditHub_HappyFlow()
        {
            // Arrange
            var hubData = HubMockFactory.GetHubData();
            var apiResponse = MockHelper.GetOkApiCallResponseHubNickname("Wellbank");
            hubData.NickName = "Wellbank2";
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var hub = HubMockFactory.GetWellbankFullHub();
            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(user, hub).AsEnumerable(); ;

            _hubRepositoryMock
                .Setup(h => h.GetHubByIdAndUser(hubData.Id, user.CognitoId))
                .Returns(Task.FromResult(hub));
            _addressRepoMock
                .Setup(a => a.GetAddressByIdAndUserId(hubData.Address.Id, user.CognitoId))
                .Returns(Task.FromResult(hub.Address));

            _deviceRepositoryMock
                .Setup(d => d.GetDevicesByHubId(hub.Guid))
                .Returns(Task.FromResult(devices));
            hub.NickName = hubData.NickName;
            _apiCallerHelperMock
                .Setup(rep => rep.GetKeyValueResult(It.IsAny<HubData>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(apiResponse));


            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var hubResponse = hubBl.EditHub(hubData, user.CognitoId).Result;

            // Act
            Assert.AreEqual(true, hubResponse.Status);
        }

        [Test]
        public void HubBlTests_EditHub_ExceptionFlow()
        {
            // Arrange
            var hubData = HubMockFactory.GetHubData();
            hubData.NickName = "Wellbank2";
            var user = UserMockFactory.GetNewFullUser(CognitoId);

            _hubRepositoryMock
                .Setup(h => h.GetHubByIdAndUser(hubData.Id, user.CognitoId))
                .Returns(Task.FromResult((Hub)null));

            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var exception = Assert.ThrowsAsync<UnauthorizedAccessException>(() => hubBl.EditHub(hubData, user.CognitoId));

            // Act
            Assert.AreEqual("Access denied!!", exception.Message);
        }


        [Test]
        public void HubBlTests_GetUserHubsAndDevices_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var hub = HubMockFactory.GetWellbankFullHub();
            var devices = DeviceMockFactory.GetListOfDevicesForUserAndHub(user, hub).AsEnumerable();
            var devicesData = DeviceMockFactory.GetDeviceDataBusinessModel();

            _deviceBlMock
                .Setup(d => d.GetGuestDevicesData(user.CognitoId))
                .Returns(Task.FromResult(devicesData));

            _deviceRepositoryMock
                .Setup(d => d.GetDevicesByUserId(user.CognitoId))
                .Returns(Task.FromResult(devices));


            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var hubResponse = hubBl.GetUserHubsAndDevices(user.CognitoId).Result;
            var hubs = (List<object>)hubResponse.GetType().GetProperty("hubs")?.GetValue(hubResponse, null);
            var firstHub = hubs.First();
            var secondHub = hubs.ElementAt(1);

            var firstHubUser = firstHub.GetType().GetProperty("isGuestUser")?.GetValue(firstHub);
            var firstHubId = firstHub.GetType().GetProperty("hubId")?.GetValue(firstHub);
            var firstHubNickname = firstHub.GetType().GetProperty("hubNickName")?.GetValue(firstHub);
            var firstHubDevices = (List<object>) firstHub.GetType().GetProperty("devices")?.GetValue(firstHub);

            var secondHubUser = secondHub.GetType().GetProperty("isGuestUser")?.GetValue(secondHub);
            var secondHubId = secondHub.GetType().GetProperty("hubId")?.GetValue(secondHub);
            var secondHubNickname = secondHub.GetType().GetProperty("hubNickName")?.GetValue(secondHub);
            var secondHubDevices = (List<object>)secondHub.GetType().GetProperty("devices")?.GetValue(secondHub);

            // Act
            Assert.AreEqual(2, hubs.Count());
            Assert.IsNotNull(firstHubId);
            Assert.IsNull(secondHubId);
            Assert.AreEqual(false, firstHubUser);
            Assert.AreEqual("Wellbank", firstHubNickname);
            Assert.AreEqual(3, firstHubDevices.Count);
            Assert.AreEqual(null, secondHubNickname);
            Assert.AreEqual(true, secondHubUser);
            Assert.AreEqual(1, secondHubDevices.Count);
        }

        [Test]
        public void HubBlTests_AddNewHub_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var hub = HubMockFactory.GetWellbankFullHub();
            var hubData = HubMockFactory.GetHubData();

            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNoAndUserId(hubData.SerialNo, user.CognitoId))
                .Returns(Task.FromResult((Hub)null));
            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNoIsDeleted(hubData.SerialNo))
                .Returns(Task.FromResult(hub));
            _addressRepoMock
                .Setup(a => a.GetAddressByIdAndUserId(hubData.Address.Id, user.CognitoId))
                .Returns(Task.FromResult(hub.Address));
            _hubRepositoryMock
                .Setup(d => d.GetHubBySerialNo(hubData.SerialNo))
                .Returns(Task.FromResult((Hub)null));


            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var hubResponse = hubBl.AddNewHub(hubData,user.CognitoId);

            // Act
            Assert.AreEqual(TaskStatus.RanToCompletion, hubResponse.Status);
            Assert.AreEqual(null, hubResponse.Exception);
        }

        [Test]
        public void HubBlTests_AddNewHub_ExceptionFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var hubData = HubMockFactory.GetHubData();
            hubData.Address = null;

            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var exception = Assert.ThrowsAsync<Exception>(() => hubBl.AddNewHub(hubData, user.CognitoId));

            // Act
            Assert.AreEqual("Energy setup not saved, please press Back twice to re-enter your details.", exception.Message);
        }

        [Test]
        public void HubBlTests_AddNewHub_ExceptionFlowAccessDenied()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var hub = HubMockFactory.GetWellbankFullHub();
            var hubData = HubMockFactory.GetHubData();
            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNoAndUserId(hubData.SerialNo, user.CognitoId))
                .Returns(Task.FromResult((Hub)null));
            _hubRepositoryMock
                .Setup(h => h.GetHubBySerialNoIsDeleted(hubData.SerialNo))
                .Returns(Task.FromResult(hub));
            _addressRepoMock
                .Setup(a => a.GetAddressByIdAndUserId(hubData.Address.Id, user.CognitoId))
                .Returns(Task.FromResult((Address)null));

            var hubBl = new HubBL(_hubRepositoryMock.Object, _deviceRepositoryMock.Object, _userRepositoryMock.Object, _addressRepoMock.Object, null, null, _apiCallerHelperMock.Object, _deviceBlMock.Object, _mapper,
                null);
            var exception = Assert.ThrowsAsync<UnauthorizedAccessException>(() => hubBl.AddNewHub(hubData, user.CognitoId));

            // Act
            Assert.AreEqual("Access denied!!", exception.Message);
        }

        [TearDown]
        public void HubBl_TearDown()
        {
            _accountSettingsDeviceMock = null;
            _apiCallerHelperMock = null;
            _hubRepositoryMock = null;
            _userRepositoryMock = null;
            _deviceRepositoryMock = null;
            _addressRepoMock = null;
            _deviceBlMock = null;
        }
    }
}
