﻿using AutoMapper;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.AutoMapper;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System.Threading.Tasks;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class UserBlTests
    {
        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IVehicleRepository> _vehicleRepositoryMock;
        private Mock<IInvitationRepository> _invitationRepositoryMock;
        private Mock<IUserBL> _userBlMock;
        
        private IMapper _mapper;
        private string _token;
        private string _cognitoId;

        [SetUp]
        public void UserBlTests_SetUp()
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            _mapper = mappingConfig.CreateMapper();
            var user = UserMockFactory.GetNewFullUser();
            _token = MockHelper.GenerateJwtToken(user);

            _userRepositoryMock = new Mock<IUserRepository>();
            _vehicleRepositoryMock = new Mock<IVehicleRepository>();
            _invitationRepositoryMock = new Mock<IInvitationRepository>();
            _userBlMock = new Mock<IUserBL>();
            _cognitoId = MockConstants.CognitoIdFromToken;
        }

        [Test]
        public void UserBl_SaveUser_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser(_cognitoId);
            var config = UserRegistrationMockFactory.GetEmailConfiguration();
            _userRepositoryMock.Setup(rep => rep.GetUserByEmail(config.MailAddress))
                .Returns(Task.FromResult(user));
            var data = UserRegistrationMockFactory.GetDefaultUserRegistrationData();

            // Act
            var result = _userBlMock.Object.SaveUser(data);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(null, result.Exception);
            Assert.AreEqual(TaskStatus.RanToCompletion, result.Status);
        }

        [TearDown]
        public void UserBlTests_TearDown()
        {
            _userRepositoryMock = null;
            _vehicleRepositoryMock = null;
            _invitationRepositoryMock = null;
            _userBlMock = null;

            _mapper = null;
            _token = null;
            _cognitoId = null;
        }
    }
}
