﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using MyEnergi.Business.Model.AutoMapper;
using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class UserPreferencesBlTests
    {
        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IConfiguration> _configurationMock;
        private IMapper _mapper;

        private const string CognitoId = MockConstants.CognitoIdFromToken;
        private const string UserDoesNotExistsMessage = "User does not exists in the DB";

        [SetUp]
        public void UserPreferencesBlTests_SetUp()
        {
            var mappingConfig = new MapperConfiguration(mc => { mc.AddProfile(new MappingProfile()); });
            _mapper = mappingConfig.CreateMapper();

            _userRepositoryMock = new Mock<IUserRepository>();

            _configurationMock = new Mock<IConfiguration>();

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:Currency")])
                .Returns("GBP");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:DateFormat")])
                .Returns("dd/MM/yyyy");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:IsMetric")])
                .Returns("false");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:Language")])
                .Returns("en-GB");
        }

        [Test]
        public void UserBl_GetPreference_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser(CognitoId);

            _userRepositoryMock.Setup(rep => rep.GetUserByCognitoId(CognitoId))
                .Returns(Task.FromResult(user));

            var userPreferencesBl = new UserPreferencesBL(_userRepositoryMock.Object, _mapper, null);

            // Act
            var result = userPreferencesBl.GetPreference(CognitoId).Result;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("GBP", result.Currency);
            Assert.AreEqual("dd/MM/yyyy", result.DateFormat);
            Assert.AreEqual(false, result.IsMetric);
            Assert.AreEqual("en-GB", result.Language);
        }

        [Test]
        public void UserBl_GetPreference_Throws_Exception_With_UserDoesNotExists()
        {
            // Arrange
            _userRepositoryMock.Setup(rep => rep.GetUserByCognitoId(CognitoId))
                .Returns(Task.FromResult((User)null));

            var userPreferencesBl = new UserPreferencesBL(_userRepositoryMock.Object, _mapper, null);

            // Act
            var exception = Assert.ThrowsAsync<Exception>(() => userPreferencesBl.GetPreference(CognitoId));

            // Assert
            Assert.AreEqual(UserDoesNotExistsMessage, exception.Message);
            Assert.AreEqual(true, exception is Exception);
        }

        [Test]
        public void UserBl_SaveUserPreference_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser(CognitoId);
            var userPreferenceData = GetDefaultUserPreferenceData();

            _userRepositoryMock.Setup(rep => rep.GetUserByCognitoId(CognitoId))
                .Returns(Task.FromResult(user));

            var userPreferencesBl = new UserPreferencesBL(_userRepositoryMock.Object, _mapper, _configurationMock.Object);

            // Act && Assert
            Assert.DoesNotThrow(() => { userPreferencesBl.SaveUserPreferences(userPreferenceData, user).Wait(); });
        }

        private UserPreferenceData GetDefaultUserPreferenceData()
        {
            return new UserPreferenceData
            {
                Currency = "GBP",
                DateFormat = "dd/MM/yyyy",
                IsMetric = false,
                Language = "en-GB"
            };
        }

        [TearDown]
        public void UserPreferencesBlTests_TearDown()
        {
            _userRepositoryMock = null;
            _mapper = null;
        }
    }
}
