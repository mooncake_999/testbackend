﻿using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Configuration;
using Moq;
using MyEnergi.AppServer.Model.Weather;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyEnergi.Business.Tests
{
    [TestFixture]
    public class WeatherBlTests
    {
        private Mock<IConfiguration> _configurationMock;
        private Mock<IUserRepository> _userRepositoryMock;
        private string _cognitoId;

        [SetUp]
        public void WeatherBl_SetUp()
        {
            _configurationMock = new Mock<IConfiguration>();
            _userRepositoryMock = new Mock<IUserRepository>();

            _cognitoId = MockConstants.CognitoIdFromToken;
            var user = UserMockFactory.GetNewFullUser();

            _userRepositoryMock
                .Setup(rep => rep.GetFirstOrDefaultBy(
                    It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>>()))
                .Returns(Task.FromResult(user));

            var configuration = GetConfigurationSetup();
            SetupConfigurationMock(configuration);
        }

        private void SetupConfigurationMock(IConfiguration configuration)
        {
            var apiKey = configuration["WeatherbitAPI:ApiKey"];
            var dailyForecastEndpoint = configuration["WeatherbitAPI:DailyForecastEndpoint"];
            var hourlyForecastEndpoint = configuration["WeatherbitAPI:HourlyForecastEndpoint"];

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "WeatherbitAPI:ApiKey")])
                .Returns(apiKey);

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "WeatherbitAPI:DailyForecastEndpoint")])
                .Returns(dailyForecastEndpoint);

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "WeatherbitAPI:HourlyForecastEndpoint")])
                .Returns(hourlyForecastEndpoint);
        }

        private static IConfigurationRoot GetConfigurationSetup()
        {
            var baseDirectory = AppContext.BaseDirectory.Substring(0,
                AppContext.BaseDirectory.IndexOf("MyEnergi", StringComparison.Ordinal));

            var directoryResult = baseDirectory + "MyEnergi\\appsettings.Development.json";

            var builder = new ConfigurationBuilder()
                .AddJsonFile(directoryResult, optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            var configuration = builder.Build();
            return configuration;
        }

        [Test]
        public void WeatherBl_GetForecastData_HappyFlow()
        {
            // Arrange
            var weatherBl = new WeatherBL(_configurationMock.Object, _userRepositoryMock.Object);

            // Act
            var forecast = weatherBl.GetForecastData(_cognitoId).Result;

            var firstForecast = forecast.First();
            var dailyForecastData = (DailyWeatherForecastData)firstForecast.GetType().GetProperty("dailyForecastData")
                ?.GetValue(firstForecast);

            var hourlyForecastData = (List<HourlyWeatherForecastData>)firstForecast.GetType()
                .GetProperty("hourlyForecastData")?.GetValue(firstForecast);

            // Assert
            Assert.AreEqual(true, dailyForecastData is DailyWeatherForecastData);

            var firstHourForecastData = hourlyForecastData.First();
            Assert.AreEqual(true, firstHourForecastData is HourlyWeatherForecastData);
            Assert.AreEqual(24, hourlyForecastData.Count);
        }

        [Test]
        public void WeatherBl_GetForecastData_Throws_User_NotFoundException()
        {
            // Arrange
            _userRepositoryMock = new Mock<IUserRepository>();
            var weatherBl = new WeatherBL(_configurationMock.Object, _userRepositoryMock.Object);

            // Act & Assert
            var exception = Assert.ThrowsAsync<NotFoundException>(() => weatherBl.GetForecastData(_cognitoId));

            Assert.AreEqual("User not found", exception.Message);
        }

        [Test]
        public void WeatherBl_GetForecastData_Throws_Address_NotFoundException()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            user.Addresses = new List<Address>();

            _userRepositoryMock
                .Setup(rep => rep.GetFirstOrDefaultBy(
                    It.IsAny<Expression<Func<User, bool>>>(),
                    It.IsAny<Func<IQueryable<User>, IIncludableQueryable<User, object>>>()))
                .Returns(Task.FromResult(user));

            var weatherBl = new WeatherBL(_configurationMock.Object, _userRepositoryMock.Object);

            // Act & Assert
            var exception = Assert.ThrowsAsync<NotFoundException>(() => weatherBl.GetForecastData(_cognitoId));

            Assert.AreEqual("Addresses not found", exception.Message);
        }

        [Test]
        public void WeatherBl_GetDailyForecast_HappyFlow()
        {
            // Arrange
            var weatherBl = new WeatherBL(_configurationMock.Object, _userRepositoryMock.Object);
            var userAddress = UserMockFactory.GetNewFullUser().Addresses.First();

            var postalCode = userAddress.PostalCode;
            var countryCode = userAddress.CountryCode;

            // Act
            var dailyForecast = weatherBl.GetDailyForecast(postalCode, countryCode);
            var firstDayForecast = dailyForecast.First();

            // Assert
            Assert.AreEqual(5, dailyForecast.Count);
            Assert.AreEqual(true, firstDayForecast is DailyWeatherForecastData);
        }

        [Test]
        public void WeatherBl_GetHourlyForecast_HappyFlow()
        {
            // Arrange
            var weatherBl = new WeatherBL(_configurationMock.Object, _userRepositoryMock.Object);
            var userAddress = UserMockFactory.GetNewFullUser().Addresses.First();

            var postalCode = userAddress.PostalCode;
            var countryCode = userAddress.CountryCode;

            // Act
            var hourlyForecast = weatherBl.GetHourlyForecast(postalCode, countryCode);
            var firstHourForecast = hourlyForecast.First();

            // Assert
            Assert.AreEqual(121, hourlyForecast.Count);
            Assert.AreEqual(true, firstHourForecast is HourlyWeatherForecastData);
        }

        [Test]
        public void WeatherBl_GetForecastResultData_HappyFlow()
        {
            // Arrange
            var getDailyForecast = WeatherForecastMockFactory.GetDailyWeatherForecastData();
            var hourlyForecastList = WeatherForecastMockFactory.GetHourlyWeatherForecastForOneDay();

            var weatherBl = new WeatherBL(_configurationMock.Object, _userRepositoryMock.Object);

            // Act
            var forecast = weatherBl.GetForecastResultData(getDailyForecast, hourlyForecastList);

            var dailyForecastData = (DailyWeatherForecastData)forecast.GetType().GetProperty("dailyForecastData")
                ?.GetValue(forecast);

            var hourlyForecastData = (List<HourlyWeatherForecastData>)forecast.GetType()
                .GetProperty("hourlyForecastData")?.GetValue(forecast);

            // Assert
            Assert.AreEqual(true, dailyForecastData is DailyWeatherForecastData);

            var firstHourForecastData = hourlyForecastData.First();
            Assert.AreEqual(true, firstHourForecastData is HourlyWeatherForecastData);
            Assert.AreEqual(24, hourlyForecastData.Count);
        }

        [TearDown]
        public void WeatherBl_TearDown()
        {
            _configurationMock = null;
            _userRepositoryMock = null;
            _cognitoId = null;
        }
    }
}
