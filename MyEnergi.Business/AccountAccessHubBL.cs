﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Helpers;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class AccountAccessHubBL : IAccountAccessHubBL
    {
        private readonly IUserRepository _userRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IHubRepository _hubRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IInvitationRepository _invitationRepository;
        private readonly IUserFeaturePermissionRepository _userFeaturePermissionRepository;
        private readonly IUserFeaturePermissionHistoryRepository _userFeaturePermissionHistoryRepository;
        private readonly IAccountAccessShareBL _invitationAccountAccessBl;
        private readonly IHubBL _hubBl;
        private readonly IApiCallerHelper _apiCallerHelper;
        private readonly IMapper _mapper;

        public AccountAccessHubBL(IUserRepository userRepository, 
            IAddressRepository addressRepository,
            IHubRepository hubRepository,
            IDeviceRepository deviceRepository,
            IInvitationRepository invitationRepository,
            IUserFeaturePermissionRepository userFeaturePermissionRepository,
            IUserFeaturePermissionHistoryRepository userFeaturePermissionHistoryRepository,
            IAccountAccessShareBL invitationAccountAccessBl,
            IHubBL hubBl,
            IApiCallerHelper apiCallerHelper,
            IMapper mapper)
        {
            _userRepository = userRepository;
            _addressRepository = addressRepository;
            _hubRepository = hubRepository;
            _deviceRepository = deviceRepository;
            _invitationRepository = invitationRepository;
            _userFeaturePermissionRepository = userFeaturePermissionRepository;
            _userFeaturePermissionHistoryRepository = userFeaturePermissionHistoryRepository;
            _invitationAccountAccessBl = invitationAccountAccessBl;
            _hubBl = hubBl;
            _apiCallerHelper = apiCallerHelper;
            _mapper = mapper;
        }

        public async Task<ICollection<object>> GetHubs(string cognitoId)
        {
            var userHubs = new List<object>();

            var ownerHubs = await GetOwnerUserHubsData(cognitoId);
            userHubs.AddRange(ownerHubs);

            var guestHubs = await GetGuestUserHubsData(cognitoId);
            userHubs.AddRange(guestHubs);

            return userHubs;
        }

        public async Task TransferHubOwnership(string hubSerialNo, string email, string ownerCognitoId)
        {
            var hub = await _hubRepository.GetHubBySerialNoAndUserId(hubSerialNo, ownerCognitoId);
            var newOwner = await _userRepository.GetUserByEmail(email);
            var currentOwner = await _userRepository.GetUserByCognitoId(ownerCognitoId);

            if (hub == null || newOwner == null)
            {
                throw new BadRequestException("The hub or the new owner does not exists!");
            }

            CreateFakeAddress(newOwner);

            await DeleteInvitationIfTheNewOwnerWasAGuest(hub, currentOwner, newOwner);

            hub.Address = newOwner.Addresses.First();
            hub.LastUpdate = DateTime.UtcNow;
            await _hubRepository.AddOrUpdate(hub);
            _hubRepository.CommitChanges();

            await UpdateUserFeaturePermissions(currentOwner, newOwner, hub.SerialNo);
            await UpdateHistoryUserFeaturePermissions(currentOwner, newOwner, hub.SerialNo);

            await _invitationAccountAccessBl.ExecuteOperationsToMakeAnInvitationWithGivenStatus(hub, currentOwner, currentOwner.Email,
                newOwner.CognitoId, Enumerators.InvitationStatus.Accepted);
        }

        public async Task<IEnumerable<object>> MigrateHubs(IEnumerable<MigrateHubData> hubs, string requestUserCognitoId)
        {
            var hubsResponse = new List<object>();
            foreach (var migrationHubData in hubs)
            {
                var requestUser = await _userRepository.GetUserByCognitoId(requestUserCognitoId);
                ValidateThatUserExists(requestUser);
                CreateFakeAddress(requestUser);

                var requestUserMainAddress = requestUser.Addresses.First();
                var hubFromDb = await _hubRepository.GetHubBySerialNo(migrationHubData.SerialNo);

                // Scenario 6: Hub has no owner 
                if (hubFromDb == null)
                {
                    var hubData = GetHubData(migrationHubData, requestUserMainAddress);

                    await _hubBl.RegisterNewHub(hubData, requestUserCognitoId);

                    hubFromDb = await _hubRepository.GetHubBySerialNo(migrationHubData.SerialNo);
                    ValidateNewHubRegistration(hubFromDb);
                }

                var decryptedPassword = KMSCipher.Decrypt(hubFromDb.AppPassword).Result;
                if (migrationHubData.HubPassword.Equals(decryptedPassword))
                {
                    // Scenario 1: Hub is already registered as owner to the request user.
                    if (hubFromDb.Address.Guid.Equals(requestUserMainAddress.Guid))
                    {
                        var currentOwner = GetMigrationHubOwnerResponse(migrationHubData);
                        hubsResponse.Add(currentOwner);
                    }
                    else
                    {
                        // Scenarios 2,3,5
                        var guestUserHub =
                            GetTheHubResponseWhenUserIsNotTheOwner(hubFromDb, requestUser, migrationHubData);
                        hubsResponse.Add(guestUserHub);
                    }
                }
                else
                {
                    // Scenario 4: Hub password is invalid
                    var accessDeniedHubResponse = GetAccessDeniedHubResponse(migrationHubData);
                    hubsResponse.Add(accessDeniedHubResponse);
                }
            }

            return hubsResponse;
        }

        public async Task<ResponseModel> RegisterNewHub(string hubSerialNo, string hubRegistrationCode, string userCognitoId)
        {
            var hubData = new HubData
            {
                SerialNo = hubSerialNo,
                RegistrationCode = hubRegistrationCode
            };

            await RegisterNewHub(hubData);

            var randomPsw = KMSCipher.GeneratePassword();
                
            
            hubData.Password = hubData.ConfirmPassword = randomPsw;

            var addresses = (await _addressRepository.GetAddresses(userCognitoId)).ToList();
            CreateHubAddressData(addresses, hubData);

            await RegisterExistingHub(userCognitoId, hubData);

            var responseModel = new ResponseModel(new
            {
                hubSerialNo,
                hubData.Password,
                relation = Enumerators.UserRole.Guest.ToString()
            });

            return responseModel;
        }

        private void CreateHubAddressData(IReadOnlyCollection<Address> addresses, HubData hubData)
        {
            if (addresses.Any())
            {
                var mainAddress = addresses.First();
                hubData.Address = _mapper.Map<AddressData>(mainAddress);
            }
            else
            {
                hubData.Address = new AddressData();
            }
        }

        private async Task RegisterExistingHub(string userCognitoId, HubData hubData)
        {
            var registerResponse = await _hubBl.RegisterNewHub(hubData, userCognitoId);

            if (!registerResponse.Status)
            {
                throw new Exception($"An error occurred while registering the hub. Password = {hubData.Password}");
            }
        }

        private async Task RegisterNewHub(HubData hubData)
        {
            var result = await _apiCallerHelper.GetHubRegisterResult(hubData);

            if (result.HttpStatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedAccessException("Invalid credentials");
            }
        }

        private void CreateFakeAddress(User user)
        {
            if (!user.Addresses.Any())
            {
                user.Addresses = new List<Address>
                {
                    new Address
                    {
                        User = user,
                        LastUpdate = DateTime.UtcNow
                    }
                };
            }
        }

        private object GetAccessDeniedHubResponse(MigrateHubData migrationHubData)
        {
            return new
            {
                hubId = migrationHubData.SerialNo,
                relation = Enumerators.UserRole.AccessDenied.ToString()
            };
        }

        private void ValidateNewHubRegistration(Hub hubFromDb)
        {
            if (hubFromDb == null)
            {
                throw new Exception("New hub registration failed");
            }
        }

        private async Task<object> GetTheHubResponseWhenUserIsNotTheOwner(Hub hubFromDb, User requestUser,
            MigrateHubData migrationHubData)
        {
            var hubsResponse = new List<object>();
            var hubAddress = (await _hubRepository.GetHubBySerialNo(hubFromDb.SerialNo)).Address;
            var hubOwner = hubAddress.User;

            var invitations = (await _invitationRepository.GetOwnerUserSendInvitations(hubOwner.CognitoId))
                .ToList();

            // Scenario 5: User is not invited but the password is correct -> user is given guest access
            if (!invitations.Any(i => string.Equals(i.InvitationDetails.Email, requestUser.Email,
                StringComparison.InvariantCultureIgnoreCase)))
            {
                await _invitationAccountAccessBl.ExecuteOperationsToMakeAnInvitationWithGivenStatus(hubFromDb, requestUser,
                    requestUser.Email, hubOwner.CognitoId, Enumerators.InvitationStatus.Accepted);

                var guestHubResponse = GetMigrationHubResponseWithGivenRelation(migrationHubData,
                    hubFromDb, hubOwner, Enumerators.UserRole.Guest.ToString());
                return guestHubResponse;
            }

            foreach (var invitationStatus in from invitation in invitations
                                             where string.Equals(invitation.InvitationDetails.Email, requestUser.Email,
                                                 StringComparison.InvariantCultureIgnoreCase)
                                             select invitation.InvitationStatus.Status.ToLower())
            {
                switch (invitationStatus)
                {
                    // Scenario 3: User accepted the invitation

                    case "accepted":
                        {
                            var guestHubResponse = GetMigrationHubResponseWithGivenRelation(migrationHubData, hubFromDb,
                                hubOwner, Enumerators.UserRole.Guest.ToString());
                            return guestHubResponse;
                        }
                    // Scenario 2: User is invited as guest
                    case "pending":
                        {
                            var pendingHubInvitationResponse = GetMigrationHubResponseWithGivenRelation(
                                migrationHubData, hubFromDb, hubOwner, Enumerators.UserRole.Invited.ToString());
                            return pendingHubInvitationResponse;
                        }
                }
            }

            return hubsResponse;
        }

        private object GetMigrationHubResponseWithGivenRelation(MigrateHubData migrationHubData, Hub hubFromDb,
            User hubOwner, string relation)
        {
            return new
            {
                hubId = migrationHubData.SerialNo,
                hubName = hubFromDb.NickName,
                relation,
                owner = GetUserNameOrEmail(hubOwner)
            };
        }

        private object GetMigrationHubOwnerResponse(MigrateHubData migrationHubData)
        {
            return new
            {
                hubId = migrationHubData.SerialNo,
                hubPassword = migrationHubData.HubPassword,
                relation = Enumerators.UserRole.Owner.ToString()
            };
        }

        private HubData GetHubData(MigrateHubData migrationHubData, Address requestUserMainAddress)
        {
            return new HubData
            {
                SerialNo = migrationHubData.SerialNo,
                Password = migrationHubData.HubPassword,
                Address = _mapper.Map<AddressData>(requestUserMainAddress)
            };
        }

        private async Task DeleteInvitationIfTheNewOwnerWasAGuest(Hub hub, User currentOwner, User newOwner)
        {
            var device = (await _deviceRepository.GetDevicesByHubId(hub.Guid)).FirstOrDefault();
            if (device == null) return;

            var guestInvitations = (await _invitationRepository.GetOwnerUserSendInvitations(currentOwner.CognitoId))
                .Where(i => i.GuestUser != null && i.GuestUser.CognitoId == newOwner.CognitoId)
                .ToList();

            if (!guestInvitations.Any()) return;

            Invitation invitationToDelete = null;
            var ufpToDelete = new List<UserFeaturePermission>();
            foreach (var guestInvitation in guestInvitations
                .Where(guestInvitation => guestInvitation.UserFeaturePermissions
                    .Any(x => x.Device.Guid == device.Guid)))
            {
                invitationToDelete = guestInvitation;
                ufpToDelete.AddRange(guestInvitation.UserFeaturePermissions);
            }

            if (invitationToDelete == null) return;

            var historyUfpToDelete = (await _userFeaturePermissionHistoryRepository
                .GetBy(ufp => ufp.Invitation.Id == invitationToDelete.Id)).ToList();

            if (historyUfpToDelete.Any())
            {
                await _userFeaturePermissionHistoryRepository.DeleteMultiple(historyUfpToDelete);
            }
            await _userFeaturePermissionRepository.DeleteMultiple(ufpToDelete);
            await _invitationRepository.Delete(invitationToDelete);
        }

        private async Task UpdateUserFeaturePermissions(User currentOwner, User newOwner, string hubSerialNo)
        {
            var currentOwnerUserFeaturePermissions =
                (await _userFeaturePermissionRepository
                    .GetBy(ufp => ufp.OwnerUser.Id == currentOwner.Id &&
                                  ufp.Device.Hub.SerialNo.Equals(hubSerialNo))).ToList();

            if (currentOwnerUserFeaturePermissions.Any())
            {
                foreach (var currentOwnerUserFeaturePermission in currentOwnerUserFeaturePermissions)
                {
                    currentOwnerUserFeaturePermission.OwnerUser = newOwner;
                    currentOwnerUserFeaturePermission.LastUpdate = DateTime.UtcNow;
                }

                await _userFeaturePermissionRepository.AddOrUpdate(currentOwnerUserFeaturePermissions);
            }

            _userFeaturePermissionRepository.CommitChanges();
        }

        private async Task UpdateHistoryUserFeaturePermissions(User currentOwner, User newOwner, string hubSerialNo)
        {
            var currentOwnerHistoryUfps = (await _userFeaturePermissionHistoryRepository
                .GetBy(ufp => ufp.OwnerUser.Id == currentOwner.Id &&
                              ufp.Device.Hub.SerialNo.Equals(hubSerialNo))).ToList();

            if (currentOwnerHistoryUfps.Any())
            {
                foreach (var currentOwnerHistoryUfp in currentOwnerHistoryUfps)
                {
                    currentOwnerHistoryUfp.OwnerUser = newOwner;
                }

                await _userFeaturePermissionHistoryRepository.UpdateMultiple(currentOwnerHistoryUfps);
            }

            _userFeaturePermissionHistoryRepository.CommitChanges();
        }

        private async Task<IEnumerable<HubData>> GetHubsByUserId(string userId)
        {
            var hub = await _hubRepository.GetHubByUserId(userId);
            return _mapper.Map<List<HubData>>(hub);
        }

        private async Task<IEnumerable<object>> GetOwnerUserHubsData(string cognitoId)
        {
            var hubs = await GetHubsByUserId(cognitoId);
            var ownerUser = await _userRepository.GetUserByCognitoId(cognitoId);
            ValidateThatUserExists(ownerUser);

            if (!ownerUser.Addresses.Any())
            {
                return new List<object>();
            }

            var mainAddress = _mapper.Map<IList<AddressData>>(ownerUser.Addresses).First();
            var ownerUserHubsData = new List<object>();

            foreach (var hubData in hubs)
            {
                if (hubData.Address.Id.Equals(mainAddress.Id)) // relation is Owner
                {
                    var invitationShares = new List<object>();
                    var allOwnerInvitations = (await _invitationRepository.GetOwnerUserSendInvitations(cognitoId))
                        .Where(i => !i.InvitationStatus.Status.Equals(Enumerators.InvitationStatus.Deleted.ToString())).ToList();

                    foreach (var invitation in allOwnerInvitations)
                    {
                        if (invitation.UserFeaturePermissions.Any(ufp => ufp.Device.Hub.Guid == hubData.Id))
                        {
                            var invitationShare = GetInvitationShare(invitation);
                            invitationShares.Add(invitationShare);
                        }
                    }

                    var ownerHub = GetOwnerUserHubObject(ownerUser, hubData, invitationShares);
                    ownerUserHubsData.Add(ownerHub);
                }
            }

            return ownerUserHubsData;
        }

        private async Task<IEnumerable<object>> GetGuestUserHubsData(string cognitoId)
        {
            var guestUserHubsData = new List<object>();
            var guestUserInvitations = (await _invitationRepository.GetGuestUserInvitations(cognitoId)).ToList();

            var invitationsWithSpecificStatuses = guestUserInvitations.Where(i =>
                i.InvitationStatus.Status.ToLower().Equals(Enumerators.InvitationStatus.Accepted.ToString().ToLower()) ||
                i.InvitationStatus.Status.ToLower().Equals(Enumerators.InvitationStatus.Pending.ToString().ToLower()));

            foreach (var guestUserInvitation in invitationsWithSpecificStatuses)
            {
                var hub = guestUserInvitation.UserFeaturePermissions.First().Device.Hub;
                var hubOwner = guestUserInvitation.UserFeaturePermissions.First().OwnerUser;
                ValidateThatUserExists(hubOwner);

                var invitationStatus = guestUserInvitation.InvitationStatus.Status.ToLower();
                switch (invitationStatus)
                {
                    case "pending": // relation is Invited
                        {
                            var invitedUserHub = GetInvitedUserHub(hub, hubOwner);
                            guestUserHubsData.Add(invitedUserHub);
                            break;
                        }
                    case "accepted": // relation is Guest
                        {
                            var devices = guestUserInvitation.UserFeaturePermissions.Select(ufp => ufp.Device)
                                .Distinct();
                            var sharedDevices = new List<object>();

                            foreach (var device in devices)
                            {
                                var validityData = GetValidityData(guestUserInvitation);
                                var sharedDevice = GetSharedDevice(device, validityData);
                                sharedDevices.Add(sharedDevice);
                            }

                            var featuresData =
                                guestUserInvitation.UserFeaturePermissions.Select(ufp => ufp.AppFeature.Feature).Distinct();
                            var guestUserHub = GetGuestUserHub(hub, guestUserInvitation, hubOwner, sharedDevices,
                                featuresData);
                            guestUserHubsData.Add(guestUserHub);
                            break;
                        }
                }
            }

            return guestUserHubsData;
        }

        private object GetInvitationShare(Invitation invitation)
        {
            if (invitation.GuestUser?.UserDetails == null)
            {
                return new
                {
                    email = invitation.InvitationDetails.Email,
                    status = invitation.InvitationStatus.Status
                };
            }

            return new
            {
                email = invitation.InvitationDetails.Email,
                name = invitation.GuestUser.UserDetails.FirstName + " " + invitation.GuestUser.UserDetails.LastName,
                status = invitation.InvitationStatus.Status
            };
        }

        private object GetOwnerUserHubObject(User ownerUser, HubData hubData, List<object> invitationShares)
        {
            return new
            {
                hubName = hubData.NickName,
                owner = GetUserNameOrEmail(ownerUser),
                serialNo = hubData.SerialNo,
                hubPassword = KMSCipher.Decrypt(hubData.Password).Result,
                relation = Enumerators.UserRole.Owner.ToString(),
                shares = invitationShares,
            };
        }

        private object GetGuestUserHub(Hub hub, Invitation guestUserInvitation, User user, List<object> sharedDevices, IEnumerable<string> featuresData)
        {
            return new
            {
                serialNo = hub.SerialNo,
                hubPassword = KMSCipher.Decrypt(hub.AppPassword).Result,
                relation = Enumerators.UserRole.Guest.ToString(),
                hubName = hub.NickName,
                owner = GetUserNameOrEmail(user),
                devices = sharedDevices,
                features = featuresData
            };
        }

        private object GetSharedDevice(Device device, object validityData)
        {
            return new
            {
                deviceSerialNo = device.SerialNumber,
                validity = validityData,
            };
        }

        private object GetValidityData(Invitation guestUserInvitation)
        {
            return new
            {
                from = guestUserInvitation.StartDate != null ? Helper.ConvertToTimestamp((DateTime)guestUserInvitation.StartDate) : 0,
                to = guestUserInvitation.EndDate != null ? Helper.ConvertToTimestamp((DateTime)guestUserInvitation.EndDate) : 0,
                recurrence = guestUserInvitation.CronExpressionRecurrence
            };
        }

        private object GetInvitedUserHub(Hub hub, User user)
        {
            return new
            {
                serialNo = hub.SerialNo,
                relation = Enumerators.UserRole.Invited.ToString(),
                hubName = hub.NickName,
                owner = GetUserNameOrEmail(user)
            };
        }

        private string GetUserNameOrEmail(User hubOwner)
        {
            if (hubOwner.UserDetails == null)
            {
                return hubOwner.Email;
            }

            return hubOwner.UserDetails.FirstName != null &&
                   hubOwner.UserDetails.LastName != null
                ? hubOwner.UserDetails.FirstName + " " + hubOwner.UserDetails.LastName
                : hubOwner.Email;
        }

        private void ValidateThatUserExists(User user)
        {
            if (user == null)
            {
                throw new BadRequestException("User does not exists");
            }
        }
    }
}
