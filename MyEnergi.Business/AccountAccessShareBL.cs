﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using MyEnergi.Common.CustomMappers;

namespace MyEnergi.Business
{
    public class AccountAccessShareBL : IAccountAccessShareBL
    {
        private readonly IInvitationRepository _invitationRepository;
        private readonly IInvitationStatusRepository _invitationStatusRepository;
        private readonly IUserFeaturePermissionRepository _userFeaturePermissionRepository;
        private readonly IUserFeaturePermissionHistoryRepository _userFeaturePermissionHistoryRepository;
        private readonly IAppFeatureRepository _appFeatureRepository;
        private readonly IHubRepository _hubRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IUserRepository _userRepository;

        public AccountAccessShareBL(IInvitationRepository invitationRepository,
            IInvitationStatusRepository invitationStatusRepository,
            IUserFeaturePermissionRepository userFeaturePermissionRepository,
            IUserFeaturePermissionHistoryRepository userFeaturePermissionHistoryRepository,
            IAppFeatureRepository appFeatureRepository,
            IHubRepository hubRepository,
            IDeviceRepository deviceRepository,
            IUserRepository userRepository)
        {
            _invitationRepository = invitationRepository;
            _invitationStatusRepository = invitationStatusRepository;
            _userFeaturePermissionRepository = userFeaturePermissionRepository;
            _userFeaturePermissionHistoryRepository = userFeaturePermissionHistoryRepository;
            _appFeatureRepository = appFeatureRepository;
            _hubRepository = hubRepository;
            _deviceRepository = deviceRepository;
            _userRepository = userRepository;
        }

        public async Task<Enumerators.UserRole?> GetUserRoleRelativeToHub(string cognitoId, string serialNo)
        {
            var hub = await _hubRepository.GetHubBySerialNoAndUserId(serialNo, cognitoId);
            if (hub != null) return Enumerators.UserRole.Owner;

            if ((await _hubRepository.GetHubsOfGuestUser(cognitoId)).Any(x => x.SerialNo == serialNo))
                return Enumerators.UserRole.Guest;
            return null;
        }

        public async Task<bool> CheckIfInvitationExists(string ownerCognitoId, string guestEmail, string serialNo)
        {
            var invitation = await _invitationRepository.GetOwnerInvitationForHub(ownerCognitoId, serialNo, guestEmail);
            return CheckThatInvitationIsNotNullAndNotDeleted(invitation);
        }

        public bool CheckThatInvitationIsNotNullAndNotDeleted(Invitation invitation)
        {
            return invitation != null && !string.Equals(invitation.InvitationStatus.Status,
                Enumerators.InvitationStatus.Deleted.ToString(), StringComparison.InvariantCultureIgnoreCase);
        }

        public async Task ShareHub(string ownerCognitoId, string serialNo, string guestEmail)
        {
            await ValidateThatTheRequestUserIsOwner(ownerCognitoId, serialNo);

            var hub = await _hubRepository.GetHubBySerialNo(serialNo);
            var guestUser = await _userRepository.GetUserByEmail(guestEmail);

            await ExecuteOperationsToMakeAnInvitationWithGivenStatus(hub, guestUser, guestEmail, ownerCognitoId,
                Enumerators.InvitationStatus.Pending);
        }

        private InvitationDetails GetInvitationDetails(string guestEmail)
        {
            return new InvitationDetails
            {
                Email = guestEmail,
                AppAccess = true,
                AllDevicesAccess = true,
                AdministratorAccess = false
            };
        }

        public async Task<Guid> CreateInvitation(InvitationCreationData invitationCreationData)
        {
            var invitationStatusFromDb = await _invitationStatusRepository.GetByStatus(invitationCreationData.InvitationStatus.ToString());
            var ownerUser = await _userRepository.GetUserByCognitoId(invitationCreationData.OwnerCognitoId);
            var newInvitation = GetInvitation(invitationCreationData, invitationStatusFromDb, ownerUser);

            await _invitationRepository.AddOrUpdate(newInvitation);
            _invitationRepository.CommitChanges();

            var ufps = new List<UserFeaturePermission>();
            var devices = invitationCreationData.Devices.ToList();
            devices.ForEach(device =>
            {
                device.LastUpdate = DateTime.UtcNow;

                var features = invitationCreationData.Features.ToList();
                features.ForEach(feature =>
                {
                    var ufp = GetUserFeaturePermission(device, newInvitation, ownerUser, feature);
                    ufps.Add(ufp);
                });
            });
            await _userFeaturePermissionRepository.AddOrUpdate(ufps);
            _userFeaturePermissionRepository.CommitChanges();

            return newInvitation.Guid;
        }

        private UserFeaturePermission GetUserFeaturePermission(Device device, Invitation invitationEntity, User owner, AppFeature feature)
        {
            return new UserFeaturePermission
            {
                Device = device,
                Invitation = invitationEntity,
                OwnerUser = owner,
                AppFeature = feature,
                LastUpdate = DateTime.UtcNow
            };
        }

        private Invitation GetInvitation(InvitationCreationData invitationCreationData, InvitationStatus invitationStatusFromDb, User ownerUser)
        {
            return new Invitation
            {
                InvitationDetails = invitationCreationData.InvitationDetails,
                OwnerUser = ownerUser,
                GuestUser = invitationCreationData.GuestUser,
                CreationDate = DateTime.UtcNow,
                InvitationStatus = invitationStatusFromDb,
                LastUpdate = DateTime.UtcNow,
                StartDate = invitationCreationData.StartDate,
                EndDate = invitationCreationData.EndDate,
                ExpirationDate = invitationCreationData.ExpirationDate
            };
        }

        public async Task RejectInvitation(string cognitoId, string serialNo)
        {
            var hubFromDb = await _hubRepository.GetHubBySerialNo(serialNo);
            if (hubFromDb == null)
            {
                throw new BadRequestException($"Invalid serialNo {serialNo}");
            }

            var invitation = await _invitationRepository.GetGuestInvitationForHub(cognitoId, hubFromDb.SerialNo);
            if (invitation == null)
            {
                throw new BadRequestException("Invitation does not exists!");
            }

            await ExecuteOperationsToRejectTheInvitation(invitation);
        }

        public async Task ExecuteOperationsToRejectTheInvitation(Invitation invitation)
        {
            if (string.Equals(invitation.InvitationStatus.Status, Enumerators.InvitationStatus.Pending.ToString(),
                StringComparison.CurrentCultureIgnoreCase))
            {
                var invitationStatus =
                    await _invitationStatusRepository.GetByStatus(Enumerators.InvitationStatus.Rejected.ToString());
                invitation.InvitationStatus = invitationStatus;
                invitation.LastUpdate = DateTime.UtcNow;
                await _invitationRepository.AddOrUpdate(invitation);

                var userFeaturePermissionToMove =
                    (await _userFeaturePermissionRepository.GetByInvitationGuid(invitation.Guid)).ToList();

                if (userFeaturePermissionToMove.Any())
                {
                    var historyUserFeaturePermissions = userFeaturePermissionToMove.MapToUserFeaturePermissionHistory();

                    await _userFeaturePermissionHistoryRepository.AddOrUpdate(historyUserFeaturePermissions);
                    await _userFeaturePermissionRepository.DeleteMultiple(userFeaturePermissionToMove);
                }

                _invitationRepository.CommitChanges();
            }
            else
            {
                throw new BadRequestException($"Invitation has the wrong status: {invitation.InvitationStatus.Status}");
            }
        }

        public async Task ExecuteOperationsToMakeAnInvitationWithGivenStatus(Hub hub, User guestUser, string guestEmail,
            string ownerCognitoId, Enumerators.InvitationStatus invitationStatus)
        {
            if (await CheckIfInvitationExists(ownerCognitoId, guestEmail, hub.SerialNo))
            {
                throw new BadRequestException("Invitation already exists.");
            }

            var devices = (await _deviceRepository.GetDevicesByHubId(hub.Guid)).ToList();
            var appFeatures = (await _appFeatureRepository.GetAll()).ToList();

            var invitationDetails = GetInvitationDetails(guestEmail);
            var invitationCreationData = GetInvitationCreationData(ownerCognitoId, invitationDetails, devices,
                appFeatures, invitationStatus, guestUser, hub.SerialNo);
            await CreateInvitation(invitationCreationData);
        }

        public InvitationCreationData GetInvitationCreationData(string ownerCognitoId, InvitationDetails invitationDetails,
            IList<Device> devices, IList<AppFeature> appFeatures, Enumerators.InvitationStatus invitationStatus,
            User guestUser = null, string hubSerialNo = null, DateTime? startDate = null, DateTime? endDate = null,
            DateTime? expirationDate = null)
        {
            return new InvitationCreationData
            {
                OwnerCognitoId = ownerCognitoId,
                InvitationDetails = invitationDetails,
                GuestUser = guestUser,
                SerialNo = hubSerialNo,
                Devices = devices,
                Features = appFeatures,
                InvitationStatus = invitationStatus,
                StartDate = startDate,
                EndDate = endDate,
                ExpirationDate = expirationDate
            };
        }

        public async Task<object> AcceptInvitation(string cognitoId, string serialNo)
        {
            var guestUserPendingInvitations = (await _invitationRepository.GetGuestUserInvitations(cognitoId))
                .Where(i => i.InvitationStatus.Status.Equals(Enumerators.InvitationStatus.Pending.ToString(),
                    StringComparison.InvariantCultureIgnoreCase)).ToList();

            var invitationForHubWithGivenSerialNo = guestUserPendingInvitations.Where(i =>
                i.UserFeaturePermissions.First().Device.Hub.SerialNo.Equals(serialNo)).ToList();

            if (!invitationForHubWithGivenSerialNo.Any())
            {
                throw new HttpRequestException("There is no existing invitation.");
            }

            var invitationAccepted =
                await _invitationStatusRepository.GetByStatus(Enumerators.InvitationStatus.Accepted.ToString());
            var guestHubData = new object();

            foreach (var invitation in invitationForHubWithGivenSerialNo)
            {
                var hub = await _hubRepository.GetHubByInvitationIdAndSerialNo(invitation.Id, serialNo);
                if (hub == null)
                {
                    continue;
                }

                var invitedUserHub = GetGuestUserHub(hub);
                guestHubData = invitedUserHub;

                await UpdateInvitationStatusToAccepted(invitation, invitationAccepted);
            }

            _invitationRepository.CommitChanges();
            return guestHubData;
        }

        public async Task UpdateInvitationStatusToAccepted(Invitation invitation, InvitationStatus invitationAccepted)
        {
            var invitationStatus = invitation.InvitationStatus.Status;
            if (string.Equals(invitationStatus, Enumerators.InvitationStatus.Pending.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                invitation.InvitationStatus = invitationAccepted;
                invitation.LastUpdate = DateTime.UtcNow;
                await _invitationRepository.AddOrUpdate(invitation);
            }
            else
            {
                throw new BadRequestException($"Invitation has the wrong status: {invitation.InvitationStatus.Status}");
            }
        }

        private object GetGuestUserHub(Hub hub)
        {
            return new
            {
                serialNo = hub.SerialNo,
                hubPassword = KMSCipher.Decrypt(hub.AppPassword).Result,
                relation = Enumerators.UserRole.Guest.ToString()
            };
        }

        public async Task ExecuteOperationsToSuspendThePermissions(Invitation invitation)
        {
            var statusEnded = await _invitationStatusRepository.GetByStatus(Enumerators.InvitationStatus.Ended.ToString());
            invitation.InvitationStatus = statusEnded;
            invitation.LastUpdate = DateTime.UtcNow;
            await _invitationRepository.AddOrUpdate(invitation);

            var userFeaturePermissionToMove =
                (await _userFeaturePermissionRepository.GetByInvitationGuid(invitation.Guid)).ToList();

            var historyUserFeaturePermissions = userFeaturePermissionToMove.MapToUserFeaturePermissionHistory();

            await _userFeaturePermissionHistoryRepository.AddOrUpdate(historyUserFeaturePermissions);

            await _userFeaturePermissionRepository.DeleteMultiple(userFeaturePermissionToMove);
        }

        public async Task RevokeShare(string requestUserCognitoId, string email, string serialNo)
        {
            var isRequestUserOwner =
                await GetUserRoleRelativeToHub(requestUserCognitoId, serialNo) == Enumerators.UserRole.Owner;

            var invitations = isRequestUserOwner
                ? await GetInvitationForOwnerUserRequest(requestUserCognitoId, email)
                : await GetInvitationsForGuestUserRequest(requestUserCognitoId, email);

            var invitationsToBeProcessed = invitations.Where(x =>
                    string.Equals(x.InvitationStatus.Status, Enumerators.InvitationStatus.Accepted.ToString(),
                        StringComparison.CurrentCultureIgnoreCase) ||
                    string.Equals(x.InvitationStatus.Status,
                        Enumerators.InvitationStatus.Pending.ToString(), StringComparison.CurrentCultureIgnoreCase))
                .ToList();

            if (!invitationsToBeProcessed.Any())
            {
                throw new BadRequestException($"There is no invitation to be revoked for {email}.");
            }

            foreach (var invitation in invitationsToBeProcessed)
            {
                var hub = isRequestUserOwner
                    ? await _hubRepository.GetHubByInvitationIdAndOwner(invitation.Id, serialNo, requestUserCognitoId)
                    : await _hubRepository.GetHubByInvitationIdAndSerialNo(invitation.Id, serialNo);

                if (hub == null)
                {
                    continue;
                }

                await ExecuteOperationsToSuspendThePermissions(invitation);
            }

            _invitationRepository.CommitChanges();
        }

        private async Task<IEnumerable<Invitation>> GetInvitationsForGuestUserRequest(string requestUserCognitoId, string email)
        {
            return await _invitationRepository.GetBy(i =>
                    i.InvitationDetails.Email.ToLower().Equals(email.ToLower()) &&
                    i.GuestUser.CognitoId.Equals(requestUserCognitoId),
                include: source => source
                    .Include(inv => inv.InvitationStatus));
        }

        private async Task<IEnumerable<Invitation>> GetInvitationForOwnerUserRequest(string requestUserCognitoId, string email)
        {
            return await _invitationRepository.GetBy(i =>
                    i.InvitationDetails.Email.ToLower().Equals(email.ToLower()) &&
                    i.OwnerUser.CognitoId.Equals(requestUserCognitoId),
                include: source => source
                    .Include(inv => inv.InvitationStatus));
        }

        private async Task ValidateThatTheRequestUserIsOwner(string userCognitoId, string serialNo)
        {
            if (await GetUserRoleRelativeToHub(userCognitoId, serialNo) != Enumerators.UserRole.Owner)
            {
                throw new BadRequestException("User is not Owner of the hub");
            }
        }
    }
}
