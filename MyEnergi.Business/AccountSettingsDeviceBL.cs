﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DeviceStatus = MyEnergi.Common.Enumerators.DeviceStatus;

namespace MyEnergi.Business
{
    public class AccountSettingsDeviceBL : IAccountSettingsDeviceBL
    {
        private readonly IUserBL _userBl;
        private readonly IHubBL _hubBl;
        private readonly IDeviceBL _deviceBl;
        private readonly IApiCallerHelper _apiCallerHelper;
        private readonly IMapper _mapper;

        public AccountSettingsDeviceBL(IUserBL userBl,
            IHubBL hubBl,
            IDeviceBL deviceBl,
            IApiCallerHelper apiCallerHelper,
            IMapper mapper)
        {
            _userBl = userBl;
            _hubBl = hubBl;
            _deviceBl = deviceBl;
            _apiCallerHelper = apiCallerHelper;
            _mapper = mapper;
        }

        public async Task<List<DeviceData>> GetNewDevices(string userId)
        {
            var newDevices = (await _deviceBl.GetDevicesByUserId(userId))
                .Where(x => x.Status == DeviceStatus.New).ToList();

            return newDevices;
        }

        public async Task<List<object>> GetDeviceStatusOfHub(string userId, Guid hubId)
        {
            var hubData = await _hubBl.GetHubById(hubId, userId);

            var hubDevices = await GetDevices(hubData, userId);

            return hubDevices;
        }

        public async Task<IEnumerable<object>> GetPhantomDeviceStatus(string userId)
        {
            var usersDevices = await _deviceBl.GetDevicesByUserId(userId);
            var usersAddresses = (await _userBl.GetUsersAddresses(userId)).ToList();

            var phantomDevices = usersAddresses
                .SelectMany(address => GetPhantomDevices(usersDevices, address)).ToList();

            return phantomDevices;
        }

        public async Task<List<object>> GetAllDeviceStatus(string userId)
        {
            var ownerAddresses = (await _userBl.GetUsersAddresses(userId)).ToList();
            var usersHubs = await _hubBl.GetHubsByUserId(userId);
            var usersDevices = await _deviceBl.GetDevicesByUserId(userId);

            var ownerHubs = usersHubs.Where(h => !h.IsGuestUser).ToList();
            var guestHubs = usersHubs.Where(h => h.IsGuestUser).ToList();

            var ownerDevices = usersDevices.Where(h => !h.IsGuestUser).ToList();
            var guestDevices = usersDevices.Where(h => h.IsGuestUser).ToList();

            var guestAddresses = GetDistinctGuestAddresses(guestHubs);

            var data = new List<object>();
            var ownerData = GetHubsAndDeviceData(userId, ownerAddresses, ownerHubs, ownerDevices);
            var guestData = GetHubsAndDeviceData(userId, guestAddresses, guestHubs, guestDevices);

            data.AddRange(ownerData);
            data.AddRange(guestData);
            
            return data;
        }

        private static IEnumerable<AddressData> GetDistinctGuestAddresses(IEnumerable<HubData> guestHubs)
        {
            var guestAddresses = guestHubs.Select(h => h.Address).ToList();
            var distinctGuestAddresses = guestAddresses
                .GroupBy(a => a.AddressLine1)
                .Select(g => g.First())
                .ToList();

            return distinctGuestAddresses;
        }

        private IEnumerable<object> GetHubsAndDeviceData(string userId, IEnumerable<AddressData> usersAddresses, IReadOnlyCollection<HubData> usersHubs, List<DeviceData> usersDevices)
        {
            var data = new List<object>();
            
            foreach (var addressData in usersAddresses)
            {
                var hubInfo = GetHubs(usersHubs, addressData, userId);
                var phantomDevices = GetPhantomDevices(usersDevices, addressData);

                data.Add(new
                {
                    Address = addressData,
                    Hubs = hubInfo,
                    PhantomDevices = phantomDevices
                });
            }

            return data;
        }

        private async Task<List<object>> GetDevices(HubData hubData, string userId)
        {
            var result =
                await _apiCallerHelper.GetExistingHubStatusResult(hubData);

            if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
            {
                return new List<object>();
            }
            
            var (hubFirmware, devicesCastContent) = (Tuple<string, List<AppServer.Model.DeviceData>>)result.Content;
            hubData.Firmware = hubFirmware;

            var serverDevices = _mapper.Map<List<AppServer.Model.DeviceData>, List<DeviceData>>(devicesCastContent);
            if (!serverDevices.Any()) return new List<object>();

            var dbDevicesIncludingDeleted = await _deviceBl.GetDevicesByUserIdIsDeleted(userId);
            var dbDevices = dbDevicesIncludingDeleted.Where(x => x.Hub != null && x.Hub.Id == hubData.Id).ToList();

            var deviceInfo = new List<object>(); 
            serverDevices.ForEach(d =>
            {
                d.Hub = hubData;
                d.Address = hubData.Address;

                var dbDevice = dbDevices.SingleOrDefault(x => x.SerialNumber == d.SerialNumber);
                if (dbDevice != null && dbDevice.IsDeleted)
                    return;

                if (d.DeviceType == Enumerators.DeviceType.Eddi.ToString().ToLower())
                {
                    SetEddiOutputs(d).Wait();
                    if (dbDevice != null && (dbDevice.HeaterOutput1 != d.HeaterOutput1 ||
                                             dbDevice.HeaterOutput2 != d.HeaterOutput2))
                    {
                        _deviceBl.UpdateOutputs(dbDevice.Id, d.HeaterOutput1, d.HeaterOutput2).Wait();
                    }
                }

                if (dbDevice == null)
                {
                    d.Status = DeviceStatus.New;
                    _deviceBl.AddNewDevice(new List<DeviceData> { d }, userId, false).Wait();

                    dbDevice = _deviceBl.GetDeviceBySerialNo(d.SerialNumber).Result;
                }

                if (dbDevice.Status != DeviceStatus.New)
                {
                    dbDevice.Status = (DateTime.UtcNow - d.LastActiveTime) > new TimeSpan(0, 5, 0)
                        ? DeviceStatus.Offline
                        : DeviceStatus.Active;
                }

                dbDevice.Firmware = d.Firmware;
                _deviceBl.UpdateFirmware(dbDevice.Id, d.Firmware).Wait();

                deviceInfo.Add(new { Device = dbDevice });
            });

            hubData.Status = serverDevices.All(x => x.Status == DeviceStatus.Offline)
                ? DeviceStatus.Offline
                : DeviceStatus.Active;
            // hubData.Address = null;

            return deviceInfo;
        }

        private List<object> GetPhantomDevices(List<DeviceData> usersDevices, AddressData addressData)
        {
            if (!usersDevices.Any()) return new List<object>();

            var phantomDevices = usersDevices.Where(x => x.Hub == null && x.Address.Id == addressData.Id).ToList();

            return phantomDevices.Cast<object>().ToList();
        }

        private List<object> GetHubs(IEnumerable<HubData> userHubs, AddressData addressData, string userId)
        {
            var hubInfo = new List<object>();
            foreach (var hubData in userHubs.Where(h => h.Address.Id == addressData.Id))
            {
                var deviceInfo = GetDevices(hubData, userId).Result;

                hubInfo.Add(new
                {
                    Hub = hubData,
                    Devices = deviceInfo
                });
            }

            return hubInfo;
        }

        private async Task SetEddiOutputs(DeviceData device)
        {
            var kvResult = await _apiCallerHelper.GetKeyValueResult(device.Hub,
                key: device.DeviceType.ToUpper().First() + device.SerialNumber + 1);
            if (kvResult.Content != null)
            {
                device.HeaterOutput1 = kvResult.Content.ToString();
            }

            kvResult = await _apiCallerHelper.GetKeyValueResult(device.Hub,
                key: device.DeviceType.ToUpper().First() + device.SerialNumber + 2);
            if (kvResult.Content != null)
            {
                device.HeaterOutput2 = kvResult.Content.ToString();
            }
        }
    }
}
