﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Common.CustomMappers;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyEnergi.AppServer.Interfaces;
using Device = MyEnergi.Data.Entity.Device;
using Invitation = MyEnergi.Data.Entity.Invitation;
using InvitationStatus = MyEnergi.Common.Enumerators.InvitationStatus;
using User = MyEnergi.Data.Entity.User;

namespace MyEnergi.Business
{
    public class AccountSettingsShareBL : IAccountSettingsShareBL
    {
        private readonly IInvitationRepository _invitationRepository;
        private readonly IUserFeaturePermissionRepository _userFeaturePermissionRepository;
        private readonly IUserFeaturePermissionHistoryRepository _userFeaturePermissionHistoryRepository;
        private readonly IInvitationStatusRepository _invitationStatusRepository;
        private readonly IUserRepository _userRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IAppFeatureRepository _appFeatureRepository;
        private readonly IAccountAccessShareBL _invitationAccountAccessBl;
        private readonly IInvitationEmailSender _invitationEmailSender;
        private readonly IMapper _mapper;

        public AccountSettingsShareBL(IInvitationRepository invitationRepository,
            IUserFeaturePermissionRepository userFeaturePermissionRepository,
            IUserFeaturePermissionHistoryRepository userFeaturePermissionHistoryRepository,
            IInvitationStatusRepository invitationStatusRepository,
            IUserRepository userRepository, IDeviceRepository deviceRepository,
            IAppFeatureRepository appFeatureRepository,
            IAccountAccessShareBL invitationAccountAccessBl, IInvitationEmailSender invitationEmailSender, IMapper mapper)
        {
            _invitationRepository = invitationRepository;
            _userFeaturePermissionRepository = userFeaturePermissionRepository;
            _userFeaturePermissionHistoryRepository = userFeaturePermissionHistoryRepository;
            _invitationStatusRepository = invitationStatusRepository;
            _userRepository = userRepository;
            _deviceRepository = deviceRepository;
            _appFeatureRepository = appFeatureRepository;
            _invitationAccountAccessBl = invitationAccountAccessBl;
            _invitationEmailSender = invitationEmailSender;
            _mapper = mapper;
        }

        public async Task<Guid> CreateNewInvitation(string userCognitoId, UserPermissionData requestData)
        {
            if (await CheckIfInvitationExists(userCognitoId, requestData.Email))
            {
                throw new BadRequestException("Invitation already exists.");
            }

            await ValidateDeviceIds(userCognitoId, requestData.DevicesIds);
            var devices = (await _deviceRepository.GetBy(d => requestData.DevicesIds.Contains(d.Guid),
                include: source => source.Include(x => x.Hub))).ToList();

            var features = (await _appFeatureRepository.GetAllAppFeaturesByName(requestData.Features)).ToList();
            var invitationDetails = GetInvitationDetails(requestData);
            var guestUser = await _userRepository.GetUserByEmail(requestData.Email);

            var invitationCreationData = _invitationAccountAccessBl.GetInvitationCreationData(userCognitoId,
                invitationDetails, devices, features, InvitationStatus.Pending, guestUser,
                startDate: requestData.StartDate, endDate: requestData.EndDate);
            var invitationGuid = await _invitationAccountAccessBl.CreateInvitation(invitationCreationData);

            return invitationGuid;
        }

        public async Task<bool> CheckIfInvitationExists(string ownerCognitoId, string guestEmail)
        {
            var invitation = await _invitationRepository.GetOwnerInvitationForHub(ownerCognitoId, guestEmail);
            return _invitationAccountAccessBl.CheckThatInvitationIsNotNullAndNotDeleted(invitation);
        }

        public async Task<ICollection<InvitationData>> GetInvitations(string ownerCognitoId)
        {
            var user = await _userRepository.GetUserByCognitoId(ownerCognitoId);

            ValidateUser(user);

            var invitations = await _invitationRepository.GetBy(
                i => i.OwnerUser != null
                     && i.OwnerUser.CognitoId.Equals(ownerCognitoId)
                     && !i.InvitationStatus.Status.ToUpper().Equals(InvitationStatus.Deleted.ToString().ToUpper()),
                include: source => source
                    .Include(inv => inv.UserFeaturePermissions)
                    .Include(inv => inv.InvitationStatus)
                    .Include(inv => inv.InvitationDetails));

            await UpdateInvitationStatusBasedOnEndDates(invitations);
            
            var invitationsData = _mapper.Map<IList<InvitationData>>(invitations);
            return invitationsData;
        }

        private async Task UpdateInvitationStatusBasedOnEndDates(IEnumerable<Invitation> invitations)
        {
            foreach (var invitation in invitations)
            {
                if(DateTime.UtcNow.Date > invitation.ExpirationDate?.Date)
                {
                    var expiredStatus = await _invitationStatusRepository.GetByStatus(InvitationStatus.Expired.ToString());
                    invitation.InvitationStatus = expiredStatus;
                    invitation.LastUpdate = DateTime.UtcNow;
                }

                if(DateTime.UtcNow.Date > invitation.EndDate?.Date)
                {
                    var endedStatus = await _invitationStatusRepository.GetByStatus(InvitationStatus.Ended.ToString());
                    invitation.InvitationStatus = endedStatus;
                    invitation.LastUpdate = DateTime.UtcNow;
                }
            }
            
            _invitationRepository.CommitChanges();
        }

        private static void ValidateUser(User user)
        {
            if (user == null)
            {
                throw new NotFoundException("User does not exists!");
            }
        }

        public async Task<object> GetUserPermissions(string ownerCognitoid, Guid invitationId)
        {
            var invitation = await _invitationRepository.GetFirstOrDefaultBy(
                predicate: i =>
                    i.Guid.Equals(invitationId) && i.OwnerUser != null &&
                    i.OwnerUser.CognitoId.Equals(ownerCognitoid),
                include: source => source
                    .Include(inv => inv.UserFeaturePermissions)
                    .ThenInclude(ufp => ufp.Device)
                    .ThenInclude(d => d.Hub)
                    .Include(inv => inv.UserFeaturePermissions)
                    .ThenInclude(ufp => ufp.AppFeature)
                    .Include(inv => inv.InvitationDetails));

            ValidateInvitation(invitation);

            var hubsAndDevices = GetHubsAndDevicesDictionary(invitation);
            return GetUserPermissionsResponse(invitation, hubsAndDevices);
        }

        public async Task<object> GetGuestPermissions(string guestCognitoId, Guid invitationGuid)
        {
            var user = await _userRepository.GetUserByCognitoId(guestCognitoId);

            ValidateUser(user);

            var invitation = await _invitationRepository.GetFirstOrDefaultBy(i => i.Guid.Equals(invitationGuid),
                include: source => source
                    .Include(i => i.InvitationDetails)
                    .Include(i => i.OwnerUser)
                    .ThenInclude(u => u.UserDetails));

            ValidateInvitation(invitation);

            var features = (await _userFeaturePermissionRepository
                .GetBy(i => i.Invitation.Guid.Equals(invitationGuid), 
                    include: source => source
                        .Include(x => x.AppFeature)))
                .Select(ufp => ufp.AppFeature.Feature)
                .Distinct();

            return new
            {
                isNewUser = invitation.GuestUser == null,
                ownerFirstName = invitation.OwnerUser.UserDetails.FirstName,
                appAccess = invitation.InvitationDetails.AppAccess,
                features
            };
        }

        public async Task SuspendAccess(string ownerCognitoid, Guid invitationId)
        {
            var invitation = await _invitationRepository.GetFirstOrDefaultBy(i =>
                i.Guid.Equals(invitationId) && i.OwnerUser != null &&
                i.OwnerUser.CognitoId.Equals(ownerCognitoid));

            ValidateInvitation(invitation);

            await _invitationAccountAccessBl.ExecuteOperationsToSuspendThePermissions(invitation);
            _invitationRepository.CommitChanges();
        }

        public async Task ResendInvitationEmail(string ownerCognitoid, ResendInvitationEmailData request)
        {
            var invitation = await _invitationRepository.GetFirstOrDefaultBy(i =>
                        i.Guid.Equals(request.InvitationId) && i.OwnerUser != null &&
                        i.OwnerUser.CognitoId.Equals(ownerCognitoid),
                    include: source => source
                        .Include(inv => inv.UserFeaturePermissions)
                        .ThenInclude(ufp => ufp.AppFeature)
                        .Include(inv => inv.UserFeaturePermissionHistories)
                        .ThenInclude(ufp => ufp.AppFeature)
                        .Include(inv => inv.InvitationDetails));

            ValidateInvitation(invitation);

            var newInvitationStatus =
                await _invitationStatusRepository.GetByStatus(InvitationStatus.Pending.ToString());

            invitation.StartDate = request.StartDate;
            invitation.EndDate = request.EndDate;
            invitation.InvitationStatus = newInvitationStatus;

            await _invitationRepository.AddOrUpdate(invitation);
            _invitationRepository.CommitChanges();

            var features = invitation.UserFeaturePermissionHistories.Select(ufp => ufp.AppFeature.Feature).Distinct()
                .ToList();
            if (!features.Any())
            {
                features = invitation.UserFeaturePermissions.Select(ufp => ufp.AppFeature.Feature).Distinct().ToList();
            }

            var user = await _userRepository.GetUserByCognitoId(ownerCognitoid);
            
            var configurationData =
                _invitationEmailSender.SetConfigurationData(invitation.InvitationDetails.AppAccess, features);
            
            await _invitationEmailSender.SendInvitationEmail(invitation.Guid, user, request.Email, invitation.InvitationDetails.FirstName,
                configurationData);
        }

        public async Task AcceptInvitation(string guestCognitoId, Guid invitationId)
        {
            var invitation = await _invitationRepository.GetFirstOrDefaultBy(i =>
                    i.Guid.Equals(invitationId),
                include: source => source
                    .Include(inv => inv.InvitationStatus)
                    .Include(inv => inv.GuestUser)
                    .Include(inv => inv.InvitationDetails));

            ValidateInvitation(invitation);

            await UpdateInvitationGuestUser(guestCognitoId, invitation);

            var acceptedInvitationStatus =
                await _invitationStatusRepository.GetByStatus(InvitationStatus.Accepted.ToString());

            await _invitationAccountAccessBl.UpdateInvitationStatusToAccepted(invitation, acceptedInvitationStatus);
            _invitationRepository.CommitChanges();
        }

        private async Task UpdateInvitationGuestUser(string guestCognitoId, Invitation invitation)
        {
            if (invitation.GuestUser == null)
            {
                var guestUser = await _userRepository.GetUserByCognitoId(guestCognitoId);
                ValidateUser(guestUser);

                if (string.Equals(guestUser.Email, invitation.InvitationDetails.Email,
                    StringComparison.CurrentCultureIgnoreCase))
                {
                    invitation.GuestUser = guestUser;
                }
                else
                {
                    throw new BadRequestException("Invalid guest user");
                }
            }
        }

        public async Task RejectInvitation(string guestCognitoId, Guid invitationId)
        {
            var invitation = await _invitationRepository.GetFirstOrDefaultBy(i =>
                    i.Guid.Equals(invitationId) && i.GuestUser != null &&
                    i.GuestUser.CognitoId.Equals(guestCognitoId),
                include: source => source.Include(inv => inv.InvitationStatus));

            ValidateInvitation(invitation);

            await _invitationAccountAccessBl.ExecuteOperationsToRejectTheInvitation(invitation);
        }

        public async Task DeleteInvitation(string ownerCognitoId, Guid invitationId)
        {
            var invitationFromDb = await _invitationRepository.GetFirstOrDefaultBy(i => i.Guid.Equals(invitationId) &&
                    i.OwnerUser != null &&
                    i.OwnerUser.CognitoId.Equals(ownerCognitoId),
                include: source => source
                    .Include(inv => inv.UserFeaturePermissions)
                    .ThenInclude(ufp => ufp.AppFeature)
                    .Include(inv => inv.UserFeaturePermissions)
                    .ThenInclude(ufp => ufp.Device));

            ValidateInvitation(invitationFromDb);

            var ufps = invitationFromDb.UserFeaturePermissions;
            if (ufps.Any())
            {
                var historyUfps = ufps.MapToUserFeaturePermissionHistory();
                await _userFeaturePermissionHistoryRepository.AddOrUpdate(historyUfps);
                await _userFeaturePermissionRepository.DeleteMultiple(ufps);
            }

            var deletedInvitationStatus = await _invitationStatusRepository.GetByStatus(InvitationStatus.Deleted.ToString());
            invitationFromDb.InvitationStatus = deletedInvitationStatus;
            invitationFromDb.LastUpdate = DateTime.UtcNow;

            await _invitationRepository.AddOrUpdate(invitationFromDb);
            _invitationRepository.CommitChanges();
        }

        public async Task EditPermissions(string ownerCognitoId, UserPermissionData requestData)
        {
            var invitationFromDb = await _invitationRepository.GetFirstOrDefaultBy(
                i => i.Guid.Equals(requestData.InvitationId) && i.OwnerUser != null &&
                     i.OwnerUser.CognitoId.Equals(ownerCognitoId),
                include: source => source
                    .Include(inv => inv.OwnerUser)
                    .Include(inv => inv.InvitationDetails)
                    .Include(inv => inv.UserFeaturePermissions)
                    .ThenInclude(ufp => ufp.AppFeature)
                    .Include(inv => inv.UserFeaturePermissions)
                    .ThenInclude(ufp => ufp.Device)
                    .Include(inv => inv.UserFeaturePermissionHistories)
                    .ThenInclude(ufp => ufp.AppFeature)
                    .Include(inv => inv.UserFeaturePermissionHistories)
                    .ThenInclude(ufp => ufp.Device));

            ValidateInvitation(invitationFromDb);
            var ownerUser = invitationFromDb.OwnerUser;

            await ValidateDeviceIds(ownerCognitoId, requestData.DevicesIds);
            var features = (await _appFeatureRepository.GetAllAppFeaturesByName(requestData.Features)).ToList();
            var devicesFromRequest = (await _deviceRepository.GetBy(d => requestData.DevicesIds.Contains(d.Guid),
                include: source
                    => source.Include(x => x.Hub))).ToList();

            UpdateInvitationDates(requestData, invitationFromDb);
            UpdateFlagsFromInvitationDetails(requestData, invitationFromDb);
            UpdateEmailFromInvitationDetails(requestData, invitationFromDb);
            UpdateNameFromInvitationDetails(requestData, invitationFromDb);

            var userFeaturePermissionsFromDb = invitationFromDb.UserFeaturePermissions;
            var devicesFromDb = userFeaturePermissionsFromDb.Select(ufp => ufp.Device).Distinct().ToList();
            var devicesToDelete = devicesFromDb.Except(devicesFromRequest);

            var (ufpsToDelete, historyUfpsToAdd) =
                GetUfpsToDeleteAndHistoryUfpsToAdd(devicesToDelete, userFeaturePermissionsFromDb, features);

            var (ufpsToAdd, historyUfpsToDelete) =
                GetUfpsToAddAndHistoryUfpsToDelete(devicesFromRequest, features, ownerUser, invitationFromDb);

            await ExecuteRepositoryOperations(invitationFromDb, ufpsToDelete, historyUfpsToAdd, ufpsToAdd,
                historyUfpsToDelete);
        }

        public async Task<object> GetSharedFeatures(string userCognitoId)
        {
            var featureDevices = (await _userFeaturePermissionRepository
                .GetBy(ufp => ufp.Invitation.GuestUser != null &&
                              ufp.Invitation.GuestUser.CognitoId.Equals(userCognitoId),
                    include: source => source
                        .Include(ufp => ufp.AppFeature)
                        .Include(ufp => ufp.Device)))
                .Select(ufp => new { ufp.AppFeature.Feature, deviceGuid = ufp.Device.Guid.ToString() })
                .ToList();

            var deviceFeaturesDictionary = new Dictionary<string, List<string>>();
            foreach (var featureDevice in featureDevices)
            {
                if (deviceFeaturesDictionary.ContainsKey(featureDevice.deviceGuid))
                {
                    deviceFeaturesDictionary[featureDevice.deviceGuid].Add(featureDevice.Feature);
                }
                else
                {
                    deviceFeaturesDictionary[featureDevice.deviceGuid] = new List<string>
                    {
                        featureDevice.Feature
                    };
                }
            }

            return deviceFeaturesDictionary;
        }

        public async Task<List<object>> GetUserAccounts(string userCognitoId)
        {
            var invitations = (await _invitationRepository
                .GetBy(i => i.GuestUser != null &&
                            i.GuestUser.CognitoId.Equals(userCognitoId) &&
                            i.InvitationDetails.AdministratorAccess &&
                            i.InvitationStatus.Status.Equals(InvitationStatus.Accepted.ToString()),
                    include: source => source
                        .Include(i => i.InvitationDetails)
                        .Include(i => i.GuestUser)
                        .ThenInclude(u => u.UserDetails)
                        .Include(i => i.OwnerUser)
                        .ThenInclude(u => u.UserDetails))).ToList();

            if (!invitations.Any())
            {
                return new List<object>();
            }

            var response = new List<object>();

            var currentUserAccount = GetCurrentUserAccount(invitations);
            response.Add(currentUserAccount);

            var userAccounts = GetGuestUserAccountsWithAdminAccess(invitations);
            response.AddRange(userAccounts);

            return response;
        }

        public async Task SetSelectedAccount(string userCognitoId, Guid invitationId)
        {
            var invitations = (await _invitationRepository
                .GetBy(i => i.GuestUser != null &&
                            i.GuestUser.CognitoId.Equals(userCognitoId) &&
                            i.InvitationDetails.AdministratorAccess &&
                            i.InvitationStatus.Status.Equals(InvitationStatus.Accepted.ToString())))
                .ToList();

            if (!invitations.Any())
            {
                throw new BadRequestException("There is no account to be selected!");
            }
            
            foreach (var invitation in invitations)
            {
                invitation.IsSelectedAccount = invitation.Guid.Equals(invitationId);
            }
            
            _invitationRepository.CommitChanges();
        }

        public async Task<string> GetInvitationOwnerCognitoId(string currentUserCognitoId)
        {
            var invitation = await _invitationRepository.GetFirstOrDefaultBy(i =>
                    i.GuestUser != null &&
                    i.GuestUser.CognitoId.Equals(currentUserCognitoId) &&
                    i.IsSelectedAccount &&
                    i.InvitationDetails.AdministratorAccess &&
                    i.InvitationStatus.Status.Equals(Enumerators.InvitationStatus.Accepted.ToString()),
                include: source => source.Include(i => i.OwnerUser));

            return invitation == null ? currentUserCognitoId
                : invitation.OwnerUser.CognitoId;
        }

        public async Task<Guid> SendInvitation(string userCognitoId, UserPermissionData userPermissionData)
        {
            var invitationId = await CreateNewInvitation(userCognitoId, userPermissionData);
            var configurationData = _invitationEmailSender.SetConfigurationData(userPermissionData.AppAccess, userPermissionData.Features);

            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateUser(user);
            
            await _invitationEmailSender.SendInvitationEmail(invitationId, user, userPermissionData.Email, userPermissionData.FirstName,
                configurationData);

            return invitationId;
        }

        private static IEnumerable<object> GetGuestUserAccountsWithAdminAccess(IEnumerable<Invitation> invitations)
        {
            return invitations
                .Select(invitation => new
                {
                    InvitationId = invitation.Guid,
                    invitation.OwnerUser.UserDetails.FirstName,
                    invitation.OwnerUser.UserDetails.LastName,
                    invitation.OwnerUser.Email,
                    invitation.IsSelectedAccount,
                    IsGuestUser = true
                });
        }

        private object GetCurrentUserAccount(IEnumerable<Invitation> invitations)
        {
            var firstInvitation = invitations.First();
            return new
            {
                firstInvitation.GuestUser.UserDetails.FirstName,
                firstInvitation.GuestUser.UserDetails.LastName,
                firstInvitation.GuestUser.Email,
                IsGuestUser = false
            };
        }

        private Tuple<List<UserFeaturePermission>, List<UserFeaturePermissionHistory>>
            GetUfpsToAddAndHistoryUfpsToDelete(IEnumerable<Device> devicesFromRequest,
                IReadOnlyCollection<AppFeature> features,
                User ownerUser, Invitation invitationFromDb)
        {
            var userFeaturePermissionsFromDb = invitationFromDb.UserFeaturePermissions;
            var historyUfpsFromDb = invitationFromDb.UserFeaturePermissionHistories;

            // item1-> UfpsToAdd AND item2->HistoryUfpsToDelete
            var ufpTuple =
                new Tuple<List<UserFeaturePermission>, List<UserFeaturePermissionHistory>>(
                    new List<UserFeaturePermission>(), new List<UserFeaturePermissionHistory>());

            foreach (var device in devicesFromRequest)
            {
                foreach (var appFeature in features)
                {
                    // If AppFeature doesn't exists -> add it
                    var ufpForDevice = userFeaturePermissionsFromDb
                        .Where(ufp => ufp.Device.Guid.Equals(device.Guid));
                    if (!ufpForDevice.Any(ufp => ufp.AppFeature.Feature.Equals(appFeature.Feature)))
                    {
                        var newUfp = GetUserFeaturePermission(ownerUser, invitationFromDb, device, appFeature);
                        ufpTuple.Item1.Add(newUfp);
                    }
                }
            }

            if (ufpTuple.Item1.Any())
            {
                // If ufpToAdd exists in historyUfp table delete them
                var historyUfpToDelete = ufpTuple.Item1
                    .Select(userFeaturePermission =>
                        historyUfpsFromDb.SingleOrDefault(hufp =>
                            hufp.Invitation.Guid.Equals(userFeaturePermission.Invitation.Guid) &&
                            hufp.OwnerUser.Guid.Equals(userFeaturePermission.OwnerUser.Guid) &&
                            hufp.AppFeature.Guid.Equals(userFeaturePermission.AppFeature.Guid) &&
                            hufp.Device.Guid.Equals(userFeaturePermission.Device.Guid)))
                    .Where(historyUfp => historyUfp != null).ToList();

                ufpTuple.Item2.AddRange(historyUfpToDelete);
            }

            return ufpTuple;
        }

        private Tuple<List<UserFeaturePermission>, List<UserFeaturePermissionHistory>>
            GetUfpsToDeleteAndHistoryUfpsToAdd(IEnumerable<Device> devicesToDelete,
                IList<UserFeaturePermission> userFeaturePermissionsFromDb, IReadOnlyCollection<AppFeature> features)
        {
            // item1-> UfpsToDelete AND item2->HistoryUfpsToAdd
            var ufpTuple =
                new Tuple<List<UserFeaturePermission>, List<UserFeaturePermissionHistory>>(
                    new List<UserFeaturePermission>(), new List<UserFeaturePermissionHistory>());

            foreach (var device in devicesToDelete)
            {
                var ufpToDeleteForDevice =
                    userFeaturePermissionsFromDb.Where(ufp => ufp.Device.Guid.Equals(device.Guid)).ToList();

                ufpTuple.Item1.AddRange(ufpToDeleteForDevice);

                var historyUfps = ufpToDeleteForDevice.MapToUserFeaturePermissionHistory();
                ufpTuple.Item2.AddRange(historyUfps);
            }

            // If app feature exists in DB -> but not in request -> Remove from ufp and add to historyUfp
            var ufpsToDeleteForFeature = userFeaturePermissionsFromDb
                .Where(userFeaturePermission =>
                    !features.Any(f => f.Guid.Equals(userFeaturePermission.AppFeature.Guid))).ToList();

            var ufpsToDeleteThatAreNotAlreadyAdded = ufpsToDeleteForFeature.Except(ufpTuple.Item1).ToList();

            var historyUfpToAddForFeature = ufpsToDeleteThatAreNotAlreadyAdded.MapToUserFeaturePermissionHistory();

            if (ufpsToDeleteForFeature.Any() && historyUfpToAddForFeature.Any())
            {
                ufpTuple.Item1.AddRange(ufpsToDeleteThatAreNotAlreadyAdded);
                ufpTuple.Item2.AddRange(historyUfpToAddForFeature);
            }

            return ufpTuple;
        }

        private IDictionary<string, Dictionary<string, List<Guid>>> GetHubsAndDevicesDictionary(Invitation invitation)
        {
            var hubsAndDevices = new Dictionary<string, Dictionary<string, List<Guid>>>();
            var hubNameCounter = 1;

            foreach (var hub in invitation.UserFeaturePermissions.Select(ufp => ufp.Device.Hub).Distinct())
            {
                var deviceTypesAndIds = new Dictionary<string, List<Guid>>();

                foreach (var device in invitation.UserFeaturePermissions.Select(ufp => ufp.Device).Distinct())
                {
                    if (device.Hub.Guid.Equals(hub.Guid))
                    {
                        var hubNickname = hub.NickName;
                        var deviceType = device.DeviceType;
                        if (string.IsNullOrEmpty(hubNickname))
                        {
                            hubNickname = "Hub " + hubNameCounter;
                        }

                        if (hubsAndDevices.ContainsKey(hubNickname))
                        {
                            if (deviceTypesAndIds.ContainsKey(deviceType))
                            {
                                deviceTypesAndIds[deviceType].Add(device.Guid);
                            }
                            else
                            {
                                var deviceIds = new List<Guid> { device.Guid };
                                deviceTypesAndIds.Add(deviceType, deviceIds);
                            }
                        }
                        else
                        {
                            hubsAndDevices.Add(hubNickname, deviceTypesAndIds);

                            var deviceIds = new List<Guid> { device.Guid };
                            deviceTypesAndIds.Add(deviceType, deviceIds);
                        }
                    }
                }

                hubNameCounter++;
            }

            return hubsAndDevices;
        }

        private object GetUserPermissionsResponse(Invitation invitation,
            IDictionary<string, Dictionary<string, List<Guid>>> hubsAndDevices)
        {
            return new
            {
                invitation.InvitationDetails.FirstName,
                invitation.InvitationDetails.LastName,
                invitation.InvitationDetails.Email,
                invitation.InvitationDetails.AdministratorAccess,
                invitation.InvitationDetails.AppAccess,
                invitation.InvitationDetails.AllDevicesAccess,
                Features = invitation.UserFeaturePermissions.Select(ufp => ufp.AppFeature.Feature).Distinct(),
                hubsAndDevices,
                invitation.StartDate,
                invitation.EndDate
            };
        }

        private void ValidateInvitation(Invitation invitation)
        {
            if (invitation == null)
            {
                throw new NotFoundException("Invitation not found");
            }
        }

        private async Task ValidateDeviceIds(string ownerCognitoId, IEnumerable<Guid> deviceGuidsFromRequest)
        {
            var deviceGuidsFromDb = (await _deviceRepository.GetDevicesByUserId(ownerCognitoId))
                .Select(d => d.Guid).ToList();

            var deviceGuidsDifference = deviceGuidsFromRequest.Except(deviceGuidsFromDb);

            if (deviceGuidsDifference.Any())
            {
                throw new BadRequestException("Invalid device ids");
            }
        }

        private void UpdateInvitationDates(UserPermissionData requestData, Invitation invitationFromDb)
        {
            invitationFromDb.StartDate = requestData.StartDate;
            invitationFromDb.EndDate = requestData.EndDate;
            invitationFromDb.LastUpdate = DateTime.UtcNow;
        }

        private void UpdateNameFromInvitationDetails(UserPermissionData requestData, Invitation invitationFromDb)
        {
            var invitationDetails = invitationFromDb.InvitationDetails;

            if (!string.Equals(invitationDetails.FirstName, requestData.LastName))
            {
                invitationFromDb.InvitationDetails.FirstName = requestData.FirstName;
                invitationFromDb.InvitationDetails.LastUpdate = DateTime.UtcNow;
            }

            if (!string.Equals(invitationDetails.LastName, requestData.LastName))
            {
                invitationFromDb.InvitationDetails.LastName = requestData.LastName;
                invitationFromDb.InvitationDetails.LastUpdate = DateTime.UtcNow;
            }
        }

        private void UpdateEmailFromInvitationDetails(UserPermissionData requestData, Invitation invitationFromDb)
        {
            var invitationDetails = invitationFromDb.InvitationDetails;

            if (!string.Equals(invitationDetails.Email, requestData.Email,
                StringComparison.InvariantCultureIgnoreCase))
            {
                invitationFromDb.InvitationDetails.Email = requestData.Email;
            }
        }

        private void UpdateFlagsFromInvitationDetails(UserPermissionData requestData, Invitation invitationFromDb)
        {
            var invitationDetails = invitationFromDb.InvitationDetails;

            if (invitationDetails.AdministratorAccess != requestData.AdministratorAccess)
            {
                invitationDetails.AdministratorAccess = requestData.AdministratorAccess;
                invitationFromDb.InvitationDetails.LastUpdate = DateTime.UtcNow;
            }

            if (invitationDetails.AllDevicesAccess != requestData.AllDevicesAccess)
            {
                invitationFromDb.InvitationDetails.AllDevicesAccess = requestData.AllDevicesAccess;
                invitationFromDb.InvitationDetails.LastUpdate = DateTime.UtcNow;
            }

            if (invitationFromDb.InvitationDetails.AppAccess != requestData.AppAccess)
            {
                invitationFromDb.InvitationDetails.AppAccess = requestData.AppAccess;
                invitationFromDb.InvitationDetails.LastUpdate = DateTime.UtcNow;
            }
        }

        private async Task ExecuteRepositoryOperations(Invitation invitationToUpdate,
            IReadOnlyCollection<UserFeaturePermission> ufpToDelete,
            IReadOnlyCollection<UserFeaturePermissionHistory> historyUfpsToAdd,
            IReadOnlyCollection<UserFeaturePermission> ufpToAdd,
            IReadOnlyCollection<UserFeaturePermissionHistory> historyUfpsToDelete)
        {
            await _invitationRepository.AddOrUpdate(invitationToUpdate);

            if (ufpToAdd.Any())
            {
                await _userFeaturePermissionRepository.AddOrUpdate(ufpToAdd);
            }

            if (ufpToDelete.Any())
            {
                await _userFeaturePermissionRepository.DeleteMultiple(ufpToDelete);
            }

            if (historyUfpsToAdd.Any())
            {
                await _userFeaturePermissionHistoryRepository.AddOrUpdate(historyUfpsToAdd);
            }

            if (historyUfpsToDelete.Any())
            {
                await _userFeaturePermissionHistoryRepository.DeleteMultiple(historyUfpsToDelete);
            }

            _userFeaturePermissionRepository.CommitChanges();
        }

        private UserFeaturePermission GetUserFeaturePermission(User ownerUser, Invitation invitationFromDb,
            Device device, AppFeature appFeature)
        {
            return new UserFeaturePermission
            {
                OwnerUser = ownerUser,
                Invitation = invitationFromDb,
                Device = device,
                AppFeature = appFeature,
                LastUpdate = DateTime.UtcNow,
            };
        }

        private InvitationDetails GetInvitationDetails(UserPermissionData requestData)
        {
            return new InvitationDetails
            {
                Email = requestData.Email,
                FirstName = requestData.FirstName,
                LastName = requestData.LastName,
                AllDevicesAccess = requestData.AllDevicesAccess,
                AdministratorAccess = requestData.AdministratorAccess,
                AppAccess = requestData.AppAccess,
                LastUpdate = DateTime.UtcNow
            };
        }
    }
}
