﻿using AutoMapper;
using MyEnergi.AppServer;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Interfaces;
using Serilog;
using System;
using System.Threading.Tasks;
using MyEnergi.Common;
using ApiCallResponse = MyEnergi.AppServer.Model.ApiCallResponse;

namespace MyEnergi.Business.ApiCallerHelpers
{
    public class ApiCallerHelper : IApiCallerHelper
    {
        private readonly IHubRepository _hubRepository;
        private readonly IMapper _mapper;

        public ApiCallerHelper(IHubRepository hubRepository, IMapper mapper)
        {
            _hubRepository = hubRepository;
            _mapper = mapper;
        }

        public async Task<ApiCallResponse> GetKeyValueResult(HubData hub, bool isPswDecrypted = false, string key = null, string value = null)
        {
            var apiResult = new KeyValueManager(hub.HostServer, hub.SerialNo,
                isPswDecrypted ? hub.Password : KMSCipher.Decrypt(hub.Password).Result, key, value).GetResult();

            var result = _mapper.Map<ApiCallResponse>(apiResult);

            if (hub.HostServer != result.HostServer)
            {
                hub.HostServer = result.HostServer;
                await UpdateHostServer(hub.Id, hub.HostServer);
            }

            return result;
        }

        public async Task<ApiCallResponse> GetExistingHubStatusResult(HubData hub, bool isPswDecrypted = false)
        {
            var apiResult = new ExistingHubStatus(hub.HostServer, hub.SerialNo,
                isPswDecrypted ? hub.Password : KMSCipher.Decrypt(hub.Password).Result).GetResult();

            var result = _mapper.Map<ApiCallResponse>(apiResult);

            if (hub.HostServer != result.HostServer)
            {
                hub.HostServer = result.HostServer;
                await UpdateHostServer(hub.Id, hub.HostServer);
            }

            return result;
        }

        public async Task<ApiCallResponse> GetHubRegisterResult(HubData hub)
        {
            var apiResult = new NewHubRegister(hub.HostServer, hub.SerialNo, hub.RegistrationCode, hub.Password, hub.ConfirmPassword).GetResult();

            var result = _mapper.Map<ApiCallResponse>(apiResult);

            if (hub.HostServer != result.HostServer)
            {
                hub.HostServer = result.HostServer;
                await UpdateHostServer(hub.Id, hub.HostServer);
            }

            return result;
        }

        public async Task<ApiCallResponse> GetEddiHeaterResult(HubData hub, string deviceSNo)
        {
            var apiResult = new EddiHeaterPriority(hub.HostServer, deviceSNo, hub.SerialNo, KMSCipher.Decrypt(hub.Password).Result).GetResult();

            var result = _mapper.Map<ApiCallResponse>(apiResult);

            if (hub.HostServer != result.HostServer)
            {
                hub.HostServer = result.HostServer;
                await UpdateHostServer(hub.Id, hub.HostServer);
            }

            return result;
        }

        public async Task<ApiCallResponse> GetDeviceEnergyInfoResult(HubData hub, string deviceType, string deviceSNo, DateTime queryDate, int minutesInterval = 0)
        {
            var apiResult = new DeviceEnergyInfoForDayMinutely(Log.Logger, hub.SerialNo, KMSCipher.Decrypt(hub.Password).Result, hub.HostServer, deviceType, deviceSNo, queryDate, minutesInterval).GetResult();

            var result = _mapper.Map<ApiCallResponse>(apiResult);

            if (hub.HostServer != result.HostServer)
            {
                hub.HostServer = result.HostServer;
                await UpdateHostServer(hub.Id, hub.HostServer);
            }

            return result;
        }

        public ApiCallResponse GetDeviceBoostTimeResult(HubData hub, string deviceType, string deviceSNo,
            string chargingSlot, string chargingFrom, string chargeTime, string chargeDays)
        {
            var apiResult = new DeviceBoostTime(hub.HostServer, deviceType, deviceSNo, chargingSlot, chargingFrom, chargeTime, chargeDays, hub.SerialNo, KMSCipher.Decrypt(hub.Password).Result).GetResult();

            var result = _mapper.Map<ApiCallResponse>(apiResult);

            return result;
        }

        async Task UpdateHostServer(Guid hubId, string hostServer)
        {
            if (hubId == Guid.Empty) return;

            var hub = await _hubRepository.GetHubById(hubId);
            hub.HostServer = hostServer;
            _hubRepository.CommitChanges();
        }

        public async Task<ApiCallResponse> GetKVPairResult(Guid hubId, string hostServer, string serialNo, string password)
        {
            var apiResult = new PushNotification(hostServer, serialNo, KMSCipher.Decrypt(password).Result).GetResult();
            var result = _mapper.Map<ApiCallResponse>(apiResult);

            if (hostServer != result.HostServer)
            {
                hostServer = result.HostServer;
                await UpdateHostServer(hubId, hostServer);
            }

            return result;
        }
    }
}
