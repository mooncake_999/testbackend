﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Helpers;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static MyEnergi.Common.Enumerators;

namespace MyEnergi.Business
{
    public class ChargeSchedulesBL : IChargeSchedulesBL
    {
        private readonly IDeviceRepository _deviceRepository;
        private readonly IChargeScheduleRepository _chargeScheduleRepository;
        private readonly IEnergyProviderRepository _energyProviderRepository;
        private readonly IApiCallerHelper _apiCallerHelper;
        private readonly IDeviceBL _deviceBl;
        private readonly IMapper _mapper;

        public ChargeSchedulesBL(IDeviceRepository deviceRepository,
            IChargeScheduleRepository chargeScheduleRepository,
            IEnergyProviderRepository energyProviderRepository, 
            IApiCallerHelper apiCallerHelper, 
            IDeviceBL deviceBl,
            IMapper mapper)
        {
            _deviceRepository = deviceRepository;
            _chargeScheduleRepository = chargeScheduleRepository;
            _energyProviderRepository = energyProviderRepository;
            _apiCallerHelper = apiCallerHelper;
            _deviceBl = deviceBl;
            _mapper = mapper;
        }

        async Task<List<ChargeSchedulesResponseData>> IChargeSchedulesBL.GetAllSchedules(string userCognitoId)
        {
            var ownerDevices = (await _deviceRepository.GetDevicesWithHubsAndChargeSchedulesByUserId(userCognitoId)).ToList();
            var guestDeviceData = await _deviceBl.GetGuestDevicesData(userCognitoId);
            var guestDevices = _mapper.Map<List<Device>>(guestDeviceData);

            if (!ownerDevices.Any() && !guestDevices.Any())
            {
                throw new NotFoundException("No devices available!!");
            }

            var ownerChargingSchedules = await GetChargingSchedulesForDevices(ownerDevices, false);
            var guestChargingSchedules = await GetChargingSchedulesForDevices(guestDevices, true);

            var response = new List<ChargeSchedulesResponseData>();
            response.AddRange(ownerChargingSchedules);
            response.AddRange(guestChargingSchedules);
            return response;
        }

        private async Task<IEnumerable<ChargeSchedulesResponseData>> GetChargingSchedulesForDevices(List<Device> allDevices, bool isGuestUser)
        {
            var response = new List<ChargeSchedulesResponseData>();

            var serverDevices = allDevices.Select(d => d.Hub).Distinct().SelectMany(GetServerDevices).ToList();
            allDevices = allDevices.Where(d => serverDevices.Any(s => s.SerialNumber == d.SerialNumber)).ToList();

            await DeleteExpiredSingleCharges(allDevices);
            var deviceIds = allDevices.Select(d => d.Guid).Distinct().ToList();
            var devicesBestPriceIntervals =
                (await _energyProviderRepository.GetDeviceBestPriceIntervalsByDeviceIds(deviceIds)).ToList();
            var hubIds = allDevices.Select(s => s.Hub.Guid).Distinct().ToList();

            foreach (var hubId in hubIds)
            {
                var chargeSchedulesResponseData = new ChargeSchedulesResponseData();
                var deviceSchedules = allDevices.Where(s => s.Hub.Guid == hubId).ToList();
                var hub = deviceSchedules.First().Hub;
                var address = deviceSchedules.First().Address;
                chargeSchedulesResponseData.HubId = hubId;
                chargeSchedulesResponseData.HubNickName = hub.NickName;
                chargeSchedulesResponseData.IsGuestUser = isGuestUser;
                var energySetup = await _energyProviderRepository.GetEnergySetupByAddressId(address.Guid);
                if (energySetup?.EnergyProv != null)
                {
                    chargeSchedulesResponseData.EnergySetupProvider = energySetup.EnergyProv.Provider;
                    chargeSchedulesResponseData.EnergySetupTarrif = energySetup.EnergyProv.Tariff;
                    chargeSchedulesResponseData.IsEconomy = energySetup.EnergyProv.IsEconomy;
                    if (chargeSchedulesResponseData.IsEconomy.GetValueOrDefault() && energySetup.EnergyProv.IsActive ||
                        energySetup.EnergyProv.Provider.Equals(Constants.Entsoe)
                        ||  (energySetup.EnergyProv.InternalReference.ToUpper()
                            .Contains(Constants.AgileOctopusInternalReference.ToUpper())))
                    {
                        foreach (var device in deviceSchedules)
                        {
                            var bestPriceIntervalsPerDevice = devicesBestPriceIntervals
                                .Where(d => d.Device.Id == device.Id)
                                .OrderBy(d => d.From).ToList();
                            var devicesForCharge = new DevicesForChargeSchedules
                            {
                                DeviceId = device.Guid,
                                DeviceName = device.DeviceName,
                                DeviceType = device.DeviceType,
                                IsGuestUser = isGuestUser,
                                BudgetChargesActive = device.ChargeSchedules.Where(c =>
                                    string.Equals(c.ScheduleType.Trim(), TypeOfSchedule.Budget.ToString().Trim(),
                                        StringComparison.CurrentCultureIgnoreCase)).All(c => c.IsActive),
                                SingleChargesActive = device.ChargeSchedules.Where(c =>
                                    string.Equals(c.ScheduleType.Trim(), TypeOfSchedule.Single.ToString().Trim(),
                                        StringComparison.CurrentCultureIgnoreCase)).All(c => c.IsActive),
                                ScheduleChargesActive = device.ChargeSchedules.Where(c =>
                                    string.Equals(c.ScheduleType.Trim(), TypeOfSchedule.Scheduled.ToString().Trim(),
                                        StringComparison.CurrentCultureIgnoreCase)).All(c => c.IsActive)
                            };

                            foreach (var chargeSetup in device.ChargeSchedules)
                            {
                                var cs = _mapper.Map<ChargeScheduleData>(chargeSetup);
                                if (cs.ScheduleType == TypeOfSchedule.Budget.ToString())
                                    devicesForCharge.BudgetCharges.Add(cs);
                                else if (cs.ScheduleType == TypeOfSchedule.Scheduled.ToString())
                                    devicesForCharge.ScheduleCharges.Add(cs);
                                else if (cs.ScheduleType == TypeOfSchedule.Single.ToString())
                                    devicesForCharge.SingleCharges.Add(cs);
                            }

                            Helper.ConvertTimeZoneToHourMinute(device.TimeZoneRegion, out int hourUTCOffset,
                                out int minuteUTCOffset);
                            var priceIntervals = bestPriceIntervalsPerDevice.FirstOrDefault(b =>
                                b.From >= DateTime.Now.AddHours(hourUTCOffset).AddMinutes(minuteUTCOffset));
                            if (priceIntervals != null)
                            {
                                if (priceIntervals.IsScheduledType)
                                {
                                    devicesForCharge.ScheduleNextChargeTimeFrom =
                                        DateTime.SpecifyKind(priceIntervals.From, DateTimeKind.Unspecified);
                                    devicesForCharge.ScheduleNextChargeTimeTo =
                                        DateTime.SpecifyKind(priceIntervals.To, DateTimeKind.Unspecified);
                                }

                                if (priceIntervals.IsSingleType)
                                {
                                    devicesForCharge.SingleNextChargeTimeFrom =
                                        DateTime.SpecifyKind(priceIntervals.From, DateTimeKind.Unspecified);
                                    devicesForCharge.SingleNextChargeTimeTo =
                                        DateTime.SpecifyKind(priceIntervals.To, DateTimeKind.Unspecified);
                                }

                                if (priceIntervals.IsBudgetType)
                                {
                                    devicesForCharge.BudgetNextChargeTimeFrom =
                                        DateTime.SpecifyKind(priceIntervals.From, DateTimeKind.Unspecified);
                                    devicesForCharge.BudgetNextChargeTimeTo =
                                        DateTime.SpecifyKind(priceIntervals.To, DateTimeKind.Unspecified);
                                }

                                devicesForCharge.DeviceNextChargeTimeFrom =
                                    DateTime.SpecifyKind(priceIntervals.From, DateTimeKind.Unspecified);
                                devicesForCharge.DeviceNextChargeTimeTo =
                                    DateTime.SpecifyKind(priceIntervals.To, DateTimeKind.Unspecified);
                            }

                            chargeSchedulesResponseData.Devices.Add(devicesForCharge);
                        }
                    }
                }

                response.Add(chargeSchedulesResponseData);
            }

            return response;
        }

        private async Task DeleteExpiredSingleCharges(List<Device> devices)
        {
            List<DeviceChargesSetup> singleCharges = devices.SelectMany(d => d.ChargeSchedules).Where(c => c.SingleChargeDay != null).ToList();
            if (!singleCharges.Any())
                return;
            List<DeviceChargesSetup> chargesToBeDeleted = new List<DeviceChargesSetup>();
            foreach (DeviceChargesSetup chargesSetup in singleCharges)
            {
                string timeZone = chargesSetup.Device.TimeZoneRegion;
                Helper.ConvertTimeZoneToHourMinute(timeZone, out int hourUTCOffset, out int minuteUTCOffset);
                DateTime nowDateWithTimezone = DateTime.Now.AddHours(hourUTCOffset).AddMinutes(minuteUTCOffset);
                string[] toTimeSplit = chargesSetup.ToTime.Split(':');
                int toHour = Int32.Parse(toTimeSplit.First());
                int toMinutes = Int32.Parse(toTimeSplit.Last());
                DateTime chargeDate = chargesSetup.SingleChargeDay.Value.AddHours(toHour).AddMinutes(toMinutes);
                if (chargeDate < nowDateWithTimezone)
                {
                    chargesToBeDeleted.Add(chargesSetup);
                }
            }
            if (!chargesToBeDeleted.Any())
                return;
            await _chargeScheduleRepository.DeleteChargeSetups(chargesToBeDeleted);
            _chargeScheduleRepository.CommitChanges();
        }

        async Task<ChargeScheduleRequestData> IChargeSchedulesBL.SaveSchedules(ChargeScheduleRequestData chargeSchedule, string userCognitoId)
        {
            List<DeviceChargesSetup> savedCharges = new List<DeviceChargesSetup>();
            Device dbDevice = await _deviceRepository.GetDeviceWithChargeScheduleById(chargeSchedule.DeviceId, userCognitoId);
            if (dbDevice == null)
                throw new UnauthorizedAccessException("Access denied!!");
            ValidateInput(chargeSchedule, dbDevice.TimeZoneRegion);
            List<DeviceChargesSetup> dbChargesSetups = dbDevice.ChargeSchedules.Where(c => c.ScheduleType.Trim().ToUpper() == chargeSchedule.ScheduleType.Trim().ToUpper()).ToList();
            List<Guid> chargeSetupsIdToKeep = new List<Guid>();
            foreach (ChargeScheduleData charge in chargeSchedule.ChargeSchedules)
            {
                DeviceChargesSetup dbChargeSetup = dbChargesSetups.FirstOrDefault(d => d.Guid == charge.Id);
                if (dbChargeSetup == null)
                {
                    dbChargeSetup = _mapper.Map<DeviceChargesSetup>(charge);
                    dbChargeSetup.Device = dbDevice;
                }
                else
                {
                    if (ValidateThatScheduleChanged(charge, dbChargeSetup))
                    {
                        await _chargeScheduleRepository.MoveChargeToHistoryTable(dbChargeSetup);
                        _mapper.Map(charge, dbChargeSetup);
                    }
                    chargeSetupsIdToKeep.Add(charge.Id);
                }
                dbChargeSetup.IsActive = chargeSchedule.IsActive;
                await _chargeScheduleRepository.AddOrUpdate(dbChargeSetup);
                savedCharges.Add(dbChargeSetup);
            }

            List<DeviceChargesSetup> dbChargesSetupsToDelete = dbChargesSetups.Where(a => !chargeSetupsIdToKeep.Contains(a.Guid)).ToList();
            await _chargeScheduleRepository.DeleteChargeSetups(dbChargesSetupsToDelete);

            _chargeScheduleRepository.CommitChanges();
            chargeSchedule.ChargeSchedules = _mapper.Map<List<ChargeScheduleData>>(savedCharges);
            return chargeSchedule;
        }

        private bool ValidateThatScheduleChanged(ChargeScheduleData charge, DeviceChargesSetup dbChargeSetup)
        {
            if (charge.ChargeAmountMinutes != dbChargeSetup.ChargeAmountMinutes)
                return true;
            if (charge.ChargingOutputName != dbChargeSetup.ChargingOutputName)
                return true;
            if (charge.FromTime != dbChargeSetup.FromTime)
                return true;
            if (charge.ToTime != dbChargeSetup.ToTime)
                return true;
            if (charge.DaysOfWeek != dbChargeSetup.DaysOfWeek)
                return true;
            if (charge.PriceBelow != dbChargeSetup.PriceBelow)
                return true;
            if (charge.ScheduleNickName != dbChargeSetup.ScheduleNickName)
                return true;
            if ((!string.IsNullOrEmpty(charge.SingleChargeDay) ? DateTime.ParseExact(charge.SingleChargeDay, "dd-MM-yyyy", CultureInfo.InvariantCulture) : (DateTime?)null) != dbChargeSetup.SingleChargeDay)
                return true;
            return false;
        }

        private void ValidateInput(ChargeScheduleRequestData chargeSchedule, string timeZoneUTCDifference)
        {
            ValidateScheduleType(chargeSchedule.ScheduleType);
            if (chargeSchedule.ChargeSchedules == null)
                throw new FormatException("Charge schedules are missing!!");
            List<ChargeScheduleData> singleChargesNotSaved = new List<ChargeScheduleData>();
            ValidateThatSingleChargeSchedulesAreNotInTheSameTime(chargeSchedule.ChargeSchedules);
            
            foreach (ChargeScheduleData charge in chargeSchedule.ChargeSchedules)
            {
                ValidateScheduleType(charge.ScheduleType);
                ValidateDaysOfWeek(charge.DaysOfWeek);
                ValidateFromToTime(charge.FromTime, charge.ToTime);
                if (!ValidateSingleCharge(charge, timeZoneUTCDifference))
                    singleChargesNotSaved.Add(charge);
                if (charge.ScheduleType.Trim().ToUpper() == TypeOfSchedule.Single.ToString().ToUpper() && string.IsNullOrEmpty(charge.SingleChargeDay))
                    throw new FormatException("Cannot have single charge with no day set.");
                if (charge.ScheduleType.Trim().ToUpper() != TypeOfSchedule.Single.ToString().ToUpper() && !string.IsNullOrEmpty(charge.SingleChargeDay))
                    throw new FormatException("Cannot have single charge day set when type is not single.");
            }
            chargeSchedule.ChargeSchedules = chargeSchedule.ChargeSchedules.Except(singleChargesNotSaved).ToList();
        }

        private void ValidateThatSingleChargeSchedulesAreNotInTheSameTime(
            IReadOnlyCollection<ChargeScheduleData> chargeSchedules)
        {
            foreach (var chargeSchedule in chargeSchedules)
            {
                var otherSchedules = chargeSchedules
                    .Where(sch => !sch.ScheduleNickName.Equals(chargeSchedule.ScheduleNickName));

                if (otherSchedules.Any(otherSchedule => chargeSchedule.FromTime.Equals(otherSchedule.FromTime) &&
                                                        chargeSchedule.ToTime.Equals(otherSchedule.ToTime) &&
                                                        chargeSchedule.SingleChargeDay != null && 
                                                        chargeSchedule.SingleChargeDay.Equals(otherSchedule
                                                            .SingleChargeDay)))
                {
                    throw new BadRequestException("You cannot save identical single schedules");
                }
            }
        }

        private bool ValidateSingleCharge(ChargeScheduleData charge, string timeZone)
        {
            if (!string.IsNullOrEmpty(charge.SingleChargeDay))
            {
                if (string.IsNullOrEmpty(charge.FromTime) && string.IsNullOrEmpty(charge.ToTime))
                    throw new FormatException("For SingleCharge please enter from&to time.");

                DateTime singleChargeDate = DateTime.ParseExact(charge.SingleChargeDay, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                Helper.ConvertTimeZoneToHourMinute(timeZone, out int hourUTCOffset, out int minuteUTCOffset);
                DateTime nowDateWithTimezone = DateTime.Now.AddHours(hourUTCOffset).AddMinutes(minuteUTCOffset);
                string[] toTimeSplit = charge.ToTime.Split(':');
                int toHour = Int32.Parse(toTimeSplit.First());
                int toMinutes = Int32.Parse(toTimeSplit.Last());
                DateTime chargeDate = singleChargeDate.AddHours(toHour).AddMinutes(toMinutes);
                if (chargeDate < nowDateWithTimezone)
                    return false;
                //throw new Exception("Please select a date that's in the future.");
            }
            return true;

        }

        private void ValidateFromToTime(string fromTime, string toTime)
        {
            string pattern = @"^([0-1][0-9]|2[0-3]):[0-5][0-9]$";
            Regex rg = new Regex(pattern);
            if (!string.IsNullOrEmpty(fromTime) && !rg.IsMatch(fromTime))
                throw new FormatException("From time not in correct format, should be HH:MM");
            if (!string.IsNullOrEmpty(toTime) && !rg.IsMatch(toTime))
                throw new FormatException("To time not in correct format, should be HH:MM");
        }

        private void ValidateDaysOfWeek(string daysOfWeek)
        {
            string pattern = @"^[0-6](?(,[0-6]),[0-6])(?(,[0-6]),[0-6])(?(,[0-6]),[0-6])(?(,[0-6]),[0-6])(?(,[0-6]),[0-6])(?(,[0-6]),[0-6])$";
            Regex rg = new Regex(pattern);
            if (!string.IsNullOrEmpty(daysOfWeek) && !rg.IsMatch(daysOfWeek))
                throw new FormatException("Days of week is in bad format, should be dayOfWeek,dayOfWeek,..(0=Sun,1=Mon..)");
        }

        private void ValidateScheduleType(string scheduleType)
        {
            if (string.IsNullOrEmpty(scheduleType) || scheduleType.Trim().ToUpper() != TypeOfSchedule.Budget.ToString().ToUpper() &&
                scheduleType.Trim().ToUpper() != TypeOfSchedule.Single.ToString().ToUpper() &&
                scheduleType.Trim().ToUpper() != TypeOfSchedule.Scheduled.ToString().ToUpper())
                throw new FormatException("Schedule type is not in a correct format!!");
        }

        async Task IChargeSchedulesBL.SetIsActiveSchedule(Guid deviceId, string scheduleType, bool isActive, string userCognitoId)
        {
            ValidateScheduleType(scheduleType);
            Device device = await _deviceRepository.GetDeviceWithChargeScheduleById(deviceId, userCognitoId);
            if (device == null)
                throw new UnauthorizedAccessException("Access denied!!");
            List<DeviceChargesSetup> filteredSchedules = device.ChargeSchedules.Where(c => c.ScheduleType.Trim().ToUpper() == scheduleType.Trim().ToUpper()).ToList();
            foreach (DeviceChargesSetup chargeSetup in filteredSchedules)
            {
                await _chargeScheduleRepository.MoveChargeToHistoryTable(chargeSetup);
                chargeSetup.IsActive = isActive;
                chargeSetup.LastUpdated = DateTime.Now;
                await _chargeScheduleRepository.AddOrUpdate(chargeSetup);
            }
            _chargeScheduleRepository.CommitChanges();
        }

        private List<Device> GetServerDevices(Hub hubData)
        {
            var result = _apiCallerHelper.GetExistingHubStatusResult(_mapper.Map<HubData>(hubData)).Result;

            if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
            {
                return new List<Device>();
            }

            var resultTuple = (Tuple<string, List<AppServer.Model.DeviceData>>) result.Content;
            var castDevicesContent = resultTuple.Item2;
                
            var serverDevices = _mapper.Map<List<AppServer.Model.DeviceData>, List<Device>>(castDevicesContent);
            return !serverDevices.Any() ? new List<Device>() : serverDevices;
        }
    }
}
