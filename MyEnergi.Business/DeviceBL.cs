﻿using AutoMapper;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Common.CustomMappers;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static MyEnergi.Common.Enumerators;

namespace MyEnergi.Business
{
    public class DeviceBL : IDeviceBL
    {
        private readonly IUserRepository _userRepository;
        private readonly IHubRepository _hubRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IInstallerRepository _installerRepository;
        private readonly IUserFeaturePermissionRepository _userFeaturePermissionRepository;
        private readonly IUserFeaturePermissionHistoryRepository _userFeaturePermissionHistoryRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IChargeScheduleRepository _chargeScheduleRepository;
        private readonly IApiCallerHelper _apiCallerHelper;
        private readonly IMapper _mapper;
        private readonly IDeleteDeviceRequestEmailSender _deleteDeviceRequestEmailSender;
        private readonly IGuestAccessRemovalEmailSender _guestAccessRemovalEmailSender;
        private readonly IDeviceHistoryRepository _deviceHistoryRepository;
        private readonly IInvitationStatusRepository _invitationStatusRepository;

        public DeviceBL(IUserRepository userRepository, IHubRepository hubRepository,
            IDeviceRepository deviceRepository, IInstallerRepository installerRepository,
            IUserFeaturePermissionRepository userFeaturePermissionRepository,
            IUserFeaturePermissionHistoryRepository userFeaturePermissionHistoryRepository,
            IAddressRepository addressRepository,
            IChargeScheduleRepository chargeScheduleRepository, IApiCallerHelper apiCallerHelper, IMapper mapper,
            IDeleteDeviceRequestEmailSender deleteDeviceRequestEmailSender,
            IGuestAccessRemovalEmailSender guestAccessRemovalEmailSender,
            IDeviceHistoryRepository deviceHistoryRepository, 
            IInvitationStatusRepository invitationStatusRepository)
        {
            _userRepository = userRepository;
            _hubRepository = hubRepository;
            _deviceRepository = deviceRepository;
            _installerRepository = installerRepository;
            _userFeaturePermissionRepository = userFeaturePermissionRepository;
            _userFeaturePermissionHistoryRepository = userFeaturePermissionHistoryRepository;
            _addressRepository = addressRepository;
            _apiCallerHelper = apiCallerHelper;
            _mapper = mapper;
            _deleteDeviceRequestEmailSender = deleteDeviceRequestEmailSender;
            _chargeScheduleRepository = chargeScheduleRepository;
            _deviceHistoryRepository = deviceHistoryRepository;
            _invitationStatusRepository = invitationStatusRepository;
            _guestAccessRemovalEmailSender = guestAccessRemovalEmailSender;
        }

        public async Task AddNewDevice(List<DeviceData> deviceData, string userCognitoId, bool addedWithoutHub)
        {
            string errorMessage = null;

            if (deviceData.Count != deviceData.Distinct().Count())
            {
                throw new Exception(
                    "Please enter distinct devices.");
            }

            foreach (var device in deviceData)
            {
                if (device.Address == null)
                    throw new Exception(
                        "Energy setup not saved, please press Back twice to re-enter your details.");
                var address = await _addressRepository.GetAddressByIdAndUserId(device.Address.Id, userCognitoId);
                if (address == null)
                    throw new UnauthorizedAccessException("Access denied!!");
                var dbDevice = await _deviceRepository.GetDeviceBySerialNoIsDeleted(device.SerialNumber);
                if (dbDevice == null)
                {
                    dbDevice = _mapper.Map<Device>(device);
                    dbDevice.Address = address;
                    if (device.Hub != null)
                    {
                        var dbHub = await _hubRepository.GetHubBySerialNoAndUserId(device.Hub.SerialNo, userCognitoId);
                        if (dbHub != null)
                            dbDevice.Hub = dbHub;
                    }

                     if (addedWithoutHub) dbDevice.Status = (int)DeviceStatus.NewByUser;
                    await _deviceRepository.AddOrUpdate(dbDevice);
                }
                else if (!addedWithoutHub || dbDevice.IsDeleted)
                {
                    dbDevice.Address = address;
                    dbDevice.DeviceName = device.DeviceName;
                    if (dbDevice.IsDeleted)
                    {
                        dbDevice.IsDeleted = false;
                        dbDevice.Status = (int)DeviceStatus.NewByUser;
                    }
                    else
                    {
                        dbDevice.Hub =
                            await _hubRepository.GetHubBySerialNoAndUserId(device.Hub.SerialNo, userCognitoId);
                    }

                    dbDevice.HeaterOutput1 = device.HeaterOutput1;
                    dbDevice.HeaterOutput2 = device.HeaterOutput2;
                    dbDevice.TimeZoneRegion = device.TimeZoneRegion;
                    dbDevice.Firmware = device.Firmware;
                    await _deviceRepository.AddOrUpdate(dbDevice);
                }
                else errorMessage += device.SerialNumber + " ";
            }

            if (errorMessage != null)
                throw new Exception("Devices with the following serial numbers: " + errorMessage +
                                    " already exist in the database.");
            _hubRepository.CommitChanges();
        }

        public async Task<List<DeviceFeedbackData>> GetDevicesWithInstallers(string userId)
        {
            var hubs = (await _hubRepository.GetHubByUserId(userId))
                .Where(h => h.Installation == null).ToList();

            var result = new List<DeviceFeedbackData>();
            foreach (var hub in hubs)
            {
                var installerByCountry = await _installerRepository.GetInstallersByCountry(hub.Address.CountryCode);
                var deviceFeedbackData = new DeviceFeedbackData
                {
                    DeviceSerialNumber = hub.SerialNo,
                    DeviceType = "hub",
                    Id = hub.Guid,
                    Installers = _mapper.Map<List<InstallerData>>(installerByCountry)
                };
                result.Add(deviceFeedbackData);
            }

            var devices = (await _deviceRepository.GetDevicesByUserId(userId))
                .Where(d => d.Installation == null).ToList();

            foreach (var device in devices)
            {
                var installersByCountry = await _installerRepository.GetInstallersByCountry(device.Address.CountryCode);
                var deviceFeedBackData = new DeviceFeedbackData
                {
                    DeviceSerialNumber = device.SerialNumber,
                    DeviceType = device.DeviceType,
                    Id = device.Guid,
                    Installers = _mapper.Map<List<InstallerData>>(installersByCountry)
                };
                result.Add(deviceFeedBackData);
            }

            return result;
        }

        public async Task<List<DeviceFeedbackData>> GetDevicesWithInstallers(List<DeviceData> devices)
        {
            var result = new List<DeviceFeedbackData>();
            foreach (var deviceData in devices)
            {
                var installersByCountry = await _installerRepository.GetInstallersByCountry(deviceData.Address.CountryCode);
                var deviceFeedbackData = new DeviceFeedbackData
                {
                    DeviceSerialNumber = deviceData.SerialNumber,
                    DeviceType = deviceData.DeviceType,
                    Id = deviceData.Id,
                    Hub = deviceData.Hub,
                    Status = deviceData.Status,
                    Installers = _mapper.Map<List<InstallerData>>(installersByCountry)
                };
                result.Add(deviceFeedbackData);
            }

            return result;
        }

        public async Task<List<DeviceData>> GetDevicesByUserId(string userId)
        {
            var ownerDevices = await _deviceRepository.GetDevicesByUserId(userId);
            var ownerDeviceData = _mapper.Map<List<DeviceData>>(ownerDevices);
            var guestDeviceData = await GetGuestDevicesData(userId);

            var allDevices = new List<DeviceData>();
            allDevices.AddRange(ownerDeviceData);
            allDevices.AddRange(guestDeviceData);

            return allDevices;
        }

        public async Task<List<DeviceData>> GetDevicesByUserIdJustOwner(string userId)
        {
            var ownerDevices = await _deviceRepository.GetDevicesByUserId(userId);
            var ownerDeviceData = _mapper.Map<List<DeviceData>>(ownerDevices);

            var allDevices = new List<DeviceData>();
            allDevices.AddRange(ownerDeviceData);

            return allDevices;
        }

        public async Task<List<DeviceData>> GetDevicesByUserIdIsDeleted(string userId)
        {
            var ownerDevice = await _deviceRepository.GetDevicesByUserIdIsDeleted(userId);
            var ownerDeviceData = _mapper.Map<List<DeviceData>>(ownerDevice);
            var guestDeviceData = await GetGuestDevicesDataIsDeleted(userId);

            var allDevices = new List<DeviceData>();
            allDevices.AddRange(ownerDeviceData);
            allDevices.AddRange(guestDeviceData);

            return allDevices;
        }

        public async Task<List<DeviceData>> GetDevicesByHubExceptHarvi(Guid hubId)
        {
            var devices = (await _deviceRepository.GetDevicesByHubId(hubId))
                .Where(x => !string.Equals(x.DeviceType, Enumerators.GetDeviceType(Enumerators.DeviceType.Harvi),
                    StringComparison.CurrentCultureIgnoreCase));

            return _mapper.Map<List<DeviceData>>(devices);
        }

        public async Task<ResponseModel> EditDevice(DeviceData device, string userCognitoId)
        {
            var dbDevice = await _deviceRepository.GetDeviceByIdAndUserId(device.Id, userCognitoId);
            if (dbDevice == null)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            if (dbDevice.Hub != null && device.DeviceName != null && dbDevice.DeviceName != device.DeviceName)
            {
                var result = await _apiCallerHelper.GetKeyValueResult(_mapper.Map<HubData>(dbDevice.Hub),
                    key: dbDevice.DeviceType.ToUpper().First() + dbDevice.SerialNumber,
                    value: device.DeviceName);
                if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
                {
                    return new ResponseModel(result.HttpMessage + " " + result.StatusText, false);
                }
            }

            if (dbDevice.DeviceType == Enumerators.DeviceType.Eddi.ToString().ToLower())
            {
                if (dbDevice.Hub != null && device.HeaterOutput1 != null && dbDevice.HeaterOutput1 != device.HeaterOutput1)
                {
                    var result = await _apiCallerHelper.GetKeyValueResult(_mapper.Map<HubData>(dbDevice.Hub),
                        key: dbDevice.DeviceType.ToUpper().First() + dbDevice.SerialNumber + 1,
                        value: device.HeaterOutput1);
                    if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
                    {
                        return new ResponseModel(result.HttpMessage + " " + result.StatusText, false);
                    }
                    await UpdateChargeScheduleHeaterNickName(dbDevice.Guid, dbDevice.HeaterOutput1, device.HeaterOutput1);
                }

                if (dbDevice.Hub != null && device.HeaterOutput2 != null && dbDevice.HeaterOutput2 != device.HeaterOutput2)
                {
                    var result = await _apiCallerHelper.GetKeyValueResult(_mapper.Map<HubData>(dbDevice.Hub),
                        key: dbDevice.DeviceType.ToUpper().First() + dbDevice.SerialNumber + 2,
                        value: device.HeaterOutput2);
                    if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
                    {
                        return new ResponseModel(result.HttpMessage + " " + result.StatusText, false);
                    }
                    await UpdateChargeScheduleHeaterNickName(dbDevice.Guid, dbDevice.HeaterOutput2, device.HeaterOutput2);
                }
                dbDevice.HeaterOutput1 = device.HeaterOutput1;
                dbDevice.HeaterOutput2 = device.HeaterOutput2;
            }
            dbDevice.DeviceName = device.DeviceName;

            var address = await _addressRepository.GetAddressById(device.Address.Id);
            dbDevice.Address = address;
            await _deviceRepository.AddOrUpdate(dbDevice);
            _hubRepository.CommitChanges();

            return new ResponseModel(true);
        }

        public async Task<DeviceData> GetDeviceBySerialNo(string serialNo)
        {
            var devices = await _deviceRepository.GetDeviceBySerialNo(serialNo);
            return _mapper.Map<DeviceData>(devices);
        }

        public async Task<DeviceData> GetDeviceById(Guid id, string userCognitoId)
        {
            var dbDevice = await _deviceRepository.GetDeviceByIdAndUserId(id, userCognitoId);
            if (dbDevice == null)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            return _mapper.Map<DeviceData>(dbDevice);
        }

        public async Task<List<DeviceData>> GetUserAddedNewDevices(string userId)
        {
            var newDevices = (await GetDevicesByUserIdJustOwner(userId))
                .Where(d => d.Status == DeviceStatus.NewByUser)
                .ToList();

            return newDevices;
        }

        public async Task UpdateDevicesNickname(string userCognitoId)
        {
            var devices = (await _deviceRepository.GetDevicesWithHubsAndChargeSchedulesByUserId(userCognitoId)).ToList();
            var hubs = devices.Select(d => d.Hub).Distinct().ToList();
            foreach (var hub in hubs)
            {
                var result =
                    (Dictionary<string, string>)(await _apiCallerHelper.GetKeyValueResult(_mapper.Map<HubData>(hub)))?.Content;
                if (result != null)
                {
                    if (result.ContainsKey("SITENAME"))
                        hub.NickName = result["SITENAME"];
                    var devicesOfHub = devices.Where(d => d.Hub.Id == hub.Id).ToList();
                    if (devicesOfHub != null)
                    {
                        foreach (var dev in devicesOfHub)
                        {
                            if (result.ContainsKey(dev.DeviceType.ToUpper().First() + dev.SerialNumber))
                                dev.DeviceName = result[dev.DeviceType.ToUpper().First() + dev.SerialNumber];
                            if (result.ContainsKey(dev.DeviceType.ToUpper().First() + dev.SerialNumber + 1))
                            {
                                var chargeScheduleDatas = dev.ChargeSchedules
                                    .Where(c => c.ChargingOutputName.ToUpper() == dev.HeaterOutput1?.ToUpper()).ToList();
                                dev.HeaterOutput1 = result[dev.DeviceType.ToUpper().First() + dev.SerialNumber + 1];
                                chargeScheduleDatas.ForEach(d => { d.ChargingOutputName = dev.HeaterOutput1; });
                            }

                            if (result.ContainsKey(dev.DeviceType.ToUpper().First() + dev.SerialNumber + 2))
                            {
                                var chargeScheduleDatas = dev.ChargeSchedules
                                    .Where(c => c.ChargingOutputName.ToUpper() == dev.HeaterOutput2?.ToUpper()).ToList();
                                dev.HeaterOutput2 = result[dev.DeviceType.ToUpper().First() + dev.SerialNumber + 2];
                                chargeScheduleDatas.ForEach(d => { d.ChargingOutputName = dev.HeaterOutput2; });
                            }
                        }
                    }
                }
            }
            _deviceRepository.CommitChanges();
        }

        public async Task UpdateDeviceInfo()
        {
            var hubs = (await _hubRepository.GetAllHubs()).ToList();
            var devices = (await _deviceRepository.GetDevicesByHubsId(hubs.Select(h => h.Guid))).ToList();

            Parallel.ForEach(hubs, hub =>
            {
                lock (hub)
                {
                    ExecuteOperationsToUpdateDevicesInfo(hub, devices).Wait();
                }
            });

            _hubRepository.CommitChanges();
        }

        public async Task UpdateFirmware(Guid deviceId, string firmware)
        {
            var device = await _deviceRepository.GetDeviceById(deviceId);

            if (device != null)
            {
                device.Firmware = firmware;

                _deviceRepository.CommitChanges();
            }
        }

        public async Task UpdateOutputs(Guid deviceId, string out1, string out2)
        {
            var device = await _deviceRepository.GetDeviceById(deviceId);
            var schedules = (await _chargeScheduleRepository.GetChargeSchedulesByDeviceId(deviceId)).ToList();

            var heater1 = schedules.Where(s => string.Equals(s.ChargingOutputName, device.HeaterOutput1)).ToList();
            heater1.ForEach(h => { h.ChargingOutputName = out1; });
            var heater2 = schedules.Where(s => string.Equals(s.ChargingOutputName, device.HeaterOutput2)).ToList();
            heater2.ForEach(h => { h.ChargingOutputName = out2; });

            device.HeaterOutput1 = out1;
            device.HeaterOutput2 = out2;

            _hubRepository.CommitChanges();
        }

        public async Task<List<DeviceData>> GetGuestDevicesData(string userCognitoId)
        {
            var guestDevices = await GetGuestDevices(userCognitoId);

            var guestDevicesData = _mapper.Map<List<DeviceData>>(guestDevices);
            foreach (var guestDevice in guestDevicesData)
            {
                guestDevice.IsGuestUser = true;
                guestDevice.Hub.Password = KMSCipher.Decrypt(guestDevice.Hub.Password).Result;
            }

            return guestDevicesData;
        }

        public async Task<List<DeviceData>> GetGuestDevicesDataIsDeleted(string userCognitoId)
        {
            var guestDevices = await GetGuestDevicesIsDeleted(userCognitoId);

            var guestDevicesData = _mapper.Map<List<DeviceData>>(guestDevices);
            foreach (var guestDevice in guestDevicesData)
            {
                guestDevice.IsGuestUser = true;
                guestDevice.Hub.Password = KMSCipher.Decrypt(guestDevice.Hub.Password).Result;
            }

            return guestDevicesData;
        }

        private async Task<IEnumerable<Device>> GetGuestDevices(string userCognitoId)
        {
            return (await _userFeaturePermissionRepository
                .GetUserFeaturePermissionsByCognitoId(userCognitoId))
                .Select(ufp => ufp.Device)
                .Distinct()
                .ToList();
        }

        private async Task<IEnumerable<Device>> GetGuestDevicesIsDeleted(string userCognitoId)
        {
            return (await _userFeaturePermissionRepository
                .GetUserFeaturePermissionsByCognitoIdIsDeleted(userCognitoId))
                .Select(ufp => ufp.Device)
                .Distinct()
                .ToList();
        }

        private async Task ExecuteOperationsToUpdateDevicesInfo(Hub hub, IReadOnlyCollection<Device> devices)
        {
            var result = await _apiCallerHelper.GetExistingHubStatusResult(_mapper.Map<HubData>(hub));

            if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
            {
                return;
            }
            var (hubFirmware, devicesCastContent) =
                (Tuple<string, List<AppServer.Model.DeviceData>>)result.Content;

            foreach (var deviceData in devicesCastContent)
            {
                var specificDevice = devices.SingleOrDefault(d => d.SerialNumber == deviceData.SerialNumber);
                if (specificDevice != null)
                {
                    specificDevice.TimeZoneRegion = deviceData.TimeZoneRegion;
                    specificDevice.Firmware = deviceData.Firmware;
                }
            }

            var device = devicesCastContent.FirstOrDefault(f =>
                !string.Equals(f.DeviceType.Trim(), Enumerators.DeviceType.Harvi.ToString(),
                    StringComparison.CurrentCultureIgnoreCase) && !string.IsNullOrEmpty(f.TimeZoneRegion));
            hub.TimeZoneRegion = device?.TimeZoneRegion;
            hub.Firmware = hubFirmware;
        }

        private async Task UpdateChargeScheduleHeaterNickName(Guid dbDeviceId, string oldHeaterOutput, string newHeaterOutput)
        {
            var deviceCharges = (await _chargeScheduleRepository.GetChargeSchedulesByDeviceId(dbDeviceId))
                .Where(d => string.Equals(d.ChargingOutputName.Trim(), oldHeaterOutput.Trim(), StringComparison.CurrentCultureIgnoreCase)).ToList();
            foreach (var charge in deviceCharges)
            {
                charge.ChargingOutputName = newHeaterOutput;
                await _chargeScheduleRepository.AddOrUpdate(charge);
            }
        }

        public async Task DeleteDevice(Guid deviceId, string userCognitoId)
        {
            var dbDevice = await _deviceRepository.GetDeviceByIdAndUserId(deviceId, userCognitoId);
            ValidateThatDeviceIsNotNull(deviceId, dbDevice);

            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserIsNotNull(user);

            var historyDevice = dbDevice.MapToDeviceHistory(user);
            dbDevice.Address = null;
            dbDevice.IsDeleted = true;
            dbDevice.Installation = null;

            var devices = new List<Device> { dbDevice };
            await ExecuteOperationsToRemoveUserFeaturePermissions(devices);

            await _deviceRepository.AddOrUpdate(dbDevice);
            await _deviceHistoryRepository.AddOrUpdate(historyDevice);
            _deviceRepository.CommitChanges();

            await _deleteDeviceRequestEmailSender.SendDeleteDeviceRequestMail(user, dbDevice);
        }

        private static void ValidateThatUserIsNotNull(User user)
        {
            if (user == null)
            {
                throw new NotFoundException("User not found!");
            }
        }

        private static void ValidateThatDeviceIsNotNull(Guid deviceId, Device dbDevice)
        {
            if (dbDevice == null)
            {
                throw new Exception($"There is no existing device with ID {deviceId}.");
            }
        }

        public async Task ExecuteOperationsToRemoveUserFeaturePermissions(IEnumerable<Device> devices)
        {
            var deviceIds = devices.Select(d => d.Guid).ToList();
            var ufpsToDelete = (await _userFeaturePermissionRepository.GetUserFeaturePermissionsByDeviceIds(deviceIds)).ToList();

            var historyUfps = ufpsToDelete.MapToUserFeaturePermissionHistory();

            await _userFeaturePermissionHistoryRepository.AddOrUpdate(historyUfps);
            await _userFeaturePermissionRepository.DeleteMultiple(ufpsToDelete);

            var invitations = ufpsToDelete.Select(ufp => ufp.Invitation).Distinct();

            foreach (var invitation in invitations)
            {
                var dbUfps = (await _userFeaturePermissionRepository.GetBy(inv => inv.Invitation.Guid.Equals(invitation.Guid))).ToList();
                var remainingUfps = dbUfps.Except(ufpsToDelete);
                if (!remainingUfps.Any())
                {
                    if (invitation.InvitationStatus.Status.Equals(Enumerators.InvitationStatus.Accepted.ToString(),
                        StringComparison.InvariantCultureIgnoreCase))
                    {
                        var firstUfp = dbUfps.First();
                        var hubSerialNo = firstUfp.Device.Hub.SerialNo;
                        await _guestAccessRemovalEmailSender.SendGuestAccessRemovalEmail(
                            invitation.InvitationDetails.Email,
                            invitation.InvitationDetails.FirstName, hubSerialNo);
                    }

                    var endedInvitationStatus =
                        await _invitationStatusRepository.GetByStatus(Enumerators.InvitationStatus.Ended.ToString());
                    invitation.InvitationStatus = endedInvitationStatus;
                }
            }
        }
    }
}
