﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.OneSignal;
using MyEnergi.Business.Engines;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyEnergi.AppServer.Interfaces;
using Helper = MyEnergi.Business.Model.Helpers.Helper;

namespace MyEnergi.Business
{
    public class DualTariffEnergyProviderBL : SchedulingEngine, IDualTariffEnergyProviderBL
    {
        private readonly IEnergyProviderRepository _energyProviderRepository;
        private readonly IChargeScheduleRepository _chargeScheduleRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IHubRepository _hubRepository;

        public DualTariffEnergyProviderBL(IEnergyProviderRepository energyProviderRepository,
            IChargeScheduleRepository chargeScheduleRepository, IAddressRepository addressRepository,
            IHubRepository hubRepository, IDeviceRepository deviceRepository, IApiCallerHelper apiCallerHelper,
            IConfiguration configuration, IOneSignal oneSignal, IMapper mapper)
            : base(energyProviderRepository, addressRepository, deviceRepository, apiCallerHelper, configuration,
                oneSignal, mapper)
        {
            _energyProviderRepository = energyProviderRepository;
            _chargeScheduleRepository = chargeScheduleRepository;
            _addressRepository = addressRepository;
            _hubRepository = hubRepository;
        }

        public async Task SaveDualTariffEnergyPrices(string userCognitoId, List<DualTariffRequestData> dualTariffs)
        {
            if (!IsParameterValid(dualTariffs))
            {
                throw new BadRequestException("Parameter is invalid.");
            }

            var energySetup =
                await _energyProviderRepository.GetEnergySetupByIdAndUser(dualTariffs[0].EnergySetupId, userCognitoId);
            if (energySetup == null)
            {
                throw new UnauthorizedAccessException("Id does not exist or user does not have access to it!!");
            }

            var allTariffs = (await _energyProviderRepository.GetDualTariffEnergyPriceByAddress(energySetup)).ToList();

            dualTariffs.ForEach(dualTariff =>
            {
                dualTariff.Tariffs.ForEach(tariff =>
                {
                    allTariffs.RemoveAll(x => x.Guid == tariff.Id);

                    var dualTariffData = tariff.Id == default
                        ? new DualTariffEnergyPrice()
                        : _energyProviderRepository.GetDualTariffEnergyPriceById(tariff.Id).Result;

                    dualTariffData.Days = string.Join(",", dualTariff.Days);
                    dualTariffData.FromMinutes = tariff.FromMinutes;
                    dualTariffData.ToMinutes = tariff.ToMinutes;
                    dualTariffData.Price = tariff.Price;
                    dualTariffData.EnergySetup = energySetup;
                    dualTariffData.CreationDate = tariff.Id == default ? DateTime.UtcNow : dualTariffData.CreationDate;
                    dualTariffData.LastUpdateDate = DateTime.UtcNow;
                    
                    _energyProviderRepository.SaveDualTariffEnergyPrice(dualTariffData).Wait();
                });
            });

            await _energyProviderRepository.DeleteDualTariffEnergyPrice(allTariffs);

            _energyProviderRepository.CommitChanges();
        }

        private bool IsParameterValid(ICollection<DualTariffRequestData> dualTariffs)
        {
            //validation for empty parameter
            if (!dualTariffs.Any())
                return false;

            // validation for empty tariffs
            if (dualTariffs.Any() && dualTariffs.Count == 1 &&
                !dualTariffs.First().Days.Except(new List<int> { 1, 2, 3, 4, 5, 6, 0 }).Any() &&
                !dualTariffs.First().Tariffs.Any()) return true;

            // validation for days grouping
            if (dualTariffs.Any(dualTariff => dualTariff.Days.Any(d => d < 0 || d > 6) ||
                                              dualTariff.Tariffs.Any(t => t.FromMinutes > t.ToMinutes) ||
                                              dualTariffs.Any(dualTariff2 =>
                                                  !dualTariff.Equals(dualTariff2) &&
                                                  dualTariff2.Days.Intersect(dualTariff.Days).Any())))
                return false;

            // validation for intervals overlap
            if (dualTariffs.Any(d => d.Tariffs.Any(x =>
                d.Tariffs.Any(y =>
                    !x.Equals(y) &&
                    x.FromMinutes < y.ToMinutes && y.FromMinutes < x.ToMinutes
                ))))
                return false;

            // validation for complete 0-24 intervals for each day
            return dualTariffs.All(dualTariff => dualTariff.Tariffs.Any() &&
                                                 dualTariff.Tariffs
                                                     .Select(tariff => tariff.ToMinutes - tariff.FromMinutes)
                                                     .Sum() == 1440);
        }

        public async Task<List<DualTariffRequestData>> GetDualTariffEnergyPrices(Guid hubId, string userCognitoId)
        {
            var hub = await _hubRepository.GetHubByIdAndUser(hubId, userCognitoId);
            if (hub == null)
            {
                throw new UnauthorizedAccessException("Id does not exist or user does not have access to it!!");
            }

            var energySetup = await _energyProviderRepository.GetEnergySetupByAddressId(hub.Address.Guid);
            var dualTariffs = await _energyProviderRepository.GetDualTariffEnergyPriceByAddress(energySetup);

            return await Task.FromResult(dualTariffs.GroupBy(d => d.Days).Select(g =>
                new DualTariffRequestData
                {
                    Days = g.Key.Split(',').Select(int.Parse).ToList(),
                    EnergySetupId = g.First().EnergySetup.Guid,
                    Tariffs = g.Select(t => new Tariff
                    {
                        Id = t.Guid,
                        FromMinutes = t.FromMinutes,
                        ToMinutes = t.ToMinutes,
                        Price = t.Price
                    }).ToList()
                }
            ).ToList());
        }

        public async Task GenerateDualTariff(int timeToRun)
        {
            var economyAddressId = (await _addressRepository.GetIsEconomyAddresses()).ToList();
            var charges = (await _chargeScheduleRepository.GetAllEconomyChargesByAddressId(economyAddressId)).ToList();
            charges = charges.Where(c => !string.Equals(c.ScheduleType, Enumerators.TypeOfSchedule.Budget.ToString(),
                StringComparison.CurrentCultureIgnoreCase)).ToList();
            var prices = (await _energyProviderRepository.GetDualTariffEnergyPriceByListOfAddressesId(economyAddressId))
                .ToList();
            var hubDevicesDict = new Dictionary<Hub, List<Device>>();
            foreach (var charge in charges)
            {
                Helper.ConvertTimeZoneToHourMinute(charge.Device.TimeZoneRegion, out int hour, out int minute);
                var timeToRunWithTimeZone = timeToRun + hour * 60 + minute;
                //timeToRunWithTimeZone = minute of day when the schedules begins, when adding timezone 1 for example to 11 pm=1380 minutes then timeToRun=1440 which is 12:00 am =0
                if (timeToRunWithTimeZone >= 1440)
                    timeToRunWithTimeZone -= 1440;
                Helper.ConvertTimeFromStringToMinutes(charge.FromTime, charge.ToTime, out int totalFromMinutes,
                    out int totalToMinutes);
                if (timeToRunWithTimeZone != totalFromMinutes)
                    continue;

                if (totalFromMinutes >= totalToMinutes)
                    totalToMinutes = totalToMinutes + (24 * 60);
                var chargeFrom = DateTime.UtcNow.AddHours(hour).AddMinutes(minute).Date.AddMinutes(totalFromMinutes);
                var chargeTo = DateTime.UtcNow.AddHours(hour).AddMinutes(minute).Date.AddMinutes(totalToMinutes)
                    .AddDays(1);
                var specificPrice = prices.Where(p => p.EnergySetup.Address.Id == charge.Device.Address.Id).ToList();
                var bestPriceIntervals = (await _energyProviderRepository
                    .GetDeviceBestPriceIntervalByHubId(charge.Device.Hub.Id)).ToList();
                bestPriceIntervals = bestPriceIntervals.Where(b => b.Device.Id != charge.Device.Id).ToList();
                var convertedPrices =
                    ConvertToEnergyPriceModel(specificPrice, chargeFrom, chargeTo, bestPriceIntervals);
                ProgramDevice(new List<DeviceChargesSetup> { charge }, convertedPrices, charge.Device, true);
                if (hubDevicesDict.ContainsKey(charge.Device.Hub))
                    if (hubDevicesDict[charge.Device.Hub].Contains(charge.Device))
                        continue;
                    else
                        hubDevicesDict[charge.Device.Hub].Add(charge.Device);
                else
                    hubDevicesDict.Add(charge.Device.Hub, new List<Device> { charge.Device });
            }
            var hubDevicesDictFiltered = hubDevicesDict.Where(x => x.Key.SerialNo == "10613810").ToDictionary(x => x.Key, x => x.Value);
            if (hubDevicesDictFiltered.Any())
            {
                await ConstructData(hubDevicesDictFiltered);
            }
        }
    }
}
