﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer;
using MyEnergi.AppServer.Loaders;
using MyEnergi.AppServer.Model.Entsoe;
using MyEnergi.AppServer.OneSignal;
using MyEnergi.Business.Engines;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CurrencyConverter;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyEnergi.AppServer.Interfaces;
using static MyEnergi.Common.Enumerators;
using Helper = MyEnergi.Business.Model.Helpers.Helper;

namespace MyEnergi.Core
{
    public class EnergyProviderBL : SchedulingEngine, IEnergyProviderBL
    {
        private readonly IEnergyProviderRepository _energyProviderRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IHubRepository _hubRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly ICurrencyConverter _currencyConverter;


        private readonly IMapper _mapper;

        public EnergyProviderBL(IHubRepository hubRepository, IEnergyProviderRepository energyProviderRepository,
            IUserRepository userRepository, IAddressRepository addressRepository, IMapper mapper, IApiCallerHelper apiCallerHelper,
            IConfiguration configuration, IOneSignal oneSignal, IDeviceRepository deviceRepository, ICurrencyConverter currencyConverter)
            : base(energyProviderRepository, addressRepository, deviceRepository, apiCallerHelper, configuration,
                oneSignal, mapper)
        {
            _energyProviderRepository = energyProviderRepository;
            _userRepository = userRepository;
            _addressRepository = addressRepository;
            _mapper = mapper;
            _hubRepository = hubRepository;
            _deviceRepository = deviceRepository;
            _currencyConverter = currencyConverter;
        }

        async Task<List<EnergyProviderData>> IEnergyProviderBL.GetUsedEnergyProviders(bool enableEntsoe)
        {
            var energyProvider = (await _energyProviderRepository.GetUsedEnergyProviders()).ToList();
            if (!enableEntsoe)
                energyProvider = energyProvider.Where(e => !e.Provider.Equals(Constants.Entsoe)).ToList();
            var energyProviderData = _mapper.Map<List<EnergyProviderData>>(energyProvider);
            return energyProviderData;
        }

        async Task IEnergyProviderBL.SaveEnergyProviders(IList<EnergyProviderCSV> providers)
        {
            var dbProviders = (await _energyProviderRepository.GetAllEnergyProviders()).ToList();
            providers = providers.Where(p => !string.IsNullOrEmpty(p.InternalReference.Trim())).ToList();
            var dbProviderIdInFile = new List<int>();
            foreach (var epd in providers)
            {
                var ep = dbProviders.SingleOrDefault(p =>
                    string.Equals(p.InternalReference.Trim(), epd.InternalReference.Trim(),
                        StringComparison.CurrentCultureIgnoreCase));
                if (ep == null)
                {
                    ep = _mapper.Map<EnergyProvider>(epd);
                }
                else
                {
                    _mapper.Map(epd, ep);
                    dbProviderIdInFile.Add(ep.Id);
                }

                await _energyProviderRepository.AddOrUpdate(ep);
            }

            var dbProvidersNotInFile = dbProviders.Where(d => !dbProviderIdInFile.Contains(d.Id)).ToList();
            var dbSetups = (await _energyProviderRepository.GetAllEnergySetups()).ToList();

            dbProvidersNotInFile.ForEach(d =>
            {
                _energyProviderRepository.DeleteEnergyPricesByProvider(d.Id);
                var energySetupsByProvider =
                    dbSetups.Where(p => p.EnergyProv != null && p.EnergyProv.Id == d.Id).ToList();
                if (energySetupsByProvider.Any())
                {
                    energySetupsByProvider.ForEach(y => { y.EnergyProv = null; });
                }
            });
            await _energyProviderRepository.DeleteEnergyProviders(dbProvidersNotInFile);
            _energyProviderRepository.CommitChanges();
        }

        async Task IEnergyProviderBL.SaveEnergySetupList(string userCognitoId, List<EnergyFormRequestData> energySetupList)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            if (user == null)
            {
                throw new Exception("User does not exists in the DB");
            }

            var userEnergySetups =
                (await _energyProviderRepository.GetEnergySetupByUserId(userCognitoId)).ToList();
            var userEnergySetupToBeKept = new List<Guid>();
            var addresses = (await _addressRepository.GetAll()).ToList();

            foreach (var energyModel in energySetupList)
            {
                var address = await _addressRepository.GetAddressByIdAndUserId(energyModel.Address.Id, userCognitoId);
                var mappedAddress = _mapper.Map<Address>(energyModel.Address);
                var addressExists = addresses.Any(x => x.AddressLine1 == mappedAddress.AddressLine1 && x.City == mappedAddress.City && x.Country == mappedAddress.Country && x.CountryCode == mappedAddress.CountryCode && x.PostalCode == mappedAddress.PostalCode);

                if (addressExists && address == null)
                    throw new Exception("Energy Setup was not added because address it's already used.");

                if (address == null)
                {
                    if (energyModel.Address.Id != default)
                        throw new UnauthorizedAccessException("Access denied!!");
                    address = _mapper.Map<Address>(energyModel.Address);
                }
                else
                {
                    address = _mapper.Map(energyModel.Address, address);
                    address.Guid = energyModel.Address.Id;
                }

                address.User = user;
                var energy = userEnergySetups.SingleOrDefault(e => e.Guid == energyModel.EnergySetup.Id);
                if (energy == null)
                {
                    if (energyModel.EnergySetup.Id != default)
                    {
                        throw new UnauthorizedAccessException("Access denied!!");
                    }

                    energy = _mapper.Map<EnergySetup>(energyModel.EnergySetup);
                }
                else
                {
                    energy = _mapper.Map(energyModel.EnergySetup, energy);
                    energy.Guid = energyModel.EnergySetup.Id;
                    userEnergySetupToBeKept.Add(energyModel.EnergySetup.Id);
                }

                energy.EnergyProv =
                    await _energyProviderRepository.GetEnergyProviderByGuid(energyModel.EnergySetup.EnergyProviderId);
                energy.Address = address;
                await _energyProviderRepository.AddUpdateEnergySetup(energy);
            }

            var userEnergySetupsToBeDeleted =
                userEnergySetups.Where(e => !userEnergySetupToBeKept.Contains(e.Guid)).ToList();
            await RemoveEnergySetupAddress(userEnergySetupsToBeDeleted, userCognitoId);
            _energyProviderRepository.CommitChanges();
        }

        async Task<bool> IEnergyProviderBL.SaveEnergyPrices(string userCognitoId, bool manualCall)
        {
            var energySetupList = new List<EnergySetup>();
            var updatedGsp = new List<string>();
            energySetupList = userCognitoId != null
                ? (await _energyProviderRepository.GetEnergySetupByUserId(userCognitoId)).ToList()
                : (await _energyProviderRepository.GetAllEnergySetups()).ToList();
            energySetupList = energySetupList.Where(e => e.EnergyProv != null).ToList();
            // Internal Reference Agile Octopus hardcoding
            energySetupList = energySetupList.Where(e =>
                e.EnergyProv.InternalReference.ToUpper().Contains(Constants.AgileOctopusInternalReference.ToUpper()) &&
                e.Address != null && e.EnergyProv.IsActive).ToList();
            var providerApiCaller = new EnergyProviderApiCaller();
            var prices = (await _energyProviderRepository.GetAllEnergyPrices())
                .Where(e => e.EnergyProv != null).ToList();
            foreach (var energy in energySetupList)
            {
                energy.GridSupplyPoint = providerApiCaller.GetGridSupplyPoint(energy.Address.PostalCode.Trim());
                await _energyProviderRepository.AddUpdateEnergySetup(energy);
                if (string.IsNullOrEmpty(energy.GridSupplyPoint))
                    continue;
                if (updatedGsp.Contains(energy.GridSupplyPoint))
                    continue;
                var lastAvailablePrice = prices.Where(p => p.EnergyProv.Id == energy.EnergyProv.Id &&
                                                           p.GridSupplyPoint == energy.GridSupplyPoint)
                    .OrderByDescending(p => p.ValidTo).FirstOrDefault()?.ValidTo;
                // Check if there system already has the prices for the next day locally
                if (lastAvailablePrice != null && (lastAvailablePrice.Value.Date == DateTime.Now.AddDays(1).Date))
                    continue;

                var results = providerApiCaller.GetEnergyPrices(energy.GridSupplyPoint,
                    energy.EnergyProv.Tariff);
                if (results.Any())
                {
                    var latestDate = results.OrderByDescending(r => r.ValidTo).First().ValidTo;
                    // If the energy provider has not updated their prices yet, retry again after 30 min
                    if (userCognitoId == null && latestDate.Day != DateTime.Now.AddDays(1).Day)
                        return await Task.FromResult(false);

                    await _energyProviderRepository.DeleteEnergyPrices(energy.GridSupplyPoint, energy.EnergyProv.Id);
                    results = results.Where(r => r.ValidTo >= latestDate.AddDays(-2)).ToList();

                    var energyPrices = _mapper.Map<List<EnergyPrice>>(results);
                    var energyProvider = await _energyProviderRepository.GetEnergyProviderByGuid(energy.EnergyProv.Guid);
                    energyPrices.ForEach(e => e.EnergyProv = energyProvider);
                    await _energyProviderRepository.SaveEnergyPrices(energyPrices);
                    updatedGsp.Add(energy.GridSupplyPoint);
                }
            }

            _energyProviderRepository.CommitChanges();
            if (!manualCall)
            {
                await GetChargeSchedulesToBeProcessed(Constants.OctopusAgile);
            }

            return await Task.FromResult(true);
        }

        async Task<List<EnergyFormRequestData>> IEnergyProviderBL.GetEnergySetups(string userCognitoId)
        {
            var setups = (await _energyProviderRepository.GetEnergySetupByUserId(userCognitoId)).ToList();

            return setups.Select(setup => new EnergyFormRequestData
            {
                Address = _mapper.Map<AddressData>(setup.Address),
                EnergySetup = _mapper.Map<EnergySetupData>(setup)
            }).ToList();
        }

        async Task<List<AddressData>> IEnergyProviderBL.GetUserEnergyAddresses(string userCognitoId)
        {
            var addresses = (await _addressRepository.GetUserEnergyAddresses(userCognitoId)).ToList();
            return _mapper.Map<List<AddressData>>(addresses);
        }

        async Task<GraphEnergyPriceListData> IEnergyProviderBL.GetBestPriceIntervalsForGraphs(Guid hubId, string userCognitoId)
        {
            var graphEnergyPriceListData = new GraphEnergyPriceListData();
            var hub = await _hubRepository.GetHubByIdAndUser(hubId, userCognitoId);
            if (hub == null)
            {
                hub = await _hubRepository.GetGuestHubByHubIdAndCognitoId(hubId, userCognitoId);
                if (hub == null)
                {
                    throw new UnauthorizedAccessException($"Hub with id {hubId} does not exist or access denied");
                }
            }

            var energySetup = await _energyProviderRepository.GetEnergySetupByAddressId(hub.Address.Guid);
            if (energySetup == null)
            {
                throw new ArgumentNullException($"EnergySetup with addressId {hub.Address.Id} does not exist");
            }

            if (energySetup.EnergyProv == null)
            {
                throw new ArgumentNullException(
                    $"EnergySetup with addressId {hub.Address.Id} does not have a energy provider");
            }

            var energyPrices = (await _energyProviderRepository
                .GetEnergyPrices(energySetup.EnergyProv.Id, energySetup.GridSupplyPoint))
                .OrderBy(e => e.ValidFrom)
                .ToList();
            if (energyPrices.Any())
            {
                graphEnergyPriceListData.Currency = energyPrices.First().Currency;
                graphEnergyPriceListData.energyPrices = _mapper.Map<List<EnergyPriceData>>(energyPrices);
                AddTimeZone(hub.TimeZoneRegion, graphEnergyPriceListData.energyPrices);
                var tomorrowPrices =
                    graphEnergyPriceListData.energyPrices.Where(
                        e => e.ValidFrom.Date == DateTime.UtcNow.AddDays(1).Date);
                if (tomorrowPrices.Any())
                    graphEnergyPriceListData.energyPrices = tomorrowPrices.ToList();
                else
                    graphEnergyPriceListData.energyPrices = graphEnergyPriceListData.energyPrices
                        .Where(e => e.ValidFrom.Date == DateTime.UtcNow.Date).ToList();

                CalculateIntervals(graphEnergyPriceListData.energyPrices, graphEnergyPriceListData, 6,
                    true); //hardcoded to calculate best(cheapest) 6 half hours
                CalculateIntervals(graphEnergyPriceListData.energyPrices, graphEnergyPriceListData, 3,
                    false); //hardcoded to calculate best(cheapest) 3 half hours
                // if times are continuous, then merge them into one
                if (graphEnergyPriceListData.FromInterval1 == graphEnergyPriceListData.ToInterval2)
                {
                    graphEnergyPriceListData.FromInterval1 = graphEnergyPriceListData.FromInterval2;
                    graphEnergyPriceListData.FromInterval2 = null;
                    graphEnergyPriceListData.ToInterval2 = null;
                }
                else if (graphEnergyPriceListData.ToInterval1 == graphEnergyPriceListData.FromInterval2)
                {
                    graphEnergyPriceListData.ToInterval1 = graphEnergyPriceListData.ToInterval2;
                    graphEnergyPriceListData.FromInterval2 = null;
                    graphEnergyPriceListData.ToInterval2 = null;
                }
            }

            return graphEnergyPriceListData;
        }

        private void CalculateIntervals(IReadOnlyCollection<EnergyPriceData> energyPrices,
            GraphEnergyPriceListData graphEnergyPriceListData, int nbOfIntervals, bool firstInterval)
        {
            var bestPriceIntervals = new List<PriceCalculationObject>();
            for (var i = 0; i <= energyPrices.Count - nbOfIntervals; i++)
            {
                var filteredEnergyPrice = energyPrices.Skip(i).Take(nbOfIntervals).ToList();
                if (filteredEnergyPrice.Any(f => f.AddedToCalculations))
                    continue;
                var priceCalculation = new PriceCalculationObject
                {
                    From = filteredEnergyPrice.First().ValidFrom,
                    To = filteredEnergyPrice.Last().ValidTo,
                    Average = filteredEnergyPrice.Average(a => a.ValueWithVAT)
                };
                bestPriceIntervals.Add(priceCalculation);
            }

            var cheapestInterval = bestPriceIntervals.OrderBy(a => a.Average).First();
            if (firstInterval)
            {
                graphEnergyPriceListData.FromInterval1 = cheapestInterval.From;
                graphEnergyPriceListData.ToInterval1 = cheapestInterval.To;
            }
            else
            {
                graphEnergyPriceListData.FromInterval2 = cheapestInterval.From;
                graphEnergyPriceListData.ToInterval2 = cheapestInterval.To;
            }

            energyPrices.Where(e => e.ValidFrom >= cheapestInterval.From && e.ValidTo <= cheapestInterval.To).ToList()
                .ForEach(e => { e.AddedToCalculations = true; });
        }

        async Task IEnergyProviderBL.ProgramDevicesOnDemand(Guid deviceId, string userCognitoId)
        {
            var device = await _deviceRepository.GetDeviceWithChargeScheduleById(deviceId, userCognitoId);
            if (device == null)
                return;
            if (!device.ChargeSchedules.Any(d => d.IsActive))
            {
                await DeleteProgramFromDevice(device);
            }
            else
            {
                await GetChargeSchedulesToBeProcessed(null,device);
            }
        }

        private async Task DeleteProgramFromDevice(Device device)
        {
            Helper.ConvertTimeZoneToHourMinute(device.TimeZoneRegion, out int hourUTCOffset, out int minuteUTCOffset);
            var now = DateTime.Now.AddHours(hourUTCOffset).AddMinutes(minuteUTCOffset);
            await _energyProviderRepository.DeleteBestPriceIntervals(device.Guid, now);
            _energyProviderRepository.CommitChanges();
            var deviceSlots1 = new List<string> { "11", "12", "13", "14" };
            var deviceSlots2 = new List<string> { "21", "22", "23", "24" };
            UpdateHubHostServer(device);
            Parallel.For(0, deviceSlots1.Count,
                i => { SetNewTimeSlotForDevices(device, new List<EnergyPriceData>(), deviceSlots1[i]); });
            if (string.Equals(device.DeviceType.Trim(), DeviceType.Eddi.ToString().Trim(),
                StringComparison.CurrentCultureIgnoreCase))
            {
                Parallel.For(0, deviceSlots2.Count,
                    i => { SetNewTimeSlotForDevices(device, new List<EnergyPriceData>(), deviceSlots2[i]); });
            }
        }

        private async Task RemoveEnergySetupAddress(IEnumerable<EnergySetup> energySetupId, string userCognitoId)
        {
            foreach (var energySetup in energySetupId)
            {
                var primaryAddress = (await _addressRepository.GetAddresses(userCognitoId))
                        .OrderBy(a => a.Id)
                        .FirstOrDefault();
                if (primaryAddress == null)
                {
                    throw new ArgumentNullException("There is no primary address on this user!!");
                }

                var energySetupAddress = energySetup.Address;
                await _energyProviderRepository.RemoveEnergySetup(energySetup);
                if (primaryAddress.Id != energySetupAddress.Id)
                {
                    await _addressRepository.Delete(energySetupAddress);
                }
            }
        }

        async Task IEnergyProviderBL.ValidateEnergySetupRemoval(Guid energySetupId, string userCognitoId)
        {
            var energySetup = await _energyProviderRepository.GetEnergySetupByIdAndUser(energySetupId, userCognitoId);
            if (energySetup == null)
            {
                throw new UnauthorizedAccessException("Id does not exist or user does not have access to it!!");
            }

            var hubsOnAddress = await _hubRepository.CheckIfHubExistsOnAddress(energySetup.Address.Guid);
            var devicesOnAddress = await _deviceRepository.CheckIfDevicesExistsOnAddress(energySetup.Address.Guid);
            if (hubsOnAddress || devicesOnAddress)
            {
                throw new InvalidOperationException(
                    "Before removing this address, please ensure there are no hubs/devices associated with it.");
            }
        }
        async Task IEnergyProviderBL.GenerateOctopus()
        {
            await GetChargeSchedulesToBeProcessed(Constants.OctopusAgile);
        }
        async Task<bool> IEnergyProviderBL.SaveEntsoeEnergyPrices()
        {
            var entsoeEnergyProviders = await _energyProviderRepository.GetEnergyProvidersByField(Constants.Entsoe);
            var startDate = DateTime.UtcNow.AddDays(-1).Date.AddHours(22).ToString("yyyyMMddhh00");

            var endDate = DateTime.UtcNow.Date.AddHours(46).ToString("yyyyMMddhh00");
            var exchangeRate = await _currencyConverter.ConvertAsync(1, "EUR", "SEK");
            bool checkForPriceUpdate = true;
            foreach (EnergyProvider provider in entsoeEnergyProviders)
            {
                var url = $"https://transparency.entsoe.eu/api?securityToken=d3f53bd3-e669-4562-a105-c91f48ca3a02&documentType=A44&in_Domain={provider.InternalReference}&out_Domain={provider.InternalReference}&periodStart={startDate}&periodEnd={endDate}";
                var loader = new EntsoePricesLoader(url);
                var result = loader.GetResult().Result;
                if (result.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    EntoseAPIResponse response = (EntoseAPIResponse)result.Content;
                    if (checkForPriceUpdate)
                    {
                        var lastDate = response.Prices.OrderByDescending(p => p.StartTime).FirstOrDefault().StartTime;
                        if (lastDate.Date == DateTime.UtcNow.AddDays(1).Date)
                            checkForPriceUpdate = false;
                        else 
                            return await Task.FromResult(false);
                    }
                    await _energyProviderRepository.DeleteEnergyPricesByProvider(provider.Id);
                    var energyPrices = ConvertToEnergyPrice(response, provider, exchangeRate);
                    await _energyProviderRepository.SaveEnergyPrices(energyPrices);
                    _energyProviderRepository.CommitChanges();
                }
            }
            await GetChargeSchedulesToBeProcessed(Constants.Entsoe);
            return await Task.FromResult(true);

        }

        private List<EnergyPrice> ConvertToEnergyPrice(EntoseAPIResponse entsoePrices, EnergyProvider provider, double exchangeRate)
        {
            var energyPricesList = new List<EnergyPrice>();
            foreach (EntsoePriceData price in entsoePrices.Prices)
            {
                energyPricesList.Add(new EnergyPrice()
                {
                    Currency = "kr",
                    EnergyProv = provider,
                    ValidFrom = price.StartTime,
                    ValidTo = price.EndTime,
                    // From EURO/MWH to SEK/KWH
                    ValueWithoutVAT = (float)Math.Round(price.Price * exchangeRate/1000,3),
                    ValueWithVAT = (float)Math.Round(price.Price * exchangeRate / 1000, 3)
                });
            }
            return energyPricesList;
        }
    }
}

internal class PriceCalculationObject
{
    public DateTime From { get; set; }
    public DateTime To { get; set; }
    public decimal Average { get; set; }
}