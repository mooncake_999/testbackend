﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer;
using MyEnergi.AppServer.Model;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.AppServer.OneSignal;
using static MyEnergi.Common.Enumerators;
using Helper = MyEnergi.Business.Model.Helpers.Helper;
using MyEnergi.Common;

namespace MyEnergi.Business.Engines
{
    public class SchedulingEngine
    {
        private readonly IEnergyProviderRepository _energyProviderRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IApiCallerHelper _apiCallerHelper;
        private readonly IOneSignal _oneSignal;
        private readonly IMapper _mapper;

        private readonly string _heading;
        private readonly string _contentHeading;

        private readonly bool _enableDeviceProgramming;

        public SchedulingEngine(IEnergyProviderRepository energyProviderRepository,
            IAddressRepository addressRepository, IDeviceRepository deviceRepository, IApiCallerHelper apiCallerHelper,
            IConfiguration configuration, IOneSignal oneSignal, IMapper mapper)
        {
            _energyProviderRepository = energyProviderRepository;
            _addressRepository = addressRepository;
            _deviceRepository = deviceRepository;
            _apiCallerHelper = apiCallerHelper;
            _oneSignal = oneSignal;
            _mapper = mapper;

            _heading = configuration["OneSignal:Heading"];
            _contentHeading = configuration["OneSignal:ContentHeader"];

            var flag = configuration["Flags:EnableDeviceProgramming"];
            if (flag?.ToUpper() == "TRUE")
            {
                _enableDeviceProgramming = true;
            }
        }

        protected void SetNewTimeSlotForDevices(Device device, IReadOnlyCollection<EnergyPriceData> list, string slotNb)
        {
            if (_enableDeviceProgramming)
            {
                _apiCallerHelper.GetDeviceBoostTimeResult(_mapper.Map<HubData>(device.Hub), device.DeviceType,
                    device.SerialNumber, slotNb, CalculateStartingFromTime(list), CalculateChargeTime(list),
                    CalculateChargeDays(list.FirstOrDefault()?.ValidFrom));
            }
        }

        protected void ProgramDevice(IEnumerable<DeviceChargesSetup> filteredChargeSchedules,
            List<EnergyPriceData> prices,
            Device device, bool isEconomy)
        {
            Helper.ConvertTimeZoneToHourMinute(device.TimeZoneRegion, out int hour, out int minute);
            var nowDateWithTimeZone = DateTime.Now.AddHours(hour).AddMinutes(minute);
            // zappi has only one slot range 1 and eddi has 2 (1,2)
            var deviceSlots1 = new List<string> { "11", "12", "13", "14" };
            var deviceSlots2 = new List<string> { "21", "22", "23", "24" };

            var typesOfSchedulesUsed = new List<DeviceChargesSetup>();

            // create 8 time slot lists
            var timeSlot1 = new List<EnergyPriceData>();
            var timeSlot2 = new List<EnergyPriceData>();
            var timeSlot3 = new List<EnergyPriceData>();
            var timeSlot4 = new List<EnergyPriceData>();
            var timeSlot5 = new List<EnergyPriceData>();
            var timeSlot6 = new List<EnergyPriceData>();
            var timeSlot7 = new List<EnergyPriceData>();
            var timeSlot8 = new List<EnergyPriceData>();

            var isZappi = string.Equals(device.DeviceType.Trim(), DeviceType.Zappi.ToString().Trim(),
                StringComparison.CurrentCultureIgnoreCase);
            var isEddi = string.Equals(device.DeviceType.Trim(), DeviceType.Eddi.ToString().Trim(),
                StringComparison.CurrentCultureIgnoreCase);

            //commented out until BOTH eddi programming cleared
            //int heaterPriority = 0;
            //if (filteredChargeSchedules.Any(c => c.ChargingOutputName.Trim().ToUpper() == "BOTH"))
            //{
            //    ApiCaller apiCaller = new EddiHeaterPriority(device.Hub.SerialNo, device.SerialNumber,Cipher.Decrypt(device.Hub.AppPassword));
            //    var result = apiCaller.GetResult(apiCaller.Username, apiCaller.Password);
            //    Int32.TryParse(result.Content?.ToString(), out heaterPriority);
            //}

            foreach (var charge in filteredChargeSchedules)
            {
                var filteredPrices = FilterPriceList(charge, nowDateWithTimeZone, prices, isEconomy);

                if (!filteredPrices.Any())
                    continue;

                int? intervals = null;
                if (charge.ChargeAmountMinutes != null)
                    intervals = charge.ChargeAmountMinutes.Value / 15 +
                                (charge.ChargeAmountMinutes.Value % 15 != 0 ? 1 : 0);

                if (isZappi)
                {
                    CalculateBestPrice(filteredPrices, timeSlot1, timeSlot2, timeSlot3, timeSlot4, intervals);
                }
                else
                    switch (isEddi)
                    {
                        case true when string.Equals(device.HeaterOutput1?.Trim(), charge.ChargingOutputName.Trim(),
                            StringComparison.CurrentCultureIgnoreCase):
                            CalculateBestPrice(filteredPrices, timeSlot1, timeSlot2, timeSlot3, timeSlot4, intervals);
                            break;
                        case true when string.Equals(device.HeaterOutput2?.Trim(), charge.ChargingOutputName.Trim(),
                            StringComparison.CurrentCultureIgnoreCase):
                            CalculateBestPrice(filteredPrices, timeSlot5, timeSlot6, timeSlot7, timeSlot8, intervals);
                            break;
                        case true when charge.ChargingOutputName.Trim().ToUpper() == "BOTH":
                            CalculateBestPrice(filteredPrices, timeSlot1, timeSlot2, timeSlot3, timeSlot4, intervals);

                            timeSlot5 = timeSlot1;
                            timeSlot6 = timeSlot2;
                            timeSlot7 = timeSlot3;
                            timeSlot8 = timeSlot4;

                            //commented out until BOTH eddi programming cleared
                            //if (heaterPriority == 1)
                            //{
                            //    CalculateBestPrice(filteredPrices, timeSlot1, timeSlot2, timeSlot3, timeSlot4, intervals);
                            //    CalculateBestPrice(filteredPrices, timeSlot5, timeSlot6, timeSlot7, timeSlot8, intervals);
                            //}
                            //else if (heaterPriority == 2)
                            //{
                            //    CalculateBestPrice(filteredPrices, timeSlot5, timeSlot6, timeSlot7, timeSlot8, intervals);
                            //    CalculateBestPrice(filteredPrices, timeSlot1, timeSlot2, timeSlot3, timeSlot4, intervals);
                            //}
                            break;
                    }

                typesOfSchedulesUsed.Add(charge);
            }

            _energyProviderRepository.DeleteBestPriceIntervals(device.Guid, nowDateWithTimeZone);
            _energyProviderRepository.CommitChanges();
            ApplyTimeSlotsToDevices(timeSlot1, timeSlot2, timeSlot3, timeSlot4, device, deviceSlots1,
                typesOfSchedulesUsed);
            if (isEddi)
                ApplyTimeSlotsToDevices(timeSlot5, timeSlot6, timeSlot7, timeSlot8, device, deviceSlots2,
                    typesOfSchedulesUsed);
        }

        protected List<EnergyPriceData> ConvertToEnergyPriceModel(
            IReadOnlyCollection<DualTariffEnergyPrice> specificPrice,
            DateTime chargeFrom, DateTime chargeTo, IReadOnlyCollection<DeviceBestPriceIntervals> bestPriceIntervals)
        {
            var result = new List<EnergyPriceData>();
            var splittedPrices = SplitTariffIntoMinute(specificPrice, chargeFrom, chargeTo);
            for (DateTime i = chargeFrom; i < chargeTo; i = i.AddMinutes(15))
            {
                var quarterHourPrice = new EnergyPriceData
                {
                    ValidFrom = i,
                    ValidTo = i.AddMinutes(15),
                    ValueWithVAT = CalculateValueOfInterval(splittedPrices, i),
                    AlreadyProgrammedToOtherDevice = CheckIfTimeSlotAlreadyUsedByOtherDevice(bestPriceIntervals, i),
                };
                result.Add(quarterHourPrice);
            }

            return result;
        }

        protected void AddTimeZone(string timeZoneUTCDifference, List<EnergyPriceData> prices)
        {         
            prices.ForEach(p =>
            {
                Helper.ConvertTimeZoneToHourMinute(timeZoneUTCDifference, out int hourUTCOffsetFrom, out int minuteUTCOffsetFrom, p.ValidFrom);
                Helper.ConvertTimeZoneToHourMinute(timeZoneUTCDifference, out int hourUTCOffsetTo, out int minuteUTCOffsetTo, p.ValidTo);
                p.ValidFrom = p.ValidFrom.AddHours(hourUTCOffsetFrom);
                p.ValidFrom = p.ValidFrom.AddMinutes(minuteUTCOffsetFrom);
                if (!((hourUTCOffsetFrom != hourUTCOffsetTo) || (minuteUTCOffsetFrom != minuteUTCOffsetTo)))
                {
                    p.ValidTo = p.ValidTo.AddHours(hourUTCOffsetTo);
                    p.ValidTo = p.ValidTo.AddMinutes(minuteUTCOffsetTo);
                }
            });
        }

        protected async Task GetChargeSchedulesToBeProcessed(string energyProvider, Device dev = null)
        {
            //because we will run this daily we only need schedules for current and next day
            var nextChargeTimeFrom = DateTime.Now.Date;
            var nextChargeTimeTo = DateTime.Now.Date.AddDays(2).AddSeconds(-3);
            var dayOfNextChargeTimeFrom = ((int)nextChargeTimeFrom.DayOfWeek).ToString();
            var dayOfNextChargeTimeTo = ((int)nextChargeTimeTo.DayOfWeek).ToString();

            var devices = new List<Device>();
            if (dev == null)
            {
                var addresses = await GetDeviceAddressesBasedOnSelectedEnergyProvider(energyProvider);
                devices = (await _deviceRepository.GetAllDevicesWithChargeScheduleByAddressId(addresses))
                    .OrderBy(d => d.Hub.Id).ToList();
            }
            else
                devices.Add(dev);

            var hubDevicesDict = new Dictionary<Hub, List<Device>>();
            foreach (var device in devices)
            {
                var filteredChargeSchedules = new List<DeviceChargesSetup>();
                // filter all charges by schedule day
                foreach (var charge in device.ChargeSchedules.Where(d => d.IsActive))
                {
                    if (string.Equals(charge.ScheduleType.Trim(), TypeOfSchedule.Single.ToString().Trim(),
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (charge.SingleChargeDay >= nextChargeTimeFrom && charge.SingleChargeDay <= nextChargeTimeTo)
                            filteredChargeSchedules.Add(charge);
                    }
                    else if (string.Equals(charge.ScheduleType.Trim(),
                        TypeOfSchedule.Budget.ToString().Trim(),
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        filteredChargeSchedules.Add(charge);
                    }
                    else if (string.Equals(charge.ScheduleType.Trim(),
                        TypeOfSchedule.Scheduled.ToString().Trim(),
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (charge.DaysOfWeek.Contains(dayOfNextChargeTimeFrom) ||
                            charge.DaysOfWeek.Contains(dayOfNextChargeTimeTo))
                            filteredChargeSchedules.Add(charge);
                    }
                }

                if (!filteredChargeSchedules.Any())
                    continue;

                filteredChargeSchedules =
                    filteredChargeSchedules.OrderByDescending(f => f.ChargeAmountMinutes).ToList();
                var eSetup = await _energyProviderRepository.GetEnergySetupByAddressId(device.Address.Guid);
                if (eSetup?.EnergyProv == null || !eSetup.EnergyProv.IsActive)
                    continue;
                if (eSetup.EnergyProv.IsEconomy)
                {
                    var prices = (await _energyProviderRepository
                        .GetDualTariffEnergyPriceByListOfAddressesId(new List<int> { device.Address.Id })).ToList();
                    var bestPriceIntervals = (await _energyProviderRepository.GetDeviceBestPriceIntervalByHubId(device.Hub.Id))
                        .ToList();
                    bestPriceIntervals = bestPriceIntervals.Where(b => b.Device.Id != device.Id).ToList();
                    var convertedPrices = ConvertToEnergyPriceModel(prices, nextChargeTimeFrom, nextChargeTimeTo,
                        bestPriceIntervals);
                    filteredChargeSchedules = filteredChargeSchedules
                        .Where(c => !string.Equals(c.ScheduleType, TypeOfSchedule.Budget.ToString(),
                            StringComparison.CurrentCultureIgnoreCase)).ToList();
                    ProgramDevice(filteredChargeSchedules, convertedPrices, device, true);
                }
                else
                {
                    var energyPrices = (await _energyProviderRepository
                        .GetEnergyPrices(eSetup.EnergyProv.Id, eSetup.GridSupplyPoint)).ToList();
                    if (!energyPrices.Any())
                    {
                        continue;
                    }

                    var prices = _mapper.Map<List<EnergyPriceData>>(energyPrices);
                    AddTimeZone(device.TimeZoneRegion, prices);
                    var quarterMinutePrices = SplitHalfIntoQuarterPrices(prices);
                    quarterMinutePrices = quarterMinutePrices.OrderBy(p => p.ValueWithVAT).ToList();
                    ProgramDevice(filteredChargeSchedules, quarterMinutePrices, device, false);
                }

                if (hubDevicesDict.ContainsKey(device.Hub))
                    if (hubDevicesDict[device.Hub].Contains(device))
                        continue;
                    else
                        hubDevicesDict[device.Hub].Add(device);
                else
                    hubDevicesDict.Add(device.Hub, new List<Device> { device });
            }
            var hubDevicesDictFiltered = hubDevicesDict.Where(x => x.Key.SerialNo == "10613810").ToDictionary(x => x.Key, x => x.Value);
            if (hubDevicesDictFiltered.Any())
            {
                await ConstructData(hubDevicesDictFiltered);
            }
        }

        private async Task<List<int>> GetDeviceAddressesBasedOnSelectedEnergyProvider(string energyProvider)
        {
            var addresses = new List<int>();
            if (energyProvider.Equals(Constants.OctopusAgile))
                addresses = (await _addressRepository.GetAddressFromEnergySetupBasedOnInternalReference(Constants.AgileOctopusInternalReference)).ToList();
            if (energyProvider.Equals(Constants.Entsoe))
                addresses = (await _addressRepository.GetAddressFromEnergySetupBasedOnProviderName(Constants.Entsoe)).ToList();
            return addresses;
        }

        protected async Task ConstructData(Dictionary<Hub, List<Device>> hubDevices)
        {
            var allProgramedDevices = hubDevices.SelectMany(d => d.Value).ToList();
            var idOfAllProgrammedDevices = allProgramedDevices.Select(d => d.Guid).ToList();
            var bestPriceSlots = (await _energyProviderRepository
                .GetDeviceBestPriceIntervalsByDeviceIds(idOfAllProgrammedDevices))
                .OrderBy(bp => bp.From).ToList();
            var devicesAddr = allProgramedDevices.Select(x => x.Address.Id).ToList();
            var dateFormat = await _addressRepository.GetUserDateFormatPreferenceByAddress(devicesAddr);
            foreach (var (hubToBeNotified, devicesToBeNotified) in hubDevices)
            {
                var devicesInfo = new List<string>();
                var devicesCompleteInfo = new List<string>();
                var result =
                       await _apiCallerHelper.GetKVPairResult(hubToBeNotified.Guid, hubToBeNotified.HostServer, hubToBeNotified.SerialNo, hubToBeNotified.AppPassword);
                var cast = ((IList)result.Content).Cast<PropertiesData>().ToList();
                if (!cast.Any())
                    continue;

                foreach (var device in devicesToBeNotified)
                {
                    var pricesForDevice = bestPriceSlots.Where(bp => bp.Device == device).ToList();
                    if (!pricesForDevice.Any())
                        continue;

                    var nickname = string.IsNullOrEmpty(device.DeviceName) ? string.Empty : $"({device.DeviceName}):";
                    var top = $"{device.DeviceType} {nickname}";
                    var format = dateFormat[device.Address.Id] ?? "MM/dd/yyyy";

                    pricesForDevice.ForEach(p =>
                    {
                        var to = p.From.Date.Equals(p.To.Date)
                            ? p.To.ToString("HH:mm")
                            : $"{p.To.ToString(format)} {p.To:HH:mm}";
                        var bottom = $"{p.From.ToString(format)} {p.From:HH:mm} to {to}";
                        devicesInfo.Add(bottom);
                    });

                    var joinedInfo = string.Join("\n", devicesInfo);
                    var info = string.Join("\n", top, joinedInfo);
                    devicesCompleteInfo.Add(info);
                    devicesInfo.Clear();
                }

                if (!devicesCompleteInfo.Any())
                    continue;
                var schedules = string.Join("\n\n", devicesCompleteInfo);
                var completeContent = string.Join("\n\n", _contentHeading, schedules);
                var postResult = _oneSignal.SetResponse(cast, _heading, completeContent);
                await _oneSignal.PushNotification(postResult, hubToBeNotified.SerialNo);
            }
        }

        private List<EnergyPriceData> FilterPriceList(DeviceChargesSetup charge, DateTime nowDateWithTimeZone,
            List<EnergyPriceData> prices, bool isEconomy)
        {
            var resultedList = new List<EnergyPriceData>();
            prices = prices.Where(p => p.ValidFrom >= nowDateWithTimeZone).ToList();
            if (!isEconomy)
                prices = prices.Where(p => p.ValidTo <= nowDateWithTimeZone.AddHours(24)).ToList();
            if (charge.PriceBelow != null)
                prices = prices.Where(p => p.ValueWithVAT <= charge.PriceBelow.Value).ToList();
            if (!string.IsNullOrEmpty(charge.FromTime) && !string.IsNullOrEmpty(charge.ToTime))
            {
                Helper.ConvertTimeFromStringToMinutes(charge.FromTime, charge.ToTime, out int totalFromMinutes,
                    out int totalToMinutes);
                if (totalFromMinutes >= totalToMinutes)
                    totalFromMinutes = totalFromMinutes - (24 * 60);
                if (charge.SingleChargeDay != null)
                {
                    var nextChargeTimeFrom = charge.SingleChargeDay.Value.Date.AddMinutes(totalFromMinutes);
                    var nextChargeTimeTo = charge.SingleChargeDay.Value.Date.AddMinutes(totalToMinutes);
                    resultedList.AddRange(prices.Where(p =>
                        p.ValidFrom >= nextChargeTimeFrom && p.ValidTo <= nextChargeTimeTo));
                }
                else
                {
                    var nextChargeTimeFrom = DateTime.Now.Date.AddMinutes(totalFromMinutes);
                    var nextChargeTimeTo = DateTime.Now.Date.AddMinutes(totalToMinutes);
                    if (string.IsNullOrEmpty(charge.DaysOfWeek) ||
                        charge.DaysOfWeek.Contains(((int)nextChargeTimeTo.DayOfWeek).ToString()))
                        resultedList.AddRange(prices.Where(p =>
                            p.ValidFrom >= nextChargeTimeFrom && p.ValidTo <= nextChargeTimeTo));
                    if (string.IsNullOrEmpty(charge.DaysOfWeek) ||
                        charge.DaysOfWeek.Contains(((int)(nextChargeTimeTo.AddHours(24)).DayOfWeek).ToString()))
                        resultedList.AddRange(prices.Where(p =>
                            p.ValidFrom >= nextChargeTimeFrom.AddHours(24) &&
                            p.ValidTo <= nextChargeTimeTo.AddHours(24)));
                }
            }
            else
            {
                resultedList.AddRange(prices);
            }

            return resultedList;
        }

        private void ApplyTimeSlotsToDevices(List<EnergyPriceData> timeSlot1, List<EnergyPriceData> timeSlot2,
            List<EnergyPriceData> timeSlot3, List<EnergyPriceData> timeSlot4, Device device,
            IReadOnlyList<string> deviceSlots,
            List<DeviceChargesSetup> typesOfSchedulesUsed)
        {
            if (typesOfSchedulesUsed == null) throw new ArgumentNullException(nameof(typesOfSchedulesUsed));
            //take the lists and order them by date to see which is earliest
            var dictionary = new Dictionary<DateTime, List<EnergyPriceData>>();
            if (timeSlot1.Any())
                dictionary.Add(timeSlot1.First().ValidFrom, timeSlot1);
            if (timeSlot2.Any())
                dictionary.Add(timeSlot2.First().ValidFrom, timeSlot2);
            if (timeSlot3.Any())
                dictionary.Add(timeSlot3.First().ValidFrom, timeSlot3);
            if (timeSlot4.Any())
                dictionary.Add(timeSlot4.First().ValidFrom, timeSlot4);

            var orderedLists = dictionary.OrderBy(k => k.Key);
            var nbOfLists = orderedLists.Count();
            var deviceBestPriceIntervals =
                new ConcurrentBag<DeviceBestPriceIntervals>();
            UpdateHubHostServer(device);
            Parallel.For(0, deviceSlots.Count, i =>
            {
                var list = new List<EnergyPriceData>();
                if (i < nbOfLists)
                {
                    list = orderedLists.ElementAt(i).Value;
                    deviceBestPriceIntervals.Add(SaveBestPriceInterval(device, list, typesOfSchedulesUsed,
                        deviceSlots[i]));
                }

                SetNewTimeSlotForDevices(device, list, deviceSlots[i]);
            });
            if (deviceBestPriceIntervals.Any())
            {
                _energyProviderRepository.SaveBestPriceIntervals(deviceBestPriceIntervals.ToList());
                _energyProviderRepository.CommitChanges();
            }
        }

        protected void UpdateHubHostServer(Device device)
        {
            _apiCallerHelper.GetExistingHubStatusResult(_mapper.Map<HubData>(device.Hub));
        }

        private DeviceBestPriceIntervals SaveBestPriceInterval(Device device, IReadOnlyCollection<EnergyPriceData> list,
            IReadOnlyCollection<DeviceChargesSetup> typesOfSchedulesUsed, string heaterSlot)
        {
            var bestPriceIntervals = new DeviceBestPriceIntervals
            {
                Device = device,
                From = list.First().ValidFrom,
                To = list.Last().ValidTo,
                IsBudgetType = typesOfSchedulesUsed.Any(f =>
                    string.Equals(f.ScheduleType.Trim(), TypeOfSchedule.Budget.ToString().Trim(),
                        StringComparison.CurrentCultureIgnoreCase)),
                IsScheduledType = typesOfSchedulesUsed.Any(f =>
                    string.Equals(f.ScheduleType.Trim(), TypeOfSchedule.Scheduled.ToString().Trim(),
                        StringComparison.CurrentCultureIgnoreCase)),
                IsSingleType = typesOfSchedulesUsed.Any(f =>
                    string.Equals(f.ScheduleType.Trim(), TypeOfSchedule.Single.ToString().Trim(),
                        StringComparison.CurrentCultureIgnoreCase)),
                LastUpdate = DateTime.Now,
                HeaterSlot = heaterSlot,
                SlotPriceValue = list.Sum(s => s.ValueWithVAT)
            };
            return bestPriceIntervals;
        }

        private string CalculateChargeDays(DateTime? nextChargeTime)
        {
            if (nextChargeTime == null)
                return "0000000";
            return ((int)nextChargeTime.Value.DayOfWeek) switch
            {
                0 => "0000001",
                1 => "1000000",
                2 => "0100000",
                3 => "0010000",
                4 => "0001000",
                5 => "0000100",
                6 => "0000010",
                _ => "",
            };
        }

        private string CalculateChargeTime(IReadOnlyCollection<EnergyPriceData> priceSlot)
        {
            // need charging time in format HMM
            if (priceSlot.Count == 0)
                return "000";
            var totalMinutes = priceSlot.Last().ValidTo.Subtract(priceSlot.First().ValidFrom).TotalMinutes;
            var hour = (int)totalMinutes / 60;
            var stringHour = hour.ToString();
            var minutes = (int)totalMinutes % 60;
            var stringMinutes = minutes < 10 ? "0" + minutes : minutes.ToString();
            return stringHour + stringMinutes;
        }

        private string CalculateStartingFromTime(IReadOnlyCollection<EnergyPriceData> priceSlot)
        {
            // need starting time in format HHMM
            if (priceSlot.Count == 0)
                return "0000";
            var hour = priceSlot.First().ValidFrom.Hour;
            var stringHour = hour < 10 ? "0" + hour : hour.ToString();
            var minutes = priceSlot.First().ValidFrom.Minute;
            var stringMinutes = minutes < 10 ? "0" + minutes : minutes.ToString();
            return stringHour + stringMinutes;
        }

        private bool CheckListToAddElement(IList<EnergyPriceData> list, EnergyPriceData price)
        {
            // Device time slot is limited to max 8h = 32 quarter of hour 
            if (list.Count == 32)
                return false;
            if (!list.Any())
            {
                list.Add(price);
                return true;
            }
            else if (list.Last().ValidTo == price.ValidFrom)
            {
                list.Add(price);
                return true;
            }
            else if (list.First().ValidFrom == price.ValidTo)
            {
                list.Insert(0, price);
                return true;
            }

            return false;
        }

        /// <summary>
        /// We need to calculate best prices on an interval of time, that have to be time continuous (each element has time valid from and valid to)
        /// and all the elements in the list need to make a continuos time flow(from 12:00 - 16:00 are 8 elements of 30 min)
        /// and we can have between 1-4 time slots with best price
        /// </summary>
        private void CalculateBestPrice(List<EnergyPriceData> prices, List<EnergyPriceData> first,
            List<EnergyPriceData> second, List<EnergyPriceData> third, List<EnergyPriceData> forth, int? intervals)
        {
            if (!prices.Any()) return;
            prices = prices.OrderBy(p => p.ValueWithVAT).ThenBy(p => p.AlreadyProgrammedToOtherDevice).ToList();
            var notAddedPrices = new List<EnergyPriceData>();
            // we take the prces ordered by cheapest price and add them into lists, then we set a bool on the price to know that it already was processed
            for (var i = 0; i < prices.Count; i++)
            {
                if (prices[i].AddedToCalculations)
                    continue;
                var priceAdded = CheckListToAddElement(first, prices[i]);
                if (!priceAdded)
                    priceAdded = CheckListToAddElement(second, prices[i]);
                if (!priceAdded)
                    priceAdded = CheckListToAddElement(third, prices[i]);
                if (!priceAdded)
                    priceAdded = CheckListToAddElement(forth, prices[i]);

                if (priceAdded)
                {
                    prices[i].AddedToCalculations = priceAdded;
                    if (intervals != null) intervals--;
                    // after each element that was inserted, we need to see if some lists can be merged based on the idea that their elements are not time continuos
                    ShiftValuesFromLists(first, second);
                    ShiftValuesFromLists(first, third);
                    ShiftValuesFromLists(first, forth);
                    ShiftValuesFromLists(second, third);
                    ShiftValuesFromLists(second, forth);
                    ShiftValuesFromLists(third, forth);
                }
                else
                    notAddedPrices.Add(prices[i]);

                if (priceAdded && notAddedPrices.Any())
                {
                    i = 0;
                    notAddedPrices.Clear();
                }

                if (intervals != null && intervals == 0)
                    break;
            }
        }

        private void ShiftValuesFromLists(List<EnergyPriceData> list1, ICollection<EnergyPriceData> list2)
        {
            // Device time slot is limited to max 8h = 32 quarter of hour
            if (list1.Count + list2.Count > 32)
                return;
            if (list1.Any() && list2.Any() && list1.First().ValidFrom == list2.Last().ValidTo)
            {
                list1.InsertRange(0, list2);
                list2.Clear();
            }
            else if (list1.Any() && list2.Any() && list1.Last().ValidTo == list2.First().ValidFrom)
            {
                list1.AddRange(list2);
                list2.Clear();
            }
        }

        private void CreateDateDictionary(DateTime chargeFrom, DualTariffEnergyPrice price,
            IDictionary<DateTime, decimal> result)
        {
            var dateToUse = chargeFrom.Date;
            for (var i = price.FromMinutes; i < price.ToMinutes; i++)
            {
                dateToUse = dateToUse.Date.AddMinutes(i);
                if (!result.ContainsKey(dateToUse))
                {
                    result.Add(dateToUse, price.Price / 60);
                }
            }
        }

        private bool CheckIfTimeSlotAlreadyUsedByOtherDevice(IEnumerable<DeviceBestPriceIntervals> bestPriceIntervals,
            DateTime i)
        {
            return bestPriceIntervals
                .Any(b => b.From <= i && i.AddMinutes(15) <= b.To);
        }

        private decimal CalculateValueOfInterval(Dictionary<DateTime, decimal> splittedPrices, DateTime time)
        {
            return splittedPrices.Where(s => s.Key >= time && s.Key < time.AddMinutes(15)).Sum(s => s.Value);
        }

        private Dictionary<DateTime, decimal> SplitTariffIntoMinute(IReadOnlyCollection<DualTariffEnergyPrice> prices,
            DateTime chargeFrom, DateTime chargeTo)
        {
            var result = new Dictionary<DateTime, decimal>();

            for (DateTime i = chargeFrom.Date; i <= chargeTo.Date; i = i.AddDays(1))
            {
                var filteredPrices = prices.Where(p => p.Days.Contains(((int)i.DayOfWeek).ToString())).ToList();
                foreach (DualTariffEnergyPrice price in filteredPrices)
                {
                    CreateDateDictionary(i, price, result);
                }
            }

            return result;
        }

        private List<EnergyPriceData> SplitHalfIntoQuarterPrices(IEnumerable<EnergyPriceData> prices)
        {
            var result = new List<EnergyPriceData>();
            foreach (var price in prices)
            {
                result.Add(new EnergyPriceData
                {
                    AddedToCalculations = price.AddedToCalculations,
                    GridSupplyPoint = price.GridSupplyPoint,
                    ValueWithoutVAT = price.ValueWithoutVAT,
                    ValueWithVAT = price.ValueWithVAT,
                    ValidFrom = price.ValidFrom,
                    ValidTo = price.ValidTo.AddMinutes(-15)
                });
                result.Add(new EnergyPriceData
                {
                    AddedToCalculations = price.AddedToCalculations,
                    GridSupplyPoint = price.GridSupplyPoint,
                    ValueWithoutVAT = price.ValueWithoutVAT,
                    ValueWithVAT = price.ValueWithVAT,
                    ValidFrom = price.ValidFrom.AddMinutes(15),
                    ValidTo = price.ValidTo
                });
            }

            return result;
        }
    }
}
