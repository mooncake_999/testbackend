﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Helpers;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Data.Interfaces;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class GraphBL : IGraphBL
    {
        private readonly IGraphRepository _graphRepository;
        private readonly IApiCallerHelper _apiCallerHelper;
        private readonly IMapper _mapper;

        public GraphBL(IGraphRepository graphRepository, IApiCallerHelper apiCallerHelper, IMapper mapper)
        {
            _graphRepository = graphRepository;
            _apiCallerHelper = apiCallerHelper;
            _mapper = mapper;
        }

        public async Task<IList<DeviceEnergyInfo>> GetSpecificDataForGraph(Guid hubId, long startTimestamp, string timeZoneRegion)
        {
            var startTimestampUtc = Helper.AdjustZoneToUtc(startTimestamp, timeZoneRegion);

            var utcNowTimestamp = Helper.ConvertToTimestamp(Helper.GetDateTimeHoursOnly(DateTime.UtcNow).AddMinutes(-1));

            var endTimestamp =
                Helper.ConvertToTimestamp(Helper.ConvertTimestampToDate(startTimestampUtc).AddDays(1)
                    .AddMinutes(-1));
            endTimestamp = endTimestamp > utcNowTimestamp ? utcNowTimestamp : endTimestamp;

            var info = await _graphRepository.GetDevicesEnergyInfoByHub(hubId, startTimestampUtc, endTimestamp);
            var mappedInfo = _mapper
                .Map<Dictionary<string, IEnumerable<Data.Entity.RedisModel.DeviceEnergyInfo>>,
                    Dictionary<string, IEnumerable<DeviceEnergyInfo>>>(info).ToList();

            var energyInfo = new List<DeviceEnergyInfo>();
            mappedInfo.ForEach(m =>
            {
                energyInfo.AddRange(InterpolateMissingData(Helper.ConvertTimestampToDate(startTimestampUtc),
                    Helper.ConvertTimestampToDate(endTimestamp), m.Value.ToList()));
            });

            return energyInfo;
        }

        public async Task<IList<DeviceEnergyInfo>> GetSpecificDataForGraphDST(Guid hubId, long startTimestamp, string timeZoneRegion)
        {
            var startTimestampUtc = Helper.AdjustZoneToUtc(startTimestamp, timeZoneRegion);

            var utcNowTimestamp = Helper.ConvertToTimestamp(Helper.GetDateTimeHoursOnly(DateTime.UtcNow).AddMinutes(-1));

            var endTimestamp =
                Helper.ConvertToTimestamp(Helper.ConvertTimestampToDate(startTimestampUtc).AddDays(1)
                    .AddMinutes(-1));
            endTimestamp = endTimestamp > utcNowTimestamp ? utcNowTimestamp : endTimestamp;

            var info = await _graphRepository.GetDevicesEnergyInfoByHubDST(hubId, startTimestampUtc, endTimestamp);
            var mappedInfo = _mapper
                .Map<Dictionary<string, IEnumerable<Data.Entity.RedisModel.DeviceEnergyInfo>>,
                    Dictionary<string, IEnumerable<DeviceEnergyInfo>>>(info).ToList();

            var energyInfo = new List<DeviceEnergyInfo>();
            mappedInfo.ForEach(m =>
            {
                energyInfo.AddRange(InterpolateMissingData(Helper.ConvertTimestampToDate(startTimestampUtc),
                    Helper.ConvertTimestampToDate(endTimestamp), m.Value.ToList()));
            });

            return energyInfo;
        }

        public async Task<IList<DeviceEnergyInfo>> GetSpecificDataForGraphDSTSpring(Guid hubId, long startTimestamp, string timeZoneRegion)
        {
            var startTimestampUtc = Helper.AdjustZoneToUtc(startTimestamp, timeZoneRegion);

            var utcNowTimestamp = Helper.ConvertToTimestamp(Helper.GetDateTimeHoursOnly(DateTime.UtcNow).AddMinutes(-1));

            var endTimestamp =
                Helper.ConvertToTimestamp(Helper.ConvertTimestampToDate(startTimestampUtc).AddDays(1)
                    .AddMinutes(-1));
            endTimestamp = endTimestamp > utcNowTimestamp ? utcNowTimestamp : endTimestamp;

            var info = await _graphRepository.GetDevicesEnergyInfoByHubDSTSpring(hubId, startTimestampUtc, endTimestamp);
            var mappedInfo = _mapper
                .Map<Dictionary<string, IEnumerable<Data.Entity.RedisModel.DeviceEnergyInfo>>,
                    Dictionary<string, IEnumerable<DeviceEnergyInfo>>>(info).ToList();

            var energyInfo = new List<DeviceEnergyInfo>();
            mappedInfo.ForEach(m =>
            {
                energyInfo.AddRange(InterpolateMissingData(Helper.ConvertTimestampToDate(startTimestampUtc),
                    Helper.ConvertTimestampToDate(endTimestamp), m.Value.ToList()));
            });

            return energyInfo;
        }

        public IList<DeviceEnergyInfo> GetActiveDataForGraph(List<DeviceData> devices, DateTime lastUpdate, int count = 0)
        {
            var energyInfo = new List<DeviceEnergyInfo>();

            if (count != 0)
            {
                devices.ForEach(device =>
                {
                    var result = GetEnergyUsageData(device, lastUpdate, count).Result;
                    var interpolatedData = InterpolateMissingData(lastUpdate, DateTime.UtcNow, result);
                    energyInfo.AddRange(interpolatedData);
                });

                return energyInfo;
            }

            var endDateTime = DateTime.UtcNow;
            var startDateTime = lastUpdate;

            int minutes;
            if (startDateTime.Day == endDateTime.Day)
            {
                minutes = (int)(endDateTime - startDateTime).TotalMinutes;
                devices.ForEach(device =>
                {
                    var result = GetEnergyUsageData(device, startDateTime, minutes).Result;
                    var interpolatedData = InterpolateMissingData(startDateTime, endDateTime, result);
                    energyInfo.AddRange(interpolatedData);
                });

                return energyInfo;
            }

            devices.ForEach(device =>
            {
                var allResult = new List<DeviceEnergyInfo>();

                do
                {
                    minutes = (int)startDateTime.Date.AddDays(1).Subtract(startDateTime).TotalMinutes;

                    var date = startDateTime;
                    var result = GetEnergyUsageData(device, date, minutes).Result;
                    allResult.AddRange(result);

                    startDateTime = startDateTime.Date.AddDays(1);
                } while (startDateTime.Date <= endDateTime.Date);

                var interpolatedData = InterpolateMissingData(lastUpdate, endDateTime, allResult);
                energyInfo.AddRange(interpolatedData);
            });

            return energyInfo;
        }

        public IList<object> ProcessDataForTotalGraph(IList<DeviceEnergyInfo> info, string timeZoneRegion)
        {
            var startTime = DateTime.UtcNow;
            Log.Information("Data processing started. ");

            var timeGrouped = info.OrderBy(o => o.QueryDate).GroupBy(t => t.QueryDate)
                .Select(timeGroup => new DeviceEnergyInfo
                {
                    QueryDate = timeGroup.Key,
                    Timestamp = Helper.AdjustUtcToZone(timeGroup.First().Timestamp, timeZoneRegion),
                    Imported = timeGroup.First().Imported,
                    Exported = timeGroup.First().Exported,
                    Consumed = timeGroup.First().Consumed,
                    IsInterpolated = timeGroup.Any(t=>t.IsInterpolated)
                }).ToList();

            var energyInfo = new List<object>
            {
                new
                {
                    Name = "consumed",
                    Data = timeGrouped.Select(x => new object[]{x.Timestamp, Helper.ConvertJoulesToKwh(x.Consumed)})
                },
                new
                {
                    Name = "imported",
                    Data = timeGrouped.Select(x => new object[]{x.Timestamp, Helper.ConvertJoulesToKwh(x.Imported)})
                },
                new
                {
                    Name = "exported",
                    Data = timeGrouped.Select(x => new object[]{x.Timestamp, Helper.ConvertJoulesToKwh(x.Exported)})
                },
                new
                {
                    Interpolated = timeGrouped.Where(x=>x.IsInterpolated).Select(x=>x.Timestamp)
                }
            };

            var endTime = DateTime.UtcNow;
            Log.Information("Data processing ended. ");
            Log.Information($"Data processing duration: {(endTime - startTime).TotalSeconds}. ");
            
            return energyInfo;
        }

        public IList<object> ProcessDataForDeviceGraph(IList<DeviceEnergyInfo> info, string timeZoneRegion)
        {
            var energyInfo = new List<object>();

            foreach (var typeGroup in info.GroupBy(g => g.DeviceType))
            {
                energyInfo.Add(new GraphResponse
                {
                    Name = typeGroup.Key,
                    Data = typeGroup.OrderBy(o => o.QueryDate).GroupBy(t => t.QueryDate).Select(timeGroup => new object[]
                      {

                        Helper.AdjustUtcToZone(timeGroup.First().Timestamp, timeZoneRegion),
                        timeGroup.Sum(i => Helper.ConvertJoulesToKwh(CalculateDevicePower(i)))
                      })
                });
            }

            var homeConsumption = new GraphResponse
            {
                Name = "home",
                Data = info.OrderBy(o => o.QueryDate).GroupBy(t => t.QueryDate).Select(timeGroup => new object[]
                {

                    Helper.AdjustUtcToZone(timeGroup.First().Timestamp, timeZoneRegion),
                    Math.Max(0, Helper.ConvertJoulesToKwh(CalculateHomeConsumption(timeGroup.First().Imported,
                        timeGroup.First().Consumed,
                        timeGroup.Where(x=>x.DeviceType==Enumerators.DeviceType.Eddi.ToString().ToLower()).Sum(CalculateDevicePower),
                        timeGroup.Where(x=>x.DeviceType==Enumerators.DeviceType.Zappi.ToString().ToLower()).Sum(CalculateDevicePower))))
                })
            };

            energyInfo.Add(homeConsumption);

            energyInfo.Add(new
            {
                Interpolated = info.Where(x => x.IsInterpolated).GroupBy(x=>x.QueryDate).Select(x => Helper.AdjustUtcToZone(x.First().Timestamp, timeZoneRegion))
            });

            return energyInfo;
        }

        private int CalculateDevicePower(DeviceEnergyInfo info)
        {
            var diverted = info.H1d + info.H2d + (info.DeviceType == Enumerators.DeviceType.Zappi.ToString().ToLower() ? info.H3d : 0);
            var boosted = info.H1b + info.H2b + (info.DeviceType == Enumerators.DeviceType.Zappi.ToString().ToLower() ? info.H3b : 0);

            return diverted + boosted;
        }

        private List<DeviceEnergyInfo> InterpolateMissingData(DateTime start, DateTime end, List<DeviceEnergyInfo> data)
        {
            var newEnergyInfo = new List<DeviceEnergyInfo>();
            if (data == null || !data.Any()) return newEnergyInfo;

            var i = 0;
            start = Helper.GetDateTimeMinutesOnly(start);
            end = Helper.GetDateTimeMinutesOnly(end.AddMinutes(-1));

            for (var currentTime = start; currentTime <= end; currentTime = currentTime.AddMinutes(1))
            {
                var currentPoint = data[i];

                if (currentPoint == null)
                {
                    newEnergyInfo.Add(new DeviceEnergyInfo
                    {
                        QueryDate = currentTime,
                        Timestamp = Helper.ConvertToTimestamp(currentTime),
                        DeviceType = data.First().DeviceType,
                        IsInterpolated = true
                    });
                    continue;
                }

                if (currentTime == currentPoint.QueryDate)
                {
                    newEnergyInfo.Add(data[i]);
                    if (i + 1 < data.Count) i++;
                    continue;
                }

                var currentIsBefore = currentPoint.QueryDate <= currentTime;
                var prevPoint = currentIsBefore ? currentPoint : (i < 1 ? null : data[i - 1]);
                var nextPoint = currentIsBefore ? (i + 1 >= data.Count ? null : data[i + 1]) : currentPoint;

                if (prevPoint != null && nextPoint != null)
                {
                    var prevDiff = currentTime.Subtract(prevPoint.QueryDate).TotalMinutes;
                    var nextDiff = nextPoint.QueryDate.Subtract(currentTime).TotalMinutes;

                    var missingValue = GetInterpolatedEnergyInfo((int)prevDiff, (int)nextDiff, prevPoint, nextPoint);
                    missingValue.QueryDate = currentTime;
                    missingValue.Timestamp = Helper.ConvertToTimestamp(currentTime);
                    missingValue.DeviceType = prevPoint.DeviceType;
                    missingValue.IsInterpolated = true;

                    newEnergyInfo.Add(missingValue);
                }
                else if (nextPoint != null && nextPoint.QueryDate.Subtract(currentTime).TotalMinutes < 10)
                {
                    var point = _mapper.Map<DeviceEnergyInfo>(nextPoint);
                    point.Timestamp = Helper.ConvertToTimestamp(currentTime);
                    point.QueryDate = currentTime;
                    point.IsInterpolated = true;

                    newEnergyInfo.Add(point);
                }
                else if (prevPoint != null && currentTime.Subtract(prevPoint.QueryDate).TotalMinutes < 10)
                {
                    var point = _mapper.Map<DeviceEnergyInfo>(prevPoint);
                    point.Timestamp = Helper.ConvertToTimestamp(currentTime);
                    point.QueryDate = currentTime;
                    point.IsInterpolated = true;

                    newEnergyInfo.Add(point);
                }
                else
                {
                    newEnergyInfo.Add(new DeviceEnergyInfo
                    {
                        QueryDate = currentTime,
                        Timestamp = Helper.ConvertToTimestamp(currentTime),
                        DeviceType = data.First().DeviceType,
                        IsInterpolated = true
                    });
                }
            }

            return newEnergyInfo;
        }
        
        private int Interpolate(int prevDiff, int nextDiff, int prevValue, int nextValue)
        {
            var x1 = prevDiff;
            var x2 = nextDiff;
            var y = nextValue - prevValue;
            var y1 = (x1 * y) / (x1 + x2);

            return prevValue + y1;
        }

        private DeviceEnergyInfo GetInterpolatedEnergyInfo(int prevDiff, int nextDiff, DeviceEnergyInfo prevPoint, DeviceEnergyInfo nextPoint)
        {
            var missingValue = new DeviceEnergyInfo
            {
                Consumed = Interpolate(prevDiff, nextDiff, prevPoint.Consumed, nextPoint.Consumed),
                Imported = Interpolate(prevDiff, nextDiff, prevPoint.Imported, nextPoint.Imported),
                Exported = Interpolate(prevDiff, nextDiff, prevPoint.Exported, nextPoint.Exported),
                Generated = Interpolate(prevDiff, nextDiff, prevPoint.Generated, nextPoint.Generated),
                H1b = Interpolate(prevDiff, nextDiff, prevPoint.H1b, nextPoint.H1b),
                H1d = Interpolate(prevDiff, nextDiff, prevPoint.H1d, nextPoint.H1d),
                H2d = Interpolate(prevDiff, nextDiff, prevPoint.H2d, nextPoint.H2d),
                H2b = Interpolate(prevDiff, nextDiff, prevPoint.H2b, nextPoint.H2b),
                H3d = Interpolate(prevDiff, nextDiff, prevPoint.H3d, nextPoint.H3d),
                H3b = Interpolate(prevDiff, nextDiff, prevPoint.H3b, nextPoint.H3b)
            };

            return missingValue;
        }

        private int CalculateHomeConsumption(int imported, int consumed, int eddiConsumed, int zappiConsumed)
        {
            return imported + consumed - eddiConsumed - zappiConsumed;
        }

        private async Task<List<DeviceEnergyInfo>> GetEnergyUsageData(DeviceData device, DateTime date, int minutesInterval = 0)
        {
            try
            {
                var result = await _apiCallerHelper.GetDeviceEnergyInfoResult(device.Hub, device.DeviceType,
                    device.SerialNumber, date, minutesInterval);

                var castContent = ((IList)result.Content).Cast<AppServer.Model.GraphData>().ToList();

                var content = _mapper.Map<List<MyEnergi.AppServer.Model.GraphData>, List<DeviceEnergyInfo>>(castContent);
                content.ForEach(x => x.DeviceType = device.DeviceType);
                return content;
            }
            catch (Exception)
            {
                return new List<DeviceEnergyInfo>();
            }
        }
    }

    class GraphResponse
    {
        public string Name { get; set; }
        public IEnumerable<object[]> Data { get; set; }
    }
}
