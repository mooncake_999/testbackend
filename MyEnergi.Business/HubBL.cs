﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Common.CustomMappers;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using static MyEnergi.Common.Enumerators;
using Device = MyEnergi.Data.Entity.Device;
using DeviceData = MyEnergi.Business.Model.Models.DeviceData;
using HubData = MyEnergi.Business.Model.Models.HubData;
using User = MyEnergi.Data.Entity.User;

namespace MyEnergi.Business
{
    public class HubBL : IHubBL
    {
        private readonly IHubRepository _hubRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IInstallerRepository _installerRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IUserFeaturePermissionRepository _userFeaturePermissionRepository;
        private readonly IApiCallerHelper _apiCallerHelper;
        private readonly IDeviceBL _deviceBl;
        private readonly IMapper _mapper;
        private readonly IHubHistoryRepository _hubHistoryRepository;

        public HubBL(IHubRepository hubRepository, IDeviceRepository deviceRepository, IUserRepository userRepository,
            IAddressRepository addressRepository,
            IInstallerRepository installerRepository, IUserFeaturePermissionRepository userFeaturePermissionRepository,
            IApiCallerHelper apiCallerHelper, IDeviceBL deviceBl,
            IMapper mapper, IHubHistoryRepository hubHistoryRepository)
        {
            _hubRepository = hubRepository;
            _deviceRepository = deviceRepository;
            _userRepository = userRepository;
            _addressRepository = addressRepository;
            _installerRepository = installerRepository;
            _userFeaturePermissionRepository = userFeaturePermissionRepository;
            _apiCallerHelper = apiCallerHelper;
            _deviceBl = deviceBl;
            _mapper = mapper;
            _hubHistoryRepository = hubHistoryRepository;
        }

        public async Task<ResponseModel> RegisterNewHub(HubData hubData, string userCognitoId)
        {
            if (hubData.Address == null)
            {
                throw new BadRequestException("Address cannot be empty");
            }

            var response = new ResponseModel("New hub successfully added.", true);

            if (!string.IsNullOrEmpty(hubData.RegistrationCode))
            {
                var password = KMSCipher.GeneratePassword();
                hubData.Password = password;
                hubData.ConfirmPassword = password;
                var regResult = await _apiCallerHelper.GetHubRegisterResult(hubData);

                if (regResult.HttpStatusCode != HttpStatusCode.OK || regResult.Status != 0)
                {
                    response = new ResponseModel(regResult.HttpMessage + " " + regResult.StatusText, false);
                    return response;
                }

                SendHubNickNameToAppServer(hubData);
            }
            else
            {
                hubData.NickName = await GetHubNickName(hubData);
            }

            var result = await _apiCallerHelper.GetExistingHubStatusResult(hubData, true);

            if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
            {
                response = new ResponseModel(result.HttpMessage + " " + result.StatusText, false);
            }
            else
            {
                var (hubFirmware, castDevicesContent) =
                    (Tuple<string, List<AppServer.Model.DeviceData>>)result.Content;
                var content = _mapper.Map<List<AppServer.Model.DeviceData>, List<DeviceData>>(castDevicesContent);
                var device = content.FirstOrDefault(f =>
                    !string.Equals(f.DeviceType.Trim(), DeviceType.Harvi.ToString(),
                        StringComparison.CurrentCultureIgnoreCase) && !string.IsNullOrEmpty(f.TimeZoneRegion));
                hubData.TimeZoneRegion = device?.TimeZoneRegion;
                hubData.Firmware = hubFirmware;
                content.ForEach(d =>
                {
                    d.Address = hubData.Address;
                    d.Hub = hubData;
                    d.DeviceName = GetDeviceNickName(d).Result;

                    if (string.Equals(d.DeviceType.Trim(), DeviceType.Eddi.ToString(),
                        StringComparison.CurrentCultureIgnoreCase))
                    {
                        var heater1NickName = GetHeaterNickName(d, 1).Result;
                        var heater2NickName = GetHeaterNickName(d, 2).Result;
                        if (heater1NickName != null)
                            d.HeaterOutput1 = heater1NickName;
                        if (heater2NickName != null)
                            d.HeaterOutput2 = heater2NickName;
                    }
                });

                await AddNewHub(hubData, userCognitoId);
                await _deviceBl.AddNewDevice(content, userCognitoId, false);
            }

            return response;
        }

        public async Task AddNewHub(HubData hubData, string userCognitoId)
        {
            if (hubData.Address == null)
            {
                throw new Exception("Energy setup not saved, please press Back twice to re-enter your details.");
            }

            if (await _hubRepository.GetHubBySerialNoAndUserId(hubData.SerialNo, userCognitoId) != null)
            {
                return;
            }

            var dbHub = await _hubRepository.GetHubBySerialNoIsDeleted(hubData.SerialNo);
            var address = await _addressRepository.GetAddressByIdAndUserId(hubData.Address.Id, userCognitoId);
            if (address == null)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            if (dbHub != null && dbHub.IsDeleted)
            {
                dbHub.RegistrationStartDate = DateTime.Now;
                dbHub.TimeZoneRegion = hubData.TimeZoneRegion;
                dbHub.Firmware = hubData.Firmware;
                dbHub.AppPassword = await KMSCipher.Encrypt(hubData.Password);
                dbHub.IsDeleted = false;
                dbHub.Status = (int)DeviceStatus.NewByUser;
                dbHub.Address = address;
                await _hubRepository.AddOrUpdate(dbHub);
                _hubRepository.CommitChanges();
            }
            else if (await _hubRepository.GetHubBySerialNo(hubData.SerialNo) == null)
            {
                var repoHub = _mapper.Map<Hub>(hubData);
                repoHub.Address = address;
                repoHub.Status = (int)DeviceStatus.NewByUser;

                await _hubRepository.AddOrUpdate(repoHub);
                _hubRepository.CommitChanges();
            }
            else
            {
                throw new Exception("A hub with this serial number already exists in the database.");
            }
        }

        async Task<List<DeviceFeedbackData>> IHubBL.GetHubsWithInstallers(List<HubData> hubs)
        {
            var deviceFeedBacks = new List<DeviceFeedbackData>();
            foreach (var hubData in hubs)
            {
                var installer = await _installerRepository.GetInstallersByCountry(hubData.Address.CountryCode);
                var deviceFeedbackData = new DeviceFeedbackData
                {
                    DeviceSerialNumber = hubData.SerialNo,
                    DeviceType = "hub",
                    Id = hubData.Id,
                    Hub = hubData,
                    Status = hubData.Status,
                    Installers = _mapper.Map<List<InstallerData>>(installer)
                };

                deviceFeedBacks.Add(deviceFeedbackData);
            }

            return deviceFeedBacks;
        }

        public async Task<List<HubData>> GetHubsByUserId(string userId)
        {
            var allHubs = new List<HubData>();
            var dbHubs = await _hubRepository.GetHubByUserId(userId);
            var ownerHubs = _mapper.Map<List<HubData>>(dbHubs);
            var guestHubs = await GetGuestHubs(userId);

            allHubs.AddRange(ownerHubs);
            allHubs.AddRange(guestHubs);

            await SetHubOwnerEmail(allHubs);

            return allHubs;
        }

        private async Task SetHubOwnerEmail(IReadOnlyCollection<HubData> allHubs)
        {
            var hubAddressesIds = allHubs.Select(hub => hub.Address.Id).ToList();
            var users = await _addressRepository.GetAddressGuidsAndUsersByAddressesIds(hubAddressesIds);

            foreach (var hubData in allHubs)
            { 
                hubData.OwnerEmail = users?[hubData.Address.Id]?.Email;
            }
        }

        public async Task<ResponseModel> EditHub(HubData hub, string userCognitoId)
        {
            var dbHub = await _hubRepository.GetHubByIdAndUser(hub.Id, userCognitoId);
            if (dbHub == null)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            dbHub.NickName = hub.NickName;

            var address = await _addressRepository.GetAddressByIdAndUserId(hub.Address.Id, userCognitoId);
            if (address == null)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            dbHub.Address = address;
            await _hubRepository.AddOrUpdate(dbHub);

            var devices = (await _deviceRepository.GetDevicesByHubId(dbHub.Guid)).ToList();
            foreach (var device in devices)
            {
                device.Address = address;
                await _deviceRepository.AddOrUpdate(device);
            }

            _hubRepository.CommitChanges();

            var result = await _apiCallerHelper.GetKeyValueResult(_mapper.Map<HubData>(dbHub), key: "siteName",
                value: dbHub.NickName);

            if (result.HttpStatusCode != HttpStatusCode.OK || result.Status != 0)
            {
                return new ResponseModel(result.HttpMessage + " " + result.StatusText, false);
            }

            return new ResponseModel(true);
        }

        public async Task RemoveHub(HubData hub, string userCognitoId)
        {
            var dbHub = await _hubRepository.GetHubByIdAndUser(hub.Id, userCognitoId);
            if (dbHub == null)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            await _hubRepository.RemoveHub(_mapper.Map<Hub>(hub));
        }

        public async Task<HubData> GetHubById(Guid hubId, string userCognitoId)
        {
            var dbHub = await _hubRepository.GetHubByIdAndUser(hubId, userCognitoId);
            if (dbHub == null)
            {
                var gustDbHub = await _hubRepository.GetGuestHubByHubIdAndCognitoId(hubId, userCognitoId);
                if (gustDbHub == null)
                {
                    throw new UnauthorizedAccessException("Access denied!!");
                }

                return _mapper.Map<HubData>(gustDbHub);
            }

            return _mapper.Map<HubData>(dbHub);
        }

        public async Task<List<HubData>> GetUserAddedNewHubs(string userId)
        {
            var newHubs = (await GetHubsByUserId(userId))
                .Where(x => x.Status == DeviceStatus.NewByUser)
                .ToList();

            return newHubs;
        }

        async Task IHubBL.PatchEncryptHubPsw()
        {
            var allHubs = (await _hubRepository.GetAllHubs()).ToList();
            foreach (var hub in allHubs)
            {
                hub.AppPassword = KMSCipher.Encrypt(hub.AppPassword).Result;
            }

            _hubRepository.CommitChanges();
        }

        async Task IHubBL.UpdateHostServer(Guid hubId, string hostServer)
        {
            var hub = await _hubRepository.GetHubById(hubId);
            hub.HostServer = hostServer;
            _hubRepository.CommitChanges();
        }

        public async Task<object> GetHubSerialNoPasswordByCognitoId(string userCognitoId)
        {
            var result = new { hubs = new List<object>() };
            var dbHubs = await _hubRepository.GetHubByUserId(userCognitoId);
            foreach (var hub in dbHubs)
            {
                result.hubs.Add(new { hubId = hub.SerialNo, hubPassword = KMSCipher.Decrypt(hub.AppPassword).Result });
            }

            return result;
        }

        private async Task<string> GetHubNickName(HubData hubData)
        {
            var result = await _apiCallerHelper.GetKeyValueResult(hubData, true, "siteName");
            if (result.HttpStatusCode == HttpStatusCode.OK || result.Status == 0)
            {
                return result.Content?.ToString();
            }

            return null;
        }

        private void SendHubNickNameToAppServer(HubData dbHub)
        {
            _apiCallerHelper.GetKeyValueResult(dbHub, true, "siteName", dbHub.NickName);
        }

        public async Task<HubData> GetHubBySerialNo(string serialNo)
        {
            var hub = await _hubRepository.GetHubBySerialNo(serialNo);
            return _mapper.Map<HubData>(hub);
        }

        public async Task<object> GetUserHubsAndDevices(string userCognitoId)
        {
            var result = new { hubs = new List<object>() };
            var userHubDevices = (await _deviceRepository.GetDevicesByUserId(userCognitoId))
                .GroupBy(d => d.Hub, (key, g) => new { Hub = key, Devices = g.ToList(), IsGuestUser = false })
                .ToList();

            var guestDeviceData = await _deviceBl.GetGuestDevicesData(userCognitoId);
            var guestDevices = _mapper.Map<List<Device>>(guestDeviceData);
            var guestHubDevices = guestDevices
                .GroupBy(d => d.Hub, (key, g) => new { Hub = key, Devices = g.ToList(), IsGuestUser = true });

            userHubDevices.AddRange(guestHubDevices);

            foreach (var pair in userHubDevices)
            {
                result.hubs.Add(new
                {
                    isGuestUser = pair.IsGuestUser,
                    hubId = pair.Hub?.Guid,
                    hubNickName = pair.Hub?.NickName,
                    devices = ConvertHubDevices(pair.Devices, pair.IsGuestUser),
                });
            }

            return result;
        }

        private async Task<IEnumerable<HubData>> GetGuestHubs(string userCognitoId)
        {
            var guestHubs = (await _userFeaturePermissionRepository
                    .GetUserFeaturePermissionsByCognitoId(userCognitoId))
                .Select(ufp => ufp.Device.Hub)
                .Distinct();

            var guestHubsData = _mapper.Map<List<HubData>>(guestHubs);
            foreach (var hubData in guestHubsData)
            {
                hubData.IsGuestUser = true;
            }

            return guestHubsData;
        }

        private object ConvertHubDevices(List<Device> hubDevices, bool isGuestUser)
        {
            var result = new List<object>();
            hubDevices.ForEach(d =>
            {
                result.Add(new
                {
                    deviceId = d.Guid,
                    deviceType = d.DeviceType,
                    deviceName = d.DeviceName,
                    isGuestUser
                });
            });
            return result;
        }

        private async Task<string> GetHeaterNickName(DeviceData d, int heaterNb)
        {
            var result = await _apiCallerHelper.GetKeyValueResult(d.Hub, true,
                d.DeviceType.ToUpper().First() + d.SerialNumber + heaterNb);
            if (result.HttpStatusCode == HttpStatusCode.OK || result.Status == 0)
            {
                return result.Content?.ToString();
            }

            return null;
        }

        private async Task<string> GetDeviceNickName(DeviceData d)
        {
            var result = await _apiCallerHelper.GetKeyValueResult(d.Hub, true,
                d.DeviceType.ToUpper().First() + d.SerialNumber);
            if (result.HttpStatusCode == HttpStatusCode.OK || result.Status == 0)
            {
                return result.Content?.ToString();
            }

            return null;
        }

        public async Task DeleteHub(Guid hubId, string userCognitoId)
        {
            var dbHub = await _hubRepository.GetHubByIdAndUser(hubId, userCognitoId);
            ValidateThatHubIsNotNull(hubId, dbHub);

            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserIsNotNull(user);

            var historyHub = dbHub.MapToHubHistory(user);
            dbHub.IsDeleted = true;
            dbHub.Installation = null;
            
            var devices = (await _deviceRepository.GetDevicesByHubIdIsDeleted(dbHub.Guid)).ToList();
            await _deviceBl.ExecuteOperationsToRemoveUserFeaturePermissions(devices);

            var unlinkedDevices = new List<Device>();
            foreach (var device in devices)
            {
                device.Hub = null;
                unlinkedDevices.Add(device);
            }

            await _hubRepository.AddOrUpdate(dbHub);
            await _deviceRepository.AddOrUpdate(unlinkedDevices);
            await _hubHistoryRepository.AddOrUpdate(historyHub);
            _hubRepository.CommitChanges();
        }

        private void ValidateThatUserIsNotNull(User user)
        {
            if (user == null)
            {
                throw new NotFoundException("User not found!");
            }
        }

        private void ValidateThatHubIsNotNull(Guid hubId, Hub dbHub)
        {
            if (dbHub == null)
            {
                throw new NotFoundException($"There is no existing hub with ID {hubId}.");
            }
        }

        public async Task<object> GenerateAPIKey(Guid hubId, string userCognitoId)
        {
            var dbHub = await _hubRepository.GetHubByIdAndUser(hubId, userCognitoId);
            if (dbHub == null)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            var newHubPassword = KMSCipher.GeneratePassword();
            var oldHubPassword = KMSCipher.Decrypt(dbHub.AppPassword).Result;
            var hubData = _mapper.Map<HubData>(dbHub);
            hubData.RegistrationCode = oldHubPassword;
            hubData.Password = hubData.ConfirmPassword = newHubPassword;

            int status = 0;
            if (dbHub.SerialNo.Equals("10613810"))
            {
                var result = await _apiCallerHelper.GetHubRegisterResult(hubData);
                status = result.Status;
            }

            if (status == 0)
            {
                dbHub.AppPassword = KMSCipher.Encrypt(hubData.Password).Result;
                dbHub.LastUpdateAppPassword = DateTime.UtcNow;
                await _hubRepository.AddOrUpdate(dbHub);
                _hubRepository.CommitChanges();
            }
            else
            {
                throw new Exception("Invalid password.");
            }

            return newHubPassword;
        }

        public async Task<object> CheckIsAPIKeyAndLastUpdate(List<Guid> hubIds, string userCognitoId)
        {
            var hubs = (await _hubRepository.GetHubsByIdsAndUser(hubIds, userCognitoId)).ToList();

            var resultList = new List<object>();
            foreach (var hub in hubs)
            {
                var result = new
                {
                    hubId = hub.Guid,
                    lastUpdate = hub.LastUpdateAppPassword,
                    isAPIKey = (await KMSCipher.Decrypt(hub.AppPassword)).Length == 24,
                };

                resultList.Add(result);
            }
            
            return resultList;
        }
    }
}
