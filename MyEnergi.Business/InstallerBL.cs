﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class InstallerBL : IInstallerBL
    {
        private readonly IInstallerRepository _installerRepository;
        private readonly IHubRepository _hubRepository;
        private readonly IDeviceRepository _deviceRepository;
        private readonly IMapper _mapper;

        public InstallerBL(IInstallerRepository installerRepository, IHubRepository hubRepository, IDeviceRepository deviceRepository, IMapper mapper)
        {
            _installerRepository = installerRepository;
            _hubRepository = hubRepository;
            _deviceRepository = deviceRepository;
            _mapper = mapper;
        }

        async Task IInstallerBL.SaveInstallers(IList<InstallerData> installers)
        {
            var dbInstallers = (await _installerRepository.GetAllInstallers()).ToList();
            var dbInstallersIdToBeKept = new List<int>();
            foreach (var installerData in installers)
            {
                var dbInstaller = dbInstallers.SingleOrDefault(d => d.InstallerName.Trim() == installerData.InstallerName.Trim() && d.InstallerCode.Trim() == installerData.InstallerCode.Trim());
                if (dbInstaller == null)
                {
                    var installer = _mapper.Map<Installer>(installerData);
                    await _installerRepository.AddOrUpdate(installer);
                }
                else
                    dbInstallersIdToBeKept.Add(dbInstaller.Id);
            }
            var dbInstallersToBeDeleted = dbInstallers.Where(d => !dbInstallersIdToBeKept.Contains(d.Id)).ToList();
            await _installerRepository.DeleteMultiple(dbInstallersToBeDeleted);

            _installerRepository.CommitChanges();
        }

        async Task<InstallerData> IInstallerBL.GetInstallerByCode(string code)
        {
            var installers = await _installerRepository.GetInstallerByCode(code);
            return _mapper.Map<InstallerData>(installers);
        }

        async Task<IList<InstallerData>> IInstallerBL.GetInstallersByCountry(string country)
        {
            var installers = await _installerRepository.GetInstallersByCountry(country);
            return _mapper.Map<IList<InstallerData>>(installers);
        }

        async Task IInstallerBL.AddNewInstallationFeedback(IList<InstallationFeedbackData> feedbackDatas,
            string userCognitoId)
        {
            foreach (var data in feedbackDatas)
            {
                var installationFeedback = _mapper.Map<InstallationFeedback>(data);
                if (data.HubId != null && data.DeviceId == null)
                {
                    await ExecuteOperationsToAddHubInstallationFeedback(userCognitoId, data, installationFeedback);
                }
                else if (data.HubId == null && data.DeviceId != null)
                {
                    await ExecuteOperationsToAddDeviceInstallationFeedback(userCognitoId, data, installationFeedback);
                }
            }

            _hubRepository.CommitChanges();
        }

        private async Task ExecuteOperationsToAddHubInstallationFeedback(string userCognitoId,
            InstallationFeedbackData data,
            InstallationFeedback installationFeedback)
        {
            var hub = await _hubRepository.GetHubByIdAndUser(data.HubId.Value, userCognitoId);
            if (hub == null)
            {
                var gustDbHub = await _hubRepository.GetGuestHubByHubIdAndCognitoId(data.HubId.Value, userCognitoId);
                if (gustDbHub == null)
                {
                    throw new UnauthorizedAccessException("Access denied!!");
                }

                await AddHubInstallationFeedback(installationFeedback, gustDbHub);
            }
            else
            {
                await AddHubInstallationFeedback(installationFeedback, hub);
            }
        }

        private async Task ExecuteOperationsToAddDeviceInstallationFeedback(string userCognitoId,
            InstallationFeedbackData data,
            InstallationFeedback installationFeedback)
        {
            var device = await _deviceRepository.GetDeviceByIdAndUserId(data.DeviceId.Value, userCognitoId);
            if (device == null)
            {
                var guestDevice = await _deviceRepository.GetGuestDeviceByIdAndCognitoId(data.DeviceId.Value, userCognitoId);
                if (guestDevice == null)
                {
                    throw new UnauthorizedAccessException("Access denied!!");
                }

                await AddDeviceInstallationFeedback(installationFeedback, guestDevice, data);
            }
            else
            {
                await AddDeviceInstallationFeedback(installationFeedback, device, data);
            }
        }

        private async Task AddDeviceInstallationFeedback(InstallationFeedback installationFeedback, Device device,
            InstallationFeedbackData data)
        {
            installationFeedback.Id = device.Installation?.Id ?? default(int);
            device.Installation = installationFeedback;
            if (data.IsNew && device.Status == (int)Enumerators.DeviceStatus.New ||
                device.Status == (int)Enumerators.DeviceStatus.NewByUser)
            {
                device.Status = (int)Enumerators.DeviceStatus.None;
            }

            await _deviceRepository.AddOrUpdate(device);
        }

        private async Task AddHubInstallationFeedback(InstallationFeedback installationFeedback, Hub hub)
        {
            installationFeedback.Id = hub.Installation?.Id ?? default(int);
            hub.Installation = installationFeedback;
            hub.Status = (int)Enumerators.DeviceStatus.None;
            await _hubRepository.AddOrUpdate(hub);
        }
    }
}
