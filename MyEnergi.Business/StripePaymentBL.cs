﻿using Microsoft.Extensions.Configuration;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models.Stripe;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using Stripe;
using Stripe.Checkout;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class StripePaymentBL : IStripePaymentBL
    {
        private readonly IUserRepository _userRepository;
        private readonly string _successUrl;
        private readonly string _cancelUrl;

        public StripePaymentBL(IConfiguration configuration, IUserRepository userRepository)
        {
            StripeConfiguration.ApiKey = configuration["Stripe:SecretKey"];
            _userRepository = userRepository;
            _successUrl = configuration["Stripe:SuccessUrl"];
            _cancelUrl = configuration["Stripe:CancelUrl"];
        }

        public async Task<string> CreateSession(string userCognitoId, ExtendedWarrantyPaymentRequestData request)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserExist(user);

            var options = GetSessionCreateOptions(request);

            var service = new SessionService();
            var session = await service.CreateAsync(options);

            return session.Id;
        }

        public async Task<bool> GetPaymentStatus(string userCognitoId, string sessionId)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserExist(user);

            var service = new SessionService();
            var session = await service.GetAsync(sessionId);

            return session.PaymentStatus == "paid";
        }

        private void ValidateThatUserExist(User user)
        {
            if (user == null)
            {
                throw new BadRequestException("Invalid user!");
            }
        }

        private SessionCreateOptions GetSessionCreateOptions(ExtendedWarrantyPaymentRequestData request)
        {
            return new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string>
                {
                    Enumerators.PaymentMethod.Card.ToString().ToLower()
                },
                LineItems = GetSessionLineItemOptions(request),
                Mode = Enumerators.SessionMode.Payment.ToString().ToLower(),
                SuccessUrl = _successUrl,
                CancelUrl = _cancelUrl
            };
        }

        private List<SessionLineItemOptions> GetSessionLineItemOptions(ExtendedWarrantyPaymentRequestData request)
        {
            return new List<SessionLineItemOptions>
            {
                new SessionLineItemOptions
                {
                    PriceData =
                        GetItemPriceDataOptions(request.Price, request.Currency.ToString(), request.ProductName),
                    Quantity = request.Quantity
                }
            };
        }

        private SessionLineItemPriceDataOptions GetItemPriceDataOptions(long price, string currency, string productName)
        {
            return new SessionLineItemPriceDataOptions
            {
                UnitAmount = price,
                Currency = currency,
                ProductData = new SessionLineItemPriceDataProductDataOptions
                {
                    Name = productName
                }
            };
        }
    }
}
