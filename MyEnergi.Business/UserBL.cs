﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class UserBL : IUserBL
    {
        private readonly IUserRepository _userRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IUserVehicleRepository _userVehicleRepository;
        private readonly IUserPreferencesBL _userPreferencesBl;
        private readonly IInvitationRepository _invitationRepository;

        private readonly IMapper _mapper;

        public UserBL(IUserRepository userRepository, IAddressRepository addressRepository,
            IUserVehicleRepository userVehicleRepository, IUserPreferencesBL userPreferencesBl, IMapper mapper,
            IInvitationRepository invitationRepository)
        {
            _userRepository = userRepository;
            _addressRepository = addressRepository;
            _userVehicleRepository = userVehicleRepository;
            _userPreferencesBl = userPreferencesBl;
            _mapper = mapper;
            _invitationRepository = invitationRepository;
        }

        async Task<UserPersonalDetailsData> IUserBL.GetPersonalDetails(string userCognitoId)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserIsNotNull(user);

            if (user.UserDetails == null)
            {
                throw new Exception("User does have any personal details saved in DB");
            }

            var personalDetails = _mapper.Map<UserPersonalDetailsData>(user.UserDetails);
            foreach (var address in user.Addresses)
            {
                var ad = _mapper.Map<AddressData>(address);
                personalDetails.Address.Add(ad);
            }
            foreach (var uv in user.UserVehicles)
            {
                var vd = _mapper.Map<VehicleData>(uv);
                vd.Id = uv.Guid;
                personalDetails.UserVehicles.Add(vd);
            }
            return personalDetails;
        }

        async Task IUserBL.SaveUserDetails(string userCognitoId, UserDetailsData userDetailsData)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserIsNotNull(user);
            
            user.UserDetails ??= new UserDetails();
            await SaveUserPersonalDetails(userDetailsData, user);
            
            userDetailsData.UserPreference ??= new UserPreferenceData();
            await _userPreferencesBl.SaveUserPreferences(userDetailsData.UserPreference, user);

            _userRepository.CommitChanges();
        }

        private async Task SaveUserPersonalDetails(UserPersonalDetailsData userDetailsData, User user)
        {
            UpdateUserPersonalDetails(userDetailsData, user);
            user.UserDetails.LastUpdate = DateTime.UtcNow;
            
            await _userRepository.AddOrUpdate(user);
            
            await UpdateUserVehicles(userDetailsData, user);

            await AddOrUpdateUserAddress(userDetailsData, user);
        }

        private async Task UpdateUserVehicles(UserPersonalDetailsData userDetailsData, User user)
        {
            if (userDetailsData.UserVehicles.Any())
            {
                var dbUserVehicles = user.UserVehicles.ToList();
                var vehiclesIdToBeKept = await GetVehiclesIdToBeKept(userDetailsData, user, dbUserVehicles);

                var vehiclesToBeDeleted = dbUserVehicles.Where(u => !vehiclesIdToBeKept.Contains(u.Id)).ToList();
                if (vehiclesToBeDeleted.Any())
                {
                    await _userVehicleRepository.DeleteMultiple(vehiclesToBeDeleted);
                }
            }
        }

        private void UpdateUserPersonalDetails(UserPersonalDetailsData userDetailsData, User user)
        {
            if (!string.IsNullOrEmpty(userDetailsData.DateOfBirth))
            {
                user.UserDetails.DateOfBirth = DateTime.ParseExact(userDetailsData.DateOfBirth, "dd-MM-yyyy",
                        CultureInfo.InvariantCulture);
            }

            if (!string.IsNullOrEmpty(userDetailsData.FirstName))
            {
                user.UserDetails.FirstName = userDetailsData.FirstName;
            }

            if (!string.IsNullOrEmpty(userDetailsData.LastName))
            {
                user.UserDetails.LastName = userDetailsData.LastName;
            }

            if (!string.IsNullOrEmpty(userDetailsData.Title))
            {
                user.UserDetails.Title = userDetailsData.Title;
            }

            if (!string.IsNullOrEmpty(userDetailsData.MobileCode))
            {
                user.UserDetails.MobileCode = '+' + userDetailsData.MobileCode;
            }

            if (!string.IsNullOrEmpty(userDetailsData.LandlineCode))
            {
                user.UserDetails.LandlineCode = '+' + userDetailsData.LandlineCode;
            }

            if (!string.IsNullOrEmpty(userDetailsData.MobileCode) &&
                !string.IsNullOrEmpty(userDetailsData.MobileNumber))
            {
                user.UserDetails.MobileNumber = CreateNumber(userDetailsData.MobileCode, userDetailsData.MobileNumber);
            }

            if (!string.IsNullOrEmpty(userDetailsData.LandlineCode) &&
                !string.IsNullOrEmpty(userDetailsData.LandlineNumber))
            {
                user.UserDetails.LandlineNumber =
                    CreateNumber(userDetailsData.LandlineCode, userDetailsData.LandlineNumber);
            }
        }

        private async Task AddOrUpdateUserAddress(UserPersonalDetailsData userPersonalDetails, User user)
        {
            foreach (var address in userPersonalDetails.Address)
            {
                var dbAddress = user.Addresses.FirstOrDefault(f => f.Guid == address.Id);
                if (dbAddress == null)
                {
                    if (address.Id != default)
                    {
                        throw new UnauthorizedAccessException("Access denied!!");
                    }

                    dbAddress = new Address {User = user};
                }

                _mapper.Map(address, dbAddress);
                dbAddress.LastUpdate = DateTime.UtcNow;
                await _addressRepository.AddOrUpdate(dbAddress);
            }
        }

        private async Task<List<int>> GetVehiclesIdToBeKept(UserPersonalDetailsData userPersonalDetails, User user, List<UserVehicle> dbUserVehicles)
        {
            var vehiclesIdToBeKept = new List<int>();
            foreach (var vehicle in userPersonalDetails.UserVehicles)
            {
                var uv = dbUserVehicles.FirstOrDefault(f => f.Guid == vehicle.Id);
                if (uv == null)
                {
                    if (vehicle.Id != default)
                    {
                        throw new UnauthorizedAccessException("Access denied!!");
                    }

                    uv = new UserVehicle {User = user};
                }
                else
                {
                    vehiclesIdToBeKept.Add(uv.Id);
                }

                _mapper.Map(vehicle, uv);
                uv.LastUpdate = DateTime.UtcNow;
                await _userVehicleRepository.AddOrUpdate(uv);
            }

            return vehiclesIdToBeKept;
        }

        private string CreateNumber(string code, string number)
        {
            //hardcoding for UK
            if (code == "44")
            {
                if (number[0] == '0')
                {
                    return number.Substring(1);
                }
            }

            return number;
        }

        async Task IUserBL.SaveUser(UserRegistrationData data)
        {
            var user = await _userRepository.GetUserByEmail(data.Email) ?? new User();
            _mapper.Map(data, user);
            await _userRepository.AddOrUpdate(user);
            _userRepository.CommitChanges();
        }

        async Task<IList<AddressData>> IUserBL.GetUsersAddresses(string userCognitoId)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);

            if (user == null)
                throw new UnauthorizedAccessException("Access denied!!");

            return _mapper.Map<IList<AddressData>>(user.Addresses);
        }

        async Task IUserBL.EditUserPreferences(UserRegistrationData data, string userCognitoId)
        {
            if (data.UserCognitoId != userCognitoId)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            var user = await _userRepository.GetUserByCognitoId(data.UserCognitoId);
            ValidateThatUserIsNotNull(user);

            user.MarketingOptIn = data.MarketingOptIn;
            user.ThirdPartyMarketingOptIn = data.ThirdPartyMarketingOptIn;
            user.PrivacyAcceptance = data.PrivacyAcceptance;

            await _userRepository.AddOrUpdate(user);
            _userRepository.CommitChanges();
        }

        async Task IUserBL.EditUserMail(UserRegistrationData data, string userCognitoId)
        {
            if (data.UserCognitoId != userCognitoId)
            {
                throw new UnauthorizedAccessException("Access denied!!");
            }

            var user = await _userRepository.GetUserByCognitoId(data.UserCognitoId);
            ValidateThatUserIsNotNull(user);

            if (await _userRepository.CheckIfEmailAlreadyExists(data.Email))
            {
                throw new Exception($"Email address {data.Email} is already used!");
            }

            var invitations = (await _invitationRepository.GetBy(i => i.GuestUser.CognitoId.Equals(userCognitoId),
                include: source => source
                    .Include(inv => inv.InvitationDetails))).ToList();
            if (invitations.Any())
            {
                foreach (var invitation in invitations)
                {
                    invitation.InvitationDetails.Email = data.Email;
                }
                await _invitationRepository.AddOrUpdate(invitations);
            }

            user.Email = data.Email;
            await _userRepository.AddOrUpdate(user);
            _userRepository.CommitChanges();
        }

        private void ValidateThatUserIsNotNull(User user)
        {
            if (user == null)
            {
                throw new NotFoundException("User not found in the database");
            }
        }

        public async Task ValidateThatUserExists(string userCognitoId)
        {
            if (!await _userRepository.CheckIfUserExists(userCognitoId))
            {
                throw new UnauthorizedAccessException("User does not exists!");
            }
        }

        async Task<int> IUserBL.ValidateThatUserExistsAndGetId(string userCognitoId)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserIsNotNull(user);
            return user.Id;
        }
    }
}
