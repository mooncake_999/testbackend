﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class UserPreferencesBL : IUserPreferencesBL
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public UserPreferencesBL(IUserRepository userRepository, IMapper mapper, 
            IConfiguration configuration)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _configuration = configuration;
        }
        
        public async Task<UserPreferenceData> GetPreference(string userCognitoId)
        {
            var user = await _userRepository.GetUserByCognitoId(userCognitoId);
            ValidateThatUserExists(user);

            return _mapper.Map<UserPreferenceData>(user.UserPreference);
        }

        public async Task SaveUserPreferences(UserPreferenceData userPreferenceData, User user)
        {
            user.UserPreference ??= new UserPreference();

            if (userPreferenceData.Id != default)
            {
                UpdateUserPreferences(userPreferenceData, user);
                user.UserPreference.LastUpdate = DateTime.UtcNow;
            }
            else
            {
                SetDefaultUserPreferences(user);
                user.UserPreference.LastUpdate = DateTime.UtcNow;
            }
            
            await _userRepository.AddOrUpdate(user);
        }

        private void SetDefaultUserPreferences(User user)
        {
            user.UserPreference.IsMetric = bool.Parse(_configuration["DefaultSettings:IsMetric"]);
            user.UserPreference.Currency = _configuration["DefaultSettings:Currency"];
            user.UserPreference.DateFormat = _configuration["DefaultSettings:DateFormat"];
            user.UserPreference.Language = _configuration["DefaultSettings:Language"];
        }

        private static void UpdateUserPreferences(UserPreferenceData userPreferenceData, User user)
        {
            if (userPreferenceData.IsMetric != user.UserPreference.IsMetric)
            {
                user.UserPreference.IsMetric = userPreferenceData.IsMetric;
            }

            if (!string.IsNullOrEmpty(userPreferenceData.Currency))
            {
                user.UserPreference.Currency = userPreferenceData.Currency;
            }

            if (!string.IsNullOrEmpty(userPreferenceData.DateFormat))
            {
                user.UserPreference.DateFormat = userPreferenceData.DateFormat;
            }

            if (!string.IsNullOrEmpty(userPreferenceData.Language))
            {
                user.UserPreference.Language = userPreferenceData.Language;
            }
        }

        private static void ValidateThatUserExists(User user)
        {
            if (user == null)
            {
                throw new Exception("User does not exists in the DB");
            }
        }
    }
}
