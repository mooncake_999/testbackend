﻿using AutoMapper;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class VehicleBL : IVehicleBL
    {
        private readonly IVehicleRepository _vehicleRepository;
        private readonly IUserVehicleRepository _userVehicleRepository;
        private readonly IMapper _mapper;

        public VehicleBL(IVehicleRepository vehicleRepository, IUserVehicleRepository userVehicleRepository,
            IMapper mapper)
        {
            _vehicleRepository = vehicleRepository;
            _userVehicleRepository = userVehicleRepository;
            _mapper = mapper;
        }

        public async Task<IList<string>> GetAllManufacturers()
        {
            return (await _vehicleRepository.GetAllManufactures()).ToList();
        }

        public async Task<IList<KeyValuePair<Guid, string>>> GetVehicleByManufacturer(string manufacturer)
        {
            return (await _vehicleRepository.GetVehicleByManufacturer(manufacturer)).ToList();
        }

        public async Task<IList<VehicleData>> GetVehicleDetails(Guid vehicleId)
        {
            var vehicles = (await _vehicleRepository.GetVehicleDetails(vehicleId)).ToList();
            return _mapper.Map<IList<VehicleData>>(vehicles);
        }

        public async Task UpdateVehiclesData(IList<VehicleData> vehicleData)
        {
            var dbVehicles = (await _vehicleRepository.GetAllVehicles()).ToList();
            var vehiclesToBeKept = new List<int>();

            foreach (var vd in vehicleData)
            {
                var dbVehicle = dbVehicles.SingleOrDefault(v => v.RefId == vd.RefId) ?? new Vehicle();

                vd.Id = dbVehicle.Guid;
                dbVehicle = _mapper.Map(vd, dbVehicle);
                await _vehicleRepository.AddOrUpdate(dbVehicle);
                vehiclesToBeKept.Add(dbVehicle.Id);
            }

            var vehiclesToBeDeleted = dbVehicles.Where(d => !vehiclesToBeKept.Contains(d.Id)).ToList();
            await _vehicleRepository.DeleteMultiple(vehiclesToBeDeleted);
            _vehicleRepository.CommitChanges();
        }

        public async Task<List<VehicleData>> GetUserVehicles(string userCognitoId)
        {
            var results = new List<VehicleData>();
            var userVehicles = (await _userVehicleRepository.GetUserVehicles(userCognitoId)).ToList();
            foreach (var uv in userVehicles)
            {
                var vs = _mapper.Map<VehicleData>(uv);
                vs.Id = uv.Guid;
                results.Add(vs);
            }
            return results;
        }

        public async Task<List<VehicleData>> GetAllVehicles()
        {
            var vehicles = (await _vehicleRepository.GetAllVehicles()).ToList();
            return _mapper.Map<List<VehicleData>>(vehicles);
        }
    }
}
