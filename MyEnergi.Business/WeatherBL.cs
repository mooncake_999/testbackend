﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.Loaders;
using MyEnergi.AppServer.Model.Weather;
using MyEnergi.Business.Interfaces;
using MyEnergi.Common;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Business
{
    public class WeatherBL : IWeatherBL
    {
        private readonly IUserRepository _userRepository;

        private readonly string _apiKey;
        private readonly string _dailyForecastEndpoint;
        private readonly string _hourlyForecastEndpoint;

        private const short Days = 5;
        private const short Hours = 120;

        public WeatherBL(IConfiguration configuration, IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _apiKey = configuration["WeatherbitAPI:ApiKey"];
            _dailyForecastEndpoint = configuration["WeatherbitAPI:DailyForecastEndpoint"];
            _hourlyForecastEndpoint = configuration["WeatherbitAPI:HourlyForecastEndpoint"];
        }

        public async Task<ICollection<object>> GetForecastData(string userCognitoId)
        {
            var user = await _userRepository.GetFirstOrDefaultBy(u => u.CognitoId.Equals(userCognitoId),
                include: source => source.Include(u => u.Addresses));

            ValidateUserData(user);

            var mainAddress = user.Addresses.First();

            var dailyForecastCollection = GetDailyForecast(mainAddress.PostalCode, mainAddress.CountryCode);
            var hourlyForecastCollection = GetHourlyForecast(mainAddress.PostalCode, mainAddress.CountryCode);

            var result = new List<object>();
            var startIndex = 0;
            var endIndex = 24;
            foreach (var dailyForecastData in dailyForecastCollection)
            {
                var hourlyForecastData = hourlyForecastCollection.Skip(startIndex).Take(endIndex - startIndex).ToList();

                var data = GetForecastResultData(dailyForecastData, hourlyForecastData);

                startIndex += 24;
                endIndex += 24;

                result.Add(data);
            }

            return result;
        }

        private static void ValidateUserData(User user)
        {
            if (user == null)
            {
                throw new NotFoundException("User not found");
            }

            if (user.Addresses == null || !user.Addresses.Any())
            {
                throw new NotFoundException("Addresses not found");
            }
        }

        public ICollection<DailyWeatherForecastData> GetDailyForecast(string postalCode, string countryCode)
        {
            var dailyForecastUrl =
                GetBaseEndpointUrlFormat(_dailyForecastEndpoint, postalCode, countryCode) + "&days=" + Days;
            var dailyForecastResult = new WeatherForecastLoader(dailyForecastUrl).GetResult();
            var dailyForecastCastContent = (List<DailyWeatherForecastData>)dailyForecastResult.Content;

            return dailyForecastCastContent;
        }

        public ICollection<HourlyWeatherForecastData> GetHourlyForecast(string postalCode, string countryCode)
        {
            var hourlyForecastUrl = GetBaseEndpointUrlFormat(_hourlyForecastEndpoint, postalCode, countryCode) +
                                    "&hours=" + Hours;
            var hourlyForecastResult = new WeatherForecastLoader(hourlyForecastUrl).GetResult();
            var hourlyForecastCastContent = (List<HourlyWeatherForecastData>)hourlyForecastResult.Content;

            return hourlyForecastCastContent;
        }

        public object GetForecastResultData(DailyWeatherForecastData dailyForecastData,
            List<HourlyWeatherForecastData> hourlyForecastObjects)
        {
            var dailyForecastDateTime = dailyForecastData.DateTime;
            var daySuffix = DateTimeHelper.GetDaySuffix(dailyForecastDateTime);

            dailyForecastData.DateToDisplay =
                $"{dailyForecastDateTime.DayOfWeek.ToString().Substring(0, 3)} {dailyForecastDateTime.Day}{daySuffix}";

            return new
            {
                dailyForecastData,
                hourlyForecastData = hourlyForecastObjects
            };
        }

        private string GetBaseEndpointUrlFormat(string mainEndpoint, string postalCode, string countryCode)
        {
            return mainEndpoint + "?postal_code=" + postalCode + "&country=" + countryCode + "&key=" + _apiKey;
        }
    }
}
