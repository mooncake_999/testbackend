﻿namespace MyEnergi.Common
{
    public static class Constants
    {
        public const string AgileOctopusInternalReference = "GB-0270-101";
        public const string Alias = "alias/Test2";
        public const string Entsoe = "ENTSOE";
        public const string OctopusAgile = "OctopusAgile";
    }
}
