﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyEnergi.Common.CurrencyConverter
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public const string FreeBaseUrl = "https://free.currencyconverterapi.com/api/v6/";
        private readonly string _apiKey;
        private static HttpClient Client = new HttpClient();

        public CurrencyConverter(IConfiguration configuration)
        {
            _apiKey = configuration["CurrencyConverterAPI:ApiKey"];
        }

        private double Convert(double amount, string from, string to)
        {
            return ExchangeRate(from, to, _apiKey) * amount;
        }

        public async Task<double> ConvertAsync(double amount, string from, string to)
        {
            return await Task.Run(() => Convert(amount, from, to));
        }

        private static double ExchangeRate(string from, string to, string apiKey = null)
        {
            string url;
            url = FreeBaseUrl + "convert?q=" + from + "_" + to + "&compact=ultra&apiKey=" + apiKey;

            var jsonString = GetResponse(url);
            return JObject.Parse(jsonString).First.ToObject<double>();
        }

        private static string GetResponse(string url)
        {
            Log.Information($"A call to {url} is made in order to convert currency.");
            try
            {
                var response = Client.GetStringAsync(new Uri(url)).Result;
                return response;
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception($"Something went wrong when trying to convert currency. {e.Message}");
            }
        }
    }
}
