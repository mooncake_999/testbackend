﻿using System.Threading.Tasks;

namespace MyEnergi.Common.CurrencyConverter
{
    public interface ICurrencyConverter
    {
        Task<double> ConvertAsync(double amount, string from, string to);
    }
}
