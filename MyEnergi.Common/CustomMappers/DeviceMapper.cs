﻿using MyEnergi.Data.Entity;
using System;

namespace MyEnergi.Common.CustomMappers
{
    public static class DeviceMapper
    {
        public static DeviceHistory MapToDeviceHistory(this Device device, User user)
        {
            return new DeviceHistory
            {
                OwnerUser = user,
                Installation = device.Installation,
                DeviceName = device.DeviceName,
                DeviceType = device.DeviceType,
                SerialNumber = device.SerialNumber,
                MovedToHistory = DateTime.Now
            };
        }
    }
}
