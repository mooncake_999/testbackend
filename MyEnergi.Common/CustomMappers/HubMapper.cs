﻿using MyEnergi.Data.Entity;
using System;

namespace MyEnergi.Common.CustomMappers
{
    public static class HubMapper
    {
        public static HubHistory MapToHubHistory(this Hub hub, User user)
        {
            var hubHistory = new HubHistory
            {
                OwnerUser = user,
                Installation = hub.Installation,
                Nickname = hub.NickName,
                SerialNumber = hub.SerialNo,
                MovedToHistory = DateTime.Now
            };

            return hubHistory;
        }
    }
}
