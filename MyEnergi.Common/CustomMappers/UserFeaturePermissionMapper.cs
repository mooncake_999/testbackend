﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyEnergi.Common.CustomMappers
{
    public static class UserFeaturePermissionMapper
    {
        public static IList<UserFeaturePermissionHistory> 
            MapToUserFeaturePermissionHistory(this IEnumerable<UserFeaturePermission> userFeaturePermissions)
        {
            return userFeaturePermissions.Select(userFeaturePermission => new UserFeaturePermissionHistory
            {
                Guid = userFeaturePermission.Guid,
                OwnerUser = userFeaturePermission.OwnerUser,
                Invitation = userFeaturePermission.Invitation,
                Device = userFeaturePermission.Device,
                AppFeature = userFeaturePermission.AppFeature,
                MovedToHistory = DateTime.Now
            }).ToList();
        }

        public static UserFeaturePermissionHistory
            MapToUserFeaturePermissionHistory(this UserFeaturePermission userFeaturePermission)
        {
            return new UserFeaturePermissionHistory
            {
                Guid = userFeaturePermission.Guid,
                OwnerUser = userFeaturePermission.OwnerUser,
                Invitation = userFeaturePermission.Invitation,
                Device = userFeaturePermission.Device,
                AppFeature = userFeaturePermission.AppFeature,
                MovedToHistory = DateTime.Now
            };
        }
    }
}
