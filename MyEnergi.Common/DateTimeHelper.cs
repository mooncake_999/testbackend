﻿using System;

namespace MyEnergi.Common
{
    public static class DateTimeHelper
    {
        public static string GetDaySuffix(DateTime dateTime)
        {
            return dateTime.Day % 10 == 1 && dateTime.Day % 100 != 11 ? "st"
                : dateTime.Day % 10 == 2 && dateTime.Day % 100 != 12 ? "nd"
                : dateTime.Day % 10 == 3 && dateTime.Day % 100 != 13 ? "rd"
                : "th";
        }
    }
}
