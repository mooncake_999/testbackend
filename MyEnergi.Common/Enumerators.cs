﻿using System;

namespace MyEnergi.Common
{
    public static class Enumerators
    {
        public enum DeviceStatus
        {
            None = 0,
            New = 1,
            Active = 2,
            Offline = 3,
            NewByUser = 4
        }

        public static string GetDeviceStatusName(object value)
        {
            return Enum.GetName(typeof(DeviceStatus), value);
        }

        public enum TypeOfSchedule { Scheduled, Single, Budget };


        public static string GetTypeOfSchedule(object value)
        {
            return Enum.GetName(typeof(TypeOfSchedule), value);
        }

        public enum DeviceType {Zappi,Eddi,Harvi};

        public static string GetDeviceType(object value)
        {
            return Enum.GetName(typeof(DeviceType), value);
        }

        public enum VehicleCondition
        {
            None,
            New,
            SecondHand
        }

        public enum VehicleOwnership
        {
            None,
            Personal,
            Fleet
        };

        public enum UserRole
        {
            Owner,
            Guest,
            Invited,
            AccessDenied
        }

        public enum InvitationStatus
        {
            Accepted,
            Pending,
            Expired,
            Ended,
            Rejected,
            Deleted
        }
        
        public enum PaymentMethod
        {
            Card
        }

        public enum Currency
        {
            Gbp,
            Usd
        }

        public enum SessionMode
        {
            Payment,
            Subscription,
            Setup
        }
    }
}
