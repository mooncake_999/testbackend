﻿using Amazon;
using Amazon.KeyManagementService;
using Amazon.KeyManagementService.Model;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyEnergi.Common
{
    public class KMSCipher
    {
        private static AmazonKeyManagementServiceClient _client;
        
        public KMSCipher()
        {
            _client = GetKmsClient();
        }

        private static AmazonKeyManagementServiceClient GetKmsClient()
        {
            if (_client == null)
            {
                try
                {
                    _client = new AmazonKeyManagementServiceClient(RegionEndpoint.EUWest2);
                }
                catch (Exception ex)
                {
                    Log.Error($"Error creating KMS client", ex);
                }
            }
            return _client;
        }

        public static async Task<string> Encrypt(string textToEncrypt)
        {
            try
            {
                var kmsClient = GetKmsClient();
                var aliases = kmsClient.ListAliasesAsync(new ListAliasesRequest());
                if (aliases?.Result.Aliases == null)
                {
                    return null;
                }

                var foundAlias = aliases.Result.Aliases.FirstOrDefault(x => x.AliasName == Constants.Alias);
                var masterKey = foundAlias?.TargetKeyId;
                var textBytes = Encoding.UTF8.GetBytes(textToEncrypt);

                var encryptRequest = new EncryptRequest
                {
                    KeyId = masterKey,
                    Plaintext = new MemoryStream(textBytes, 0, textBytes.Length)
                };

                var response = await kmsClient.EncryptAsync(encryptRequest);
                return Convert.ToBase64String(response.CiphertextBlob.ToArray());
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception($"Something went wrong when trying to encrypt with AWS KMS. {e.Message}");
            }
        }

        public static async Task<string> Decrypt(string textToDecrypt)
        {
            try
            {
                var kmsClient = GetKmsClient();
                var decryptRequest = new DecryptRequest();
                var fromBase64Bytes = Convert.FromBase64String(textToDecrypt);
                decryptRequest.CiphertextBlob = new MemoryStream(fromBase64Bytes, 0, fromBase64Bytes.Length);
                var response = await kmsClient.DecryptAsync(decryptRequest);
                return Encoding.UTF8.GetString(response.Plaintext.ToArray());
            }
            catch (InvalidCiphertextException ex)
            {
                Log.Error(ex.Message);
                throw new InvalidCiphertextException("This text cannot be decrypted using AWS KMS because it was previously encrypted with another algorithm.");
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
                throw new Exception($"Something went wrong when trying to decrypt with AWS KMS.{e.Message}");
            }
        }

        public static string GeneratePassword()
        {
            const string src = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            int length = 24;
            var sb = new StringBuilder();
            Random RNG = new Random();
            for (var i = 0; i < length; i++)
            {
                var c = src[RNG.Next(0, src.Length)];
                sb.Append(c);
            }
            return sb.ToString();
        }
    }
}
