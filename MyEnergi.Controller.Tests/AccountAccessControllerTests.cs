﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Business.Model.Models.AccountAccess;
using MyEnergi.Common;
using MyEnergi.Controllers;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Controller.Tests
{
    [TestFixture]
    public class AccountAccessControllerTests
    {
        private Mock<IAccountAccessShareBL> _accountAccessShareBlMock;
        private Mock<IAccountAccessHubBL> _accountAccessHubBlMock;

        private ControllerContext _controllerContext;
        private string _cognitoId;
        private string _token;

        [SetUp]
        public void AccountAccessController_SetUp()
        {
            _accountAccessShareBlMock = new Mock<IAccountAccessShareBL>();
            _accountAccessHubBlMock = new Mock<IAccountAccessHubBL>();

            var httpContext = new DefaultHttpContext();
            var user = UserMockFactory.GetNewFullUser();
            _cognitoId = MockConstants.CognitoIdFromToken;
            _token = MockHelper.GenerateJwtToken(user);
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;

            _controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }

        [Test]
        public void AccountAccessController_GetHubs_HappyFlow()
        {
            // Arrange
            var hubs = HubMockFactory.GetAccountAccessHubsObject();

            _accountAccessHubBlMock
                .Setup(bl => bl.GetHubs(_cognitoId))
                .Returns(Task.FromResult(hubs));

            var accountAccessController = new AccountAccessController(null, _accountAccessHubBlMock.Object)
            {
                ControllerContext = _controllerContext
            };

            // Act
            var result = accountAccessController.GetHubs().Result as ObjectResult;
            var actualResult = result.Value;

            var content = actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            var hubsFromResult = (ICollection<object>)content.GetType().GetProperty("hubs")?.GetValue(content);
            var firstHub = hubsFromResult.First();
            var secondHub = hubsFromResult.ElementAt(1);
            var thirdHub = hubsFromResult.ElementAt(2);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(3, hubsFromResult.Count);
            Assert.IsNotNull(firstHub);
            Assert.IsNotNull(secondHub);
            Assert.IsNotNull(thirdHub);
        }

        [Test]
        public void AccountAccessController_TransferOwnership_HappyFlow()
        {
            // Arrange
            const string serialNo = "10613333";
            const string email = "test@yopmail.com";

            _accountAccessHubBlMock
                .Setup(bl => bl.TransferHubOwnership(serialNo, email, _cognitoId))
                .Returns(Task.FromResult(0));

            var accountAccessController = new AccountAccessController(null, _accountAccessHubBlMock.Object)
            {
                ControllerContext = _controllerContext
            };

            var requestData = new TransferOwnershipRequest
            {
                SerialNo = serialNo,
                Email = email
            };

            // Act
            var result = accountAccessController.TransferOwnership(requestData).Result as ObjectResult;
            var actualResult = result.Value;

            var responseMessage = (string)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual("Ownership was successfully transferred!", responseMessage);
        }

        [Test]
        public void AccountAccessController_MigrateHubs_HappyFlow()
        {
            // Arrange
            var migrateHubsRequestData = HubMockFactory.GetMigrateHubsRequestData();
            var migratedHubsResponseMock = HubMockFactory.GetMigrateHubsResponseObject();

            _accountAccessHubBlMock
                .Setup(bl => bl.MigrateHubs(migrateHubsRequestData, _cognitoId))
                .Returns(Task.FromResult(migratedHubsResponseMock));

            var accountAccessController = new AccountAccessController(null, _accountAccessHubBlMock.Object)
            {
                ControllerContext = _controllerContext
            };

            var migrateHubsRequest = new MigrateHubsRequest
            {
                Hubs = migrateHubsRequestData.ToList()
            };

            // Act
            var result = accountAccessController.MigrateHubs(migrateHubsRequest).Result as ObjectResult;
            var actualResult = result.Value;

            var content = actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            var migratedHubsResponse = (List<object>)content.GetType().GetProperty("hubs")?.GetValue(content);
            var firstHub = migratedHubsResponse.First();
            var secondHub = migratedHubsResponse.ElementAt(1);
            var thirdHub = migratedHubsResponse.ElementAt(2);
            var fourthHub = migratedHubsResponse.ElementAt(3);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(4, migratedHubsResponse.Count);
            Assert.IsNotNull(firstHub);
            Assert.IsNotNull(secondHub);
            Assert.IsNotNull(thirdHub);
            Assert.IsNotNull(fourthHub);
        }

        [Test]
        public void AccountAccessController_RegisterNewHub_HappyFlow()
        {
            // Arrange
            const string hubSerialNo = "10613333";
            const string hubPassword = "passw0rd1234";
            const string hubRegistrationCode = "r5dee-46ef";

            var response = new ResponseModel(new
            {
                hubSerialNo,
                hubPassword,
                relation = Enumerators.UserRole.Guest.ToString()
            });

            _accountAccessHubBlMock
                .Setup(bl => bl.RegisterNewHub(hubSerialNo, hubRegistrationCode, _cognitoId))
                .Returns(Task.FromResult(response));

            var accountAccessController = new AccountAccessController(null, _accountAccessHubBlMock.Object)
            {
                ControllerContext = _controllerContext
            };

            var requestData = new RegisterNewHubRequest
            {
                SerialNo = hubSerialNo,
                RegistrationCode = hubRegistrationCode
            };

            // Act
            var result = accountAccessController.RegisterNewHub(requestData).Result as ObjectResult;
            var actualResult = result.Value;

            var content = actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            var passwordResponse = (string)content.GetType().GetProperty("hubPassword")?.GetValue(content);
            var hubSerialNoResponse = (string)content.GetType().GetProperty("hubSerialNo")?.GetValue(content);
            var relationResponse = (string)content.GetType().GetProperty("relation")?.GetValue(content);

            // Assert
            Assert.AreEqual("passw0rd1234", passwordResponse);
            Assert.AreEqual("10613333", hubSerialNoResponse);
            Assert.AreEqual(Enumerators.UserRole.Guest.ToString(), relationResponse);
        }

        [Test]
        public void AccountAccessController_ShareHub_HappyFlow()
        {
            // Arrange
            const string hubSerialNo = "10613333";
            const string hubRegistrationCode = "r5dee-46ef";
            const string email = "test@yopmail.com";

            _accountAccessShareBlMock
                .Setup(bl => bl.ShareHub(_cognitoId, hubRegistrationCode, email))
                .Returns(Task.FromResult(0));

            var accountAccessController = new AccountAccessController(_accountAccessShareBlMock.Object, null)
            {
                ControllerContext = _controllerContext
            };

            var shareHubRequest = new ShareHubRequest
            {
                SerialNo = hubSerialNo,
                Email = email
            };

            // Act
            var result = accountAccessController.ShareHub(shareHubRequest).Result as ObjectResult;
            var actualResult = result.Value;

            var content = (string)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            // Assert
            Assert.AreEqual("Successful sharing.", content);
        }

        [Test]
        public void AccountAccessController_RevokeShare_HappyFlow()
        {
            // Arrange
            const string hubSerialNo = "10613333";
            const string email = "test@yopmail.com";

            _accountAccessShareBlMock
                .Setup(bl => bl.RevokeShare(_cognitoId, email, hubSerialNo))
                .Returns(Task.FromResult(0));

            var accountAccessController = new AccountAccessController(_accountAccessShareBlMock.Object, null)
            {
                ControllerContext = _controllerContext
            };

            var shareHubRequest = new ShareHubRequest
            {
                SerialNo = hubSerialNo,
                Email = email
            };

            // Act
            var result = accountAccessController.RevokeShare(shareHubRequest).Result as ObjectResult;
            var actualResult = result.Value;

            var content = (string)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            // Assert
            Assert.AreEqual("Hubs sharing successfully revoked for test@yopmail.com.", content);
        }

        [Test]
        public void AccountAccessController_AcceptInvite_HappyFlow()
        {
            // Arrange
            const string hubSerialNo = "10613333";
            const string hubPassword = "passw0rd1234";

            var acceptInviteResult = new
            {
                serialNo = hubSerialNo,
                hubPassword,
                relation = Enumerators.UserRole.Guest.ToString()
            };

            _accountAccessShareBlMock
                .Setup(bl => bl.AcceptInvitation(_cognitoId, hubSerialNo))
                .Returns(Task.FromResult((object)acceptInviteResult));

            var accountAccessController = new AccountAccessController(_accountAccessShareBlMock.Object, null)
            {
                ControllerContext = _controllerContext
            };

            var shareHubRequest = new InvitationShareRequest
            {
                SerialNo = hubSerialNo
            };

            // Act
            var result = accountAccessController.AcceptInvite(shareHubRequest).Result as ObjectResult;
            var actualResult = result.Value;

            var content = actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            var hubPasswordResult = (string)content.GetType().GetProperty("hubPassword")?.GetValue(content);
            var relationResult = (string)content.GetType().GetProperty("relation")?.GetValue(content);
            var serialNoResult = (string)content.GetType().GetProperty("serialNo")?.GetValue(content);

            // Assert
            Assert.AreEqual("passw0rd1234", hubPasswordResult);
            Assert.AreEqual("Guest", relationResult);
            Assert.AreEqual("10613333", serialNoResult);
        }

        [Test]
        public void AccountAccessController_RejectInvitation_HappyFlow()
        {
            // Arrange
            const string hubSerialNo = "10613333";

            _accountAccessShareBlMock
                .Setup(bl => bl.RejectInvitation(_cognitoId, hubSerialNo))
                .Returns(Task.FromResult(0));

            var accountAccessController = new AccountAccessController(_accountAccessShareBlMock.Object, null)
            {
                ControllerContext = _controllerContext
            };

            var shareHubRequest = new InvitationShareRequest
            {
                SerialNo = hubSerialNo
            };

            // Act
            var result = accountAccessController.RejectInvitation(shareHubRequest).Result as ObjectResult;
            var actualResult = result.Value;

            var content = (string)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            // Assert
            Assert.AreEqual("Invitation was successfully rejected!", content);
        }

        [TearDown]
        public void AccountAccessController_TearDown()
        {
            _accountAccessShareBlMock = null;
            _accountAccessHubBlMock = null;

            _controllerContext = null;
            _cognitoId = null;
            _token = null;
        }
    }
}
