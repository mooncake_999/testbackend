﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Controllers;
using MyEnergi.Data.Interfaces;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Controller.Tests
{
    [TestFixture]
    public class AccountSettingsControllerTests
    {
        private Mock<IDeviceBL> _deviceBlMock;
        private Mock<IAccountSettingsShareBL> _accountSettingsShareBlMock;
        private Mock<IHubBL> _hubBlMock;
        private ControllerContext _controllerContext;
        
        private string _token;
        private string _cognitoId;

        [SetUp]
        public void AccountSettingsController_SetUp()
        {
            // Arrange
            _deviceBlMock = new Mock<IDeviceBL>();
            _accountSettingsShareBlMock = new Mock<IAccountSettingsShareBL>();
            _hubBlMock = new Mock<IHubBL>();

            var httpContext = new DefaultHttpContext();
            var user = UserMockFactory.GetNewFullUser();
            _cognitoId = user.CognitoId;
            _token = MockHelper.GenerateJwtToken(user);
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;

            _controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }

        [Test]
        public void AccountSettingsController_DeleteDevice_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var accountSettingsController =
                new AccountSettingsController( null, null, null, _deviceBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var deviceId = GetDefaultGuid();
            var result = accountSettingsController.DeleteDevice(deviceId).Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (object)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("Device deleted successfully!", content);
        }

        [Test]
        public void AccountSettingsController_DeleteHub_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var accountSettingsController =
                new AccountSettingsController(null, null, _hubBlMock.Object, _deviceBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var guid = GetDefaultGuid();
            var result = accountSettingsController.DeleteHub(guid).Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (object)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("Hub deleted successfully!", content);
        }

        [Test]
        public void AccountSettingsController_DeleteHub_ExceptionFlow()
        {
            // Arrange
            var guid = GetDefaultGuid();
            _hubBlMock.Setup(bl => bl.DeleteHub(guid, MockConstants.CognitoIdFromToken))
                .Throws(new Exception($"There is no existing hub with ID {guid}."));

            var accountSettingsController =
                new AccountSettingsController(null, null, _hubBlMock.Object, _deviceBlMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act

            var exception = Assert.ThrowsAsync<Exception>(() => accountSettingsController.DeleteHub(guid));

            // Assert
            Assert.AreEqual(true, exception is Exception);
            Assert.AreEqual($"There is no existing hub with ID {guid}.", exception.Message);
        }

        [Test]
        public void AccountSettingsController_DeleteDevice_ExceptionFlow()
        {
            // Arrange
            var guid = GetDefaultGuid();
            _deviceBlMock.Setup(bl => bl.DeleteDevice(guid, MockConstants.CognitoIdFromToken))
                .Throws(new Exception($"There is no existing device with ID {guid}."));

            var accountSettingsController =
                new AccountSettingsController(null, null, null, _deviceBlMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act

            var exception = Assert.ThrowsAsync<Exception>(() => accountSettingsController.DeleteDevice(guid));

            // Assert
            Assert.AreEqual(true, exception is Exception);
            Assert.AreEqual($"There is no existing device with ID {guid}.", exception.Message);
        }

        [Test]
        public void AccountSettingsController_GetUserAccounts_HappyFlow()
        {
            // Arrange
            var userAccounts = UserMockFactory.GetUserAccountResponses();

            _accountSettingsShareBlMock
                .Setup(bl => bl.GetUserAccounts(_cognitoId))
                .Returns(Task.FromResult(userAccounts));

            var accountSettingsController =
                new AccountSettingsController(null, _accountSettingsShareBlMock.Object, null, null)
                {
                    ControllerContext = _controllerContext
                };

            // Act
            var result = accountSettingsController.GetUserAccounts().Result as ObjectResult;
            var actualResult = result.Value;
            var userAccountsResponse =
                (List<object>) actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            var firstUserAccount = userAccountsResponse.First();
            var secondUserAccount = userAccountsResponse.ElementAt(1);

            var firstUserAccountFirstName =
                (string) firstUserAccount.GetType().GetProperty("FirstName")?.GetValue(firstUserAccount);
            var firstUserAccountLastName =
                (string) firstUserAccount.GetType().GetProperty("LastName")?.GetValue(firstUserAccount);
            var firstUserAccountEmail =
                (string) firstUserAccount.GetType().GetProperty("Email")?.GetValue(firstUserAccount);
            var firstUserAccountIsGuestUser =
                (bool) firstUserAccount.GetType().GetProperty("IsGuestUser")?.GetValue(firstUserAccount);

            var secondUserAccountInvitationId =
                (Guid) secondUserAccount.GetType().GetProperty("InvitationId")?.GetValue(secondUserAccount);
            var secondUserAccountFirstName =
                (string) secondUserAccount.GetType().GetProperty("FirstName")?.GetValue(secondUserAccount);
            var secondUserAccountLastName =
                (string) secondUserAccount.GetType().GetProperty("LastName")?.GetValue(secondUserAccount);
            var secondUserAccountEmail =
                (string) secondUserAccount.GetType().GetProperty("Email")?.GetValue(secondUserAccount);
            var secondUserAccountIsGuestUser =
                (bool) secondUserAccount.GetType().GetProperty("IsGuestUser")?.GetValue(secondUserAccount);
            var secondUserAccountIsSelectedAccount = (bool) secondUserAccount.GetType().GetProperty("IsSelectedAccount")
                ?.GetValue(secondUserAccount);

            // Assert
            Assert.AreEqual(2, userAccountsResponse.Count);

            Assert.AreEqual("Current", firstUserAccountFirstName);
            Assert.AreEqual("User", firstUserAccountLastName);
            Assert.AreEqual("current_user@yopmail.com", firstUserAccountEmail);
            Assert.AreEqual(false, firstUserAccountIsGuestUser);

            Assert.IsNotEmpty(secondUserAccountInvitationId.ToString());
            Assert.AreEqual("UserWith", secondUserAccountFirstName);
            Assert.AreEqual("AdminRights", secondUserAccountLastName);
            Assert.AreEqual("UserWith_AdminRights@yopmal.com", secondUserAccountEmail);
            Assert.AreEqual(true, secondUserAccountIsGuestUser);
            Assert.AreEqual(false, secondUserAccountIsSelectedAccount);
        }

        [Test]
        public void AccountSettingsController_SetSelectedAccount_HappyFlow()
        {
            // Arrange
            var accountSettingsController =
                new AccountSettingsController(null, _accountSettingsShareBlMock.Object, null, null)
                {
                    ControllerContext = _controllerContext
                };

            // Act
            var result = accountSettingsController.SetSelectedAccount(GetDefaultGuid()).Result as ObjectResult;
            var actualResult = (ResponseModel) result.Value;
            var responseMessage = (string) actualResult.Content;

            // Assert
            Assert.AreEqual("Account selected successfully!", responseMessage);
        }

        private Guid GetDefaultGuid()
        {
            return Guid.NewGuid();
        }

        [TearDown]
        public void AccountSettingsController_TearDown()
        {
            _deviceBlMock = null;
            _accountSettingsShareBlMock = null;
            _hubBlMock = null;
            _controllerContext = null;
            _controllerContext = null;
            _token = null;
            _cognitoId = null;
        }
    }
}
