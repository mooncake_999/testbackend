﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Controllers;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System.Threading.Tasks;

namespace MyEnergi.Controller.Tests
{
    [TestFixture]
    public class HubRegistrationControllerTests
    {
        private Mock<IHubBL> _hubBlMock;
        private Mock<IDeviceBL> _deviceMock;
        private Mock<IAccountSettingsDeviceBL> _accountSettingsDeviceBlMock;
        private ControllerContext _controllerContext;
        private string _token;

        [SetUp]
        public void HubRegistrationController_SetUp()
        {
            _hubBlMock = new Mock<IHubBL>();
            _deviceMock = new Mock<IDeviceBL>();
            _accountSettingsDeviceBlMock = new Mock<IAccountSettingsDeviceBL>();
            var httpContext = new DefaultHttpContext();
            var user = UserMockFactory.GetNewFullUser();
            _token = MockHelper.GenerateJwtToken(user);
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;

            _controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }

        [Test]
        public void HubRegistrationController_AddNewHub_HappyFlow()
        {
            // Arrange
            var hubData = HubMockFactory.GetHubData();
            var response = new ResponseModel()
            {
                Content = null,
                Status = true,
                Message = "New hub successfully added."
            };

            _hubBlMock
                .Setup(bl => bl.RegisterNewHub(hubData, MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(response));

            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var HubRegistrationController =
                new HubRegistrationController(_hubBlMock.Object, _deviceMock.Object, _accountSettingsDeviceBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = HubRegistrationController.AddNewHub(hubData).Result as ObjectResult;
            var actualResult = result.Value;
            var message =
                (string)actualResult.GetType().GetProperty("Message")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("New hub successfully added.", message);
        }


        [Test]
        public void HubRegistrationController_GetNewDevices_HappyFlow()
        {    // Arrange
            _accountSettingsDeviceBlMock
                .Setup(ac => ac.GetNewDevices(MockConstants.CognitoIdFromToken));


            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var HubRegistrationController =
                new HubRegistrationController(_hubBlMock.Object, _deviceMock.Object, _accountSettingsDeviceBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = HubRegistrationController.GetNewDevices().Result as ObjectResult;
            var actualResult = result.Value;
            var message =
                (string)actualResult.GetType().GetProperty("Message")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
        }

        [TearDown]
        public void HubRegistrationController_TearDown()
        {
            _hubBlMock = null;
            _deviceMock = null;
            _accountSettingsDeviceBlMock = null;
            _token = null;
            _controllerContext = null;
        }
    }
}
