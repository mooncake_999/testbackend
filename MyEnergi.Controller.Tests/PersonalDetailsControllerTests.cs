﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Controllers;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Controller.Tests
{
    [TestFixture]
    public class PersonalDetailsControllerTests
    {
        private Mock<IUserBL> _userBlMock;
        private Mock<IVehicleBL> _vehicleBlMock;
        private Mock<IAccountSettingsShareBL> _accountSettingsShareBlMock;
        private ControllerContext _controllerContext;
        private string _token;
        private const string UserDoesNotExistOr = "User does not exists in the DB or access denied";
        private const string UserDoesNotExist = "User does not exists in the DB";

        [SetUp]
        public void PersonalDetailsController_SetUp()
        {
            _userBlMock = new Mock<IUserBL>();
            _vehicleBlMock = new Mock<IVehicleBL>();
            _accountSettingsShareBlMock = new Mock<IAccountSettingsShareBL>();
            
            var httpContext = new DefaultHttpContext();
            var user = UserMockFactory.GetNewFullUser();
            _token = MockHelper.GenerateJwtToken(user);
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;

            _accountSettingsShareBlMock
                .Setup(bl => bl.GetInvitationOwnerCognitoId(user.CognitoId))
                .Returns(Task.FromResult(user.CognitoId));

            _controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }

        [Test]
        public void PersonalDetailsController_GetUserDetails_HappyFlow()
        {
            // Arrange
            var userDetails = UserMockFactory.GetUserPersonalDetailsData();

            _userBlMock
                .Setup(bl => bl.GetPersonalDetails(MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(userDetails));

            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = personalDetailsController.GetPersonalDetails().Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (UserPersonalDetailsData)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual(2, content.Address.Count);
            Assert.AreEqual(2, content.UserVehicles.Count);
            Assert.AreEqual("Mary", content.FirstName);
            Assert.AreEqual("Maybach S-Class", content.UserVehicles[0].Model);
        }

        [Test]
        public void PersonalDetailsController_GetUserDetails_ExceptionFlow()
        {
            // Arrange
            _userBlMock
                .Setup(rp => rp.GetPersonalDetails(MockConstants.CognitoIdFromToken))
                .Throws(new Exception(UserDoesNotExistOr));

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act
            var exception =
                Assert.ThrowsAsync<Exception>(() => personalDetailsController.GetPersonalDetails());

            // Assert
            Assert.AreEqual(UserDoesNotExistOr, exception.Message);
            Assert.AreEqual(true, exception is Exception);
        }

        [Test]
        public void PersonalDetailsController_GetVehicleManufacturers_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var manufacturers = UserMockFactory.GetManufacturers();
            _userBlMock
                .Setup(bl => bl.ValidateThatUserExists(MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(user));
            _vehicleBlMock
                .Setup(v => v.GetAllManufacturers())
                .Returns(Task.FromResult(manufacturers));

            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = personalDetailsController.GetVehicleManufacturers().Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (IList<string>)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("Audi", content[0]);
        }

        [Test]
        public void PersonalDetailsController_GetManufacturerVehicleModels_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var vehicles = UserMockFactory.GetVehicles();
            _userBlMock
                .Setup(bl => bl.ValidateThatUserExists(MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(user));
            _vehicleBlMock
                .Setup(v => v.GetVehicleByManufacturer("Porsche"))
                .Returns(Task.FromResult(vehicles));

            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = personalDetailsController.GetManufacturerVehicleModels("Porsche").Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (IList<KeyValuePair<Guid, string>>)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.IsNotEmpty(content);
            Assert.AreEqual("Cayenne E-Hybrid (2020-)", content[1].Value);
            Assert.AreNotEqual(content[0], content[1]);
            Assert.AreNotEqual(content[0], content[2]);
            Assert.AreNotEqual(content[0], content[3]);
            Assert.AreNotEqual(content[0], content[4]);
        }

        [Test]
        public void PersonalDetailsController_SavePersonalDetails_HappyFlow()
        {
            // Arrange
            var user = UserMockFactory.GetNewFullUser();
            var personalDetails = UserMockFactory.GetUserDetailsData();
            _userBlMock
                .Setup(bl => bl.ValidateThatUserExists(MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(user));

            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = personalDetailsController.SaveUserDetails(personalDetails).Result as ObjectResult;
            var actualResult = result.Value;
            var message =
                (object)actualResult.GetType().GetProperty("Message")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("User personal details saved.", message);
        }

        [Test]
        public void PersonalDetailsController_SavePersonalDetails_ExceptionFlow()
        {
            // Arrange
            var userData = UserMockFactory.GetUserDetailsData();
            _userBlMock
                .Setup(bl => bl.SaveUserDetails(MockConstants.CognitoIdFromToken, userData))
                .Throws(new Exception(UserDoesNotExist));

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act
            var exception =
                Assert.ThrowsAsync<Exception>(() => personalDetailsController.SaveUserDetails(userData));

            // Assert
            Assert.AreEqual(UserDoesNotExist, exception.Message);
            Assert.AreEqual(true, exception is Exception);
        }

        [Test]
        public void PersonalDetailsController_GetUsersAddresses_HappyFlow()
        {
            // Arrange
            var addresses = UserMockFactory.GetAddresses();
            _userBlMock
                .Setup(bl => bl.GetUsersAddresses(MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(addresses));


            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = personalDetailsController.GetUsersAddresses().Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (IList<AddressData>)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("GB", content[0].CountryCode);
            Assert.AreEqual("Edinburgh", content[0].Region);
        }

        [Test]
        public void PersonalDetailsController_GetUserVehicles_HappyFlow()
        {
            // Arrange
            var vehicles = UserMockFactory.GetVehicleData();
            _vehicleBlMock
                .Setup(vl => vl.GetUserVehicles(MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(vehicles));


            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var personalDetailsController =
                new PersonalDetailsController(_userBlMock.Object, _vehicleBlMock.Object, _accountSettingsShareBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = personalDetailsController.GetUserVehicles().Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (List<VehicleData>)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("Mercedes", content[0].Manufacturer);
            Assert.AreEqual("Maybach S-Class", content[0].Model);
        }

        [TearDown]
        public void PersonalDetailsController_TearDown()
        {
            _userBlMock = null;
            _vehicleBlMock = null;
            _accountSettingsShareBlMock = null;
            _controllerContext = null;
            _token = null;
        }
    }
}
