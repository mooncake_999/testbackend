﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Moq;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Controllers;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace MyEnergi.Controller.Tests
{
    [TestFixture]
    public class UserPreferencesControllerTests
    {
        private Mock<IUserPreferencesBL> _userPreferencesBlMock;
        private Mock<IAccountSettingsShareBL> _accountSettingsShareBlMock;
        private Mock<IConfiguration> _configurationMock;
        private ControllerContext _controllerContext;
        private const string UserDoesNotExistsMessage = "User does not exists in the DB";
        private string _token;

        [SetUp]
        public void UserPreferencesController_SetUp()
        {
            // Arrange
            _userPreferencesBlMock = new Mock<IUserPreferencesBL>();
            _accountSettingsShareBlMock = new Mock<IAccountSettingsShareBL>();
            _configurationMock = new Mock<IConfiguration>();

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:Currency")])
                .Returns("GBP");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:DateFormat")])
                .Returns("dd/MM/yyyy");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:IsMetric")])
                .Returns("false");

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "DefaultSettings:Language")])
                .Returns("en-GB");

            var httpContext = new DefaultHttpContext();
            var user = UserMockFactory.GetNewFullUser();
            _token = MockHelper.GenerateJwtToken(user);
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;

            _accountSettingsShareBlMock
                .Setup(bl => bl.GetInvitationOwnerCognitoId(user.CognitoId))
                .Returns(Task.FromResult(user.CognitoId));

            _controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }

        [Test]
        public void UserPreferencesController_GetDefaultPreferences_With_GetDefault_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var userPreferencesController =
                new UserPreferencesController(_userPreferencesBlMock.Object, _accountSettingsShareBlMock.Object, _configurationMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = userPreferencesController.GetDefaultPreferences() as ObjectResult;
            var actualResult = result.Value;
            var content =
                (UserPreferenceData)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual(true, content is UserPreferenceData);
            Assert.AreEqual("GBP", content.Currency);
            Assert.AreEqual("dd/MM/yyyy", content.DateFormat);
            Assert.AreEqual(false, content.IsMetric);
            Assert.AreEqual("en-GB", content.Language);
        }

        [Test]
        public void UserPreferencesController_GetDefaultPreferences_With_GetDefault_ExceptionFlow()
        {
            // Arrange
            var userPreferencesController =
                new UserPreferencesController(_userPreferencesBlMock.Object, _accountSettingsShareBlMock.Object, _configurationMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act
            var result = userPreferencesController.GetDefaultPreferences() as ObjectResult;
            var actualResult = result.Value;
            var content =
                (UserPreferenceData)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual(true, content is UserPreferenceData);
            Assert.AreEqual("GBP", content.Currency);
            Assert.AreEqual("dd/MM/yyyy", content.DateFormat);
            Assert.AreEqual(false, content.IsMetric);
            Assert.AreEqual("en-GB", content.Language);
        }

        [Test]
        public void UserPreferencesController_GetUserPreference_HappyFlow()
        {
            // Arrange
            var userPreferenceData = UserPreferenceMockFactory.GetDefaultUserPreferenceData();

            _userPreferencesBlMock
                .Setup(bl => bl.GetPreference(MockConstants.CognitoIdFromToken))
                .Returns(Task.FromResult(userPreferenceData));

            var userPreferencesController =
                new UserPreferencesController(_userPreferencesBlMock.Object, _accountSettingsShareBlMock.Object, _configurationMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act 
            var result = userPreferencesController.GetUserPreference().Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (UserPreferenceData)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("GBP", content.Currency);
            Assert.AreEqual("dd/MM/yyyy", content.DateFormat);
            Assert.AreEqual(false, content.IsMetric);
            Assert.AreEqual("en-GB", content.Language);
        }

        [Test]
        public void UserPreferencesController_GetUserPreference_HappyFlow_With_Null_UserPreferenceData()
        {
            // Arrange
            var userPreferencesController =
                new UserPreferencesController(_userPreferencesBlMock.Object, _accountSettingsShareBlMock.Object, _configurationMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act 
            var result = userPreferencesController.GetUserPreference().Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (UserPreferenceData)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("GBP", content.Currency);
            Assert.AreEqual("dd/MM/yyyy", content.DateFormat);
            Assert.AreEqual(false, content.IsMetric);
            Assert.AreEqual("en-GB", content.Language);
        }

        [Test]
        public void UserPreferencesController_GetUserPreference_Throws_Exception_With_UserDoesNotExistsMessage()
        {
            // Arrange
            _userPreferencesBlMock.Setup(bl => bl.GetPreference(MockConstants.CognitoIdFromToken))
                .Throws(new Exception(UserDoesNotExistsMessage));

            var userPreferencesController =
                new UserPreferencesController(_userPreferencesBlMock.Object, _accountSettingsShareBlMock.Object, _configurationMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act 
            var exception = Assert.ThrowsAsync<Exception>(() => userPreferencesController.GetUserPreference());

            // Assert
            Assert.AreEqual(true, exception is Exception);
            Assert.AreEqual("User does not exists in the DB", exception.Message);
        }

        [Test]
        public void UserPreferencesController_GetDefault_ExceptionFlow()
        {
            // Arrange
            var userPreferencesController =
                new UserPreferencesController(_userPreferencesBlMock.Object, _accountSettingsShareBlMock.Object, _configurationMock.Object)
                {
                    ControllerContext = _controllerContext
                };

            // Act
            var userPreferenceData = userPreferencesController.GetDefault();

            // Assert
            Assert.AreEqual("GBP", userPreferenceData.Currency);
            Assert.AreEqual("dd/MM/yyyy", userPreferenceData.DateFormat);
            Assert.AreEqual(false, userPreferenceData.IsMetric);
            Assert.AreEqual("en-GB", userPreferenceData.Language);
        }

        [Test]
        public void UserPreferencesController_GetDefault_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var userPreferencesController =
                new UserPreferencesController(_userPreferencesBlMock.Object, _accountSettingsShareBlMock.Object, _configurationMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var userPreferenceData = userPreferencesController.GetDefault();

            // Assert
            Assert.AreEqual("GBP", userPreferenceData.Currency);
            Assert.AreEqual("dd/MM/yyyy", userPreferenceData.DateFormat);
            Assert.AreEqual(false, userPreferenceData.IsMetric);
            Assert.AreEqual("en-GB", userPreferenceData.Language);
        }

        [TearDown]
        public void UserPreferencesController_TearDown()
        {
            _userPreferencesBlMock = null;
            _configurationMock = null;
            _controllerContext = null;
        }
    }
}
