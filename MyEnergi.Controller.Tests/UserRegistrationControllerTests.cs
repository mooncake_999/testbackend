﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using Moq;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.Business.Interfaces;
using MyEnergi.Controllers;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System;

namespace MyEnergi.Controller.Tests
{
    [TestFixture]
    public class UserRegistrationControllerTests
    {
        private Mock<IUserBL> _userBlMock;
        private Mock<IRegistrationEmailSender> _registrationEmailSenderMock;
        private Mock<IConfiguration> _configurationMock;
        private ControllerContext _controllerContext;
        private string _token;
        private string UserDoesntExistMessage = $"User with id:{MockConstants.CognitoIdFromToken} does not exist!";

        [SetUp]
        public void UserRegistrationController_SetUp()
        {
            _userBlMock = new Mock<IUserBL>();
            _registrationEmailSenderMock = new Mock<IRegistrationEmailSender>();
            _configurationMock = new Mock<IConfiguration>();

            var config = UserRegistrationMockFactory.GetEmailConfiguration();
            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "RegistrationMailTemplate:Path")])
                .Returns(config.FilePath);
            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfiguration:MailSender")])
                .Returns(config.Sender);

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfiguration:MailSubject")])
                .Returns(config.MailSubject);

            _configurationMock
                .SetupGet(m => m[It.Is<string>(s => s == "EmailConfiguration:MailAddress")])
                .Returns(config.MailAddress);


            var httpContext = new DefaultHttpContext();
            var user = UserMockFactory.GetNewFullUser();
            _token = MockHelper.GenerateJwtToken(user);
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;

            _controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }

        [Test]
        public void UserRegistrationController_Registration_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var userRegistrationController =
                new UserRegistrationController(null, null, _userBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var data = UserRegistrationMockFactory.GetDefaultUserRegistrationData();
            var result = userRegistrationController.Registration(data).Result as ObjectResult;
            var actualResult = result.Value;
            var message =
                (object)actualResult.GetType().GetProperty("Message")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("User successfully registered.", message);
        }

        [Test]
        public void UserRegistrationController_EditUserPreferences_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var userRegistrationController =
                new UserRegistrationController(null, null, _userBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var data = UserRegistrationMockFactory.GetModifiedUserPreferencesData();
            var result = userRegistrationController.EditUserPreferences(data).Result as ObjectResult;
            var actualResult = result.Value;
            var message =
                (object)actualResult.GetType().GetProperty("Message")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("User preferences edited.", message);
        }

        [Test]
        public void UserRegistrationController_EditUserMail_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var userRegistrationController =
                new UserRegistrationController(null, null, _userBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var data = UserRegistrationMockFactory.GetModifiedEmailUserPreferencesData();
            var result = userRegistrationController.EditUserMail(data).Result as ObjectResult;
            var actualResult = result.Value;
            var message =
                (object)actualResult.GetType().GetProperty("Message")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("User mail edited.", message);
        }

        [Test]
        public void UserRegistrationController_EditUserMail_ExceptionFlow()
        {
            // Arrange
            var data = UserRegistrationMockFactory.GetDefaultUserRegistrationData();
            _userBlMock
                .Setup(bl => bl.EditUserMail(data, MockConstants.CognitoIdFromToken))
                .Throws(new Exception($"Email address {data.Email} is already used!"));

            var userRegistrationController =
                new UserRegistrationController(null, null, _userBlMock.Object)
                {
                    ControllerContext = _controllerContext
                };
            // Act
            var exception = Assert.ThrowsAsync<Exception>(() => userRegistrationController.EditUserMail(data));

            // Assert
            Assert.AreEqual(true, exception is Exception);
            Assert.AreEqual($"Email address {data.Email} is already used!", exception.Message);
        }

        [Test]
        public void UserRegistrationController_RegistrationMail_HappyFlow()
        {
            // Arrange
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;
            httpContext.Request.Headers[HeaderNames.AcceptLanguage] = MockConstants.AcceptedLanguage;

            var controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };

            var userRegistrationController =
                new UserRegistrationController(_configurationMock.Object, _registrationEmailSenderMock.Object, _userBlMock.Object)
                {
                    ControllerContext = controllerContext
                };

            // Act
            var result = userRegistrationController.RegistrationMail().Result as ObjectResult;
            var actualResult = result.Value;
            var message =
                (object)actualResult.GetType().GetProperty("Message")?.GetValue(actualResult, null);

            // Assert
            Assert.AreEqual(200, result.StatusCode);
            Assert.AreEqual(true, result is OkObjectResult);
            Assert.AreEqual("Registration mail successfully sent.", message);
        }


        [Test]
        public void UserRegistrationController_EditUserPreferences_ExceptionFlow()
        {
            // Arrange
            var data = UserRegistrationMockFactory.GetModifiedUserPreferencesData();
            _userBlMock
                .Setup(bl => bl.EditUserPreferences(data, MockConstants.CognitoIdFromToken))
                .Throws(new Exception(UserDoesntExistMessage));

            var userRegistrationController =
                new UserRegistrationController(null, null, _userBlMock.Object)
                {
                    ControllerContext = _controllerContext
                };
            // Act
            var exception = Assert.ThrowsAsync<Exception>(() => userRegistrationController.EditUserPreferences(data));

            // Assert
            Assert.AreEqual(true, exception is Exception);
            Assert.AreEqual(UserDoesntExistMessage, exception.Message);
        }

        [Test]
        public void UserRegistrationController_RegistrationMail_ExceptionFlow()
        {
            // Arrange
            var data = UserRegistrationMockFactory.GetEmailConfiguration();
            _registrationEmailSenderMock
                .Setup(bl => bl.SendRegistrationMail(MockConstants.CognitoIdFromToken, data.FilePath,data.Sender, data.MailAddress, data.MailSubject))
                .Throws(new Exception(UserDoesntExistMessage));

            var userRegistrationController =
                new UserRegistrationController(_configurationMock.Object, _registrationEmailSenderMock.Object, _userBlMock.Object)
                {
                    ControllerContext = _controllerContext
                };
            // Act
            var exception = Assert.ThrowsAsync<Exception>(() => userRegistrationController.RegistrationMail());

            // Assert
            Assert.AreEqual(true, exception is Exception);
            Assert.AreEqual(UserDoesntExistMessage, exception.Message);
        }

        [TearDown]
        public void UserRegistrationController_TearDown()
        {
            _userBlMock = null;
            _configurationMock = null;
            _controllerContext = null;
            _registrationEmailSenderMock = null;
            _token = null;
        }
    }
}
