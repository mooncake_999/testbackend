﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Moq;
using MyEnergi.AppServer.Model.Weather;
using MyEnergi.Business.Interfaces;
using MyEnergi.Common.CustomExceptions;
using MyEnergi.Controllers;
using MyEnergi.Tests.ObjectFactory;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Controller.Tests
{
    [TestFixture]
    public class WeatherControllerTests
    {
        private Mock<IWeatherBL> _weatherBl;
        private Mock<IAccountSettingsShareBL> _accountSettingsShareBl;
        
        private ControllerContext _controllerContext;
        private string _userCognitoId;
        private string _token;

        [SetUp]
        public void WeatherController_SetUp()
        {
            _weatherBl = new Mock<IWeatherBL>();
            _accountSettingsShareBl = new Mock<IAccountSettingsShareBL>();

            var httpContext = new DefaultHttpContext();
            var user = UserMockFactory.GetNewFullUser();
            _userCognitoId = user.CognitoId;
            _token = MockHelper.GenerateJwtToken(user);
            httpContext.Request.Headers[HeaderNames.Authorization] = _token;

            _accountSettingsShareBl
                .Setup(bl => bl.GetInvitationOwnerCognitoId(_userCognitoId))
                .Returns(Task.FromResult(_userCognitoId));

            _controllerContext = new ControllerContext
            {
                HttpContext = httpContext
            };
        }

        [Test]
        public void WeatherController_GetForecast_HappyFlow()
        {
            // Arrange
            var weatherController = new WeatherController(_weatherBl.Object, _accountSettingsShareBl.Object)
            {
                ControllerContext = _controllerContext
            };

            var forecastData = WeatherForecastMockFactory.GetForecastData();
            _weatherBl
                .Setup(bl => bl.GetForecastData(_userCognitoId))
                .Returns(Task.FromResult(forecastData));

            // Act
            var result = weatherController.GetForecast().Result as ObjectResult;
            var actualResult = result.Value;
            var content =
                (List<object>)actualResult.GetType().GetProperty("Content")?.GetValue(actualResult, null);

            var firstForecast = content.First();

            var dailyForecastData = (DailyWeatherForecastData)firstForecast.GetType().GetProperty("dailyForecastData")
                ?.GetValue(firstForecast);

            var hourlyForecastData = (List<HourlyWeatherForecastData>)firstForecast.GetType()
                .GetProperty("hourlyForecastData")?.GetValue(firstForecast);

            // Assert
            Assert.AreEqual(true, dailyForecastData is DailyWeatherForecastData);
            Assert.AreEqual("Tue 13th", dailyForecastData.DateToDisplay);
            Assert.AreEqual(1.1d, dailyForecastData.DailyMinTemp);
            Assert.AreEqual(11.5d, dailyForecastData.DailyMaxTemp);

            var firstHourForecastData = hourlyForecastData.First();
            Assert.AreEqual(true, firstHourForecastData is HourlyWeatherForecastData);
            Assert.AreEqual(24, hourlyForecastData.Count);
            Assert.AreEqual(true, hourlyForecastData is List<HourlyWeatherForecastData>);
            Assert.AreEqual(0d, firstHourForecastData.HourOfTheDay);
            Assert.AreEqual(165.695d, firstHourForecastData.SolarRadiation);
        }

        [Test]
        public void WeatherController_GetForecast_Throws_User_NotFoundException()
        {
            // Arrange
            var weatherController = new WeatherController(_weatherBl.Object, _accountSettingsShareBl.Object)
            {
                ControllerContext = _controllerContext
            };

            const string exceptionMessage = "User not found";
            _weatherBl
                .Setup(bl => bl.GetForecastData(_userCognitoId))
                .Throws(new NotFoundException(exceptionMessage));

            // Act & Assert
            var exception = Assert.ThrowsAsync<NotFoundException>(() => weatherController.GetForecast());

            Assert.AreEqual(exceptionMessage, exception.Message);
        }

        [Test]
        public void WeatherController_GetForecast_Throws_Address_NotFoundException()
        {
            // Arrange
            var weatherController = new WeatherController(_weatherBl.Object, _accountSettingsShareBl.Object)
            {
                ControllerContext = _controllerContext
            };

            const string exceptionMessage = "Addresses not found";
            _weatherBl
                .Setup(bl => bl.GetForecastData(_userCognitoId))
                .Throws(new NotFoundException(exceptionMessage));

            // Act & Assert
            var exception = Assert.ThrowsAsync<NotFoundException>(() => weatherController.GetForecast());

            Assert.AreEqual(exceptionMessage, exception.Message);
        }

        [TearDown]
        public void WeatherController_TearDown()
        {
            _weatherBl = null;
            _controllerContext = null;
            _userCognitoId = null;
            _token = null;
        }
    }
}
