﻿using System;

namespace MyEnergi.Data.Entity
{
    public class Address : Entity
    {
        public User User { get; set; }
        public string SiteName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
