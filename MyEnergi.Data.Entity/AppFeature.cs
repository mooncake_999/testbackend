﻿using System;

namespace MyEnergi.Data.Entity
{
    public class AppFeature : Entity
    {
        public string Feature { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
