﻿using AutoMapper;
using System;

namespace MyEnergi.Data.Entity.AutoMapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Vehicle, Vehicle>().ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<Hub, Hub>().ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<Device, Device>().ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<DeviceChargesSetup, DeviceChargesSetupHistory>().ForMember(x => x.Id, opt => opt.Ignore())
                                                                    .ForMember(x => x.DeviceId, opt => opt.MapFrom(y => y.Device.Id))
                                                                    .ForMember(x => x.MovedToHistory, opt => opt.MapFrom(o => DateTime.Now));
            CreateMap<DeviceBestPriceIntervals, DeviceBestPriceHistory>().ForMember(x => x.Id, opt => opt.Ignore())
                                                                    .ForMember(x => x.PriceCreated, opt => opt.MapFrom(o => o.LastUpdate))
                                                                    .ForMember(x => x.DeviceId, opt => opt.MapFrom(y => y.Device.Id))
                                                                    .ForMember(x => x.MovedToHistory, opt => opt.MapFrom(o => DateTime.Now));
        }
    }
}
