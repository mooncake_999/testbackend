﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Data.Entity
{
    public class Device : Entity
    {
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string SerialNumber { get; set; }
        public Address Address { get; set; }
        public Hub Hub { get; set; }
        public DateTime RegistrationStartDate { get; set; }
        public DateTime? RegistrationEndDate { get; set; }
        public InstallationFeedback Installation { get; set; }
        public string HeaterOutput1 { get; set; }
        public string HeaterOutput2 { get; set; }
        public int Status { get; set; }
        public string TimeZoneRegion { get; set; }
        public string Firmware { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool IsDeleted { get; set; }
        public IList<DeviceChargesSetup> ChargeSchedules { get; set; }

        public Device()
        {
            ChargeSchedules = new List<DeviceChargesSetup>();
        }
    }
}
