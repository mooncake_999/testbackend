﻿using System;

namespace MyEnergi.Data.Entity
{
    public class DeviceBestPriceHistory : Entity
    {
        public DateTime MovedToHistory { get; set; }
        public int DeviceId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public bool IsScheduledType { get; set; }
        public bool IsSingleType { get; set; }
        public bool IsBudgetType { get; set; }
        public string HeaterSlot { get; set; }
        public decimal SlotPriceValue { get; set; }
        public DateTime PriceCreated { get; set; }
    }
}
