﻿using System;

namespace MyEnergi.Data.Entity
{
    public class DeviceChargesSetup : Entity
    {
        public DateTime LastUpdated { get; set; }
        public Device Device { get; set; }
        public string ScheduleType { get; set; }
        public string ScheduleNickName { get; set; }
        public string ChargingOutputName { get; set; }
        public int? ChargeAmountMinutes { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string DaysOfWeek { get; set; }
        public decimal? PriceBelow { get; set; }
        public DateTime? SingleChargeDay { get; set; }
        public bool IsActive { get; set; }
    }
}
