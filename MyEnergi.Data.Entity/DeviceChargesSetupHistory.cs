﻿using System;

namespace MyEnergi.Data.Entity
{
    public class DeviceChargesSetupHistory : Entity
    {
        public DateTime MovedToHistory { get; set; }
        public int DeviceId { get; set; }
        public string ScheduleType { get; set; }
        public string ScheduleNickName { get; set; }
        public string ChargingOutputName { get; set; }
        public int? ChargeAmountMinutes { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string DaysOfWeek { get; set; }
        public decimal? PriceBelow { get; set; }
        public DateTime? SingleChargeDay { get; set; }
        public bool IsActive { get; set; }
    }
}
