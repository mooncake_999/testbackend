﻿using System;

namespace MyEnergi.Data.Entity
{
    public class DeviceHistory : Entity
    {
        public User OwnerUser { get; set; }
        public InstallationFeedback Installation { get; set; }
        public string DeviceName { get; set; }
        public string DeviceType { get; set; }
        public string SerialNumber { get; set; }
        public DateTime MovedToHistory { get; set; }
    }
}
