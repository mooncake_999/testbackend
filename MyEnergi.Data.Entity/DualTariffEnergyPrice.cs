﻿using System;

namespace MyEnergi.Data.Entity
{
    public class DualTariffEnergyPrice : Entity
    {
        public string Days { get; set; }
        public int FromMinutes { get; set; }
        public int ToMinutes { get; set; }
        public decimal Price { get; set; }
        public EnergySetup EnergySetup { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastUpdateDate { get; set; }
    }
}
