﻿using System;

namespace MyEnergi.Data.Entity
{
    public class EnergyPrice : Entity
    {
        public string GridSupplyPoint { get; set; }
        public float ValueWithoutVAT { get; set; }
        public float ValueWithVAT { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public EnergyProvider EnergyProv { get; set; }
        public DateTime? LastUpdate { get; set; } = DateTime.UtcNow;
        public string Currency { get; set; }

    }
}
