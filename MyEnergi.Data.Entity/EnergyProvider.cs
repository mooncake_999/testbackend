﻿using System;

namespace MyEnergi.Data.Entity
{
    public class EnergyProvider : Entity
    {
        public string Provider { get; set; }
        public string Tariff { get; set; }
        public bool IsFlexible { get; set; }
        public bool IsGreen { get; set; }
        public bool IsEV { get; set; }
        public bool IsEconomy { get; set; }
        public string Country { get; set; }
        public string InternalReference { get; set; }
        public bool IsActive { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
