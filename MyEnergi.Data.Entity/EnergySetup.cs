﻿using System;

namespace MyEnergi.Data.Entity
{
    public class EnergySetup : Entity
    {
        public DateTime? TariffExpirationDate { get; set; }
        public bool SolarPanels { get; set; }
        public bool WindTurbine { get; set; }
        public bool Hydroelectric { get; set; }
        public string OtherGeneration { get; set; }
        public decimal? MaxPowerOutput { get; set; }
        public bool AirSourceHeatPump { get; set; }
        public bool GroundSourceHeatPump { get; set; }
        public bool ElectricStorageHeating { get; set; }
        public string OtherElectricHeating { get; set; }
        public Address Address { get; set; }
        public string GridSupplyPoint { get; set; }
        public EnergyProvider EnergyProv { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
