﻿using System;

namespace MyEnergi.Data.Entity
{
    public class Entity
    {
        public int Id { get; set; }

        public Guid Guid { get; set; }
    }
}
