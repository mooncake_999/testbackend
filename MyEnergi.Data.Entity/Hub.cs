﻿using System;

namespace MyEnergi.Data.Entity
{
    public class Hub : Entity
    {
        public Address Address { get; set; }
        public string SerialNo { get; set; }
        public string RegistrationCode { get; set; }
        public string NickName { get; set; }
        public string AppPassword { get; set; }
        public DateTime RegistrationStartDate { get; set; }
        public DateTime? RegistrationEndDate { get; set; }
        public InstallationFeedback Installation { get; set; }
        public string TimeZoneRegion { get; set; }
        public string Firmware { get; set; }
        public int Status { get; set; }
        public string HostServer { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastUpdateAppPassword { get; set; }
    }
}
