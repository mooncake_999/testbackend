﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Data.Entity
{
    public class HubHistory : Entity
    {
        public User OwnerUser { get; set; }
        public InstallationFeedback Installation { get; set; }
        public string Nickname { get; set; }
        public string SerialNumber { get; set; }
        public DateTime MovedToHistory { get; set; }

    }
}
