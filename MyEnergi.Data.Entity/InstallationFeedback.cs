﻿using System;

namespace MyEnergi.Data.Entity
{
    public class InstallationFeedback : Entity
    {
        public string InstalledBy { get; set; }
        public string Installer { get; set; }
        public string InstallerCode { get; set; }
        public string InstallerName { get; set; }
        public string InstallerWebsite { get; set; }
        public string InstallerPhoneNumber { get; set; }
        public DateTime? InstallationDate { get; set; }
        public string QualityOfInstall { get; set; }
        public string Feedback { get; set; }
        public bool ShareFeedback { get; set; }
        public bool PrizeDrawOptIn { get; set; }
        public bool SurveyOptIn { get; set; }
        public bool RecievedGrant { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
