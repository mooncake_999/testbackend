﻿using System;

namespace MyEnergi.Data.Entity
{
    public class Installer : Entity
    {
        public string InstallerName { get; set; }
        public string InstallerCode { get; set; }
        public string Country { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
