﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Data.Entity
{
    public class Invitation : Entity
    {
        public User GuestUser { get; set; }
        public User OwnerUser { get; set; }
        public bool IsSelectedAccount { get; set; }
        public InvitationStatus InvitationStatus { get; set; }
        public InvitationDetails InvitationDetails { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime CreationDate { get; set; }
        public string CronExpressionRecurrence { get; set; }
        public DateTime? LastUpdate { get; set; }
        public IList<UserFeaturePermission> UserFeaturePermissions { get; set; }
        public IList<UserFeaturePermissionHistory> UserFeaturePermissionHistories { get; set; }

        public Invitation()
        {
            UserFeaturePermissions = new List<UserFeaturePermission>();
            UserFeaturePermissionHistories = new List<UserFeaturePermissionHistory>();
        }
    }
}
