﻿using System;

namespace MyEnergi.Data.Entity
{
    public class InvitationDetails : Entity
    {
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public bool AppAccess { get; set; }
        public bool AdministratorAccess { get; set; }
        public bool AllDevicesAccess { get; set; }

        public DateTime? LastUpdate { get; set; }
    }
}
