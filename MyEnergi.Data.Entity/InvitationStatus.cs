﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Data.Entity
{
    public class InvitationStatus : Entity
    {
        public string Status { get; set; }
        public DateTime? LastUpdate { get; set; }
        public IEnumerable<Invitation> Invitations { get; set; }

        public InvitationStatus()
        {
            Invitations = new List<Invitation>();
        }
    }
}
