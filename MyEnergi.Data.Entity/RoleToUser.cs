﻿using System;

namespace MyEnergi.Data.Entity
{
    public class RoleToUser : Entity
    {
        public User User { get; set; }
        public UserRole Role { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
