﻿using System;

namespace MyEnergi.Data.Entity
{
    public class SystemFeature : Entity
    {
        public string Feature { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
