﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Data.Entity
{
    public class User : Entity
    {
        public string Email { get; set; }
        public bool PrivacyAcceptance { get; set; }
        public bool MarketingOptIn { get; set; }
        public bool ThirdPartyMarketingOptIn { get; set; }
        public string CognitoId { get; set; }
        public UserDetails UserDetails { get; set; }
        public UserPreference UserPreference { get; set; }
        public IList<UserVehicle> UserVehicles { get; set; }
        public IList<Address> Addresses { get; set; }
        public DateTime? LastUpdate { get; set; }

        public User()
        {
            UserVehicles = new List<UserVehicle>();
            Addresses = new List<Address>();
        }
    }
}
