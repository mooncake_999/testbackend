﻿using System;

namespace MyEnergi.Data.Entity
{
    public class UserDetails : Entity
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileCode { get; set; }
        public string MobileNumber { get; set; }
        public string LandlineCode { get; set; }
        public string LandlineNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
