﻿using System;

namespace MyEnergi.Data.Entity
{
    public class UserFeaturePermission : Entity
    {
        public User OwnerUser { get; set; }
        public Invitation Invitation { get; set; }
        public Device Device { get; set; }
        public AppFeature AppFeature { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
