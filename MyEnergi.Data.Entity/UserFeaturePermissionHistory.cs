﻿using System;

namespace MyEnergi.Data.Entity
{
    public class UserFeaturePermissionHistory : Entity
    {
        public User OwnerUser { get; set; }
        public Invitation Invitation { get; set; }
        public Device Device { get; set; }
        public AppFeature AppFeature { get; set; }
        public DateTime MovedToHistory { get; set; }
    }
}
