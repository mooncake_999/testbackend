﻿using System;

namespace MyEnergi.Data.Entity
{
    public class UserPreference : Entity
    {
        public string DateFormat { get; set; }
        public bool IsMetric { get; set; }
        public string Language { get; set; }
        public string Currency { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
