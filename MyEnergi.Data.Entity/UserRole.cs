﻿using System;
using System.Collections.Generic;

namespace MyEnergi.Data.Entity
{
    public class UserRole : Entity
    {
        public string Role { get; set; }
        public IList<SystemFeature> SystemFeatures { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
