﻿using System;

namespace MyEnergi.Data.Entity
{
    public class UserVehicle : Entity
    {
        public User User { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int? ManufacturingYear { get; set; }
        public float? BatterySize { get; set; }
        public int? ManufacturerOfficialEVRange { get; set; }
        public int? RealEVRange { get; set; }
        public string VehicleNickname { get; set; }
        public float? ChargeRate { get; set; }
        public string Condition { get; set; }
        public string Ownership { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
