﻿using System;

namespace MyEnergi.Data.Entity
{
    public class Vehicle : Entity
    {
        public int RefId { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int? AvailabilityDateFrom { get; set; }
        public int? AvailabilityDateTo { get; set; }
        public float? BatterySize { get; set; }
        public int? ManufacturerOfficialEVRange { get; set; }
        public int? RealEVRange { get; set; }
        public float? ChargeRate { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
