﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IAddressRepository : IRepository<Address>
    {
        Task<IEnumerable<Address>> GetAddresses(string userCognitoId);

        Task<Address> GetAddressById(Guid addressId);

        Task<IEnumerable<int>> GetAddressesIdByUserId(string userId);
        
        Task<IEnumerable<Address>> GetUserEnergyAddresses(string userCognitoId);

        Task<Address> GetAddressByIdAndUserId(Guid addressId, string userCognitoId);

        Task<IEnumerable<int>> GetIsEconomyAddresses();
        
        Task<IEnumerable<int>> GetNonEconomyAddressId();
        
        Task<Dictionary<int, string>> GetUserDateFormatPreferenceByAddress(List<int> addrIds);

        Task<Dictionary<Guid, User>> GetAddressGuidsAndUsersByAddressesIds(List<Guid> addressIds);
        Task<IEnumerable<int>> GetAddressFromEnergySetupBasedOnInternalReference(string providerInternalReference);
        Task<IEnumerable<int>> GetAddressFromEnergySetupBasedOnProviderName(string providerName);
    }
}
