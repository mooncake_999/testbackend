﻿using MyEnergi.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IAppFeatureRepository : IRepository<AppFeature>
    {
        Task<IEnumerable<AppFeature>> GetAllAppFeaturesByName(IEnumerable<string> featuresName);
    }
}