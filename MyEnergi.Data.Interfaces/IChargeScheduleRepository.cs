﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IChargeScheduleRepository : IRepository<DeviceChargesSetup>
    {
        Task<IEnumerable<DeviceChargesSetup>> GetAllChargeSchedulesByUser(string userCognitoId);
        Task<IEnumerable<DeviceChargesSetup>> GetChargeSchedules();
        Task DeleteChargeSetups(List<DeviceChargesSetup> dbChargesSetupsToDelete);
        Task<IEnumerable<DeviceChargesSetup>> GetChargeSchedulesByDeviceId(Guid id);
        Task MoveChargeToHistoryTable(DeviceChargesSetup dbChargeSetup);
        Task<IEnumerable<DeviceChargesSetup>> GetAllEconomyChargesByAddressId(List<int> economyAddressId);
    }
}
