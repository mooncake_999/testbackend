﻿using MyEnergi.Data.Entity;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IDeviceHistoryRepository : IRepository<DeviceHistory>
    {
        Task<DeviceHistory> GetDeviceHistoryBySerial(string serialNo);
    }
}
