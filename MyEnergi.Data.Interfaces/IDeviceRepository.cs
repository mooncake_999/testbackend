﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IDeviceRepository : IRepository<Device>
    {
        Task<Device> GetDeviceById(Guid deviceId);

        Task<IEnumerable<Device>> GetDevicesByUserId(string userId);

        Task<Device> GetDeviceBySerialNo(string serialNumber);

        Task<IEnumerable<Device>> GetDevicesByHubId(Guid id);

        Task<IEnumerable<Device>> GetDevicesWithHubsAndChargeSchedulesByUserId(string userCognitoId);

        Task<Device> GetDeviceWithChargeScheduleById(Guid deviceId, string userCognitoId);

        Task<IEnumerable<Device>> GetAllDevicesWithChargeSchedule();

        Task<IEnumerable<Device>> GetDevicesWithHubsByUserId(string userCognitoId);

        Task<bool> CheckIfDevicesExistsOnAddress(Guid addressId);

        Task<Device> GetDeviceByIdAndUserId(Guid deviceId, string userCognitoId);

        Task<IEnumerable<Device>> GetDevicesByHubsId(IEnumerable<Guid> hubIds);

        Task<IEnumerable<Device>> GetAllDevicesWithChargeScheduleByAddressId(List<int> addressesNotEconomy);

        Task<IEnumerable<Device>> GetDevicesByUserIdIsDeleted(string userId);
        
        Task<Device> GetDeviceBySerialNoIsDeleted(string serialNumber);
        
        Task<IEnumerable<Device>> GetDevicesByHubIdIsDeleted(Guid id);

        Task<Device> GetGuestDeviceByIdAndCognitoId(Guid deviceId, string userCognitoId);
    }
}
