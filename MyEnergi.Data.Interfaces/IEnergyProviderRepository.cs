﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IEnergyProviderRepository : IRepository<EnergyProvider>
    {
        Task<EnergyProvider> GetEnergyProviderByFields(string energyProvider, string tariffName);
        Task<IEnumerable<EnergyProvider>> GetAllEnergyProviders();
        Task AddUpdateEnergySetup(EnergySetup energy);
        Task<IEnumerable<EnergySetup>> GetEnergySetupByUserId(string userCognitoId);
        Task<IEnumerable<EnergySetup>> GetAllEnergySetups();
        Task SaveEnergyPrices(List<EnergyPrice> energyPrices);
        Task DeleteEnergyPricesWithGSP(string gridSupplyPoint);
        Task DeleteEnergyPrices(string gridSupplyPoint, int energyProviderId);
        Task<IEnumerable<EnergyPrice>> GetEnergyPrices(int energyProviderId, string gsp);
        Task<IEnumerable<EnergyPrice>> GetAllEnergyPrices();
        Task<EnergySetup> GetEnergySetupByAddressId(Guid addressId);
        Task<EnergySetup> GetEnergySetupById(Guid id);
        Task SaveBestPriceIntervals(List<DeviceBestPriceIntervals> bestPriceIntervals);
        Task DeleteBestPriceIntervals(Guid deviceId, DateTime now);
        Task<IEnumerable<DeviceBestPriceIntervals>> GetDeviceBestPriceIntervalsByDeviceIds(List<Guid> deviceIds);
        Task DeleteEnergyProviders(List<EnergyProvider> dbProvidersToBeDeleted);
        Task<EnergySetup> GetEnergySetupByIdAndUser(Guid energySetupId, string userCognitoId);
        Task RemoveEnergySetup(EnergySetup energySetup);
        Task SaveDualTariffEnergyPrice(DualTariffEnergyPrice dualTariffEnergyPrice);
        Task<DualTariffEnergyPrice> GetDualTariffEnergyPriceById(Guid dualTariffId);
        Task<IEnumerable<EnergyProvider>> GetUsedEnergyProviders();
        Task DeleteEnergyPricesByProvider(int energyProviderId);
        Task<IEnumerable<DualTariffEnergyPrice>> GetDualTariffEnergyPriceByAddress(EnergySetup energySetup);
        Task DeleteDualTariffEnergyPrice(IEnumerable<DualTariffEnergyPrice> dualTariffEnergyPrice);
        Task<IEnumerable<DualTariffEnergyPrice>> GetDualTariffEnergyPriceByListOfAddressesId(List<int> addressId);
        Task<IEnumerable<DeviceBestPriceIntervals>> GetDeviceBestPriceIntervalByHubId(int hubId);
        Task<EnergyProvider> GetEnergyProviderByGuid(Guid energyProviderId);
        Task<IEnumerable<EnergyProvider>> GetEnergyProvidersByField(string energyProvider);

    }
}
