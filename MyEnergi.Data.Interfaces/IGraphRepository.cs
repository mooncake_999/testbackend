﻿using MyEnergi.Data.Entity.RedisModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IGraphRepository
    {
        Task<Dictionary<string, IEnumerable<DeviceEnergyInfo>>> GetDevicesEnergyInfoByHub(Guid hubId, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity);
        Task<Dictionary<string, IEnumerable<DeviceEnergyInfo>>> GetDevicesEnergyInfoByHubDST(Guid hubId, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity);
        Task<Dictionary<string, IEnumerable<DeviceEnergyInfo>>> GetDevicesEnergyInfoByHubDSTSpring(Guid hubId, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity);
    }
}