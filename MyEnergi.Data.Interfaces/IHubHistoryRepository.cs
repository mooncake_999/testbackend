﻿using MyEnergi.Data.Entity;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IHubHistoryRepository : IRepository<HubHistory>
    {
        Task<HubHistory> GetHubHistory(string serialNo);
    }
}
