﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IHubRepository : IRepository<Hub>
    {
        Task<Hub> GetHubById(Guid hubId);
        Task<Hub> GetHubBySerialNo(string serialNo);
        Task<IEnumerable<Hub>> GetHubByUserId(string userId);
        Task RemoveHub(Hub hub);
        Task<bool> CheckIfHubExistsOnAddress(Guid addressId);
        Task<Hub> GetHubByIdAndUser(Guid id, string userCognitoId);
        Task<IEnumerable<Hub>> GetHubsByIdsAndUser(List<Guid> hubIds, string userCognitoId);
        Task<Hub> GetGuestHubByHubIdAndCognitoId(Guid hubId, string userCognitoId);
        Task<Hub> GetHubBySerialNoAndUserId(string serialNo, string userCognitoId);
        Task<IEnumerable<Hub>> GetAllHubs();
        Task<Hub> GetHubByInvitationIdAndSerialNo(int invitationId, string serialNo);
        Task<Hub> GetHubByInvitationIdAndOwner(int invitationId, string serialNo, string cognitoId);
        Task<IEnumerable<Hub>> GetHubsOfGuestUser(string cognitoId);
        Task<Hub> GetHubBySerialNoIsDeleted(string serialNo);
    }
}