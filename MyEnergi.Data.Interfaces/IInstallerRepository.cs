﻿using MyEnergi.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IInstallerRepository : IRepository<Installer>
    {
        Task<Installer> GetInstallerByCode(string code);
        Task<IEnumerable<Installer>> GetInstallersByCountry(string country);
        Task<IEnumerable<Installer>> GetAllInstallers();
    }
}