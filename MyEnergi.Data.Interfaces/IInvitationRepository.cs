﻿using MyEnergi.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IInvitationRepository : IRepository<Invitation>
    {
        Task<IEnumerable<Invitation>> GetOwnerUserSendInvitations(string cognitoId);
        
        Task<Invitation> GetOwnerInvitationForHub(string cognitoId, string serialNo, string guestEmail);
        
        Task<Invitation> GetOwnerInvitationForHub(string cognitoId, string guestEmail);

        Task<Invitation> GetGuestInvitationForHub(string cognitoId, string serialNo);

        Task<IEnumerable<Invitation>> GetGuestUserInvitations(string cognitoId);
    }
}
