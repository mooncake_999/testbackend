﻿using System.Threading.Tasks;
using MyEnergi.Data.Entity;

namespace MyEnergi.Data.Interfaces
{
    public interface IInvitationStatusRepository : IRepository<InvitationStatus>
    { 
        Task<InvitationStatus> GetByStatus(string status);
    }
}
