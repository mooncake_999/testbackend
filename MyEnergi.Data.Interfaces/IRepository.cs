﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        void CommitChanges();

        Task<IEnumerable<TEntity>> GetAll(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null);

        Task<TEntity> GetSingleOrDefaultBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null);

        Task<TEntity> GetFirstOrDefaultBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null);

        Task<IEnumerable<TEntity>> GetBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null);

        Task Update(TEntity entity);
        Task UpdateMultiple(IEnumerable<TEntity> entities);

        Task Delete(TEntity entity);
        Task DeleteMultiple(IEnumerable<TEntity> entities);

        Task AddOrUpdate(TEntity entity);
        Task AddOrUpdate(IEnumerable<TEntity> entities);
    }
}