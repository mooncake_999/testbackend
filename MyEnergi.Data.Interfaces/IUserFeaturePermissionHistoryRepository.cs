﻿using MyEnergi.Data.Entity;

namespace MyEnergi.Data.Interfaces
{
    public interface IUserFeaturePermissionHistoryRepository : IRepository<UserFeaturePermissionHistory>
    {
    }
}
