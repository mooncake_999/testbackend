﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IUserFeaturePermissionRepository : IRepository<UserFeaturePermission>
    {
        Task<IEnumerable<UserFeaturePermission>> GetByInvitationGuid(Guid invitationGuid);

        Task<IEnumerable<UserFeaturePermission>> GetUserFeaturePermissionsByCognitoId(string userCognitoId);

        Task<IEnumerable<UserFeaturePermission>> GetUserFeaturePermissionsByCognitoIdIsDeleted(string userCognitoId);

        Task<IEnumerable<UserFeaturePermission>> GetUserFeaturePermissionsByDeviceIds(List<Guid> deviceGuids);
    }
}
