﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<UserVehicle>> GetUserVehicles(Guid id);
        Task<User> GetUserRegistrationInfoByEmail(string email);
        Task<User> GetUserByCognitoId(string userCognitoId);
        Task<bool> CheckIfEmailAlreadyExists(string email);
        Task<User> GetUserByEmail(string email);
        Task<bool> CheckIfUserExists(string userCognitoId);
    }
}
