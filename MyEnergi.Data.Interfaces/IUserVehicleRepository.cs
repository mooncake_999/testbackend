﻿using MyEnergi.Data.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IUserVehicleRepository : IRepository<UserVehicle>
    {
        Task<IEnumerable<UserVehicle>> GetUserVehicles(string userCognitoId);
    }
}
