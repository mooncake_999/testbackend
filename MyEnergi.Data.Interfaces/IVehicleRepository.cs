﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Interfaces
{
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        Task<Vehicle> GetVehicleById(Guid guid);
        Task<IEnumerable<string>> GetAllManufactures();
        Task<IEnumerable<KeyValuePair<Guid, string>>> GetVehicleByManufacturer(string manufacturer);
        Task<IEnumerable<Vehicle>> GetVehicleDetails(Guid vehicleId);
        Task<IEnumerable<Vehicle>> GetAllVehicles();
    }
}
