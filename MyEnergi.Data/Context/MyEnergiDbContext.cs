﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace MyEnergi.Data.Context
{
    public class MyEnergiDbContext : DbContext
    {

        //public MyEnergiDbContext(string connectionString) : base(GetOptions(connectionString))
        //{
        //}
        //private static DbContextOptions GetOptions(string connectionString)
        //{
        //    return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
        //}

        //public MyEnergiDbContext() { }
        public MyEnergiDbContext(DbContextOptions<MyEnergiDbContext> options)
        : base(options)
        { }
        // will use these for migrations
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=localhost;Database=MyEnergi;Trusted_Connection=True;");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(u => u.Id);
            modelBuilder.Entity<User>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<Address>().HasKey(u => u.Id);
            modelBuilder.Entity<Address>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<Vehicle>().HasKey(v => v.Id);
            modelBuilder.Entity<Vehicle>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<UserDetails>().HasKey(u => u.Id);
            modelBuilder.Entity<UserDetails>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<UserVehicle>().HasKey(u => u.Id);
            modelBuilder.Entity<UserVehicle>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<Hub>().HasKey(h => h.Id);
            modelBuilder.Entity<Hub>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<EnergyProvider>().HasKey(v => v.Id);
            modelBuilder.Entity<EnergyProvider>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<EnergySetup>().HasKey(v => v.Id);
            modelBuilder.Entity<EnergySetup>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<Device>().HasKey(h => h.Id);
            modelBuilder.Entity<Device>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<Installer>().HasKey(h => h.Id);
            modelBuilder.Entity<Installer>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<InstallationFeedback>().HasKey(h => h.Id);
            modelBuilder.Entity<InstallationFeedback>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<EnergyPrice>().HasKey(h => h.Id);
            modelBuilder.Entity<EnergyPrice>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<DeviceChargesSetup>().HasKey(h => h.Id);
            modelBuilder.Entity<DeviceChargesSetup>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<DeviceBestPriceIntervals>().HasKey(h => h.Id);
            modelBuilder.Entity<DeviceBestPriceIntervals>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<DeviceBestPriceHistory>().HasKey(h => h.Id);
            modelBuilder.Entity<DeviceBestPriceHistory>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<DeviceChargesSetupHistory>().HasKey(h => h.Id);
            modelBuilder.Entity<DeviceChargesSetupHistory>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<UserPreference>().HasKey(h => h.Id);
            modelBuilder.Entity<UserPreference>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<DualTariffEnergyPrice>().HasKey(h => h.Id);
            modelBuilder.Entity<DualTariffEnergyPrice>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<DeviceHistory>().HasKey(r => r.Id);
            modelBuilder.Entity<DeviceHistory>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<HubHistory>().HasKey(r => r.Id);
            modelBuilder.Entity<HubHistory>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            #region Account Access Entities
            modelBuilder.Entity<UserRole>().HasKey(r => r.Id);
            modelBuilder.Entity<UserRole>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<SystemFeature>().HasKey(r => r.Id);
            modelBuilder.Entity<SystemFeature>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<AppFeature>().HasKey(r => r.Id);
            modelBuilder.Entity<AppFeature>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<RoleToUser>().HasKey(r => r.Id);
            modelBuilder.Entity<RoleToUser>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<Invitation>().HasKey(r => r.Id);
            modelBuilder.Entity<Invitation>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<UserFeaturePermission>().HasKey(r => r.Id);
            modelBuilder.Entity<UserFeaturePermission>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<UserFeaturePermissionHistory>().HasKey(r => r.Id);
            modelBuilder.Entity<UserFeaturePermissionHistory>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");

            modelBuilder.Entity<InvitationStatus>().HasKey(r => r.Id);
            modelBuilder.Entity<InvitationStatus>().Property(u => u.Guid).HasDefaultValueSql("newsequentialid()");
            modelBuilder.Entity<InvitationStatus>().HasMany(x => x.Invitations);

            modelBuilder.Entity<InvitationDetails>().HasKey(i => i.Id);
            modelBuilder.Entity<InvitationDetails>().Property(i => i.Guid).HasDefaultValueSql("newsequentialid()");
            #endregion Account Access Entities

            modelBuilder.Entity<User>().HasOne(u => u.UserPreference);
            modelBuilder.Entity<User>().HasOne(a => a.UserDetails);
            modelBuilder.Entity<User>().HasMany(c => c.UserVehicles)
                                       .WithOne(e => e.User);
            modelBuilder.Entity<User>().HasMany(a => a.Addresses)
                                       .WithOne(e => e.User);
            modelBuilder.Entity<Hub>().HasOne(h => h.Address);
            modelBuilder.Entity<Hub>().HasOne(h => h.Installation);
            modelBuilder.Entity<EnergySetup>().HasOne(h => h.Address);
            modelBuilder.Entity<EnergySetup>().HasOne(h => h.EnergyProv);
            modelBuilder.Entity<Device>().HasOne(h => h.Address);
            modelBuilder.Entity<Device>().HasOne(h => h.Hub);
            modelBuilder.Entity<Device>().HasOne(h => h.Installation);
            modelBuilder.Entity<Device>().HasMany(h => h.ChargeSchedules)
                                         .WithOne(h => h.Device);
            modelBuilder.Entity<DeviceBestPriceIntervals>().HasOne(h => h.Device);
            modelBuilder.Entity<DeviceChargesSetup>().HasOne(h => h.Device);
            modelBuilder.Entity<DualTariffEnergyPrice>().HasOne(d => d.EnergySetup);
            modelBuilder.Entity<EnergyPrice>().HasOne(e => e.EnergyProv);
            modelBuilder.Entity<DeviceHistory>().HasOne(i => i.OwnerUser);
            modelBuilder.Entity<DeviceHistory>().HasOne(i => i.Installation);
            modelBuilder.Entity<HubHistory>().HasOne(i => i.OwnerUser);
            modelBuilder.Entity<HubHistory>().HasOne(i => i.Installation);

            #region Account Access Relations

            modelBuilder.Entity<RoleToUser>().HasOne(ru => ru.User);
            modelBuilder.Entity<RoleToUser>().HasOne(ru => ru.Role);

            modelBuilder.Entity<UserRole>().HasMany(ur => ur.SystemFeatures);

            modelBuilder.Entity<Invitation>().HasOne(i => i.GuestUser);
            modelBuilder.Entity<Invitation>().HasOne(i => i.OwnerUser);
            modelBuilder.Entity<Invitation>().HasOne(i => i.InvitationStatus);
            modelBuilder.Entity<Invitation>().HasOne(i => i.InvitationDetails);
            modelBuilder.Entity<Invitation>().HasMany(i => i.UserFeaturePermissions);
            modelBuilder.Entity<Invitation>().HasMany(i => i.UserFeaturePermissionHistories);

            modelBuilder.Entity<UserFeaturePermission>().HasOne(i => i.OwnerUser);
            modelBuilder.Entity<UserFeaturePermission>().HasOne(i => i.Invitation);
            modelBuilder.Entity<UserFeaturePermission>().HasOne(i => i.Device);
            modelBuilder.Entity<UserFeaturePermission>().HasOne(i => i.AppFeature);

            modelBuilder.Entity<UserFeaturePermissionHistory>().HasOne(i => i.OwnerUser);
            modelBuilder.Entity<UserFeaturePermissionHistory>().HasOne(i => i.Invitation);
            modelBuilder.Entity<UserFeaturePermissionHistory>().HasOne(i => i.Device);
            modelBuilder.Entity<UserFeaturePermissionHistory>().HasOne(i => i.AppFeature);

            #endregion Account Access Relations

            #region Hub/Device Soft Delete                 
            modelBuilder.Entity<Device>().Property<bool>("IsDeleted");
            modelBuilder.Entity<Device>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);

            modelBuilder.Entity<Hub>().Property<bool>("IsDeleted");
            modelBuilder.Entity<Hub>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
            #endregion
        }

        public DbSet<User> Users { get; set; }
        public DbSet<UserDetails> UsersDetails { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<UserVehicle> UserVehicles { get; set; }
        public DbSet<Hub> Hubs { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<EnergyProvider> EnergyProviders { get; set; }
        public DbSet<EnergySetup> EnergySetup { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Installer> Installers { get; set; }
        public DbSet<InstallationFeedback> InstallationFeedbacks { get; set; }
        public DbSet<EnergyPrice> EnergyPrices { get; set; }
        public DbSet<DeviceChargesSetup> DeviceChargesSetups { get; set; }
        public DbSet<DeviceBestPriceIntervals> DeviceBestPriceIntervals { get; set; }
        public DbSet<DeviceBestPriceHistory> DeviceBestPriceHistories { get; set; }
        public DbSet<DeviceChargesSetupHistory> DeviceChargesSetupHistories { get; set; }
        public DbSet<UserPreference> UserPreferences { get; set; }
        public DbSet<DualTariffEnergyPrice> DualTariffEnergyPrices { get; set; }

        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<SystemFeature> SystemFeatures { get; set; }
        public DbSet<AppFeature> AppFeatures { get; set; }
        public DbSet<RoleToUser> RolesToUser { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<UserFeaturePermission> UserFeaturePermissions { get; set; }
        public DbSet<UserFeaturePermissionHistory> UserFeaturePermissionHistories { get; set; }
        public DbSet<InvitationStatus> InvitationStatuses { get; set; }
        public DbSet<InvitationDetails> InvitationDetails { get; set; }
        public DbSet<DeviceHistory> DeviceHistories { get; set; }
        public DbSet<HubHistory> HubHistories { get; set; }
    }
}
