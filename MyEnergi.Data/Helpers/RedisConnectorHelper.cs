﻿using StackExchange.Redis;
using System;

namespace MyEnergi.Data.Helpers
{
    public class RedisConnectorHelper
    {
        static RedisConnectorHelper()
        {
            LazyConnection = new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect("localhost"));
        }

        private static readonly Lazy<ConnectionMultiplexer> LazyConnection;

        public static ConnectionMultiplexer Connection => LazyConnection.Value;
    }
}
