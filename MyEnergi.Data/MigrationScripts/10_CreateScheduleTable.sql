﻿CREATE TABLE [DeviceChargesSetups] (
    [Id] int NOT NULL IDENTITY,
    [LastUpdated] datetime2 NOT NULL,
    [DeviceId] int NULL,
    [ScheduleType] nvarchar(max) NULL,
    [ScheduleNickName] nvarchar(max) NULL,
    [ChargingOutputName] nvarchar(max) NULL,
    [ChargeAmountMinutes] int NULL,
    [FromTime] nvarchar(max) NULL,
    [ToTime] nvarchar(max) NULL,
    [DaysOfWeek] nvarchar(max) NULL,
    [PriceBelow] int NULL,
    [SingleChargeDay] datetime2 NULL,
    CONSTRAINT [PK_DeviceChargesSetups] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_DeviceChargesSetups_Devices_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [Devices] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_DeviceChargesSetups_DeviceId] ON [DeviceChargesSetups] ([DeviceId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200803140437_CreateScheduleTable', N'3.1.5');

GO

