﻿ALTER TABLE [Hubs] ADD [TimeZoneUTCDifference] nvarchar(max) NULL;

GO

ALTER TABLE [Devices] ADD [TimeZoneUTCDifference] nvarchar(max) NULL;

GO

ALTER TABLE [DeviceChargesSetups] ADD [IsActive] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200810064808_AddedFieldsForDevicesAndSchedules', N'3.1.5');

GO

