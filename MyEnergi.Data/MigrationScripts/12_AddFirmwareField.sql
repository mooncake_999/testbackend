﻿ALTER TABLE [Hubs] ADD [Firmware] nvarchar(max) NULL;

GO

ALTER TABLE [Devices] ADD [Firmware] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200811140530_AddFirmwareField', N'3.1.5');

GO

