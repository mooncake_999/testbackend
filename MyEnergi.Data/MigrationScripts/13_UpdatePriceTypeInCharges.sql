﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[DeviceChargesSetups]') AND [c].[name] = N'PriceBelow');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [DeviceChargesSetups] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [DeviceChargesSetups] ALTER COLUMN [PriceBelow] decimal(18,2) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200813131759_UpdatePriceFieldInCharges', N'3.1.5');

GO

