﻿ALTER TABLE [UserVehicles] ADD [ChargeRate] real NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200819125426_AddChargeRateUserVehicles', N'3.1.5');

GO

