﻿ALTER TABLE [UsersDetails] ADD [LandlineCode] nvarchar(max) NULL;

GO

ALTER TABLE [UsersDetails] ADD [MobileCode] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200826071125_UpdatePhoneNumberFields', N'3.1.5');

GO

