﻿CREATE TABLE [DeviceBestPriceIntervals] (
    [Id] int NOT NULL IDENTITY,
    [DeviceId] int NULL,
    [From] datetime2 NOT NULL,
    [To] datetime2 NOT NULL,
    [IsScheduledType] bit NOT NULL,
    [IsSingleType] bit NOT NULL,
    [IsBudgetType] bit NOT NULL,
    [LastUpdate] datetime2 NOT NULL,
    CONSTRAINT [PK_DeviceBestPriceIntervals] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_DeviceBestPriceIntervals_Devices_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [Devices] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_DeviceBestPriceIntervals_DeviceId] ON [DeviceBestPriceIntervals] ([DeviceId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200831100939_AddTableForBestPriceIntervals', N'3.1.5');

GO

