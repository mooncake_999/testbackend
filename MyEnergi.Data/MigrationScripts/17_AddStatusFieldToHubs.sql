﻿ALTER TABLE [Hubs] ADD [Status] int NOT NULL DEFAULT 0;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200901132921_AddStatusFieldToHubs', N'3.1.5');

GO

