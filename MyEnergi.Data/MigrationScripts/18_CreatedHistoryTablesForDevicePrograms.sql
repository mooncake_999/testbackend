﻿CREATE TABLE [DeviceBestPriceHistories] (
    [Id] int NOT NULL IDENTITY,
    [Created] datetime2 NOT NULL,
    [DeviceId] int NOT NULL,
    [From] datetime2 NOT NULL,
    [To] datetime2 NOT NULL,
    [IsScheduledType] bit NOT NULL,
    [IsSingleType] bit NOT NULL,
    [IsBudgetType] bit NOT NULL,
    CONSTRAINT [PK_DeviceBestPriceHistories] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [DeviceChargeSetupHistories] (
    [Id] int NOT NULL IDENTITY,
    [Created] datetime2 NOT NULL,
    [DeviceId] int NOT NULL,
    [ScheduleType] nvarchar(max) NULL,
    [ScheduleNickName] nvarchar(max) NULL,
    [ChargingOutputName] nvarchar(max) NULL,
    [ChargeAmountMinutes] int NULL,
    [FromTime] nvarchar(max) NULL,
    [ToTime] nvarchar(max) NULL,
    [DaysOfWeek] nvarchar(max) NULL,
    [PriceBelow] decimal(18,2) NULL,
    [SingleChargeDay] datetime2 NULL,
    CONSTRAINT [PK_DeviceChargeSetupHistories] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200915083059_CreatedHistoryTablesForDevicePrograms', N'3.1.5');

GO

