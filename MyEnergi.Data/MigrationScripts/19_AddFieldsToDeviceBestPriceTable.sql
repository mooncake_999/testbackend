﻿ALTER TABLE [DeviceBestPriceIntervals] ADD [HeaterSlot] nvarchar(max) NULL;

GO

ALTER TABLE [DeviceBestPriceIntervals] ADD [SlotPriceValue] decimal(18,2) NOT NULL DEFAULT 0.0;

GO

ALTER TABLE [DeviceBestPriceHistories] ADD [HeaterSlot] nvarchar(max) NULL;

GO

ALTER TABLE [DeviceBestPriceHistories] ADD [SlotPriceValue] decimal(18,2) NOT NULL DEFAULT 0.0;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200915092156_AddFieldsToDeviceBestPriceTable', N'3.1.5');

GO

