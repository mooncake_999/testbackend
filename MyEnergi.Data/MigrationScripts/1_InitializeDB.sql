﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [UsersDetails] (
    [Id] int NOT NULL IDENTITY,
    [Title] nvarchar(max) NULL,
    [FirstName] nvarchar(max) NULL,
    [LastName] nvarchar(max) NULL,
    [AddressLine1] nvarchar(max) NULL,
    [AddressLine2] nvarchar(max) NULL,
    [City] nvarchar(max) NULL,
    [Region] nvarchar(max) NULL,
    [PostalCode] nvarchar(max) NULL,
    [Country] nvarchar(max) NULL,
    [MobileNumber] nvarchar(max) NULL,
    [LandlineNumber] nvarchar(max) NULL,
    [DateOfBirth] datetime2 NOT NULL,
    CONSTRAINT [PK_UsersDetails] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Vehicles] (
    [Id] int NOT NULL IDENTITY,
    [RefId] int NOT NULL,
    [Manufacturer] nvarchar(max) NULL,
    [Model] nvarchar(max) NULL,
    [AvailabilityDateFrom] int NULL,
    [AvailabilityDateTo] int NULL,
    [BatterySize] real NULL,
    [ManufacturerOfficialEVRange] int NULL,
    [RealEVRange] int NULL,
    CONSTRAINT [PK_Vehicles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Users] (
    [Id] int NOT NULL IDENTITY,
    [Email] nvarchar(max) NULL,
    [PrivacyAcceptance] bit NOT NULL,
    [MarketingOptIn] bit NOT NULL,
    [ThirdPartyMarketingOptIn] bit NOT NULL,
    [CognitoId] nvarchar(max) NULL,
    [UserDetailsId] int NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Users_UsersDetails_UserDetailsId] FOREIGN KEY ([UserDetailsId]) REFERENCES [UsersDetails] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UserVehicles] (
    [Id] int NOT NULL IDENTITY,
    [UserId] int NULL,
    [Manufacturer] nvarchar(max) NULL,
    [Model] nvarchar(max) NULL,
    [ManufacturingYear] int NOT NULL,
    [BatterySize] real NOT NULL,
    [ManufacturerOfficialEVRange] int NOT NULL,
    [RealEVRange] int NOT NULL,
    [VehicleNickname] nvarchar(max) NULL,
    CONSTRAINT [PK_UserVehicles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UserVehicles_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Users_UserDetailsId] ON [Users] ([UserDetailsId]);

GO

CREATE INDEX [IX_UserVehicles_UserId] ON [UserVehicles] ([UserId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200630055520_InitializeDB', N'3.1.5');

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UsersDetails]') AND [c].[name] = N'AddressLine1');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [UsersDetails] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [UsersDetails] DROP COLUMN [AddressLine1];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UsersDetails]') AND [c].[name] = N'AddressLine2');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [UsersDetails] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [UsersDetails] DROP COLUMN [AddressLine2];

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UsersDetails]') AND [c].[name] = N'City');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [UsersDetails] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [UsersDetails] DROP COLUMN [City];

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UsersDetails]') AND [c].[name] = N'Country');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [UsersDetails] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [UsersDetails] DROP COLUMN [Country];

GO

DECLARE @var4 sysname;
SELECT @var4 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UsersDetails]') AND [c].[name] = N'PostalCode');
IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [UsersDetails] DROP CONSTRAINT [' + @var4 + '];');
ALTER TABLE [UsersDetails] DROP COLUMN [PostalCode];

GO

DECLARE @var5 sysname;
SELECT @var5 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UsersDetails]') AND [c].[name] = N'Region');
IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [UsersDetails] DROP CONSTRAINT [' + @var5 + '];');
ALTER TABLE [UsersDetails] DROP COLUMN [Region];

GO

DECLARE @var6 sysname;
SELECT @var6 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UserVehicles]') AND [c].[name] = N'RealEVRange');
IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [UserVehicles] DROP CONSTRAINT [' + @var6 + '];');
ALTER TABLE [UserVehicles] ALTER COLUMN [RealEVRange] int NULL;

GO

DECLARE @var7 sysname;
SELECT @var7 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UserVehicles]') AND [c].[name] = N'ManufacturingYear');
IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [UserVehicles] DROP CONSTRAINT [' + @var7 + '];');
ALTER TABLE [UserVehicles] ALTER COLUMN [ManufacturingYear] int NULL;

GO

DECLARE @var8 sysname;
SELECT @var8 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UserVehicles]') AND [c].[name] = N'ManufacturerOfficialEVRange');
IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [UserVehicles] DROP CONSTRAINT [' + @var8 + '];');
ALTER TABLE [UserVehicles] ALTER COLUMN [ManufacturerOfficialEVRange] int NULL;

GO

DECLARE @var9 sysname;
SELECT @var9 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UserVehicles]') AND [c].[name] = N'BatterySize');
IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [UserVehicles] DROP CONSTRAINT [' + @var9 + '];');
ALTER TABLE [UserVehicles] ALTER COLUMN [BatterySize] real NULL;

GO

CREATE TABLE [Addresses] (
    [Id] int NOT NULL IDENTITY,
    [UserId] int NULL,
    [AddressLine1] nvarchar(max) NULL,
    [AddressLine2] nvarchar(max) NULL,
    [City] nvarchar(max) NULL,
    [Region] nvarchar(max) NULL,
    [PostalCode] nvarchar(max) NULL,
    [Country] nvarchar(max) NULL,
    CONSTRAINT [PK_Addresses] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Addresses_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Hubs] (
    [Id] int NOT NULL IDENTITY,
    [AddressId] int NULL,
    [SerialNo] nvarchar(max) NULL,
    [RegistrationCode] nvarchar(max) NULL,
    [NickName] nvarchar(max) NULL,
    [AppPassword] nvarchar(max) NULL,
    [RegistrationDate] datetime2 NOT NULL,
    CONSTRAINT [PK_Hubs] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Hubs_Addresses_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [Addresses] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Addresses_UserId] ON [Addresses] ([UserId]);

GO

CREATE INDEX [IX_Hubs_AddressId] ON [Hubs] ([AddressId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200702111732_MultiAddressAndHubRegistration', N'3.1.5');

GO

CREATE TABLE [EnergyProviders] (
    [Id] int NOT NULL IDENTITY,
    [Provider] nvarchar(max) NULL,
    [Tariff] nvarchar(max) NULL,
    [IsFlexible] bit NOT NULL,
    [IsGreen] bit NOT NULL,
    [IsEV] bit NOT NULL,
    [Country] nvarchar(max) NULL,
    CONSTRAINT [PK_EnergyProviders] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200702130256_AddingEnergyProivderTable', N'3.1.5');

GO

CREATE TABLE [EnergySetup] (
    [Id] int NOT NULL IDENTITY,
    [EnergyProvider] nvarchar(max) NULL,
    [EnergyTariff] nvarchar(max) NULL,
    [TariffExpiration] datetime2 NOT NULL,
    [SolarPanel] bit NOT NULL,
    [WindTurbine] bit NOT NULL,
    [Hydroelectric] bit NOT NULL,
    [OtherGenerator] nvarchar(max) NULL,
    [MaxPower] decimal(18,2) NOT NULL,
    [AirSourceHeatPump] bit NOT NULL,
    [GroundSourceHeatPump] bit NOT NULL,
    [ElectricStorageHeating] bit NOT NULL,
    [OtherElectricHeating] nvarchar(max) NULL,
    [AddressId] int NULL,
    CONSTRAINT [PK_EnergySetup] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_EnergySetup_Addresses_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [Addresses] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_EnergySetup_AddressId] ON [EnergySetup] ([AddressId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200703124257_AddingEnergySetupTable', N'3.1.5');

GO

