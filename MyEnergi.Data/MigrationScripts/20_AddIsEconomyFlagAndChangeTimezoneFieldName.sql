﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Hubs]') AND [c].[name] = N'TimeZoneUTCDifference');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Hubs] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Hubs] DROP COLUMN [TimeZoneUTCDifference];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'TimeZoneUTCDifference');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Devices] DROP COLUMN [TimeZoneUTCDifference];

GO

ALTER TABLE [Hubs] ADD [TimeZoneRegion] nvarchar(max) NULL;

GO

ALTER TABLE [EnergyProviders] ADD [IsEconomy] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [Devices] ADD [TimeZoneRegion] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201028073319_AddIsEconomyFlagAndChangeTimezoneFieldName', N'3.1.5');

GO

