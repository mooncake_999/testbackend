ALTER TABLE [Users] ADD [UserPreferenceId] int NULL;

GO

CREATE TABLE [UserPreferences] (
    [Id] int NOT NULL IDENTITY,
    [DateFormat] nvarchar(max) NULL,
    [IsMetric] bit NOT NULL,
    [Language] nvarchar(max) NULL,
    [Currency] nvarchar(max) NULL,
    CONSTRAINT [PK_UserPreferences] PRIMARY KEY ([Id])
);

GO

CREATE INDEX [IX_Users_UserPreferenceId] ON [Users] ([UserPreferenceId]);

GO

ALTER TABLE [Users] ADD CONSTRAINT [FK_Users_UserPreferences_UserPreferenceId] FOREIGN KEY ([UserPreferenceId]) REFERENCES [UserPreferences] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201103151018_AddUserPreferences', N'3.1.5');

GO

