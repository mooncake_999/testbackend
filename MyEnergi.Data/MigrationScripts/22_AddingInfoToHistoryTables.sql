﻿EXEC sp_rename N'[DeviceChargeSetupHistories]', N'DeviceChargesSetupHistories';

GO

EXEC sp_rename N'[DeviceChargesSetupHistories].[Created]', N'MovedToHistory', N'COLUMN';

GO

EXEC sp_rename N'[DeviceBestPriceHistories].[Created]', N'MovedToHistory', N'COLUMN';

GO

ALTER TABLE [DeviceChargesSetupHistories] ADD [IsActive] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [DeviceBestPriceHistories] ADD [PriceCreated] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201109134153_AddingInfoToHistoryTables', N'3.1.5');

GO

