﻿CREATE TABLE [DualTariffEnergyPrices] (
    [Id] int NOT NULL IDENTITY,
    [Day] int NOT NULL,
    [FromMinutes] int NOT NULL,
    [ToMinutes] int NOT NULL,
    [Price] decimal(18,2) NOT NULL,
    [EnergySetupId] int NULL,
    [CreationDate] datetime2 NOT NULL,
    [LastUpdateDate] datetime2 NULL,
    CONSTRAINT [PK_DualTariffEnergyPrices] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_DualTariffEnergyPrices_EnergySetup_EnergySetupId] FOREIGN KEY ([EnergySetupId]) REFERENCES [EnergySetup] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_DualTariffEnergyPrices_EnergySetupId] ON [DualTariffEnergyPrices] ([EnergySetupId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201111170904_AddDualTariffEnergyPrices', N'3.1.5');

GO

