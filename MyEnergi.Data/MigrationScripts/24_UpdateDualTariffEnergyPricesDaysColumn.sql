﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[DualTariffEnergyPrices]') AND [c].[name] = N'Day');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [DualTariffEnergyPrices] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [DualTariffEnergyPrices] DROP COLUMN [Day];

GO

ALTER TABLE [DualTariffEnergyPrices] ADD [Days] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201113181541_UpdateDualTariffEnergyPricesDaysColumn', N'3.1.5');

GO

