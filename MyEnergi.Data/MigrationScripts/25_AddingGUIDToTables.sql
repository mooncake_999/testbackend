﻿ALTER TABLE [Vehicles] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [UserVehicles] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [UsersDetails] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [Users] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [UserPreferences] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [Installers] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [InstallationFeedbacks] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [Hubs] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [EnergySetup] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [EnergyProviders] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [EnergyPrices] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [DualTariffEnergyPrices] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [Devices] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [DeviceChargesSetups] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [DeviceChargesSetupHistories] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [DeviceBestPriceIntervals] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [DeviceBestPriceHistories] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

ALTER TABLE [Addresses] ADD [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid());

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201116075339_AddingGUIDToTables', N'3.1.5');

GO

