﻿ALTER TABLE [Hubs] ADD [HostServer] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201203113042_AddHostServerToHubs', N'3.1.5');

GO

