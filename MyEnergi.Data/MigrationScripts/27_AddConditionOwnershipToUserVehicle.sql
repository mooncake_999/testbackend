﻿ALTER TABLE [UserVehicles] ADD [Condition] nvarchar(max) NULL;

GO

ALTER TABLE [UserVehicles] ADD [Ownership] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201216135515_AddConditionOwnershipToUserVehicle', N'3.1.5');

GO

