﻿ALTER TABLE [EnergySetup] ADD [IsEconomy] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201216184343_AddIsEconomyFieldToEnergySetup', N'3.1.5');

GO

