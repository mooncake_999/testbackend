﻿ALTER TABLE [Vehicles] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [UserVehicles] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [UsersDetails] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [Users] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [UserPreferences] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [Installers] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [InstallationFeedbacks] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [Hubs] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [EnergySetup] ADD [EnergyProvId] int NULL;

GO

ALTER TABLE [EnergySetup] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [EnergyProviders] ADD [InternalReference] nvarchar(max) NULL;

GO

ALTER TABLE [EnergyProviders] ADD [IsUsed] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [EnergyProviders] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [EnergyPrices] ADD [EnergyProvId] int NULL;

GO

ALTER TABLE [EnergyPrices] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [Devices] ADD [LastUpdate] datetime2 NULL;

GO

ALTER TABLE [Addresses] ADD [LastUpdate] datetime2 NULL;

GO

CREATE INDEX [IX_EnergySetup_EnergyProvId] ON [EnergySetup] ([EnergyProvId]);

GO

CREATE INDEX [IX_EnergyPrices_EnergyProvId] ON [EnergyPrices] ([EnergyProvId]);

GO

ALTER TABLE [EnergyPrices] ADD CONSTRAINT [FK_EnergyPrices_EnergyProviders_EnergyProvId] FOREIGN KEY ([EnergyProvId]) REFERENCES [EnergyProviders] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [EnergySetup] ADD CONSTRAINT [FK_EnergySetup_EnergyProviders_EnergyProvId] FOREIGN KEY ([EnergyProvId]) REFERENCES [EnergyProviders] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210121185017_UpdateEnergyProviderAddingUpdateDateToAll', N'3.1.5');

GO

