﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'MaxPower');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [EnergySetup] DROP COLUMN [MaxPower];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'OtherGenerator');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [EnergySetup] DROP COLUMN [OtherGenerator];

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'SolarPanel');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [EnergySetup] DROP COLUMN [SolarPanel];

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'TariffExpiration');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [EnergySetup] DROP COLUMN [TariffExpiration];

GO

ALTER TABLE [EnergySetup] ADD [MaxPowerOutput] decimal(18,2) NOT NULL DEFAULT 0.0;

GO

ALTER TABLE [EnergySetup] ADD [OtherGeneration] nvarchar(max) NULL;

GO

ALTER TABLE [EnergySetup] ADD [SolarPanels] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [EnergySetup] ADD [TariffExpirationDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';

GO

ALTER TABLE [Addresses] ADD [SiteName] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200706135308_UpdateFieldNamesEnergySetup', N'3.1.5');

GO

