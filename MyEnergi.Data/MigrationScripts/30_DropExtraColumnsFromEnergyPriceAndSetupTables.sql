﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'EnergyProvider');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [EnergySetup] DROP COLUMN [EnergyProvider];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'EnergyTariff');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [EnergySetup] DROP COLUMN [EnergyTariff];

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'IsEconomy');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [EnergySetup] DROP COLUMN [IsEconomy];

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergyPrices]') AND [c].[name] = N'EnergyProvider');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [EnergyPrices] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [EnergyPrices] DROP COLUMN [EnergyProvider];

GO

DECLARE @var4 sysname;
SELECT @var4 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergyPrices]') AND [c].[name] = N'EnergyTariff');
IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [EnergyPrices] DROP CONSTRAINT [' + @var4 + '];');
ALTER TABLE [EnergyPrices] DROP COLUMN [EnergyTariff];

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210122072207_DropExtraColumnsFromEnergyPriceAndSetupTables', N'3.1.5');

GO

