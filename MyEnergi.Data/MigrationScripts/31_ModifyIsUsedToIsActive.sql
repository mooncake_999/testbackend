﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergyProviders]') AND [c].[name] = N'IsUsed');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [EnergyProviders] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [EnergyProviders] DROP COLUMN [IsUsed];

GO

ALTER TABLE [EnergyProviders] ADD [IsActive] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210201150809_ModifyIsUsedToIsActive', N'3.1.5');

GO

