﻿CREATE TABLE [AppFeatures] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [Feature] nvarchar(max) NULL,
    [LastUpdate] datetime2 NULL,
    CONSTRAINT [PK_AppFeatures] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [InvitationStatuses] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [Status] nvarchar(max) NULL,
    [LastUpdate] datetime2 NULL,
    CONSTRAINT [PK_InvitationStatuses] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [UserRoles] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [Role] nvarchar(max) NULL,
    [LastUpdate] datetime2 NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Invitations] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [GuestUserId] int NULL,
    [Email] nvarchar(max) NULL,
    [InvitationStatusId] int NULL,
    [StartDate] datetime2 NULL,
    [EndDate] datetime2 NULL,
    [ExpirationDate] datetime2 NULL,
    [CreationDate] datetime2 NOT NULL,
    [CronExpressionRecurrence] nvarchar(max) NULL,
    [LastUpdate] datetime2 NULL,
    CONSTRAINT [PK_Invitations] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Invitations_Users_GuestUserId] FOREIGN KEY ([GuestUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Invitations_InvitationStatuses_InvitationStatusId] FOREIGN KEY ([InvitationStatusId]) REFERENCES [InvitationStatuses] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [RolesToUser] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [UserId] int NULL,
    [RoleId] int NULL,
    [LastUpdate] datetime2 NULL,
    CONSTRAINT [PK_RolesToUser] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_RolesToUser_UserRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [UserRoles] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_RolesToUser_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [SystemFeatures] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [Feature] nvarchar(max) NULL,
    [LastUpdate] datetime2 NULL,
    [UserRoleId] int NULL,
    CONSTRAINT [PK_SystemFeatures] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_SystemFeatures_UserRoles_UserRoleId] FOREIGN KEY ([UserRoleId]) REFERENCES [UserRoles] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UserFeaturePermissionHistories] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [OwnerUserId] int NULL,
    [InvitedUserId] int NULL,
    [DeviceId] int NULL,
    [AppFeatureId] int NULL,
    [MovedToHistory] datetime2 NOT NULL,
    CONSTRAINT [PK_UserFeaturePermissionHistories] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UserFeaturePermissionHistories_AppFeatures_AppFeatureId] FOREIGN KEY ([AppFeatureId]) REFERENCES [AppFeatures] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UserFeaturePermissionHistories_Devices_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [Devices] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UserFeaturePermissionHistories_Invitations_InvitedUserId] FOREIGN KEY ([InvitedUserId]) REFERENCES [Invitations] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UserFeaturePermissionHistories_Users_OwnerUserId] FOREIGN KEY ([OwnerUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UserFeaturePermissions] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [OwnerUserId] int NULL,
    [InvitedUserId] int NULL,
    [DeviceId] int NULL,
    [AppFeatureId] int NULL,
    [LastUpdate] datetime2 NULL,
    CONSTRAINT [PK_UserFeaturePermissions] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UserFeaturePermissions_AppFeatures_AppFeatureId] FOREIGN KEY ([AppFeatureId]) REFERENCES [AppFeatures] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UserFeaturePermissions_Devices_DeviceId] FOREIGN KEY ([DeviceId]) REFERENCES [Devices] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UserFeaturePermissions_Invitations_InvitedUserId] FOREIGN KEY ([InvitedUserId]) REFERENCES [Invitations] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UserFeaturePermissions_Users_OwnerUserId] FOREIGN KEY ([OwnerUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Invitations_GuestUserId] ON [Invitations] ([GuestUserId]);

GO

CREATE INDEX [IX_Invitations_InvitationStatusId] ON [Invitations] ([InvitationStatusId]);

GO

CREATE INDEX [IX_RolesToUser_RoleId] ON [RolesToUser] ([RoleId]);

GO

CREATE INDEX [IX_RolesToUser_UserId] ON [RolesToUser] ([UserId]);

GO

CREATE INDEX [IX_SystemFeatures_UserRoleId] ON [SystemFeatures] ([UserRoleId]);

GO

CREATE INDEX [IX_UserFeaturePermissionHistories_AppFeatureId] ON [UserFeaturePermissionHistories] ([AppFeatureId]);

GO

CREATE INDEX [IX_UserFeaturePermissionHistories_DeviceId] ON [UserFeaturePermissionHistories] ([DeviceId]);

GO

CREATE INDEX [IX_UserFeaturePermissionHistories_InvitedUserId] ON [UserFeaturePermissionHistories] ([InvitedUserId]);

GO

CREATE INDEX [IX_UserFeaturePermissionHistories_OwnerUserId] ON [UserFeaturePermissionHistories] ([OwnerUserId]);

GO

CREATE INDEX [IX_UserFeaturePermissions_AppFeatureId] ON [UserFeaturePermissions] ([AppFeatureId]);

GO

CREATE INDEX [IX_UserFeaturePermissions_DeviceId] ON [UserFeaturePermissions] ([DeviceId]);

GO

CREATE INDEX [IX_UserFeaturePermissions_InvitedUserId] ON [UserFeaturePermissions] ([InvitedUserId]);

GO

CREATE INDEX [IX_UserFeaturePermissions_OwnerUserId] ON [UserFeaturePermissions] ([OwnerUserId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210209165129_AddAccountAccessStructure', N'3.1.5');

GO

