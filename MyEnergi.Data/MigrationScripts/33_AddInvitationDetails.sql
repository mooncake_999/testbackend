﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Invitations]') AND [c].[name] = N'Email');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Invitations] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Invitations] DROP COLUMN [Email];

GO

ALTER TABLE [Invitations] ADD [InvitationDetailsId] int NULL;

GO

CREATE TABLE [InvitationDetails] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [Email] nvarchar(max) NULL,
    [FirstName] nvarchar(max) NULL,
    [LastName] nvarchar(max) NULL,
    [AppAccess] bit NOT NULL,
    [AdministratorAccess] bit NOT NULL,
    [AllDevicesAccess] bit NOT NULL,
    [LastUpdate] datetime2 NULL,
    CONSTRAINT [PK_InvitationDetails] PRIMARY KEY ([Id])
);

GO

CREATE INDEX [IX_Invitations_InvitationDetailsId] ON [Invitations] ([InvitationDetailsId]);

GO

ALTER TABLE [Invitations] ADD CONSTRAINT [FK_Invitations_InvitationDetails_InvitationDetailsId] FOREIGN KEY ([InvitationDetailsId]) REFERENCES [InvitationDetails] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210222143926_AddInvitationDetails', N'3.1.5');

GO

