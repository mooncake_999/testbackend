﻿ALTER TABLE [Invitations] ADD [OwnerUserId] int NULL;

GO

CREATE INDEX [IX_Invitations_OwnerUserId] ON [Invitations] ([OwnerUserId]);

GO

ALTER TABLE [Invitations] ADD CONSTRAINT [FK_Invitations_Users_OwnerUserId] FOREIGN KEY ([OwnerUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210323103035_AddOwnerUserToInvitation', N'3.1.5');

GO

