﻿ALTER TABLE [Devices] ADD [DateOfDelete] datetime2 NULL;

GO

ALTER TABLE [Devices] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210405140707_AddedDeviceIsDeletedAndDateOfDelete', N'3.1.5');

GO

