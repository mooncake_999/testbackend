﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'DateOfDelete');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Devices] DROP COLUMN [DateOfDelete];

GO

ALTER TABLE [Hubs] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

CREATE TABLE [DeviceHistories] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [OwnerUserId] int NULL,
    [DeviceName] nvarchar(max) NULL,
    [DeviceType] nvarchar(max) NULL,
    [SerialNumber] nvarchar(max) NULL,
    [MovedToHistory] datetime2 NOT NULL,
    CONSTRAINT [PK_DeviceHistories] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_DeviceHistories_Users_OwnerUserId] FOREIGN KEY ([OwnerUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [HubHistories] (
    [Id] int NOT NULL IDENTITY,
    [Guid] uniqueidentifier NOT NULL DEFAULT (newsequentialid()),
    [OwnerUserId] int NULL,
    [Nickname] nvarchar(max) NULL,
    [SerialNumber] nvarchar(max) NULL,
    [MovedToHistory] datetime2 NOT NULL,
    CONSTRAINT [PK_HubHistories] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_HubHistories_Users_OwnerUserId] FOREIGN KEY ([OwnerUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_DeviceHistories_OwnerUserId] ON [DeviceHistories] ([OwnerUserId]);

GO

CREATE INDEX [IX_HubHistories_OwnerUserId] ON [HubHistories] ([OwnerUserId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210407101549_AddedHistoryTablesAndIsDeletedHub', N'3.1.5');

GO

