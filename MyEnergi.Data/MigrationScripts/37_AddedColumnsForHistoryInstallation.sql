﻿ALTER TABLE [HubHistories] ADD [InstallationId] int NULL;

GO

ALTER TABLE [DeviceHistories] ADD [InstallationId] int NULL;

GO

CREATE INDEX [IX_HubHistories_InstallationId] ON [HubHistories] ([InstallationId]);

GO

CREATE INDEX [IX_DeviceHistories_InstallationId] ON [DeviceHistories] ([InstallationId]);

GO

ALTER TABLE [DeviceHistories] ADD CONSTRAINT [FK_DeviceHistories_InstallationFeedbacks_InstallationId] FOREIGN KEY ([InstallationId]) REFERENCES [InstallationFeedbacks] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [HubHistories] ADD CONSTRAINT [FK_HubHistories_InstallationFeedbacks_InstallationId] FOREIGN KEY ([InstallationId]) REFERENCES [InstallationFeedbacks] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210428104137_AddedColumnsForHistoryInstallation', N'3.1.5');

GO

