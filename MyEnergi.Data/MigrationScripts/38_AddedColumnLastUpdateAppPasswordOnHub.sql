﻿ALTER TABLE [Hubs] ADD [LastUpdateAppPassword] datetime2 NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210526132840_AddedColumnLastUpdateAppPasswordOnHub', N'3.1.5');

GO

