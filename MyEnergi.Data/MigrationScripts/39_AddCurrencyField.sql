﻿ALTER TABLE [EnergyPrices] ADD [Currency] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210609063852_AddCurrencyField', N'3.1.5');

GO

