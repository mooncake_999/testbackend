﻿ALTER TABLE [UserFeaturePermissionHistories] DROP CONSTRAINT [FK_UserFeaturePermissionHistories_Invitations_InvitedUserId];

GO

ALTER TABLE [UserFeaturePermissions] DROP CONSTRAINT [FK_UserFeaturePermissions_Invitations_InvitedUserId];

GO

DROP INDEX [IX_UserFeaturePermissions_InvitedUserId] ON [UserFeaturePermissions];

GO

DROP INDEX [IX_UserFeaturePermissionHistories_InvitedUserId] ON [UserFeaturePermissionHistories];

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UserFeaturePermissions]') AND [c].[name] = N'InvitedUserId');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [UserFeaturePermissions] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [UserFeaturePermissions] DROP COLUMN [InvitedUserId];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UserFeaturePermissionHistories]') AND [c].[name] = N'InvitedUserId');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [UserFeaturePermissionHistories] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [UserFeaturePermissionHistories] DROP COLUMN [InvitedUserId];

GO

ALTER TABLE [UserFeaturePermissions] ADD [InvitationId] int NULL;

GO

ALTER TABLE [UserFeaturePermissionHistories] ADD [InvitationId] int NULL;

GO

ALTER TABLE [Invitations] ADD [IsSelectedAccount] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

CREATE INDEX [IX_UserFeaturePermissions_InvitationId] ON [UserFeaturePermissions] ([InvitationId]);

GO

CREATE INDEX [IX_UserFeaturePermissionHistories_InvitationId] ON [UserFeaturePermissionHistories] ([InvitationId]);

GO

ALTER TABLE [UserFeaturePermissionHistories] ADD CONSTRAINT [FK_UserFeaturePermissionHistories_Invitations_InvitationId] FOREIGN KEY ([InvitationId]) REFERENCES [Invitations] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [UserFeaturePermissions] ADD CONSTRAINT [FK_UserFeaturePermissions_Invitations_InvitationId] FOREIGN KEY ([InvitationId]) REFERENCES [Invitations] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210614075540_AddIsSelectedAccountAndInvitedUserIdRenaming', N'3.1.5');

GO

