﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Hubs]') AND [c].[name] = N'RegistrationDate');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Hubs] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Hubs] DROP COLUMN [RegistrationDate];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UsersDetails]') AND [c].[name] = N'DateOfBirth');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [UsersDetails] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [UsersDetails] ALTER COLUMN [DateOfBirth] datetime2 NULL;

GO

ALTER TABLE [Hubs] ADD [RegistrationEndDate] datetime2 NULL;

GO

ALTER TABLE [Hubs] ADD [RegistrationStartDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'TariffExpirationDate');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [EnergySetup] ALTER COLUMN [TariffExpirationDate] datetime2 NULL;

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergySetup]') AND [c].[name] = N'MaxPowerOutput');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [EnergySetup] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [EnergySetup] ALTER COLUMN [MaxPowerOutput] decimal(18,2) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200707073231_MakingFieldsNullable', N'3.1.5');

GO

