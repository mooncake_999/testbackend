﻿CREATE TABLE [Devices] (
    [Id] int NOT NULL IDENTITY,
    [DeviceName] nvarchar(max) NULL,
    [DeviceType] nvarchar(max) NULL,
    [SerialNumber] nvarchar(max) NULL,
    [AddressId] int NULL,
    [HubId] int NULL,
    [RegistrationStartDate] datetime2 NOT NULL,
    [RegistrationEndDate] datetime2 NULL,
    CONSTRAINT [PK_Devices] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Devices_Addresses_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [Addresses] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Devices_Hubs_HubId] FOREIGN KEY ([HubId]) REFERENCES [Hubs] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_Devices_AddressId] ON [Devices] ([AddressId]);

GO

CREATE INDEX [IX_Devices_HubId] ON [Devices] ([HubId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200707134302_CreateDeviceTable', N'3.1.5');

GO

