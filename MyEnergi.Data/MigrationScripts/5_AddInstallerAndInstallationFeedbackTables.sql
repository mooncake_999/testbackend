﻿CREATE TABLE [Installers] (
    [Id] int NOT NULL IDENTITY,
    [InstallerName] nvarchar(max) NULL,
    [InstallerCode] nvarchar(max) NULL,
    [Country] nvarchar(max) NULL,
    CONSTRAINT [PK_Installers] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200709082031_AddingInstallers', N'3.1.5');

GO

ALTER TABLE [Hubs] ADD [InstallationId] int NULL;

GO

ALTER TABLE [Devices] ADD [InstallationId] int NULL;

GO

CREATE TABLE [InstallationFeedbacks] (
    [Id] int NOT NULL IDENTITY,
    [InstalledBy] nvarchar(max) NULL,
    [Installer] nvarchar(max) NULL,
    [InstallerCode] nvarchar(max) NULL,
    [InstallerName] nvarchar(max) NULL,
    [InstallerWebsite] nvarchar(max) NULL,
    [InstallerPhoneNumber] nvarchar(max) NULL,
    [InstallationDate] datetime2 NULL,
    [QualityOfInstall] nvarchar(max) NULL,
    [Feedback] nvarchar(max) NULL,
    [ShareFeedback] bit NOT NULL,
    [PrizeDrawOptIn] bit NOT NULL,
    [SurveyOptIn] bit NOT NULL,
    [ReceivedGrant] bit NOT NULL,
    CONSTRAINT [PK_InstallationFeedbacks] PRIMARY KEY ([Id])
);

GO

CREATE INDEX [IX_Hubs_InstallationId] ON [Hubs] ([InstallationId]);

GO

CREATE INDEX [IX_Devices_InstallationId] ON [Devices] ([InstallationId]);

GO

ALTER TABLE [Devices] ADD CONSTRAINT [FK_Devices_InstallationFeedbacks_InstallationId] FOREIGN KEY ([InstallationId]) REFERENCES [InstallationFeedbacks] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Hubs] ADD CONSTRAINT [FK_Hubs_InstallationFeedbacks_InstallationId] FOREIGN KEY ([InstallationId]) REFERENCES [InstallationFeedbacks] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200709131943_CreateInstallationFeedbackTable', N'3.1.5');

GO

