﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[InstallationFeedbacks]') AND [c].[name] = N'ReceivedGrant');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [InstallationFeedbacks] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [InstallationFeedbacks] DROP COLUMN [ReceivedGrant];

GO

ALTER TABLE [InstallationFeedbacks] ADD [RecievedGrant] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [EnergySetup] ADD [GridSupplyPoint] nvarchar(max) NULL;

GO

ALTER TABLE [Devices] ADD [HeaterOutput1] nvarchar(max) NULL;

GO

ALTER TABLE [Devices] ADD [HeaterOutput2] nvarchar(max) NULL;

GO

ALTER TABLE [Devices] ADD [Status] int NOT NULL DEFAULT 0;

GO

CREATE TABLE [EnergyPrices] (
    [Id] int NOT NULL IDENTITY,
    [GridSupplyPoint] nvarchar(max) NULL,
    [ValueWithoutVAT] decimal(18,2) NOT NULL,
    [ValueWithVAT] decimal(18,2) NOT NULL,
    [ValidFrom] datetime2 NOT NULL,
    [ValidTo] datetime2 NOT NULL,
    CONSTRAINT [PK_EnergyPrices] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200727122044_CreateEnergyPriceTable', N'3.1.5');

GO

