﻿ALTER TABLE [EnergyPrices] ADD [EnergyProvider] nvarchar(max) NULL;

GO

ALTER TABLE [EnergyPrices] ADD [EnergyTariff] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200727130433_AddFieldsEnergyPrice', N'3.1.5');

GO

