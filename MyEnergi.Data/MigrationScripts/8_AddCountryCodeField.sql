﻿ALTER TABLE [Addresses] ADD [CountryCode] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200728064613_AddCountryCodeFieldAddress', N'3.1.5');

GO

