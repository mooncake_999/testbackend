﻿ALTER TABLE [Vehicles] ADD [ChargeRate] real NULL;

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergyPrices]') AND [c].[name] = N'ValueWithoutVAT');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [EnergyPrices] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [EnergyPrices] ALTER COLUMN [ValueWithoutVAT] real NOT NULL;

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EnergyPrices]') AND [c].[name] = N'ValueWithVAT');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [EnergyPrices] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [EnergyPrices] ALTER COLUMN [ValueWithVAT] real NOT NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200730135859_AddChargeRateField', N'3.1.5');

GO

