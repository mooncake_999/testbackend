﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddingEnergySetupTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnergySetup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EnergyProvider = table.Column<string>(nullable: true),
                    EnergyTariff = table.Column<string>(nullable: true),
                    TariffExpiration = table.Column<DateTime>(nullable: false),
                    SolarPanel = table.Column<bool>(nullable: false),
                    WindTurbine = table.Column<bool>(nullable: false),
                    Hydroelectric = table.Column<bool>(nullable: false),
                    OtherGenerator = table.Column<string>(nullable: true),
                    MaxPower = table.Column<decimal>(nullable: false),
                    AirSourceHeatPump = table.Column<bool>(nullable: false),
                    GroundSourceHeatPump = table.Column<bool>(nullable: false),
                    ElectricStorageHeating = table.Column<bool>(nullable: false),
                    OtherElectricHeating = table.Column<string>(nullable: true),
                    AddressId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergySetup", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnergySetup_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EnergySetup_AddressId",
                table: "EnergySetup",
                column: "AddressId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnergySetup");
        }
    }
}
