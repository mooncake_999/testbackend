﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class UpdateFieldNamesEnergySetup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxPower",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "OtherGenerator",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "SolarPanel",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "TariffExpiration",
                table: "EnergySetup");

            migrationBuilder.AddColumn<decimal>(
                name: "MaxPowerOutput",
                table: "EnergySetup",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "OtherGeneration",
                table: "EnergySetup",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SolarPanels",
                table: "EnergySetup",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "TariffExpirationDate",
                table: "EnergySetup",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SiteName",
                table: "Addresses",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxPowerOutput",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "OtherGeneration",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "SolarPanels",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "TariffExpirationDate",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "SiteName",
                table: "Addresses");

            migrationBuilder.AddColumn<decimal>(
                name: "MaxPower",
                table: "EnergySetup",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "OtherGenerator",
                table: "EnergySetup",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "SolarPanel",
                table: "EnergySetup",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "TariffExpiration",
                table: "EnergySetup",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
