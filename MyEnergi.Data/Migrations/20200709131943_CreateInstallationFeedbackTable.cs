﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class CreateInstallationFeedbackTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InstallationId",
                table: "Hubs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstallationId",
                table: "Devices",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "InstallationFeedbacks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    InstalledBy = table.Column<string>(nullable: true),
                    Installer = table.Column<string>(nullable: true),
                    InstallerCode = table.Column<string>(nullable: true),
                    InstallerName = table.Column<string>(nullable: true),
                    InstallerWebsite = table.Column<string>(nullable: true),
                    InstallerPhoneNumber = table.Column<string>(nullable: true),
                    InstallationDate = table.Column<DateTime>(nullable: true),
                    QualityOfInstall = table.Column<string>(nullable: true),
                    Feedback = table.Column<string>(nullable: true),
                    ShareFeedback = table.Column<bool>(nullable: false),
                    PrizeDrawOptIn = table.Column<bool>(nullable: false),
                    SurveyOptIn = table.Column<bool>(nullable: false),
                    ReceivedGrant = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstallationFeedbacks", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Hubs_InstallationId",
                table: "Hubs",
                column: "InstallationId");

            migrationBuilder.CreateIndex(
                name: "IX_Devices_InstallationId",
                table: "Devices",
                column: "InstallationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Devices_InstallationFeedbacks_InstallationId",
                table: "Devices",
                column: "InstallationId",
                principalTable: "InstallationFeedbacks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Hubs_InstallationFeedbacks_InstallationId",
                table: "Hubs",
                column: "InstallationId",
                principalTable: "InstallationFeedbacks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Devices_InstallationFeedbacks_InstallationId",
                table: "Devices");

            migrationBuilder.DropForeignKey(
                name: "FK_Hubs_InstallationFeedbacks_InstallationId",
                table: "Hubs");

            migrationBuilder.DropTable(
                name: "InstallationFeedbacks");

            migrationBuilder.DropIndex(
                name: "IX_Hubs_InstallationId",
                table: "Hubs");

            migrationBuilder.DropIndex(
                name: "IX_Devices_InstallationId",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "InstallationId",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "InstallationId",
                table: "Devices");
        }
    }
}
