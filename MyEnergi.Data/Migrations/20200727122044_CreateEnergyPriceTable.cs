﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class CreateEnergyPriceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReceivedGrant",
                table: "InstallationFeedbacks");

            migrationBuilder.AddColumn<bool>(
                name: "RecievedGrant",
                table: "InstallationFeedbacks",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "GridSupplyPoint",
                table: "EnergySetup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HeaterOutput1",
                table: "Devices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HeaterOutput2",
                table: "Devices",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Devices",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "EnergyPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GridSupplyPoint = table.Column<string>(nullable: true),
                    ValueWithoutVAT = table.Column<decimal>(nullable: false),
                    ValueWithVAT = table.Column<decimal>(nullable: false),
                    ValidFrom = table.Column<DateTime>(nullable: false),
                    ValidTo = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergyPrices", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnergyPrices");

            migrationBuilder.DropColumn(
                name: "RecievedGrant",
                table: "InstallationFeedbacks");

            migrationBuilder.DropColumn(
                name: "GridSupplyPoint",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "HeaterOutput1",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "HeaterOutput2",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Devices");

            migrationBuilder.AddColumn<bool>(
                name: "ReceivedGrant",
                table: "InstallationFeedbacks",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
