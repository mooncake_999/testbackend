﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddFieldsEnergyPrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EnergyProvider",
                table: "EnergyPrices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EnergyTariff",
                table: "EnergyPrices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnergyProvider",
                table: "EnergyPrices");

            migrationBuilder.DropColumn(
                name: "EnergyTariff",
                table: "EnergyPrices");
        }
    }
}
