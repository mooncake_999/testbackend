﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddChargeRateField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "ChargeRate",
                table: "Vehicles",
                nullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "ValueWithoutVAT",
                table: "EnergyPrices",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<float>(
                name: "ValueWithVAT",
                table: "EnergyPrices",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChargeRate",
                table: "Vehicles");

            migrationBuilder.AlterColumn<decimal>(
                name: "ValueWithoutVAT",
                table: "EnergyPrices",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<decimal>(
                name: "ValueWithVAT",
                table: "EnergyPrices",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(float));
        }
    }
}
