﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class CreateScheduleTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeviceChargesSetups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    DeviceId = table.Column<int>(nullable: true),
                    ScheduleType = table.Column<string>(nullable: true),
                    ScheduleNickName = table.Column<string>(nullable: true),
                    ChargingOutputName = table.Column<string>(nullable: true),
                    ChargeAmountMinutes = table.Column<int>(nullable: true),
                    FromTime = table.Column<string>(nullable: true),
                    ToTime = table.Column<string>(nullable: true),
                    DaysOfWeek = table.Column<string>(nullable: true),
                    PriceBelow = table.Column<int>(nullable: true),
                    SingleChargeDay = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceChargesSetups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeviceChargesSetups_Devices_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Devices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeviceChargesSetups_DeviceId",
                table: "DeviceChargesSetups",
                column: "DeviceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeviceChargesSetups");
        }
    }
}
