﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddedFieldsForDevicesAndSchedules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TimeZoneUTCDifference",
                table: "Hubs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeZoneUTCDifference",
                table: "Devices",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "DeviceChargesSetups",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeZoneUTCDifference",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "TimeZoneUTCDifference",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "DeviceChargesSetups");
        }
    }
}
