﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddFirmwareField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Firmware",
                table: "Hubs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Firmware",
                table: "Devices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Firmware",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "Firmware",
                table: "Devices");
        }
    }
}
