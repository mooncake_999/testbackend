﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddChargeRateUserVehicles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "ChargeRate",
                table: "UserVehicles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChargeRate",
                table: "UserVehicles");
        }
    }
}
