﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class UpdatePhoneNumberFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LandlineCode",
                table: "UsersDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MobileCode",
                table: "UsersDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LandlineCode",
                table: "UsersDetails");

            migrationBuilder.DropColumn(
                name: "MobileCode",
                table: "UsersDetails");
        }
    }
}
