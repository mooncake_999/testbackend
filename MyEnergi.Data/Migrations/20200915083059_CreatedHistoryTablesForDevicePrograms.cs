﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class CreatedHistoryTablesForDevicePrograms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeviceBestPriceHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    DeviceId = table.Column<int>(nullable: false),
                    From = table.Column<DateTime>(nullable: false),
                    To = table.Column<DateTime>(nullable: false),
                    IsScheduledType = table.Column<bool>(nullable: false),
                    IsSingleType = table.Column<bool>(nullable: false),
                    IsBudgetType = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceBestPriceHistories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DeviceChargeSetupHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    DeviceId = table.Column<int>(nullable: false),
                    ScheduleType = table.Column<string>(nullable: true),
                    ScheduleNickName = table.Column<string>(nullable: true),
                    ChargingOutputName = table.Column<string>(nullable: true),
                    ChargeAmountMinutes = table.Column<int>(nullable: true),
                    FromTime = table.Column<string>(nullable: true),
                    ToTime = table.Column<string>(nullable: true),
                    DaysOfWeek = table.Column<string>(nullable: true),
                    PriceBelow = table.Column<decimal>(nullable: true),
                    SingleChargeDay = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceChargeSetupHistories", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeviceBestPriceHistories");

            migrationBuilder.DropTable(
                name: "DeviceChargeSetupHistories");
        }
    }
}
