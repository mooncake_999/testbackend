﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddFieldsToDeviceBestPriceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HeaterSlot",
                table: "DeviceBestPriceIntervals",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SlotPriceValue",
                table: "DeviceBestPriceIntervals",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "HeaterSlot",
                table: "DeviceBestPriceHistories",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SlotPriceValue",
                table: "DeviceBestPriceHistories",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HeaterSlot",
                table: "DeviceBestPriceIntervals");

            migrationBuilder.DropColumn(
                name: "SlotPriceValue",
                table: "DeviceBestPriceIntervals");

            migrationBuilder.DropColumn(
                name: "HeaterSlot",
                table: "DeviceBestPriceHistories");

            migrationBuilder.DropColumn(
                name: "SlotPriceValue",
                table: "DeviceBestPriceHistories");
        }
    }
}
