﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddIsEconomyFlagAndChangeTimezoneFieldName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeZoneUTCDifference",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "TimeZoneUTCDifference",
                table: "Devices");

            migrationBuilder.AddColumn<string>(
                name: "TimeZoneRegion",
                table: "Hubs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsEconomy",
                table: "EnergyProviders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "TimeZoneRegion",
                table: "Devices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeZoneRegion",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "IsEconomy",
                table: "EnergyProviders");

            migrationBuilder.DropColumn(
                name: "TimeZoneRegion",
                table: "Devices");

            migrationBuilder.AddColumn<string>(
                name: "TimeZoneUTCDifference",
                table: "Hubs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeZoneUTCDifference",
                table: "Devices",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
