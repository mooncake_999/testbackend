﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddUserPreferences : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserPreferenceId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserPreferences",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateFormat = table.Column<string>(nullable: true),
                    IsMetric = table.Column<bool>(nullable: false),
                    Language = table.Column<string>(nullable: true),
                    Currency = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPreferences", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserPreferenceId",
                table: "Users",
                column: "UserPreferenceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserPreferences_UserPreferenceId",
                table: "Users",
                column: "UserPreferenceId",
                principalTable: "UserPreferences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserPreferences_UserPreferenceId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "UserPreferences");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserPreferenceId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserPreferenceId",
                table: "Users");
        }
    }
}
