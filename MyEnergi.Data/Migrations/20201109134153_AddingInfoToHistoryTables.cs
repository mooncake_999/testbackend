﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddingInfoToHistoryTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "DeviceChargeSetupHistories",
                newName: "DeviceChargesSetupHistories");
                
            migrationBuilder.RenameColumn(
                name: "Created",
                table: "DeviceChargesSetupHistories",
                newName: "MovedToHistory");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "DeviceBestPriceHistories",
                newName: "MovedToHistory");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "DeviceChargesSetupHistories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "PriceCreated",
                table: "DeviceBestPriceHistories",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "DeviceChargesSetupHistories",
                newName: "DeviceChargeSetupHistories");

            migrationBuilder.RenameColumn(
                name: "MovedToHistory",
                table: "DeviceChargeSetupHistories",
                newName: "Created");

            migrationBuilder.RenameColumn(
                name: "MovedToHistory",
                table: "DeviceBestPriceHistories",
                newName: "Created");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "DeviceChargeSetupHistories");

            migrationBuilder.DropColumn(
                name: "PriceCreated",
                table: "DeviceBestPriceHistories");

        }
    }
}
