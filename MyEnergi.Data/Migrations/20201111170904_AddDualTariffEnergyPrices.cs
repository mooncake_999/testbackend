﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddDualTariffEnergyPrices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DualTariffEnergyPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Day = table.Column<int>(nullable: false),
                    FromMinutes = table.Column<int>(nullable: false),
                    ToMinutes = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    EnergySetupId = table.Column<int>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdateDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DualTariffEnergyPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DualTariffEnergyPrices_EnergySetup_EnergySetupId",
                        column: x => x.EnergySetupId,
                        principalTable: "EnergySetup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DualTariffEnergyPrices_EnergySetupId",
                table: "DualTariffEnergyPrices",
                column: "EnergySetupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DualTariffEnergyPrices");
        }
    }
}
