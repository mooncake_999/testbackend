﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class UpdateDualTariffEnergyPricesDaysColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Day",
                table: "DualTariffEnergyPrices");

            migrationBuilder.AddColumn<string>(
                name: "Days",
                table: "DualTariffEnergyPrices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Days",
                table: "DualTariffEnergyPrices");

            migrationBuilder.AddColumn<int>(
                name: "Day",
                table: "DualTariffEnergyPrices",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
