﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddingGUIDToTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Vehicles",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "UserVehicles",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "UsersDetails",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Users",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "UserPreferences",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Installers",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "InstallationFeedbacks",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Hubs",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "EnergySetup",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "EnergyProviders",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "EnergyPrices",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "DualTariffEnergyPrices",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Devices",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "DeviceChargesSetups",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "DeviceChargesSetupHistories",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "DeviceBestPriceIntervals",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "DeviceBestPriceHistories",
                nullable: false,
                defaultValueSql: "newsequentialid()");

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Addresses",
                nullable: false,
                defaultValueSql: "newsequentialid()");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "UserVehicles");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "UsersDetails");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "UserPreferences");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Installers");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "InstallationFeedbacks");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "EnergyProviders");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "EnergyPrices");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "DualTariffEnergyPrices");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "DeviceChargesSetups");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "DeviceChargesSetupHistories");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "DeviceBestPriceIntervals");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "DeviceBestPriceHistories");

            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Addresses");
        }
    }
}
