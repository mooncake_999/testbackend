﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddConditionOwnershipToUserVehicle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Condition",
                table: "UserVehicles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ownership",
                table: "UserVehicles",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Condition",
                table: "UserVehicles");

            migrationBuilder.DropColumn(
                name: "Ownership",
                table: "UserVehicles");
        }
    }
}
