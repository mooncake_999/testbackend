﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class UpdateEnergyProviderAddingUpdateDateToAll : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "Vehicles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "UserVehicles",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "UsersDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "UserPreferences",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "Installers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "InstallationFeedbacks",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "Hubs",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EnergyProvId",
                table: "EnergySetup",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "EnergySetup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InternalReference",
                table: "EnergyProviders",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsUsed",
                table: "EnergyProviders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "EnergyProviders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EnergyProvId",
                table: "EnergyPrices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "EnergyPrices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "Devices",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdate",
                table: "Addresses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EnergySetup_EnergyProvId",
                table: "EnergySetup",
                column: "EnergyProvId");

            migrationBuilder.CreateIndex(
                name: "IX_EnergyPrices_EnergyProvId",
                table: "EnergyPrices",
                column: "EnergyProvId");

            migrationBuilder.AddForeignKey(
                name: "FK_EnergyPrices_EnergyProviders_EnergyProvId",
                table: "EnergyPrices",
                column: "EnergyProvId",
                principalTable: "EnergyProviders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EnergySetup_EnergyProviders_EnergyProvId",
                table: "EnergySetup",
                column: "EnergyProvId",
                principalTable: "EnergyProviders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EnergyPrices_EnergyProviders_EnergyProvId",
                table: "EnergyPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_EnergySetup_EnergyProviders_EnergyProvId",
                table: "EnergySetup");

            migrationBuilder.DropIndex(
                name: "IX_EnergySetup_EnergyProvId",
                table: "EnergySetup");

            migrationBuilder.DropIndex(
                name: "IX_EnergyPrices_EnergyProvId",
                table: "EnergyPrices");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "Vehicles");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "UserVehicles");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "UsersDetails");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "UserPreferences");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "Installers");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "InstallationFeedbacks");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "EnergyProvId",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "InternalReference",
                table: "EnergyProviders");

            migrationBuilder.DropColumn(
                name: "IsUsed",
                table: "EnergyProviders");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "EnergyProviders");

            migrationBuilder.DropColumn(
                name: "EnergyProvId",
                table: "EnergyPrices");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "EnergyPrices");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "Devices");

            migrationBuilder.DropColumn(
                name: "LastUpdate",
                table: "Addresses");
        }
    }
}
