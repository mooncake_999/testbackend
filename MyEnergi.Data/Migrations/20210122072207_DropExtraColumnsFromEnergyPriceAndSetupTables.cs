﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class DropExtraColumnsFromEnergyPriceAndSetupTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnergyProvider",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "EnergyTariff",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "IsEconomy",
                table: "EnergySetup");

            migrationBuilder.DropColumn(
                name: "EnergyProvider",
                table: "EnergyPrices");

            migrationBuilder.DropColumn(
                name: "EnergyTariff",
                table: "EnergyPrices");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EnergyProvider",
                table: "EnergySetup",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EnergyTariff",
                table: "EnergySetup",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsEconomy",
                table: "EnergySetup",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "EnergyProvider",
                table: "EnergyPrices",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EnergyTariff",
                table: "EnergyPrices",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
