﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class ModifyIsUsedToIsActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUsed",
                table: "EnergyProviders");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "EnergyProviders",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "EnergyProviders");

            migrationBuilder.AddColumn<bool>(
                name: "IsUsed",
                table: "EnergyProviders",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
