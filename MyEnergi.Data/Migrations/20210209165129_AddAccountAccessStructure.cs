﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddAccountAccessStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Feature = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppFeatures", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InvitationStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Status = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvitationStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Role = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Invitations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    GuestUserId = table.Column<int>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    InvitationStatusId = table.Column<int>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    ExpirationDate = table.Column<DateTime>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CronExpressionRecurrence = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invitations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invitations_Users_GuestUserId",
                        column: x => x.GuestUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Invitations_InvitationStatuses_InvitationStatusId",
                        column: x => x.InvitationStatusId,
                        principalTable: "InvitationStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RolesToUser",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    UserId = table.Column<int>(nullable: true),
                    RoleId = table.Column<int>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolesToUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RolesToUser_UserRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "UserRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RolesToUser_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SystemFeatures",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Feature = table.Column<string>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    UserRoleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SystemFeatures_UserRoles_UserRoleId",
                        column: x => x.UserRoleId,
                        principalTable: "UserRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserFeaturePermissionHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    OwnerUserId = table.Column<int>(nullable: true),
                    InvitedUserId = table.Column<int>(nullable: true),
                    DeviceId = table.Column<int>(nullable: true),
                    AppFeatureId = table.Column<int>(nullable: true),
                    MovedToHistory = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFeaturePermissionHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissionHistories_AppFeatures_AppFeatureId",
                        column: x => x.AppFeatureId,
                        principalTable: "AppFeatures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissionHistories_Devices_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Devices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissionHistories_Invitations_InvitedUserId",
                        column: x => x.InvitedUserId,
                        principalTable: "Invitations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissionHistories_Users_OwnerUserId",
                        column: x => x.OwnerUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserFeaturePermissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    OwnerUserId = table.Column<int>(nullable: true),
                    InvitedUserId = table.Column<int>(nullable: true),
                    DeviceId = table.Column<int>(nullable: true),
                    AppFeatureId = table.Column<int>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserFeaturePermissions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissions_AppFeatures_AppFeatureId",
                        column: x => x.AppFeatureId,
                        principalTable: "AppFeatures",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissions_Devices_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Devices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissions_Invitations_InvitedUserId",
                        column: x => x.InvitedUserId,
                        principalTable: "Invitations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserFeaturePermissions_Users_OwnerUserId",
                        column: x => x.OwnerUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_GuestUserId",
                table: "Invitations",
                column: "GuestUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_InvitationStatusId",
                table: "Invitations",
                column: "InvitationStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_RolesToUser_RoleId",
                table: "RolesToUser",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_RolesToUser_UserId",
                table: "RolesToUser",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemFeatures_UserRoleId",
                table: "SystemFeatures",
                column: "UserRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissionHistories_AppFeatureId",
                table: "UserFeaturePermissionHistories",
                column: "AppFeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissionHistories_DeviceId",
                table: "UserFeaturePermissionHistories",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissionHistories_InvitedUserId",
                table: "UserFeaturePermissionHistories",
                column: "InvitedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissionHistories_OwnerUserId",
                table: "UserFeaturePermissionHistories",
                column: "OwnerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissions_AppFeatureId",
                table: "UserFeaturePermissions",
                column: "AppFeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissions_DeviceId",
                table: "UserFeaturePermissions",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissions_InvitedUserId",
                table: "UserFeaturePermissions",
                column: "InvitedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissions_OwnerUserId",
                table: "UserFeaturePermissions",
                column: "OwnerUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RolesToUser");

            migrationBuilder.DropTable(
                name: "SystemFeatures");

            migrationBuilder.DropTable(
                name: "UserFeaturePermissionHistories");

            migrationBuilder.DropTable(
                name: "UserFeaturePermissions");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "AppFeatures");

            migrationBuilder.DropTable(
                name: "Invitations");

            migrationBuilder.DropTable(
                name: "InvitationStatuses");
        }
    }
}
