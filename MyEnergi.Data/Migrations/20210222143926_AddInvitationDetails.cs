﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddInvitationDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Invitations");

            migrationBuilder.AddColumn<int>(
                name: "InvitationDetailsId",
                table: "Invitations",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "InvitationDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    AppAccess = table.Column<bool>(nullable: false),
                    AdministratorAccess = table.Column<bool>(nullable: false),
                    AllDevicesAccess = table.Column<bool>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvitationDetails", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_InvitationDetailsId",
                table: "Invitations",
                column: "InvitationDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Invitations_InvitationDetails_InvitationDetailsId",
                table: "Invitations",
                column: "InvitationDetailsId",
                principalTable: "InvitationDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invitations_InvitationDetails_InvitationDetailsId",
                table: "Invitations");

            migrationBuilder.DropTable(
                name: "InvitationDetails");

            migrationBuilder.DropIndex(
                name: "IX_Invitations_InvitationDetailsId",
                table: "Invitations");

            migrationBuilder.DropColumn(
                name: "InvitationDetailsId",
                table: "Invitations");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Invitations",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
