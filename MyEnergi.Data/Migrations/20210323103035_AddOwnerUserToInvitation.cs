﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddOwnerUserToInvitation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OwnerUserId",
                table: "Invitations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_OwnerUserId",
                table: "Invitations",
                column: "OwnerUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Invitations_Users_OwnerUserId",
                table: "Invitations",
                column: "OwnerUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Invitations_Users_OwnerUserId",
                table: "Invitations");

            migrationBuilder.DropIndex(
                name: "IX_Invitations_OwnerUserId",
                table: "Invitations");

            migrationBuilder.DropColumn(
                name: "OwnerUserId",
                table: "Invitations");
        }
    }
}
