﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddedHistoryTablesAndIsDeletedHub : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOfDelete",
                table: "Devices");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Hubs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "DeviceHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    OwnerUserId = table.Column<int>(nullable: true),
                    DeviceName = table.Column<string>(nullable: true),
                    DeviceType = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<string>(nullable: true),
                    MovedToHistory = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeviceHistories_Users_OwnerUserId",
                        column: x => x.OwnerUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HubHistories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Guid = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    OwnerUserId = table.Column<int>(nullable: true),
                    Nickname = table.Column<string>(nullable: true),
                    SerialNumber = table.Column<string>(nullable: true),
                    MovedToHistory = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HubHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HubHistories_Users_OwnerUserId",
                        column: x => x.OwnerUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DeviceHistories_OwnerUserId",
                table: "DeviceHistories",
                column: "OwnerUserId");

            migrationBuilder.CreateIndex(
                name: "IX_HubHistories_OwnerUserId",
                table: "HubHistories",
                column: "OwnerUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeviceHistories");

            migrationBuilder.DropTable(
                name: "HubHistories");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Hubs");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfDelete",
                table: "Devices",
                type: "datetime2",
                nullable: true);
        }
    }
}
