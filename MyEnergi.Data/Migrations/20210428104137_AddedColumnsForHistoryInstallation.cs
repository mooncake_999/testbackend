﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddedColumnsForHistoryInstallation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InstallationId",
                table: "HubHistories",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstallationId",
                table: "DeviceHistories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HubHistories_InstallationId",
                table: "HubHistories",
                column: "InstallationId");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceHistories_InstallationId",
                table: "DeviceHistories",
                column: "InstallationId");

            migrationBuilder.AddForeignKey(
                name: "FK_DeviceHistories_InstallationFeedbacks_InstallationId",
                table: "DeviceHistories",
                column: "InstallationId",
                principalTable: "InstallationFeedbacks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HubHistories_InstallationFeedbacks_InstallationId",
                table: "HubHistories",
                column: "InstallationId",
                principalTable: "InstallationFeedbacks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeviceHistories_InstallationFeedbacks_InstallationId",
                table: "DeviceHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_HubHistories_InstallationFeedbacks_InstallationId",
                table: "HubHistories");

            migrationBuilder.DropIndex(
                name: "IX_HubHistories_InstallationId",
                table: "HubHistories");

            migrationBuilder.DropIndex(
                name: "IX_DeviceHistories_InstallationId",
                table: "DeviceHistories");

            migrationBuilder.DropColumn(
                name: "InstallationId",
                table: "HubHistories");

            migrationBuilder.DropColumn(
                name: "InstallationId",
                table: "DeviceHistories");
        }
    }
}
