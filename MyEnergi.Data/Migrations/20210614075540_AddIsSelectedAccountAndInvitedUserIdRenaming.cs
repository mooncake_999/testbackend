﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyEnergi.Data.Migrations
{
    public partial class AddIsSelectedAccountAndInvitedUserIdRenaming : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserFeaturePermissionHistories_Invitations_InvitedUserId",
                table: "UserFeaturePermissionHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_UserFeaturePermissions_Invitations_InvitedUserId",
                table: "UserFeaturePermissions");

            migrationBuilder.DropIndex(
                name: "IX_UserFeaturePermissions_InvitedUserId",
                table: "UserFeaturePermissions");

            migrationBuilder.DropIndex(
                name: "IX_UserFeaturePermissionHistories_InvitedUserId",
                table: "UserFeaturePermissionHistories");

            migrationBuilder.DropColumn(
                name: "InvitedUserId",
                table: "UserFeaturePermissions");

            migrationBuilder.DropColumn(
                name: "InvitedUserId",
                table: "UserFeaturePermissionHistories");

            migrationBuilder.AddColumn<int>(
                name: "InvitationId",
                table: "UserFeaturePermissions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InvitationId",
                table: "UserFeaturePermissionHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSelectedAccount",
                table: "Invitations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissions_InvitationId",
                table: "UserFeaturePermissions",
                column: "InvitationId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissionHistories_InvitationId",
                table: "UserFeaturePermissionHistories",
                column: "InvitationId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserFeaturePermissionHistories_Invitations_InvitationId",
                table: "UserFeaturePermissionHistories",
                column: "InvitationId",
                principalTable: "Invitations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserFeaturePermissions_Invitations_InvitationId",
                table: "UserFeaturePermissions",
                column: "InvitationId",
                principalTable: "Invitations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserFeaturePermissionHistories_Invitations_InvitationId",
                table: "UserFeaturePermissionHistories");

            migrationBuilder.DropForeignKey(
                name: "FK_UserFeaturePermissions_Invitations_InvitationId",
                table: "UserFeaturePermissions");

            migrationBuilder.DropIndex(
                name: "IX_UserFeaturePermissions_InvitationId",
                table: "UserFeaturePermissions");

            migrationBuilder.DropIndex(
                name: "IX_UserFeaturePermissionHistories_InvitationId",
                table: "UserFeaturePermissionHistories");

            migrationBuilder.DropColumn(
                name: "InvitationId",
                table: "UserFeaturePermissions");

            migrationBuilder.DropColumn(
                name: "InvitationId",
                table: "UserFeaturePermissionHistories");

            migrationBuilder.DropColumn(
                name: "IsSelectedAccount",
                table: "Invitations");

            migrationBuilder.AddColumn<int>(
                name: "InvitedUserId",
                table: "UserFeaturePermissions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InvitedUserId",
                table: "UserFeaturePermissionHistories",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissions_InvitedUserId",
                table: "UserFeaturePermissions",
                column: "InvitedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserFeaturePermissionHistories_InvitedUserId",
                table: "UserFeaturePermissionHistories",
                column: "InvitedUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserFeaturePermissionHistories_Invitations_InvitedUserId",
                table: "UserFeaturePermissionHistories",
                column: "InvitedUserId",
                principalTable: "Invitations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserFeaturePermissions_Invitations_InvitedUserId",
                table: "UserFeaturePermissions",
                column: "InvitedUserId",
                principalTable: "Invitations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
