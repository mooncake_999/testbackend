﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class AddressRepository : Repository<Address>, IAddressRepository
    {
        public AddressRepository(MyEnergiDbContext context) :
            base(context)
        {
        }

        public async Task<IEnumerable<Address>> GetAddresses(string userCognitoId)
        {
            return await GetBy(a => a.User.CognitoId.Equals(userCognitoId));
        }

        public async Task<Address> GetAddressById(Guid id)
        {
            return await GetSingleOrDefaultBy(a => a.Guid == id);
        }

        public async Task<IEnumerable<int>> GetAddressesIdByUserId(string userId)
        {
            return (await GetBy(a => a.User.CognitoId == userId))
                .Select(a => a.Id);
        }

        public async Task<IEnumerable<Address>> GetUserEnergyAddresses(string userCognitoId)
        {
            return await Context.EnergySetup
                .Include(e => e.Address)
                .Where(e => e.Address.User.CognitoId == userCognitoId)
                .Select(e => e.Address)
                .Distinct()
                .ToListAsync();
        }

        public async Task<Address> GetAddressByIdAndUserId(Guid id, string userCognitoId)
        {
            return await GetSingleOrDefaultBy(a => a.Guid == id && a.User.CognitoId == userCognitoId);
        }

        public async Task<IEnumerable<int>> GetIsEconomyAddresses()
        {
            return await Context.EnergySetup
                .Include(e => e.EnergyProv)
                .Where(e => e.EnergyProv.IsEconomy)
                .Select(e => e.Address.Id)
                .Distinct()
                .ToListAsync();
        }

        public async Task<IEnumerable<int>> GetNonEconomyAddressId()
        {
            return await Context.EnergySetup
                .Include(e => e.EnergyProv)
                .Where(e => !e.EnergyProv.IsEconomy)
                .Select(e => e.Address.Id)
                .Distinct()
                .ToListAsync();
        }

        public async Task<Dictionary<int, string>> GetUserDateFormatPreferenceByAddress(List<int> addrIds)
        {
            var userAddresses = await GetBy(x => addrIds.Contains(x.Id), 
                include: source => source
                    .Include(x => x.User.UserPreference));
            return userAddresses.ToDictionary(x => x.Id, x => x.User.UserPreference?.DateFormat);
        }
        
        public async Task<Dictionary<Guid, User>> GetAddressGuidsAndUsersByAddressesIds(List<Guid> addressIds)
        {
            var addresses = await GetBy(adr => addressIds.Contains(adr.Guid),
                include: source => source.Include(a => a.User));

            return addresses.ToDictionary(a => a.Guid, a => a.User);
        }

        async Task<IEnumerable<int>> IAddressRepository.GetAddressFromEnergySetupBasedOnInternalReference(string providerInternalReference)
        {
            return await Context.EnergySetup
                .Include(e => e.EnergyProv)
                .Where(e => e.EnergyProv.InternalReference.Equals(providerInternalReference))
                .Select(e => e.Address.Id)
                .Distinct()
                .ToListAsync();
        }

        async Task<IEnumerable<int>> IAddressRepository.GetAddressFromEnergySetupBasedOnProviderName(string providerName)
        {
            return await Context.EnergySetup
                .Include(e => e.EnergyProv)
                .Where(e => e.EnergyProv.Provider.Equals(providerName))
                .Select(e => e.Address.Id)
                .Distinct()
                .ToListAsync();
        }
    }
}
