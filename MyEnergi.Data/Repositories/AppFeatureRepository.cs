﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class AppFeatureRepository : Repository<AppFeature>, IAppFeatureRepository
    {
        public AppFeatureRepository(MyEnergiDbContext context) : 
            base(context)
        {
        }

        public async Task<IEnumerable<AppFeature>> GetAllAppFeaturesByName(IEnumerable<string> featuresName)
        {
            return await Context.AppFeatures
                .Where(x => featuresName.Contains(x.Feature))
                .ToListAsync();
        }
    }
}
