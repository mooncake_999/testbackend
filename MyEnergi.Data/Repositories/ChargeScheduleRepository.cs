﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class ChargeScheduleRepository : Repository<DeviceChargesSetup>, IChargeScheduleRepository
    {
        private readonly IMapper _mapper;

        public ChargeScheduleRepository(MyEnergiDbContext context, IMapper mapper) :
            base(context)
        {
            _mapper = mapper;
        }

        async Task<IEnumerable<DeviceChargesSetup>> IChargeScheduleRepository.GetAllChargeSchedulesByUser(
            string userCognitoId)
        {
            return await Context.DeviceChargesSetups
                .Include(d => d.Device)
                .Where(d => d.Device.Address.User.CognitoId == userCognitoId)
                .ToListAsync();
        }

        async Task<IEnumerable<DeviceChargesSetup>> IChargeScheduleRepository.GetChargeSchedules()
        {
            return await Context.DeviceChargesSetups
                .Include(d => d.Device)
                .ThenInclude(d => d.Hub)
                .ThenInclude(d => d.Address)
                .ToListAsync();
        }

        async Task IChargeScheduleRepository.DeleteChargeSetups(List<DeviceChargesSetup> dbChargesSetupsToDelete)
        {
            Context.DeviceChargesSetupHistories.AddRange(_mapper.Map<List<DeviceChargesSetupHistory>>(dbChargesSetupsToDelete));
            Context.DeviceChargesSetups.RemoveRange(dbChargesSetupsToDelete);
            await Task.FromResult(0);
        }

        async Task<IEnumerable<DeviceChargesSetup>> IChargeScheduleRepository.GetChargeSchedulesByDeviceId(Guid id)
        {
            return await Context.DeviceChargesSetups
                .Include(c => c.Device)
                .Where(c => c.Device.Guid == id)
                .ToListAsync();
        }

        async Task IChargeScheduleRepository.MoveChargeToHistoryTable(DeviceChargesSetup dbChargeSetup)
        {
            var historyChargeSetup = _mapper.Map<DeviceChargesSetupHistory>(dbChargeSetup);
            Context.DeviceChargesSetupHistories.AddRange(historyChargeSetup);
            await Task.FromResult(0);
        }

        async Task<IEnumerable<DeviceChargesSetup>> IChargeScheduleRepository.GetAllEconomyChargesByAddressId(List<int> economyAddressId)
        {
            return await Context.DeviceChargesSetups
                .Include(d =>d.Device)
                .Include(d =>d.Device.Address)
                .Include(d =>d.Device.Hub)
                .Where(d => economyAddressId.Contains(d.Device.Address.Id) && d.IsActive && d.Device.Hub != null)
                .ToListAsync();
        }
    }
}
