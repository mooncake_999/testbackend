﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class DeviceRepository : Repository<Device>, IDeviceRepository
    {
        public DeviceRepository(MyEnergiDbContext context) :
            base(context)
        {
        }

        public async Task<Device> GetDeviceById(Guid deviceId)
        {
            return await GetSingleOrDefaultBy(h => h.Guid == deviceId,
                include: source => source
                    .Include(h => h.Installation)
                    .Include(h => h.Hub));
        }

        public async Task<IEnumerable<Device>> GetDevicesByUserId(string userId)
        {
            return await GetBy(d => d.Address.User.CognitoId == userId,
                include: source => source
                    .Include(d => d.Address)
                    .Include(d => d.Hub)
                    .Include(d => d.Installation));
        }

        public async Task<IEnumerable<Device>> GetDevicesByUserIdIsDeleted(string userId)
        {
            return await GetBy(d => d.Hub.Address.User.CognitoId == userId,
                include: source => source.IgnoreQueryFilters()
                    .Include(d => d.Address)
                    .Include(d => d.Hub)
                    .Include(d => d.Installation));
        }

        public async Task<Device> GetDeviceBySerialNo(string serialNumber)
        {
            return await GetSingleOrDefaultBy(h => h.SerialNumber == serialNumber);
        }

        public async Task<Device> GetDeviceBySerialNoIsDeleted(string serialNumber)
        {
            return await GetSingleOrDefaultBy(h => h.SerialNumber == serialNumber,
                include: source => source.IgnoreQueryFilters()
                    .Include(h => h.Hub));
        }

        public async Task<IEnumerable<Device>> GetDevicesByHubId(Guid id)
        {
            return await GetBy(d => d.Hub.Guid == id);
        }

        public async Task<IEnumerable<Device>> GetDevicesByHubIdIsDeleted(Guid id)
        {
            return await Context.Devices
                .IgnoreQueryFilters()
                .Where(d => d.Hub.Guid == id)
                .ToListAsync();
        }

        public async Task<IEnumerable<Device>> GetDevicesWithHubsAndChargeSchedulesByUserId(string userCognitoId)
        {
            return await GetBy(d => d.Address.User.CognitoId == userCognitoId && d.Hub != null,
                include: source => source
                    .Include(d => d.Hub)
                    .Include(d => d.Address)
                    .Include(d => d.ChargeSchedules));
        }

        public async Task<Device> GetDeviceWithChargeScheduleById(Guid deviceId, string userCognitoId)
        {
            return await GetFirstOrDefaultBy(
                d => d.Guid == deviceId &&
                     d.Hub != null &&
                     d.Address.User.CognitoId == userCognitoId,
                include: source => source
                    .Include(d => d.Address)
                    .Include(d => d.Hub)
                    .Include(d => d.ChargeSchedules));
        }

        public async Task<IEnumerable<Device>> GetAllDevicesWithChargeSchedule()
        {
            return await GetBy(d => d.Hub != null && d.ChargeSchedules.Any(c => c.IsActive),
                include: source => source
                    .Include(d => d.Address)
                    .Include(d => d.Hub)
                    .Include(d => d.ChargeSchedules));
        }

        public async Task<IEnumerable<Device>> GetDevicesWithHubsByUserId(string userCognitoId)
        {
            return await GetBy(d => d.Hub != null && d.Address.User.CognitoId == userCognitoId,
                include: source => source.Include(h => h.Hub));
        }

        public async Task<bool> CheckIfDevicesExistsOnAddress(Guid addressId)
        {
            return await Context.Devices.AnyAsync(d => d.Address.Guid == addressId);
        }

        public async Task<Device> GetDeviceByIdAndUserId(Guid deviceId, string userCognitoId)
        {
            return await GetSingleOrDefaultBy(d => d.Guid == deviceId && d.Address.User.CognitoId == userCognitoId,
                include: source => source
                    .Include(h => h.Installation)
                    .Include(h => h.Hub));
        }

        public async Task<IEnumerable<Device>> GetDevicesByHubsId(IEnumerable<Guid> hubIds)
        {
            return await GetBy(d => hubIds.Contains(d.Hub.Guid));
        }

        public async Task<IEnumerable<Device>> GetAllDevicesWithChargeScheduleByAddressId(List<int> addressesNotEconomy)
        {
            return await GetBy(d => d.Hub != null &&
                              d.ChargeSchedules.Any(c => c.IsActive) &&
                              addressesNotEconomy.Contains(d.Address.Id),
                include: source => source
                    .Include(d => d.Address)
                    .Include(d => d.Hub)
                    .Include(d => d.ChargeSchedules));
        }

        public async Task<Device> GetGuestDeviceByIdAndCognitoId(Guid deviceId, string userCognitoId)
        {
            var deviceHub = await Context.UserFeaturePermissions
                .Where(ufp => ufp.Device.Guid.Equals(deviceId) &&
                              ufp.Invitation.GuestUser != null &&
                              ufp.Invitation.GuestUser.CognitoId.Equals(userCognitoId))
                .Select(ufp => ufp.Device)
                .FirstOrDefaultAsync();

            return deviceHub;
        }
    }
}
