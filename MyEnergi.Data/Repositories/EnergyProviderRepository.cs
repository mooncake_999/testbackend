﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class EnergyProviderRepository : Repository<EnergyProvider>, IEnergyProviderRepository
    {
        private readonly IMapper _mapper;

        public EnergyProviderRepository(MyEnergiDbContext context, IMapper mapper) :
            base(context)
        {
            _mapper = mapper;
        }

        async Task IEnergyProviderRepository.DeleteBestPriceIntervals(Guid deviceId, System.DateTime now)
        {
            IEnumerable<DeviceBestPriceIntervals> bestPrices =
                await Context.DeviceBestPriceIntervals.Where(d => d.Device.Guid == deviceId).ToListAsync();
            var bestPriceToSaveHistory =
                _mapper.Map<IEnumerable<DeviceBestPriceHistory>>(bestPrices.Where(b => now > b.From));

            Context.DeviceBestPriceHistories.AddRange(bestPriceToSaveHistory);
            Context.DeviceBestPriceIntervals.RemoveRange(bestPrices);
        }

        async Task IEnergyProviderRepository.DeleteEnergyPrices(string gridSupplyPoint, int energyProviderId)
        {
            var result = await Context.EnergyPrices.Where(e =>
                e.GridSupplyPoint == gridSupplyPoint && e.EnergyProv.Id == energyProviderId).ToListAsync();

            Context.EnergyPrices.RemoveRange(result);
        }

        async Task IEnergyProviderRepository.DeleteEnergyPricesByProvider(int energyProviderId)
        {
            var result = await Context.EnergyPrices.Where(e => e.EnergyProv.Id == energyProviderId).ToListAsync();
            Context.EnergyPrices.RemoveRange(result);
        }

        async Task IEnergyProviderRepository.DeleteEnergyPricesWithGSP(string gridSupplyPoint)
        {
            var result = await Context.EnergyPrices.Where(e => e.GridSupplyPoint == gridSupplyPoint).ToListAsync();
            Context.EnergyPrices.RemoveRange(result);
        }

        async Task IEnergyProviderRepository.DeleteEnergyProviders(List<EnergyProvider> dbProvidersToBeDeleted)
        {
            Context.EnergyProviders.RemoveRange(dbProvidersToBeDeleted);
            await Task.FromResult(0);
        }

        async Task<IEnumerable<EnergyPrice>> IEnergyProviderRepository.GetAllEnergyPrices()
        {
            return await Context.EnergyPrices
                .Include(e => e.EnergyProv)
                .ToListAsync();
        }

        async Task<IEnumerable<EnergyProvider>> IEnergyProviderRepository.GetAllEnergyProviders()
        {
            return await Context.EnergyProviders.ToListAsync();
        }

        async Task IEnergyProviderRepository.AddUpdateEnergySetup(EnergySetup energy)
        {
            if (energy.Id == default)
            {
                Context.EnergySetup.Add(energy);
            }
            else
            {
                Context.EnergySetup.Update(energy);
            }
            
            await Task.FromResult(0);
        }

        async Task<IEnumerable<EnergySetup>> IEnergyProviderRepository.GetAllEnergySetups()
        {
            return await Context.EnergySetup
                .Include(e => e.Address)
                .Include(e => e.EnergyProv)
                .ToListAsync();
        }

        async Task<IEnumerable<DeviceBestPriceIntervals>> IEnergyProviderRepository.GetDeviceBestPriceIntervalsByDeviceIds(List<Guid> deviceIds)
        {
            return await Context.DeviceBestPriceIntervals
                .Where(d => deviceIds.Contains(d.Device.Guid))
                .ToListAsync();
        }

        async Task<IEnumerable<EnergyPrice>> IEnergyProviderRepository.GetEnergyPrices(int energyProviderId, string gsp)
        {
            return await Context.EnergyPrices
                .Where(e => e.EnergyProv.Id == energyProviderId && e.GridSupplyPoint == gsp)
                .ToListAsync();
        }

        async Task<EnergyProvider> IEnergyProviderRepository.GetEnergyProviderByFields(string energyProvider, string tariffName)
        {
            return await GetSingleOrDefaultBy(e => e.Provider == energyProvider && e.Tariff == tariffName);
        }

        async Task<IEnumerable<EnergyProvider>> IEnergyProviderRepository.GetEnergyProvidersByField(string energyProvider)
        {
            return await GetBy(e => e.Provider == energyProvider);
        }

        async Task<EnergySetup> IEnergyProviderRepository.GetEnergySetupByAddressId(Guid addressId)
        {
            return await Context.EnergySetup
                .Include(e => e.EnergyProv)
                .FirstOrDefaultAsync(e => e.Address.Guid == addressId);
        }

        async Task<EnergySetup> IEnergyProviderRepository.GetEnergySetupById(Guid id)
        {
            return await Context.EnergySetup.FirstOrDefaultAsync(e => e.Guid == id);
        }

        async Task<EnergySetup> IEnergyProviderRepository.GetEnergySetupByIdAndUser(Guid energySetupId, string userCognitoId)
        {
            return await Context.EnergySetup
                .Include(e => e.Address)
                .SingleOrDefaultAsync(e => e.Guid == energySetupId && e.Address.User.CognitoId == userCognitoId);
        }

        async Task<IEnumerable<EnergySetup>> IEnergyProviderRepository.GetEnergySetupByUserId(string userCognitoId)
        {
            return await Context.EnergySetup
                .Include(e => e.Address)
                .Include(e => e.EnergyProv)
                .Where(e => e.Address.User.CognitoId == userCognitoId)
                .ToListAsync();
        }

        async Task IEnergyProviderRepository.RemoveEnergySetup(EnergySetup energySetup)
        {
            Context.EnergySetup.Remove(energySetup);
            await Task.FromResult(0);
        }

        async Task IEnergyProviderRepository.SaveBestPriceIntervals(List<DeviceBestPriceIntervals> bestPriceIntervals)
        {
            Context.DeviceBestPriceIntervals.AddRange(bestPriceIntervals);
            await Task.FromResult(0);
        }

        async Task IEnergyProviderRepository.SaveEnergyPrices(List<EnergyPrice> energyPrices)
        {
            Context.AddRange(energyPrices);
            await Task.FromResult(0);
        }

        async Task IEnergyProviderRepository.SaveDualTariffEnergyPrice(DualTariffEnergyPrice dualTariffEnergyPrice)
        {
            if (dualTariffEnergyPrice.Id != default)
            {
                Context.DualTariffEnergyPrices.Update(dualTariffEnergyPrice);
            }
            else
            {
                Context.DualTariffEnergyPrices.Add(dualTariffEnergyPrice);
            }

            await Task.FromResult(0);
        }

        async Task<DualTariffEnergyPrice> IEnergyProviderRepository.GetDualTariffEnergyPriceById(Guid dualTariffId)
        {
            return await Context.DualTariffEnergyPrices
                .SingleOrDefaultAsync(x => x.Guid == dualTariffId);
        }

        async Task<IEnumerable<DualTariffEnergyPrice>> IEnergyProviderRepository.GetDualTariffEnergyPriceByAddress(EnergySetup energySetup)
        {
            return await Context.DualTariffEnergyPrices
                .Where(x => x.EnergySetup.Id == energySetup.Id)
                .ToListAsync();
        }

        async Task IEnergyProviderRepository.DeleteDualTariffEnergyPrice(IEnumerable<DualTariffEnergyPrice> dualTariffEnergyPrice)
        {
            Context.DualTariffEnergyPrices.RemoveRange(dualTariffEnergyPrice);
            await Task.FromResult(0);
        }

        async Task<IEnumerable<DualTariffEnergyPrice>> IEnergyProviderRepository.GetDualTariffEnergyPriceByListOfAddressesId(List<int> addressId)
        {
            return await Context.DualTariffEnergyPrices
                .Include(d => d.EnergySetup)
                .Include(d => d.EnergySetup.Address)
                .Where(d => addressId.Contains(d.EnergySetup.Address.Id))
                .ToListAsync();
        }

        async Task<IEnumerable<DeviceBestPriceIntervals>> IEnergyProviderRepository.GetDeviceBestPriceIntervalByHubId(int hubId)
        {
            return await Context.DeviceBestPriceIntervals
                .Include(d => d.Device)
                .Include(d => d.Device.Hub)
                .Where(d => d.Device.Hub.Id == hubId)
                .ToListAsync();
        }

        async Task<EnergyProvider> IEnergyProviderRepository.GetEnergyProviderByGuid(Guid energyProviderId)
        {
            return await Context.EnergyProviders
                .SingleOrDefaultAsync(e => e.Guid == energyProviderId);
        }

        async Task<IEnumerable<EnergyProvider>> IEnergyProviderRepository.GetUsedEnergyProviders()
        {
            return await Context.EnergyProviders
                .OrderBy(e => e.Provider)
                .ThenBy(e => e.Tariff)
                .Where(e => e.IsActive)
                .ToListAsync();
        }
    }
}
