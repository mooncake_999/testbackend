﻿using MyEnergi.Common;
using MyEnergi.Data.Entity.RedisModel;
using MyEnergi.Data.Interfaces;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class GraphRepository : IGraphRepository
    {
        private readonly IDeviceRepository _deviceRepository;
        private readonly IDatabase _db;

        public GraphRepository(IConnectionMultiplexer connectionMultiplexer, IDeviceRepository deviceRepository)
        {
            _deviceRepository = deviceRepository;
            try
            {
                _db = connectionMultiplexer.GetDatabase();
            }
            catch(Exception)
            {
                throw new NullReferenceException("Unable to connect to REDIS!!!");
            }
        }

        public async Task<Dictionary<string, IEnumerable<DeviceEnergyInfo>>> GetDevicesEnergyInfoByHub(Guid hubId, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity)
        {
            var allEnergyInfo = new Dictionary<string, IEnumerable<DeviceEnergyInfo>>();

            var devices = (await _deviceRepository.GetDevicesByHubId(hubId)).Where(x => x.DeviceType != Enumerators.DeviceType.Harvi.ToString().ToLower());

            foreach (var device in devices)
            {
                var energyInfo = new List<DeviceEnergyInfo>();

                if (!_db.KeyExists(device.SerialNumber)) continue;

                var data = _db.SortedSetRangeByScoreWithScores(device.SerialNumber, startTimestamp, endTimestamp);

                foreach (var deviceData in data)
                {
                    var deserialized = JsonConvert.DeserializeObject<DeviceEnergyInfo>(deviceData.Element);

                    deserialized.DeviceType = device.DeviceType;
                    deserialized.Timestamp = (long)deviceData.Score;

                    energyInfo.Add(deserialized);
                }
                allEnergyInfo.Add(device.SerialNumber, energyInfo);
            }

            return allEnergyInfo;
        }

        public async Task<Dictionary<string, IEnumerable<DeviceEnergyInfo>>> GetDevicesEnergyInfoByHubDST(Guid hubId, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity)
        {
            var allEnergyInfo = new Dictionary<string, IEnumerable<DeviceEnergyInfo>>();

            var devices = (await _deviceRepository.GetDevicesByHubId(hubId))
                .Where(x => x.DeviceType != Enumerators.DeviceType.Harvi.ToString().ToLower());

            foreach (var device in devices)
            {
                var energyInfo = new List<DeviceEnergyInfo>();

                if (!_db.KeyExists(device.SerialNumber)) continue;

                var data = _db.SortedSetRangeByScoreWithScores(device.SerialNumber, startTimestamp, endTimestamp);

                foreach (var deviceData in data)
                {
                    var deserialized = JsonConvert.DeserializeObject<DeviceEnergyInfo>(deviceData.Element);

                    deserialized.DeviceType = device.DeviceType;

                    // forcing data for DST

                    var dstDate = new DateTime(2020, 10, 27);
                    var dstTimestamp = ConvertToTimestamp(dstDate);

                    var currentDate = DateTime.UtcNow.Date;
                    var currentTimestamp = ConvertToTimestamp(currentDate);
                    // returns autumn 2020 week in which on dst day 01:00 AM hr is double(25.10.2020)
                    var shiftTimestamp = (long)deviceData.Score - (currentTimestamp - dstTimestamp);// autumn
                    deserialized.Timestamp = shiftTimestamp;

                    energyInfo.Add(deserialized);
                }
                allEnergyInfo.Add(device.SerialNumber, energyInfo);
            }

            return allEnergyInfo;
        }


        public async Task<Dictionary<string, IEnumerable<DeviceEnergyInfo>>> GetDevicesEnergyInfoByHubDSTSpring(Guid hubId, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity)
        {
            var allEnergyInfo = new Dictionary<string, IEnumerable<DeviceEnergyInfo>>();

            var devices = (await _deviceRepository.GetDevicesByHubId(hubId))
                .Where(x => x.DeviceType != Enumerators.DeviceType.Harvi.ToString().ToLower());

            foreach (var device in devices)
            {
                var energyInfo = new List<DeviceEnergyInfo>();

                if (!_db.KeyExists(device.SerialNumber)) continue;

                var data = _db.SortedSetRangeByScoreWithScores(device.SerialNumber, startTimestamp, endTimestamp);

                foreach (var deviceData in data)
                {
                    var deserialized = JsonConvert.DeserializeObject<DeviceEnergyInfo>(deviceData.Element);

                    deserialized.DeviceType = device.DeviceType;

                    // forcing data for DST
                    var dstDate = new DateTime(2020, 03, 31);
                    var dstTimestamp = ConvertToTimestamp(dstDate);

                    var currentDate = DateTime.UtcNow.Date;
                    var currentTimestamp = ConvertToTimestamp(currentDate);
                    // returns spring 2020 week in which on dst day 01:00 AM doesn't exist(29.03.2020)
                    var shiftTimestamp = (long)deviceData.Score - (currentTimestamp - dstTimestamp);// spring
                    deserialized.Timestamp = shiftTimestamp;

                    energyInfo.Add(deserialized);
                }
                allEnergyInfo.Add(device.SerialNumber, energyInfo);
            }

            return allEnergyInfo;
        }

        public string GetDeviceType(string deviceSNo)
        {
            return _db.HashGet($"deviceDetails:{deviceSNo}", "DeviceType");
        }

        public static long ConvertToTimestamp(DateTime value)
        {
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalMilliseconds;
        }
    }
}
