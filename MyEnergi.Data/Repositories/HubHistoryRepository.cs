﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class HubHistoryRepository : Repository<HubHistory>, IHubHistoryRepository
    {
        public HubHistoryRepository(MyEnergiDbContext context) :
            base(context)
        {
        }

        public async Task<HubHistory> GetHubHistory(string serialNo)
        {
            return await GetSingleOrDefaultBy(h => h.SerialNumber == serialNo,
                include: source => source
                    .Include(h => h.OwnerUser));
        }
    }
}
