﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class HubRepository : Repository<Hub>, IHubRepository
    {
        public HubRepository(MyEnergiDbContext context) : 
            base(context)
        {
        }

        async Task<Hub> IHubRepository.GetHubById(Guid hubId)
        {
            return await Context.Hubs
                    .Include(h =>h.Installation)
                    .Include(h =>h.Address)
                    .SingleOrDefaultAsync(h => h.Guid == hubId);
        }

        async Task<Hub> IHubRepository.GetHubBySerialNo(string serialNo)
        {
            return await Context.Hubs
                .Include(h => h.Address)
                .Include(h => h.Address.User)
                .Include(h => h.Address.User.UserDetails)
                .SingleOrDefaultAsync(h => h.SerialNo == serialNo);
        }

        async Task<Hub> IHubRepository.GetHubBySerialNoIsDeleted(string serialNo)
        {
            return await Context.Hubs
                .IgnoreQueryFilters()
                .Include(h => h.Address)
                .Include(h => h.Address.User)
                .Include(h => h.Address.User.UserDetails)
                .SingleOrDefaultAsync(h => h.SerialNo == serialNo);
        }

        async Task<IEnumerable<Hub>> IHubRepository.GetHubByUserId(string userId)
        {
            return await Context.Hubs
                .Include(d => d.Address)
                .Include(d =>d.Installation)
                .Where(d => d.Address.User.CognitoId == userId)
                .ToListAsync();
        }

        async Task IHubRepository.RemoveHub(Hub hub)
        {
            Context.Hubs.Remove(hub);
            await Task.FromResult(0);
        }

        async Task<bool> IHubRepository.CheckIfHubExistsOnAddress(Guid addressId)
        {
            return await Context.Hubs.AnyAsync(h => h.Address.Guid == addressId);
        }

        async Task<Hub> IHubRepository.GetHubByIdAndUser(Guid hubId, string userCognitoId)
        {
            return await Context.Hubs
                .Include(h => h.Installation)
                .Include(h => h.Address)
                .SingleOrDefaultAsync(h => h.Guid == hubId && h.Address.User.CognitoId == userCognitoId);
        }

        public async Task<IEnumerable<Hub>> GetHubsByIdsAndUser(List<Guid> hubIds, string userCognitoId)
        {
            return await GetBy(h => hubIds.Contains(h.Guid) && 
                                    h.Address.User.CognitoId == userCognitoId,
                include: source => source
                    .Include(h => h.Installation)
                    .Include(h => h.Address));
        }
        
        public async Task<Hub> GetGuestHubByHubIdAndCognitoId(Guid hubId, string userCognitoId)
        {
            var guestHub = await Context.UserFeaturePermissions
                .Where(ufp => ufp.Device.Hub.Guid.Equals(hubId) &&
                              ufp.Invitation.GuestUser != null &&
                              ufp.Invitation.GuestUser.CognitoId.Equals(userCognitoId))
                .Include(ufp => ufp.Device)
                .ThenInclude(d => d.Hub)
                .ThenInclude(h => h.Address)
                .Select(ufp => ufp.Device.Hub)
                .FirstOrDefaultAsync();

            return guestHub;
        }

        async Task<Hub> IHubRepository.GetHubBySerialNoAndUserId(string serialNo, string userCognitoId)
        {
            return await Context.Hubs
                .Include(h => h.Address)
                .SingleOrDefaultAsync(h => h.SerialNo == serialNo && h.Address.User.CognitoId==userCognitoId);
        }

        async Task<IEnumerable<Hub>> IHubRepository.GetAllHubs()
        {
            return await Context.Hubs.ToListAsync();
        }

        public async Task<Hub> GetHubByInvitationIdAndSerialNo(int invitationId, string serialNo)
        {
            var userFeaturePermission = await Context.UserFeaturePermissions
                .Include(x => x.Device)
                .Include(x => x.Device.Hub)
                .FirstOrDefaultAsync(x => x.Invitation.Id == invitationId && x.Device.Hub.SerialNo == serialNo);

            return userFeaturePermission?.Device?.Hub;
        }

        public async Task<Hub> GetHubByInvitationIdAndOwner(int invitationId, string serialNo, string cognitoId)
        {
            var userFeaturePermission = await Context.UserFeaturePermissions
                .Include(x => x.Device)
                .Include(x => x.Device.Hub)
                .FirstOrDefaultAsync(x => x.Invitation.Id == invitationId && x.Device.Hub.SerialNo == serialNo && x.OwnerUser.CognitoId == cognitoId);

            return userFeaturePermission?.Device?.Hub;
        }

        public async Task<IEnumerable<Hub>> GetHubsOfGuestUser(string cognitoId)
        {
            var hubs = await Context.Invitations
                .Include(i => i.UserFeaturePermissions)
                .Where(i => i.GuestUser.CognitoId == cognitoId)
                .SelectMany(x => x.UserFeaturePermissions.Select(u => u.Device.Hub))
                .Distinct()
                .ToListAsync();

            return hubs;
        }
    }
}
