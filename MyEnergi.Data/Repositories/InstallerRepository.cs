﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class InstallerRepository : Repository<Installer>, IInstallerRepository
    {
        public InstallerRepository(MyEnergiDbContext context) : 
            base(context)
        {
        }

        async Task<Installer> IInstallerRepository.GetInstallerByCode(string code)
        {
            return await GetSingleOrDefaultBy(i => i.InstallerCode == code);
        }

        async Task<IEnumerable<Installer>> IInstallerRepository.GetInstallersByCountry(string country)
        {
            return (await GetBy(i => !string.IsNullOrEmpty(country) && i.Country.ToUpper() == country.ToUpper()))
                .OrderBy(i =>i.InstallerName);
        }

        async Task<IEnumerable<Installer>> IInstallerRepository.GetAllInstallers()
        {
            return await Context.Installers.ToListAsync();
        }
    }
}
