﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class InvitationRepository : Repository<Invitation>, IInvitationRepository
    {
        public InvitationRepository(MyEnergiDbContext context) :
            base(context)
        {
        }

        public async Task<IEnumerable<UserFeaturePermission>> GetOwnerUfp(string cognitoId)
        {
            return await Context.UserFeaturePermissions
                .Include(x => x.Invitation.GuestUser)
                .Include(x => x.Invitation.InvitationStatus)
                .Include(x => x.Invitation.GuestUser.UserDetails)
                .Include(x => x.Invitation.InvitationDetails)
                .Include(x => x.Device)
                .Include(x => x.Device.Hub)
                .Where(x => x.OwnerUser.CognitoId == cognitoId)
                .ToListAsync();
        }

        public async Task<IEnumerable<Invitation>> GetOwnerUserSendInvitations(string cognitoId)
        {
            return (await GetOwnerUfp(cognitoId))
                .Select(x => x.Invitation)
                .Distinct();
        }

        public async Task<Invitation> GetOwnerInvitationForHub(string cognitoId, string serialNo, string guestEmail)
        {
            return (await GetOwnerUfp(cognitoId))
                .FirstOrDefault(ufp => ufp.Device.Hub.SerialNo == serialNo &&
                                       string.Equals(ufp.Invitation.InvitationDetails.Email, guestEmail,
                                           StringComparison.InvariantCultureIgnoreCase))
                ?.Invitation;
        }

        public async Task<Invitation> GetOwnerInvitationForHub(string cognitoId, string guestEmail)
        {
            return (await GetOwnerUfp(cognitoId))
                .FirstOrDefault(ufp => string.Equals(ufp.Invitation.InvitationDetails.Email, guestEmail,
                    StringComparison.InvariantCultureIgnoreCase))
                ?.Invitation;
        }

        public async Task<IEnumerable<Invitation>> GetGuestUserInvitations(string cognitoId)
        {
            return await GetBy(x => x.GuestUser != null && x.GuestUser.CognitoId == cognitoId, include: source => source
                .Include(x => x.UserFeaturePermissions)
                .ThenInclude(ufp => ufp.Device)
                .ThenInclude(d => d.Hub)
                .Include(x => x.UserFeaturePermissions)
                .ThenInclude(ufp => ufp.OwnerUser)
                .ThenInclude(u => u.UserDetails)
                .Include(x => x.UserFeaturePermissions)
                .ThenInclude(ufp => ufp.AppFeature)
                .Include(x => x.InvitationStatus));
        }

        public async Task<Invitation> GetGuestInvitationForHub(string cognitoId, string serialNo)
        {
            return (await GetGuestUserInvitations(cognitoId))
                .FirstOrDefault(i => i.UserFeaturePermissions
                    .FirstOrDefault(ufp => ufp.Device.Hub.SerialNo == serialNo)?
                    .Invitation == i);
        }
    }
}

