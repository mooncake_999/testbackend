﻿using System.Threading.Tasks;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;

namespace MyEnergi.Data.Repositories
{
    public class InvitationStatusRepository : Repository<InvitationStatus>, IInvitationStatusRepository
    {
        public InvitationStatusRepository(MyEnergiDbContext context) : 
            base(context)
        {
        }

        public async Task<InvitationStatus> GetByStatus(string status)
        {
            return await GetFirstOrDefaultBy(x => x.Status.ToUpper().Equals(status.ToUpper()));
        }
    }
}
