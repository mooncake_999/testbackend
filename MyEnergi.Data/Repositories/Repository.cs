﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using MyEnergi.Data.Context;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity>
        where TEntity : Entity.Entity
    {
        protected readonly MyEnergiDbContext Context;
        protected readonly DbSet<TEntity> Entities;

        protected Repository(MyEnergiDbContext context)
        {
            Context = context;
            Entities = context.Set<TEntity>();
        }

        public void CommitChanges()
        {
            Context.SaveChanges();
        }

        public async Task<IEnumerable<TEntity>> GetAll(
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            if (include == null)
            {
                return await Entities.ToListAsync();
            }

            IQueryable<TEntity> query = Entities;
            query = include(query);
            return await query.ToListAsync();
        }

        public async Task<TEntity> GetSingleOrDefaultBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            var result = await GetBy(predicate, include);
            return result.SingleOrDefault();
        }

        public async Task<TEntity> GetFirstOrDefaultBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            var result = await GetBy(predicate, include);
            return result.FirstOrDefault();
        }

        public async Task<IEnumerable<TEntity>> GetBy(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            if (include == null)
            {
                return await Entities.Where(predicate).ToListAsync();
            }

            IQueryable<TEntity> query = Entities;
            query = include(query);
            return await query.Where(predicate).ToListAsync();
        }

        public async Task Update(TEntity entity)
        {
            Entities.Update(entity);
            await Task.FromResult(0);
        }

        public async Task UpdateMultiple(IEnumerable<TEntity> entities)
        {
            Entities.UpdateRange(entities);
            await Task.FromResult(0);
        }

        public async Task Delete(TEntity entity)
        {
            Entities.Remove(entity);
            await Task.FromResult(0);
        }

        public async Task DeleteMultiple(IEnumerable<TEntity> entities)
        {
            Entities.RemoveRange(entities);
            await Task.FromResult(0);
        }

        public async Task AddOrUpdate(TEntity entity)
        {
            if (entity.Id == default)
            {
                Entities.Add(entity);
            }
            else
            {
                Entities.Update(entity);
            }

            await Task.FromResult(0);
        }

        public async Task AddOrUpdate(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                if (entity.Id == default)
                {
                    Entities.Add(entity);
                }
                else
                {
                    Entities.Update(entity);
                }
            }

            await Task.FromResult(0);
        }
    }
}
