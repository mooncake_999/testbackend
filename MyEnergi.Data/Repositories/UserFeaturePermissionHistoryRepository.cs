﻿using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;

namespace MyEnergi.Data.Repositories
{
    public class UserFeaturePermissionHistoryRepository : Repository<UserFeaturePermissionHistory>, IUserFeaturePermissionHistoryRepository
    {
        public UserFeaturePermissionHistoryRepository(MyEnergiDbContext context) : 
            base(context)
        {
        }
    }
}
