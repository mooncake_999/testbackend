﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Common;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class UserFeaturePermissionRepository : Repository<UserFeaturePermission>, IUserFeaturePermissionRepository
    {
        public UserFeaturePermissionRepository(MyEnergiDbContext context) :
            base(context)
        {
        }

        public async Task<IEnumerable<UserFeaturePermission>> GetByInvitationGuid(Guid invitationGuid)
        {
            return await GetBy(ufp => ufp.Invitation.Guid.Equals(invitationGuid),
                include: source => source
                    .Include(x => x.AppFeature)
                    .Include(x => x.OwnerUser)
                    .Include(x => x.Device));
        }

        public async Task<IEnumerable<UserFeaturePermission>> GetUserFeaturePermissionsByCognitoId(string userCognitoId)
        {
            return await GetBy(ufp =>
                    ufp.Invitation.GuestUser != null &&
                    ufp.Invitation.GuestUser.CognitoId.Equals(userCognitoId) &&
                    ufp.Invitation.InvitationStatus.Status.ToUpper()
                        .Equals(Enumerators.InvitationStatus.Accepted.ToString().ToUpper()),
                include: source => source
                    .Include(ufp => ufp.Device)
                    .ThenInclude(d => d.Address)
                    .Include(ufp => ufp.Device)
                    .ThenInclude(d => d.Hub)
                    .ThenInclude(h => h.Address));
        }

        public async Task<IEnumerable<UserFeaturePermission>> GetUserFeaturePermissionsByCognitoIdIsDeleted(string userCognitoId)
        {
            return await GetBy(ufp =>
                    ufp.Invitation.GuestUser != null &&
                    ufp.Invitation.GuestUser.CognitoId.Equals(userCognitoId) &&
                    ufp.Invitation.InvitationStatus.Status.ToUpper()
                        .Equals(Enumerators.InvitationStatus.Accepted.ToString().ToUpper()),
                include: source => source
                    .IgnoreQueryFilters()
                    .Include(ufp => ufp.Device)
                    .ThenInclude(d => d.Hub)
                    .ThenInclude(h => h.Address));
        }

        public async Task<IEnumerable<UserFeaturePermission>> GetUserFeaturePermissionsByDeviceIds(List<Guid> deviceGuids)
        {
            return await GetBy(ufp => deviceGuids.Contains(ufp.Device.Guid),
                include: source => source
                    .Include(ufp => ufp.AppFeature)
                    .Include(ufp => ufp.Invitation)
                    .ThenInclude(inv => inv.InvitationStatus)
                    .Include(ufp => ufp.Invitation.InvitationDetails));
        }
    }
}
