﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(MyEnergiDbContext context) :
            base(context)
        {
        }

        async Task<User> IUserRepository.GetUserRegistrationInfoByEmail(string email)
        {
            return await GetSingleOrDefaultBy(u => u.Email == email);
        }

        async Task<IEnumerable<UserVehicle>> IUserRepository.GetUserVehicles(Guid id)
        {
            return await Context.UserVehicles
                .Where(u => u.User.Guid == id)
                .ToListAsync();
        }

        async Task<User> IUserRepository.GetUserByCognitoId(string userCognitoId)
        {
            return await GetSingleOrDefaultBy(u => u.CognitoId == userCognitoId, include: source => 
                source
                    .Include(u => u.UserVehicles)
                    .Include(u => u.Addresses)
                    .Include(u => u.UserPreference)
                    .Include(u => u.UserDetails));
        }

        async Task<bool> IUserRepository.CheckIfEmailAlreadyExists(string email)
        {
            return await Context.Users.AnyAsync(e => e.Email == email);
        }

        async Task<User> IUserRepository.GetUserByEmail(string email)
        {
            return await GetSingleOrDefaultBy(u => u.Email.Trim().ToUpper() == email.Trim().ToUpper(), 
                include: source => source
                    .Include(u => u.Addresses));
        }

        async Task<bool> IUserRepository.CheckIfUserExists(string userCognitoId)
        {
            return await Context.Users.AnyAsync(u => u.CognitoId == userCognitoId);
        }
    }
}
