﻿using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class UserVehicleRepository : Repository<UserVehicle>, IUserVehicleRepository
    {
        public UserVehicleRepository(MyEnergiDbContext context) :
            base(context)
        {
        }

        public async Task<IEnumerable<UserVehicle>> GetUserVehicles(string userCognitoId)
        {
            return await GetBy(v => v.User.CognitoId == userCognitoId);
        }
    }
}
