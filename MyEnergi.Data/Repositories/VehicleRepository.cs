﻿using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using MyEnergi.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Data.Repositories
{
    public class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(MyEnergiDbContext context) : 
            base(context)
        {
        }

        async Task<Vehicle> IVehicleRepository.GetVehicleById(Guid id)
        {
            return await GetFirstOrDefaultBy(v => v.Guid == id);
        }

        async Task<IEnumerable<string>> IVehicleRepository.GetAllManufactures()
        {
            var year = DateTime.Now.Year;
            return (await GetBy(v => v.AvailabilityDateFrom <= year))
                .Select(v => v.Manufacturer)
                .Distinct();
        }

        async Task<IEnumerable<KeyValuePair<Guid, string>>> IVehicleRepository.GetVehicleByManufacturer(string manufacturer)
        {
            var year = DateTime.Now.Year;
            return (await GetBy(v => v.Manufacturer == manufacturer && v.AvailabilityDateFrom <= year))
                .OrderBy(v => v.Model)
                .Select(v => new KeyValuePair<Guid, string>(v.Guid, v.Model));
        }

        async Task<IEnumerable<Vehicle>> IVehicleRepository.GetVehicleDetails(Guid vehicleId)
        {
            return await GetBy(v => v.Guid == vehicleId);
        }

        async Task<IEnumerable<Vehicle>> IVehicleRepository.GetAllVehicles()
        {
            return await Context.Vehicles.ToListAsync();
        }
    }
}
