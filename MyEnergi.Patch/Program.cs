﻿using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Microsoft.EntityFrameworkCore;
using MyEnergi.Data.Context;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using MyEnergi.Common;

namespace MyEnergi.Patch
{
    class Program
    {
        static void Main(string[] args)
        {
            //MIgrationScript();
            //PatchToUpdateCarId();
            //UpdatePriceAndSetupWithProviderId();
            UpdatePasswordEncryption();

        }

        private static void PatchToUpdateCarId()
        {
            //using (var context = new MyEnergiDbContext(""))
            //{
            //    List<UserVehicle> vehicles = context.UserVehicles.ToList();
            //    List<DeviceChargesSetup> deviceChargesSetups = context.DeviceChargesSetups.Where(d => d.Device.DeviceType.ToLower() == "zappi").ToList();
            //    foreach (DeviceChargesSetup d in deviceChargesSetups)
            //    {
            //        UserVehicle v = vehicles.SingleOrDefault(v => v.Id.ToString() == d.ChargingOutputName);
            //        if (v != null)
            //            d.ChargingOutputName = v.Guid.ToString();
            //    }
            //    context.SaveChanges();
            //}
        }

        private static void MIgrationScript()
        {
            //try
            //{
            //    List<UserDetails> liveUsers;
            //    List<Address> liveAddresses;
            //    List<Device> liveDevices;
            //    List<UserVehicle> liveVehicles;
            //    List<EnergySetup> liveEnergySetups;

            //    using (var context = new MyEnergiDbContext(""))
            //    {
            //        liveUsers = context.UsersDetails.ToList();
            //        liveAddresses = context.Addresses.ToList();
            //        liveDevices = context.Devices.Include(d => d.Hub).Include(d =>d.ChargeSchedules).Where(d => d.Hub != null).ToList();
            //        liveVehicles = context.UserVehicles.ToList();
            //        liveEnergySetups = context.EnergySetup.ToList();

            //    }
            //    List<Hub> liveHubs = liveDevices.Select(d => d.Hub).Distinct().ToList();
            //    var users = 0;
            //    var random = new Random();
            //    Stopwatch stopwatch = new Stopwatch();
            //    stopwatch.Start();
            //    Console.WriteLine("Start Watch");
            //    using (var context = new MyEnergiDbContext(""))
            //    {
            //        do
            //        {
            //            try
            //            {
            //                User newUser = CreateUser(random, liveUsers, liveAddresses, liveVehicles);
            //                Address userAddress = newUser.Addresses.First();
            //                context.Users.Add(newUser);
            //                EnergySetup userEnergySetup = CreateUserEnergySetup(random, userAddress, liveEnergySetups);
            //                context.EnergySetup.Add(userEnergySetup);
            //                Hub userHub = null;// CreateUserHub(random, userAddress, liveHubs, context);
            //                if (userHub != null)
            //                    context.Hubs.Add(userHub);
            //                List<Device> userDevices = CreateUserDevices(random, userAddress, userHub, null, context);
            //                if (userDevices.Any())
            //                    context.Devices.AddRange(userDevices);
            //                users++;
            //                Console.WriteLine($"User {users} with {newUser.UserDetails.FirstName} {newUser.UserDetails.LastName} was created {stopwatch.Elapsed}");
            //                context.SaveChanges();
            //            }
            //            catch (Exception ex)
            //            {
            //                continue;
            //            }
            //        }
            //        while (users < 10000);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.InnerException);
            //    Console.ReadLine();
            //}
        }

        private static Hub CreateUserHub(Random random, Address userAddress, List<Hub> liveHubs, MyEnergiDbContext context)
        {
            Hub resultHub = null;
            Hub hub = liveHubs[random.Next(liveHubs.Count)];
            if (!context.Hubs.Any(h => h.SerialNo == hub.SerialNo))
            {
                resultHub = new Hub();
                resultHub.Address = userAddress;
                resultHub.AppPassword = hub.AppPassword;
                resultHub.Firmware = hub.Firmware;
                resultHub.HostServer = hub.HostServer;
                resultHub.NickName = hub.NickName;
                resultHub.RegistrationCode = hub.RegistrationCode;
                resultHub.RegistrationEndDate = hub.RegistrationEndDate;
                resultHub.RegistrationStartDate = hub.RegistrationStartDate;
                resultHub.TimeZoneRegion = hub.TimeZoneRegion;
                resultHub.Status = hub.Status;
                resultHub.SerialNo = hub.SerialNo;
            }
            return resultHub;
        }

        private static List<Device> CreateUserDevices(Random random, Address userAddress, Hub userHub, List<Device> liveDevices, MyEnergiDbContext context)
        {
            List<Device> resultDevices = new List<Device>();
            if (userHub != null)
            {
                List<Device> devices = liveDevices.Where(d => d.Hub.SerialNo == userHub.SerialNo).ToList();
               
                devices.ForEach(d =>
                {
                    List<DeviceChargesSetup> liveSetups = d.ChargeSchedules.ToList();
                    List<DeviceChargesSetup> userSetups = new List<DeviceChargesSetup>();
                    if (liveSetups.Any())
                    {
                        liveSetups.ForEach(s =>
                        {
                            userSetups.Add(new DeviceChargesSetup()
                            {
                                ChargeAmountMinutes = s.ChargeAmountMinutes,
                                ChargingOutputName = s.ChargingOutputName,
                                DaysOfWeek = s.DaysOfWeek,
                                Device = d,
                                FromTime = s.FromTime,
                                IsActive = s.IsActive,
                                LastUpdated = s.LastUpdated,
                                PriceBelow = s.PriceBelow,
                                ScheduleNickName = s.ScheduleNickName,
                                ScheduleType = s.ScheduleType,
                                SingleChargeDay = s.SingleChargeDay,
                                ToTime = s.ToTime
                            });
                        });
                    }
                   resultDevices.Add(new Device()
                    {
                        Address = userAddress,
                        DeviceName = d.DeviceName,
                        DeviceType = d.DeviceType,
                        Firmware = d.Firmware,
                        HeaterOutput1 = d.HeaterOutput1,
                        HeaterOutput2 = d.HeaterOutput2,
                        RegistrationEndDate = d.RegistrationEndDate,
                        RegistrationStartDate = d.RegistrationStartDate,
                        SerialNumber = d.SerialNumber,
                        Status = d.Status,
                        TimeZoneRegion = d.TimeZoneRegion,
                        Hub = userHub,
                        ChargeSchedules=userSetups
                    });
                });
            }
            else
            {
                string serialNb;
                do
                {
                    serialNb = 1 + random.Next(1000000, 9999999).ToString();
                }
                while (context.Devices.Any(d => d.SerialNumber == serialNb));

                List<string> types = new List<string>() { "zappi", "eddi" };
                string deviceType = types[random.Next(types.Count)];
                resultDevices.Add(new Device()
                {
                    Address = userAddress,
                    DeviceName = deviceType,
                    DeviceType = deviceType,
                    RegistrationStartDate = DateTime.UtcNow,
                    SerialNumber = serialNb
                });
            }
            return resultDevices;
        }

        private static EnergySetup CreateUserEnergySetup(Random random, Address address, List<EnergySetup> liveEnergySetups)
        {
            EnergySetup energySetup = liveEnergySetups[random.Next(liveEnergySetups.Count)];
            EnergySetup newEnergySetup = new EnergySetup()
            {
                Address = address,
                AirSourceHeatPump = energySetup.AirSourceHeatPump,
                ElectricStorageHeating = energySetup.ElectricStorageHeating,
                EnergyProv = energySetup.EnergyProv,
                //EnergyTariff = energySetup.EnergyProvider,
                GridSupplyPoint = energySetup.GridSupplyPoint,
                GroundSourceHeatPump = energySetup.GroundSourceHeatPump,
                Hydroelectric = energySetup.Hydroelectric,
                //IsEconomy = energySetup.IsEconomy,
                MaxPowerOutput = energySetup.MaxPowerOutput,
               // OtherElectricHeating = energySetup.EnergyProvider,
                OtherGeneration = energySetup.OtherGeneration,
                SolarPanels = energySetup.SolarPanels,
                TariffExpirationDate = energySetup.TariffExpirationDate,
                WindTurbine = energySetup.WindTurbine
            };
            return newEnergySetup;
        }

        private static User CreateUser(Random random, List<UserDetails> liveUsers, List<Address> liveAddresses, List<UserVehicle> liveVehicles)
        {
            string firstName = liveUsers[random.Next(liveUsers.Count)].FirstName.Trim();
            string lastName = liveUsers[random.Next(liveUsers.Count)].LastName.Trim();
            string email = firstName + "." + lastName + "@yopmail.com";
            Console.WriteLine($"Email name:{email}");
            Address address = liveAddresses[random.Next(liveAddresses.Count)];
            UserVehicle vehicle = liveVehicles[random.Next(liveVehicles.Count)];
            var cognitoId = RegisterUserCognito(email, "Qwertyuiop0");
            User user = new User()
            {
                CognitoId = cognitoId,
                Email = email,
                UserDetails = new UserDetails() { FirstName = firstName, LastName = lastName, Title = "Mr" },
                Addresses = new List<Address>() { new Address(){
                    AddressLine1 = address.AddressLine1,
                    AddressLine2=address.AddressLine2,
                    City=address.City,
                    Country=address.Country,
                    CountryCode=address.CountryCode,
                    PostalCode=address.PostalCode,
                    Region=address.Region,
                    SiteName=address.SiteName
                } },
                UserVehicles = new List<UserVehicle>() { new UserVehicle(){
                    BatterySize = vehicle.BatterySize,
                    ChargeRate = vehicle.ChargeRate,
                    Condition= vehicle.Condition,
                    Manufacturer=vehicle.Manufacturer,
                    ManufacturerOfficialEVRange=vehicle.ManufacturerOfficialEVRange,
                    ManufacturingYear=vehicle.ManufacturingYear,
                    Model=vehicle.Model,
                    Ownership=vehicle.Ownership,
                    RealEVRange=vehicle.RealEVRange,
                    VehicleNickname=vehicle.VehicleNickname
                } }
            };
            return user;
        }

        public static string RegisterUserCognito(string email, string password)
        {
            AmazonCognitoIdentityProviderClient _providerClient =
               new AmazonCognitoIdentityProviderClient(Amazon.RegionEndpoint.EUWest2);
            string _clientId = "";
            string _poolId = "";
            string _clientSecret = "";
            var request = new SignUpRequest()
            {
                ClientId = _clientId,
                SecretHash = GetSecretHash(email, _clientId, _clientSecret),
                Username = email,
                Password = password
            };

            var x = _providerClient.SignUpAsync(request).Result;
            var y = _providerClient.AdminConfirmSignUpAsync(new AdminConfirmSignUpRequest()
            {
                Username = email,
                UserPoolId = _poolId
            }).Result;
            var z = _providerClient.AdminUpdateUserAttributesAsync(new AdminUpdateUserAttributesRequest()
            {
                Username = email,
                UserPoolId = _poolId,
                UserAttributes = new List<AttributeType>
                    {
                       new AttributeType {
                           Name="email_verified",
                           Value="true"
                    }, new AttributeType
                    {
                        Name="custom:registrationStep",
                        Value="fullyRegistered"
                    }
                }
            }).Result;

            var authReq = new InitiateAuthRequest
            {
                ClientId = _clientId,
                AuthFlow = AuthFlowType.USER_PASSWORD_AUTH,
            };
            authReq.AuthParameters.Add("USERNAME", email);
            authReq.AuthParameters.Add("PASSWORD", password);
            authReq.AuthParameters.Add("SECRET_HASH", GetSecretHash(email, _clientId, _clientSecret));

            var authResp = _providerClient.InitiateAuthAsync(authReq).Result;
            string token = authResp.AuthenticationResult.IdToken;
            var decodedToken = new JwtSecurityTokenHandler().ReadJwtToken(token);
            return decodedToken.Claims.First(f => f.Type == "sub").Value;
        }

        public static string GetSecretHash(string username, string appClientId, string appSecretKey)
        {
            var dataString = username + appClientId;

            var data = Encoding.UTF8.GetBytes(dataString);
            var key = Encoding.UTF8.GetBytes(appSecretKey);

            return Convert.ToBase64String(HmacSHA256(data, key));
        }

        public static byte[] HmacSHA256(byte[] data, byte[] key)
        {
            using (var shaAlgorithm = new System.Security.Cryptography.HMACSHA256(key))
            {
                var result = shaAlgorithm.ComputeHash(data);
                return result;
            }
        }
        public static List<Hub> UpdatePasswordEncryption()
        {
            var connectionString = "";

            var optionsBuilder = new DbContextOptionsBuilder<MyEnergiDbContext>();

            optionsBuilder.UseSqlServer(connectionString);
            var updatedHubs = new List<Hub>();

            using (var context = new MyEnergiDbContext(optionsBuilder.Options))
            {
                List<Hub> hubs = context.Hubs.ToList();
                foreach (Hub hub in hubs)
                {
                    var dePassword = KMSCipher.Decrypt(hub.AppPassword).Result;
                    var newEncryptedPassword = KMSCipher.Encrypt(dePassword);
                    hub.AppPassword = newEncryptedPassword.Result;
                }
                context.SaveChanges();
                List<Hub> updatedValues = context.Hubs.ToList();
                updatedHubs.AddRange(updatedValues);
            }

            return updatedHubs;
        }
    }
}
