﻿using Microsoft.Extensions.Configuration;
using MyEnergi.Processor.Models;
using MyEnergi.Processor.Repository;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Processor
{
    public class DailyProcessManager : IDailyProcessManager
    {
        private readonly IAppServerRepository _appServerRepository;
        private readonly IRedisRepository _redisRepository;
        private readonly ISQLRepository _sqlRepository;
        private readonly IConfiguration _configuration;
        private List<DeviceData> _allDevices;
        private List<string> _allDeviceSNos;

        public DailyProcessManager(IAppServerRepository appServerRepository, IRedisRepository redisRepository,
            ISQLRepository sqlRepository, IConfiguration configuration)
        {
            _appServerRepository = appServerRepository;
            _redisRepository = redisRepository;
            _sqlRepository = sqlRepository;
            _configuration = configuration;
        }

        public void StartNightlyProcess()
        {
            try
            {
                var dateNow = DateTime.UtcNow;
                _allDevices = _sqlRepository.GetDevicesNoHarvi().ToList();
                _allDeviceSNos = _allDevices.Select(x => x.DeviceSNo).ToList();

                TryReplaceMissingData(_allDevices, dateNow);
                Log.Information($"{DateTime.UtcNow} Replace missing data, done successfully.");

                AggregateDay(_allDeviceSNos, dateNow);
                Log.Information($"{DateTime.UtcNow} Minute data aggregated into hourly data, done successfully.");

                ExportHistoryToFileSystem(dateNow);
                Log.Information($"{DateTime.UtcNow} History day exported to file, done successfully.");

                RemoveOldHours(dateNow);
                Log.Information($"{DateTime.UtcNow} History hour data removed from DB successfully.");
            }
            catch (Exception e)
            {
                Log.Error($"{DateTime.UtcNow} Something went wrong on Nightly process... ", e);
            }
        }

        public void TryReplaceMissingData(List<DeviceData> devices, DateTime dateNow)
        {
            Parallel.ForEach(devices, new ParallelOptions { MaxDegreeOfParallelism = 3 },(device) =>
            {
                var allData = _redisRepository.GetEnergyUsage(device.DeviceSNo).ToList();

                var startDate = !allData.Any() ? dateNow.Date.AddDays(-6) : allData.Min(x => x.QueryDate);

                var missingData = ScanForMissingData(device.DeviceSNo, startDate, dateNow.Date.AddDays(1).AddMinutes(-1));

                var dayGroups = missingData.GroupBy(x => x.Date.Date);

                foreach (var dayGroup in dayGroups)
                {
                    try
                    {
                        var energyInfo = _appServerRepository.GetEnergyUsageData(device, dayGroup.Key);

                        var replaceData = energyInfo.Where(e => dayGroup.Select(d => d).Contains(e.QueryDate)).ToList();

                        if (replaceData.Any())
                            _redisRepository.AddEnergyInfo(device.DeviceSNo, replaceData);
                    }
                    catch (Exception e)
                    {
                        Log.Error($"An error occured while replacing data for device {device.DeviceSNo}, at datetime {dayGroup.Key}. ", e);
                    }
                }
            });
        }

        private IEnumerable<DateTime> ScanForMissingData(string deviceSNo, DateTime start, DateTime end)
        {
            var endDate = end;

            var missingData = new List<DateTime>();

            try
            {
                var allData = _redisRepository.GetEnergyUsage(deviceSNo).ToList();

                var refDate = start;

                do
                {
                    if (allData.All(x => x.QueryDate != refDate))
                        missingData.Add(refDate);

                    refDate = refDate.AddMinutes(1);
                } while (refDate <= endDate);

            }
            catch (Exception e)
            {
                Log.Error($"An error occured while scanning device {deviceSNo}. ", e);
            }

            return missingData;
        }

        public void AggregateDay(List<string> devices, DateTime dateNow)
        {
            devices.ForEach(deviceSno =>
            {
                var startDate = dateNow.Date;
                var endDate = startDate.AddDays(1);

                var energyInfo = new List<DeviceEnergyUsage>();
                try
                {
                    do
                    {
                        var energyData = GetEnergyUsageForInterval(deviceSno, startDate, startDate.AddHours(1));

                        if (energyData.Any())
                        {
                            var hourlyData = new DeviceEnergyUsage
                            {
                                QueryDate = startDate,
                                Consumed = energyData.Sum(x => x.Consumed),
                                Imported = energyData.Sum(x => x.Imported),
                                Exported = energyData.Sum(x => x.Exported),
                                Generated = energyData.Sum(x => x.Generated),
                                H1d = energyData.Sum(x => x.H1d),
                                H2d = energyData.Sum(x => x.H2d),
                                H3d = energyData.Sum(x => x.H3d),
                                H1b = energyData.Sum(x => x.H1b),
                                H2b = energyData.Sum(x => x.H2b),
                                H3b = energyData.Sum(x => x.H3b)
                            };

                            energyInfo.Add(hourlyData);
                        }

                        startDate = startDate.AddHours(1);

                    } while (startDate < endDate);

                    _redisRepository.AddEnergyInfo(deviceSno, energyInfo, true);
                }
                catch (Exception e)
                {
                    Log.Error($"{DateTime.UtcNow} An error occured while aggregating data for device {deviceSno}, for day {startDate}. ", e);
                }
            });
        }

        private void CreateFile(string deviceSno, DateTime historyDay, string data)
        {
            var path = _configuration.GetValue<string>("WorkerSettings:history.files.location");
            var folderName = deviceSno;
            var fileName = Helper.ConvertToTimestamp(historyDay) + ".json";

            var directoryPath = Path.Combine(path, folderName);
            var filePath = Path.Combine(directoryPath, fileName);

            Directory.CreateDirectory(directoryPath);

            using var file = File.CreateText(filePath);
            file.Write(data);
        }

        private void ExportHistoryToFileSystem(DateTime dateNow)
        {
            var historyDay = dateNow.Date.AddDays(-7);

            _allDeviceSNos.ForEach(dsno =>
            {
                try
                {
                    var data = GetHistoryDayEnergyUsage(dsno, historyDay);

                    if (!data.Any()) return;
                    var serializedData = JsonConvert.SerializeObject(data);

                    CreateFile(dsno, historyDay, serializedData);
                }
                catch (Exception e)
                {
                    Log.Error($"An error occured while creating history file for device {dsno}. ", e);
                }
            });
        }

        private List<DeviceEnergyUsage> GetEnergyUsageForInterval(string deviceSno, DateTime startDateTime,
            DateTime endDateTime)
        {
            var energyUsage = _redisRepository.GetEnergyUsage(deviceSno, Helper.ConvertToTimestamp(startDateTime),
                Helper.ConvertToTimestamp(endDateTime)).ToList();

            return energyUsage;
        }

        private List<DeviceEnergyUsage> GetHistoryDayEnergyUsage(string deviceSno, DateTime day)
        {
            var endDate = day.AddDays(1);

            var energyUsage = _redisRepository.GetEnergyUsage(deviceSno, Helper.ConvertToTimestamp(day),
                Helper.ConvertToTimestamp(endDate), true).ToList();

            return energyUsage;
        }

        private void RemoveOldHours(DateTime dateNow)
        {
            var date = dateNow.Date.AddDays(-7);

            foreach (var deviceSNo in _allDeviceSNos)
            {
                _redisRepository.RemoveEnergyInfo(deviceSNo, 0, Helper.ConvertToTimestamp(date.AddDays(1).AddHours(-1)),
                    true);
            }
        }
    }
}
