﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyEnergi.Processor
{
    public static class Helper
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        public static long ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalMilliseconds;
        }

        public static DateTime GetDateTimeMinutesOnly(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0);
        }
        public static DateTime GetDateTimeHoursOnly(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, 0, 0);
        }

        public static DateTime ConvertFromTimestampToDate(long timestamp)
        {
            var dtDateTime = Epoch;
            dtDateTime = dtDateTime.AddMilliseconds(timestamp);
            return dtDateTime;
        }
    }
}
