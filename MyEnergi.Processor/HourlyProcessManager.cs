﻿using Microsoft.Extensions.Configuration;
using MyEnergi.Processor.Repository;
using Serilog;
using System;

namespace MyEnergi.Processor
{
    public class HourlyProcessManager : IHourlyProcessManager
    {
        private readonly IRedisRepository _redisRepository;
        private readonly ISQLRepository _sqlRepository;
        private readonly IAppServerRepository _appServerRepository;
        private readonly int _passiveFrequency;

        public HourlyProcessManager(IRedisRepository redisRepository, ISQLRepository sqlRepository, IAppServerRepository appServerRepository, IConfiguration configuration)
        {
            _redisRepository = redisRepository;
            _sqlRepository = sqlRepository;
            _appServerRepository = appServerRepository;
            _passiveFrequency = configuration.GetValue<int>("WorkerSettings:passive.update.frequency");
        }

        public void RunHourlyProcess()
        {
            var refreshTime = DateTime.UtcNow.AddHours(-1);
            var devices = _sqlRepository.GetDevicesNoHarvi();

            foreach (var device in devices)
            {
                Log.Information($"{DateTime.UtcNow}: Hourly process started for {device.DeviceSNo}");

                try
                {
                    var runDate = Helper.GetDateTimeHoursOnly(refreshTime);
                    var energyInfo = _appServerRepository.GetEnergyUsageData(device, runDate, _passiveFrequency);

                    _redisRepository.AddEnergyInfo(device.DeviceSNo, energyInfo);

                    if (runDate.AddHours(1) == runDate.Date.AddDays(1))
                        RemoveOldMinutes(device.DeviceSNo, runDate);
                }
                catch (Exception e)
                {
                    Log.Error($"An error occured while storing hourly data for device {device.DeviceSNo}. ", e);
                }

                Log.Information($"{DateTime.UtcNow}: Hourly process ended for {device.DeviceSNo}");
            }
        }

        private void RemoveOldMinutes(string deviceSNo, DateTime startDate)
        {
            var date = startDate.Date.AddDays(-6);

            _redisRepository.RemoveEnergyInfo(deviceSNo, 0, Helper.ConvertToTimestamp(date.AddDays(1).AddMinutes(-1)));
        }
    }
}
