﻿using System;
using System.Collections.Generic;
using MyEnergi.Processor.Models;

namespace MyEnergi.Processor
{
    public interface IDailyProcessManager
    {
        void StartNightlyProcess();
        void AggregateDay(List<string> devices, DateTime dateNow);

        void TryReplaceMissingData(List<DeviceData> devices, DateTime dateNow);
    }
}