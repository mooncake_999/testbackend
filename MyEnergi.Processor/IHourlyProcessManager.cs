﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MyEnergi.Processor.Models;

namespace MyEnergi.Processor
{
    public interface IHourlyProcessManager
    {
        void RunHourlyProcess();
    }
}