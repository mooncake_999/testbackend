﻿namespace MyEnergi.Processor
{
    public interface IInitialLoadManager
    {
        void ServiceRestartWithReload();
        void ServiceRestart();
    }
}