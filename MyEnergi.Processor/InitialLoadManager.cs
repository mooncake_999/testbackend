﻿using Microsoft.Extensions.Configuration;
using MyEnergi.Processor.Repository;
using Serilog;
using System;
using System.Linq;

namespace MyEnergi.Processor
{
    public class InitialLoadManager : IInitialLoadManager
    {
        private readonly IRedisRepository _redisRepository;
        private readonly ISQLRepository _sqlRepository;
        private readonly IAppServerRepository _appServerRepository;
        private readonly IDailyProcessManager _dailyProcessManager;
        private readonly int _historyLength;

        public InitialLoadManager(IRedisRepository redisRepository, ISQLRepository sqlRepository, IAppServerRepository appServerRepository, IDailyProcessManager dailyProcessManager, IConfiguration configuration)
        {
            _redisRepository = redisRepository;
            _sqlRepository = sqlRepository;
            _appServerRepository = appServerRepository;
            _dailyProcessManager = dailyProcessManager;

            _historyLength = configuration.GetValue<int>("WorkerSettings:history.length");
        }

        public void ServiceRestartWithReload()
        {
            Log.Information("Devices loading from SQL db.");

            var devices = _sqlRepository.GetDevicesNoHarvi().ToList();

            var dateNow = Helper.GetDateTimeMinutesOnly(DateTime.UtcNow);

            foreach (var device in devices)
            {
                try
                {
                    for (var date = dateNow; date > dateNow.AddDays(-_historyLength); date = date.AddDays(-1))
                    {
                        var energyInfo = _appServerRepository.GetEnergyUsageData(device, date);

                        _redisRepository.AddEnergyInfo(device.DeviceSNo, energyInfo);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"An error occured while storing initial data for device {device.DeviceSNo}. ", e);
                }
            }

            for (var date = dateNow.AddDays(-1); date >= dateNow.AddDays(-_historyLength); date = date.AddDays(-1))
            {
                _dailyProcessManager.AggregateDay(devices.Select(x => x.DeviceSNo).ToList(), date.Date);
            }
        }

        public void ServiceRestart()
        {
            try
            {
                var devices = _sqlRepository.GetDevicesNoHarvi().ToList();
                _dailyProcessManager.TryReplaceMissingData(devices, DateTime.UtcNow);
            }
            catch (Exception e)
            {
                throw new Exception("An error occured while storing initial data. ", e);
            }
        }
    }
}
