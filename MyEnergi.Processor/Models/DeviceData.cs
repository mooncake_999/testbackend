﻿namespace MyEnergi.Processor.Models
{
    public class DeviceData
    {
        public string DeviceSNo { get; set; }
        public string Password { get; set; }
        public string DeviceType { get; set; }
        public string HubSNo { get; set; }
        public string HostServer { get; set; }
    }
}
