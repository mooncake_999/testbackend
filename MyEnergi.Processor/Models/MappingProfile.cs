﻿using AutoMapper;

namespace MyEnergi.Processor.Models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<MyEnergi.AppServer.Model.GraphData, Processor.Models.DeviceEnergyUsage>();
        }
    }
}
