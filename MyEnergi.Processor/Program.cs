using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyEnergi.Processor.Models;
using MyEnergi.Processor.Repository;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Compact;
using Serilog.Sinks.Email;
using StackExchange.Redis;
using System;
using System.Net;

namespace MyEnergi.Processor
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .MinimumLevel.Override("MyEnergi.Processor", LogEventLevel.Error)
                .WriteTo.File(new RenderedCompactJsonFormatter(),
                    configuration.GetSection("Serilog:File")["path"],
                    rollingInterval: RollingInterval.Day,
                    restrictedToMinimumLevel: LogEventLevel.Information,
                    rollOnFileSizeLimit: true)
                .WriteTo.Email(new EmailConnectionInfo
                {
                    FromEmail = configuration.GetSection("Serilog:Email")["from"],
                    ToEmail = configuration.GetSection("Serilog:Email")["to"],
                    EmailSubject = configuration.GetSection("Serilog:Email")["subject"],
                    MailServer = configuration.GetSection("Serilog:Email")["server"],
                    NetworkCredentials = new NetworkCredential
                    {
                        UserName = configuration.GetSection("Serilog:Email")["username"],
                        Password = configuration.GetSection("Serilog:Email")["password"]
                    },
                    //EnableSsl = true,
                    Port = int.Parse(configuration.GetSection("Serilog:Email")["port"])
                },
                "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}{NewLine}",
                batchPostingLimit: 10
                , restrictedToMinimumLevel: LogEventLevel.Error
                )
                .CreateLogger();

            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed.");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .UseSystemd()
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration configuration = hostContext.Configuration;

                    var redisConnectionInfo = configuration.GetSection("ConnectionStrings:Redis");
                    var redisConnectionString = redisConnectionInfo["Host"] + ":" + redisConnectionInfo["Port"] +
                                                ",allowAdmin=" + redisConnectionInfo["AllowAdmin"];
                    var connectionMultiplexer = ConnectionMultiplexer.Connect(redisConnectionString);

                    services.AddSingleton<IConnectionMultiplexer>(x => connectionMultiplexer);
                    services.AddSingleton<IServer>(x =>
                        connectionMultiplexer.GetServer(redisConnectionInfo["Host"] + ":" + redisConnectionInfo["Port"]));

                    var mappingConfig = new MapperConfiguration(mc =>
                    {
                        mc.AddProfile(new MappingProfile());
                    });

                    var mapper = mappingConfig.CreateMapper();
                    services.AddSingleton(mapper);

                    services.AddSingleton(configuration);
                    services.AddSingleton<ISQLRepository, SQLRepository>();
                    services.AddHostedService<Worker>();

                    services.AddSingleton<IRedisRepository, RedisRepository>();
                    services.AddSingleton<IHourlyProcessManager, HourlyProcessManager>();
                    services.AddSingleton<IAppServerRepository, AppServerRepository>();
                    services.AddSingleton<IDailyProcessManager, DailyProcessManager>();
                    services.AddSingleton<IInitialLoadManager, InitialLoadManager>();
                });
    }
}
