﻿using AutoMapper;
using MyEnergi.AppServer;
using MyEnergi.Processor.Models;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MyEnergi.Common;

namespace MyEnergi.Processor.Repository
{
    public class AppServerRepository : IAppServerRepository
    {
        private readonly IMapper _mapper;

        public AppServerRepository(IMapper mapper)
        {
            _mapper = mapper;
        }

        public List<DeviceEnergyUsage> GetEnergyUsageData(DeviceData device, DateTime date, int minutesInterval = 0)
        {
            try
            {
                var apiCaller = new DeviceEnergyInfoForDayMinutely(Log.Logger, device.HubSNo, KMSCipher.Decrypt(device.Password).Result, device.HostServer, device.DeviceType, device.DeviceSNo,
                    date, minutesInterval);
                var result = apiCaller.GetResult();

                if(result?.Content == null) return new List<DeviceEnergyUsage>();

                var castContent = ((IList)result.Content).Cast<AppServer.Model.GraphData>().ToList();

                var content = _mapper.Map<List<MyEnergi.AppServer.Model.GraphData>, List<DeviceEnergyUsage>>(castContent);
                return content;
            }
            catch (Exception e)
            {
                Log.Error($"An error occurred while getting data from the Server, for device {device.DeviceSNo}. {e}");
                return new List<DeviceEnergyUsage>();
            }
        }
    }
}
