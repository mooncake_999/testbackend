﻿using System;
using System.Collections.Generic;
using MyEnergi.Processor.Models;

namespace MyEnergi.Processor.Repository
{
    public interface IAppServerRepository
    {
        List<DeviceEnergyUsage> GetEnergyUsageData(DeviceData device, DateTime date, int minutesInterval = 0);
    }
}