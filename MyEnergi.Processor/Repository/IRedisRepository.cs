﻿using MyEnergi.Processor.Models;
using System.Collections.Generic;
using DeviceData = MyEnergi.Processor.Models.DeviceData;

namespace MyEnergi.Processor.Repository
{
    public interface IRedisRepository
    {
        void AddEnergyInfo(string deviceSno, List<DeviceEnergyUsage> data, bool isHistory = false);
        void RemoveEnergyInfo(string deviceSno, long startTimestamp, long endTimestamp, bool isHistory = false);
        //void AddDeviceDetails(DeviceData data);
        IEnumerable<DeviceEnergyUsage> GetEnergyUsage(string deviceSNo, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity, bool isHistory = false);
        bool KeyExists(string key, bool isHistory = false);
    }
}