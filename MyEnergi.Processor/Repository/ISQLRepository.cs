﻿using System.Collections.Generic;
using MyEnergi.Processor.Models;

namespace MyEnergi.Processor.Repository
{
    public interface ISQLRepository
    {
        List<DeviceData> GetDevices();
        IEnumerable<DeviceData> GetDevicesNoHarvi();
    }
}