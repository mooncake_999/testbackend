﻿using MyEnergi.Processor.Models;
using Newtonsoft.Json;
using Serilog;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyEnergi.Processor.Repository
{
    public class RedisRepository : IRedisRepository
    {
        private readonly IDatabase _db;
        private readonly IDatabase _historyDb;

        public RedisRepository(IConnectionMultiplexer connectionMultiplexer)
        {
            _db = connectionMultiplexer.GetDatabase();
            _historyDb = connectionMultiplexer.GetDatabase(1);
        }

        public void AddEnergyInfo(string deviceSno, List<DeviceEnergyUsage> data, bool isHistory = false)
        {
            var db = isHistory ? _historyDb : _db;

            try
            {
                var serializedData = data.Select(x => new SortedSetEntry(JsonConvert.SerializeObject(x),
                    Helper.ConvertToTimestamp(x.QueryDate))).ToArray();

                db.SortedSetAdd($"{deviceSno}", serializedData);
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
                throw;
            }
        }

        public void RemoveEnergyInfo(string deviceSno, long startTimestamp, long endTimestamp, bool isHistory = false)
        {
            var db = isHistory ? _historyDb : _db;
            try
            {
                db.SortedSetRemoveRangeByScore(deviceSno, startTimestamp, endTimestamp);
            }
            catch (Exception e)
            {
                Log.Error($"An error occured while removing old data for device {deviceSno}. ", e);
            }
        }

        public IEnumerable<DeviceEnergyUsage> GetEnergyUsage(string key, double startTimestamp = double.NegativeInfinity, double endTimestamp = double.PositiveInfinity, bool isHistory = false)
        {
            var db = isHistory ? _historyDb : _db;

            var energyInfo = new List<DeviceEnergyUsage>();
            try
            {
                var data = db.SortedSetRangeByScoreWithScores(key, startTimestamp, endTimestamp);
                energyInfo.AddRange(data.Select(deviceData =>
                    JsonConvert.DeserializeObject<DeviceEnergyUsage>(deviceData.Element)));
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }

            return energyInfo;
        }

        public bool KeyExists(string key, bool isHistory = false)
        {
            var db = isHistory ? _historyDb : _db;

            return db.KeyExists(key);
        }
    }
}
