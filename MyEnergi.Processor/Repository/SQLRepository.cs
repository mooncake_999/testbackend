﻿using Microsoft.Extensions.Configuration;
using MyEnergi.Common;
using MyEnergi.Processor.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace MyEnergi.Processor.Repository
{
    public class SQLRepository : ISQLRepository
    {
        private readonly IConfiguration _configuration;

        public SQLRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public List<DeviceData> GetDevices()
        {
            const string sql = "SELECT d.SerialNumber, d.DeviceType, h.SerialNo, h.AppPassword, h.HostServer " +
                               "FROM Devices d " +
                               "JOIN Hubs h " +
                               "ON d.HubId = h.Id";

            var connectionString = _configuration.GetConnectionString("MyEnergiDb");
            var connection = new SqlConnection(connectionString);
            var devices = new List<DeviceData>();

            try
            {
                connection.Open();

                using var command = new SqlCommand(sql, connection);
                var reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        devices.Add(new DeviceData
                        {
                            DeviceSNo = reader.IsDBNull(0) ? string.Empty : reader.GetString(0),
                            DeviceType = reader.IsDBNull(1) ? string.Empty : reader.GetString(1),
                            HubSNo = reader.IsDBNull(2) ? string.Empty : reader.GetString(2),
                            Password = reader.IsDBNull(3) ? string.Empty : reader.GetString(3),
                            HostServer = reader.IsDBNull(4) ? string.Empty : reader.GetString(4)
                        });
                    }
                }

                Log.Information($"{DateTime.UtcNow}: Devices read from SQL db.");

                return devices;
            }
            catch (SqlException sqlEx)
            {
                Log.Error($"SQL exception: {sqlEx}");
                return devices;
            }
            catch (Exception ex)
            {
                Log.Error($"{ex}");
                return devices;
            }
            finally
            {
                connection.Close();
            }
        }

        public IEnumerable<DeviceData> GetDevicesNoHarvi()
        {
            return GetDevices().Where(x => x.DeviceType != Enumerators.DeviceType.Harvi.ToString().ToLower());
        }
    }
}
