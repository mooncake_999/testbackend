using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace MyEnergi.Processor
{
    public class Worker : BackgroundService
    {
        private readonly IInitialLoadManager _initialLoadManager;
        private readonly IHourlyProcessManager _hourlyProcessManager;
        private readonly IDailyProcessManager _dailyProcessManager;
        private readonly int _dailyJobStart;
        private readonly bool _restartWithReload;

        private Timer _dailyTimer;
        private Timer _hourlyTimer;


        public Worker(IInitialLoadManager initialLoadManager, IHourlyProcessManager hourlyProcessManager, IDailyProcessManager dailyProcessManager, IConfiguration configuration)
        {
            _initialLoadManager = initialLoadManager;
            _hourlyProcessManager = hourlyProcessManager;
            _dailyProcessManager = dailyProcessManager;
            _dailyJobStart = configuration.GetValue<int>("WorkerSettings:daily.update.start");
            _restartWithReload = configuration.GetValue<bool>("WorkerSettings:restart.with.reload");
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                if (_restartWithReload)
                {
                    Log.Information($"{DateTime.UtcNow}: Service Restart with data reload, started...");
                    _initialLoadManager.ServiceRestartWithReload();
                }
                else
                {
                    Log.Information($"{DateTime.UtcNow}: Service Restart started...");
                    _initialLoadManager.ServiceRestart();
                }

                Log.Information($"{DateTime.UtcNow}: Service Restart ended.");


                _dailyTimer = new Timer { Enabled = true, Interval = 1000 * 60 * 60 };
                _dailyTimer.Elapsed += DailyProcess;

                _hourlyTimer = new Timer { Enabled = true, Interval = 1000 * 60 };
                _hourlyTimer.Elapsed += HourlyProcess;


                await base.StartAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                Log.Error("Service could not start correctly.");
                Log.Error(ex.Message, ex);
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Remove this after you uncomment the code below 
            await Task.FromResult(0);
            while (!stoppingToken.IsCancellationRequested)
            {
                //_logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                //await Task.Delay(1000, stoppingToken);

                //Console.WriteLine("Devices loading from AWS");
                //var workers = devices.Select(device => _energyUsageManager.SaveToCache(device, stoppingToken)).ToList();


                //var previousHour = DateTime.UtcNow.AddHours(-1);
                //foreach (var device in _hourlyProcessManager.GetAllDevices())
                //{
                //    Console.WriteLine($"{DateTime.UtcNow}: Hourly process started for {device.DeviceSNo}");
                //    await _hourlyProcessManager.RunRecurrentProcess(device, previousHour, stoppingToken);
                //    Console.WriteLine($"{DateTime.UtcNow}: Hourly process ended for {device.DeviceSNo}");
                //}

                //await Task.WhenAll(workers.ToArray());
                //var timeToWait = new TimeSpan(/*DateTime.UtcNow.Hour == _lastHourlyUpdate ? 2 : 1*/1, 0, 0);
                //await Task.Delay(timeToWait, stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                _dailyTimer.Close();
                _hourlyTimer.Close();

                await base.StopAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
            }
        }

        private void DailyProcess(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (DateTime.UtcNow.Hour != _dailyJobStart) return;

                Log.Information($"{DateTime.UtcNow} Daily process started...");
                _dailyProcessManager.StartNightlyProcess();
                Log.Information($"{DateTime.UtcNow} Daily process ended.");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        private void HourlyProcess(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (DateTime.UtcNow.Minute != 0) return;

                Log.Information($"{DateTime.UtcNow} Hourly process started...");
                _hourlyProcessManager.RunHourlyProcess();
                Log.Information($"{DateTime.UtcNow} Hourly process ended.");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}
