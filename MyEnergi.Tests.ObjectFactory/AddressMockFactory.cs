﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class AddressMockFactory
    {
        public static Address GetEmptyAddress()
        {
            return new Address();
        }

        public static Address WithId(this Address address, int id)
        {
            address.Id = id;
            return address;
        }

        public static Address WithGuid(this Address address, Guid guid)
        {
            address.Guid = guid;
            return address;
        }

        public static Address WithUser(this Address address, User user)
        {
            address.User = user;
            return address;
        }

        public static Address WithSiteName(this Address address, string siteName)
        {
            address.SiteName = siteName;
            return address;
        }

        public static Address WithAddressLines(this Address address, string addressLine1 = null,
            string addressLine2 = null)
        {
            address.AddressLine1 = addressLine1;
            address.AddressLine2 = addressLine2;
            return address;
        }

        public static Address WithCity(this Address address, string city)
        {
            address.City = city;
            return address;
        }

        public static Address WithRegion(this Address address, string region)
        {
            address.Region = region;
            return address;
        }

        public static Address WithPostalCode(this Address address, string postalCode)
        {
            address.PostalCode = postalCode;
            return address;
        }

        public static Address WithCountry(this Address address, string country)
        {
            address.Country = country;
            return address;
        }

        public static Address WithCountryCode(this Address address, string countryCode)
        {
            address.CountryCode = countryCode;
            return address;
        }

        public static Address WithLastUpdate(this Address address, DateTime? lastUpdate = null)
        {
            address.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return address;
        }

        public static IList<Address> GetListOfAddressesForUser(User user)
        {
            var address1 = GetEmptyAddress()
                .WithId(1)
                .WithGuid(Guid.Parse("AED28EBA-1828-EB11-8287-060BA7111D4A"))
                .WithUser(user)
                .WithAddressLines("Broadhead House", "Halifax Road")
                .WithRegion("Pembrokeshire")
                .WithPostalCode("SA73 1PE")
                .WithCity("Londonderry")
                .WithCountry("United Kingdom")
                .WithSiteName("dwhite4245")
                .WithCountryCode("GB")
                .WithLastUpdate();

            var address2 = GetEmptyAddress()
                .WithId(2)
                .WithGuid(Guid.Parse("B3D28EBA-1828-EB11-8287-060BA7111D4A"))
                .WithUser(user)
                .WithAddressLines("Mount Pleasant Stables", "Rhiwceiliog")
                .WithRegion("Co Wicklow")
                .WithPostalCode("A67 HX38")
                .WithCity("Oxford")
                .WithCountry("United Kingdom")
                .WithSiteName("jspuds")
                .WithCountryCode("GB")
                .WithLastUpdate();
            
            var address3 = GetEmptyAddress()
                .WithId(3)
                .WithGuid(Guid.Parse("C1D28EBA-1828-EB11-8287-060BA7111D4A"))
                .WithUser(user)
                .WithAddressLines("The Witterage", "Houlston, Myddle")
                .WithRegion("Derbyshire")
                .WithPostalCode("S42 6DY")
                .WithCity("Leicester")
                .WithCountry("United Kingdom")
                .WithSiteName("Our home")
                .WithCountryCode("GB")
                .WithLastUpdate();

            return new List<Address>
            {
                address1,
                address2,
                address3
            };
        }
    }
}
