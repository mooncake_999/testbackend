﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class AppFeatureMockFactory
    {
        public static AppFeature GetEmptyAppFeature()
        {
            return new AppFeature();
        }

        public static AppFeature WithId(this AppFeature appFeature, int id)
        {
            appFeature.Id = id;
            return appFeature;
        }

        public static AppFeature WithGuid(this AppFeature appFeature, Guid guid)
        {
            appFeature.Guid = guid;
            return appFeature;
        }

        public static AppFeature WithFeature(this AppFeature appFeature, string feature)
        {
            appFeature.Feature = feature;
            return appFeature;
        }

        public static AppFeature WithLastUpdate(this AppFeature appFeature, DateTime? lastUpdate = null)
        {
            appFeature.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return appFeature;
        }

        public static IList<AppFeature> GetAppFeatures()
        {
            var appFeature1 = GetEmptyAppFeature()
                .WithId(1)
                .WithGuid(Guid.NewGuid())
                .WithFeature("flexible tariffs")
                .WithLastUpdate();

            var appFeature2 = GetEmptyAppFeature()
                .WithId(2)
                .WithGuid(Guid.NewGuid())
                .WithFeature("graphs")
                .WithLastUpdate();

            var appFeature3 = GetEmptyAppFeature()
                .WithId(3)
                .WithGuid(Guid.NewGuid())
                .WithFeature("dashboard")
                .WithLastUpdate();

            return new List<AppFeature>
            {
                appFeature1, appFeature2, appFeature3
            };
        }
    }
}
