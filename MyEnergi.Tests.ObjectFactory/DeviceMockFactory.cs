﻿using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using MyEnergi.AppServer.Model;
using static MyEnergi.Common.Enumerators;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class DeviceMockFactory
    {
        public static DeviceHistory GetEmptyDeviceHistory()
        {
            return new DeviceHistory();
        }

        public static Device GetEmptyDevice()
        {
            return new Device();
        }

        public static Device WithId(this Device device, int id)
        {
            device.Id = id;
            return device;
        }

        public static Device WithGuid(this Device device, Guid guid)
        {
            device.Guid = guid;
            return device;
        }

        public static Device WithDeviceName(this Device device, string deviceName)
        {
            device.DeviceName = deviceName;
            return device;
        }

        public static Device WithDeviceType(this Device device, string deviceType)
        {
            device.DeviceType = deviceType;
            return device;
        }

        public static Device WithSerialNumber(this Device device, string serialNumber)
        {
            device.SerialNumber = serialNumber;
            return device;
        }

        public static Device WithAddress(this Device device, Address address)
        {
            device.Address = address;
            return device;
        }

        public static Device WithHub(this Device device, Hub hub)
        {
            device.Hub = hub;
            return device;
        }

        public static Device WithRegistrationStartDate(this Device device, DateTime registrationStartDate)
        {
            device.RegistrationStartDate = registrationStartDate;
            return device;
        }

        public static Device WithRegistrationEndDate(this Device device, DateTime? registrationEndDate)
        {
            device.RegistrationEndDate = registrationEndDate;
            return device;
        }

        public static Device WithInstallationFeedback(this Device device, InstallationFeedback installationFeedback)
        {
            device.Installation = installationFeedback;
            return device;
        }

        public static Device WithHeaterOutputs(this Device device, string heaterOutput1, string heaterOutput2)
        {
            device.HeaterOutput1 = heaterOutput1;
            device.HeaterOutput2 = heaterOutput2;
            return device;
        }

        public static Device WithStatus(this Device device, int status)
        {
            device.Status = status;
            return device;
        }

        public static Device WithTimeZoneRegion(this Device device, string timeZoneRegion)
        {
            device.TimeZoneRegion = timeZoneRegion;
            return device;
        }

        public static Device WithFirmware(this Device device, string firmware)
        {
            device.Firmware = firmware;
            return device;
        }

        public static Device WithLastUpdate(this Device device, DateTime? lastUpdate)
        {
            device.LastUpdate = lastUpdate;
            return device;
        }

        public static Device WithChargeSchedules(this Device device, IList<DeviceChargesSetup> chargeSchedules)
        {
            device.ChargeSchedules = chargeSchedules;
            return device;
        }

        public static IList<Device> GetListOfNewDevices(User ownerUser = null, Hub hub = null)
        {
            var user = ownerUser ?? UserMockFactory.GetNewFullUser();
            var mainAddress = user.Addresses.First();

            var device1 = GetZappiDevice(mainAddress, hub, deviceStatus: DeviceStatus.New);
            var device2 = GetHarviDevice(mainAddress, hub, deviceStatus: DeviceStatus.New);
            var device3 = GetEddiDevice(mainAddress, hub, deviceStatus: DeviceStatus.New);

            return new List<Device>
            {
                device1, device2, device3
            };
        }

        public static IList<Device> GetListOfDevicesForUserAndHub(User user, Hub hub = null)
        {
            var mainAddress = user.Addresses.First();

            var device1 = GetZappiDevice(mainAddress, hub);
            var device2 = GetHarviDevice(mainAddress, hub);
            var device3 = GetEddiDevice(mainAddress, hub);

            return new List<Device>
            {
                device1, device2, device3
            };
        }

        public static Device GetZappiDevice(Address address = null, Hub hub = null, DeviceStatus deviceStatus = DeviceStatus.None)
        {
            return GetEmptyDevice()
                .WithId(1)
                .WithGuid(Guid.Parse("A6CF8EBA-1828-EB11-8287-060BA7111D4A"))
                .WithDeviceName("MyDevice 1-zappi")
                .WithDeviceType("zappi")
                .WithSerialNumber("12057779")
                .WithAddress(address)
                .WithHub(hub)
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-1))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithStatus((int)deviceStatus)
                .WithTimeZoneRegion("Europe/London")
                .WithFirmware("3560S3.103")
                .WithLastUpdate(DateTime.UtcNow)
                .WithChargeSchedules(new List<DeviceChargesSetup>());
        }

        public static Device GetHarviDevice(Address address = null, Hub hub = null, DeviceStatus deviceStatus = DeviceStatus.None)
        {
            return GetEmptyDevice()
                .WithId(2)
                .WithGuid(Guid.Parse("B6CF8EBA-1828-EB11-8287-060BA7111D4A"))
                .WithDeviceName("MyDevice 2-harvi")
                .WithDeviceType("harvi")
                .WithSerialNumber("11100003")
                .WithAddress(address)
                .WithHub(hub)
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-2))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithStatus((int)deviceStatus)
                .WithTimeZoneRegion("Europe/London")
                .WithFirmware("3300S3.101")
                .WithLastUpdate(DateTime.UtcNow)
                .WithChargeSchedules(new List<DeviceChargesSetup>());
        }

        public static Device GetEddiDevice(Address address = null, Hub hub = null, DeviceStatus deviceStatus = DeviceStatus.None)
        {
            return GetEmptyDevice()
                .WithId(3)
                .WithGuid(Guid.Parse("C6CF8EBA-1828-EB11-8287-060BA7111D4A"))
                .WithDeviceName("MyDevice 3-eddi")
                .WithDeviceType("eddi")
                .WithSerialNumber("12067512")
                .WithAddress(address)
                .WithHub(hub)
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-3))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithStatus((int)deviceStatus)
                .WithHeaterOutputs("heat1-eddi", "heat2-eddi")
                .WithTimeZoneRegion("Europe/London")
                .WithFirmware("3200S2.093")
                .WithLastUpdate(DateTime.UtcNow)
                .WithChargeSchedules(new List<DeviceChargesSetup>());
        }

        public static Device GetPhantomDevice(
            Address address = null,
            string deviceType = "zappi",
            DeviceStatus deviceStatus = DeviceStatus.None)
        {
            var id = new Random().Next(0, int.MaxValue);

            var device = GetEmptyDevice()
                .WithId(id)
                .WithGuid(Guid.NewGuid())
                .WithDeviceName("MyDevice " + id + " " + deviceType)
                .WithDeviceType(deviceType)
                .WithSerialNumber("13100005")
                .WithAddress(address)
                .WithHub(null)
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-3))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithStatus((int)deviceStatus)
                .WithTimeZoneRegion("Europe/London")
                .WithFirmware("3200S2.093")
                .WithLastUpdate(DateTime.UtcNow)
                .WithChargeSchedules(new List<DeviceChargesSetup>());

            if (deviceType.Equals(DeviceType.Eddi.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                device.WithHeaterOutputs("heat1-eddi", "heat2-eddi");
            }

            return device;
        }

        public static List<DeviceData> GetDeviceData()
        {
            return new List<DeviceData>()
            {
                new DeviceData()
                {
                    DeviceName = "eddi",
                    DeviceType = "eddi",
                    SerialNumber = "10615665",
                    Firmware = "3200S3.047",
                    HeaterOutput1 = "Tank 1",
                    HeaterOutput2 = "Tank 2",
                    TimeZoneRegion = "Europe/London"
                }
            };
        }

        public static List<Business.Model.Models.DeviceData> GetDeviceDataBusinessModel()
        {
            return new List<Business.Model.Models.DeviceData>()
            {
                new Business.Model.Models.DeviceData()
                {
                    DeviceName = "eddi",
                    DeviceType = "eddi",
                    SerialNumber = "10615665",
                    Firmware = "3200S3.047",
                    HeaterOutput1 = "Tank 1",
                    HeaterOutput2 = "Tank 2",
                    TimeZoneRegion = "Europe/London"
                }
            };
        }
    }
}
