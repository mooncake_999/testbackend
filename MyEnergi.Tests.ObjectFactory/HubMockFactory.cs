﻿using MyEnergi.Business.Model.Models;
using MyEnergi.Common;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class HubMockFactory
    {
        public static Hub GetEmptyHub()
        {
            return new Hub();
        }

        public static HubHistory GetEmptyHubHistory()
        {
            return new HubHistory();
        }

        public static Hub WithId(this Hub hub, int id)
        {
            hub.Id = id;
            return hub;
        }

        public static Hub WithGuid(this Hub hub, Guid guid)
        {
            hub.Guid = guid;
            return hub;
        }

        public static Hub WithAddress(this Hub hub, Address address)
        {
            hub.Address = address;
            return hub;
        }

        public static Hub WithSerialNo(this Hub hub, string serialNo)
        {
            hub.SerialNo = serialNo;
            return hub;
        }

        public static Hub WithRegistrationCode(this Hub hub, string registrationCode)
        {
            hub.RegistrationCode = registrationCode;
            return hub;
        }

        public static Hub WithNickName(this Hub hub, string nickname)
        {
            hub.NickName = nickname;
            return hub;
        }

        public static Hub WithAppPassword(this Hub hub, string appPassword)
        {
            hub.AppPassword = appPassword;
            return hub;
        }

        public static Hub WithRegistrationStartDate(this Hub hub, DateTime registrationStartDate)
        {
            hub.RegistrationStartDate = registrationStartDate;
            return hub;
        }

        public static Hub WithRegistrationEndDate(this Hub hub, DateTime? registrationEndDate)
        {
            hub.RegistrationEndDate = registrationEndDate;
            return hub;
        }

        public static Hub WithInstallationFeedback(this Hub hub, InstallationFeedback installation)
        {
            hub.Installation = installation;
            return hub;
        }

        public static Hub WithTimeZoneRegion(this Hub hub, string timeZoneRegion)
        {
            hub.TimeZoneRegion = timeZoneRegion;
            return hub;
        }

        public static Hub WithFirmware(this Hub hub, string firmware)
        {
            hub.Firmware = firmware;
            return hub;
        }

        public static Hub WithStatus(this Hub hub, int status)
        {
            hub.Status = status;
            return hub;
        }

        public static Hub WithHostServer(this Hub hub, string hostServer)
        {
            hub.HostServer = hostServer;
            return hub;
        }

        public static Hub WithLastUpdate(this Hub hub, DateTime? dateTime = null)
        {
            hub.LastUpdate = dateTime ?? DateTime.UtcNow;
            return hub;
        }

        public static Hub GetNewFullHub(User ownerUser = null)
        {
            var user = ownerUser ?? UserMockFactory.GetNewFullUser();
            var mainAddress = user.Addresses.First();

            return GetEmptyHub()
                .WithGuid(Guid.NewGuid())
                .WithAddress(mainAddress)
                .WithNickName("HubNickName1")
                .WithSerialNo("10159297")
                .WithAppPassword("AQICAHiNicZoQOIdmOp21Pn4DaTpqXskiTUQnH4bjrjd9W5kYAFTnA88POlR0SGg0PS8cgk0AAAAZjBkBgkqhkiG9w0BBwagVzBVAgEAMFAGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMAEFhSxbmrjULUPPEAgEQgCNCF7ME2SlWlSbNk2iN9IGL/Q688zbDPJVOD0zj7RPEF71oZg==")
                .WithFirmware("3401S3051")
                .WithRegistrationCode("r5f9b-af13")
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-1))
                .WithRegistrationEndDate(DateTime.UtcNow.AddYears(1))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithTimeZoneRegion("Europe/London")
                .WithStatus(0)
                .WithHostServer("s7.myenergi.net")
                .WithLastUpdate();
        }

        public static IList<Hub> GetListOfHubsForUser(User hubOwnerUser = null)
        {
            var user = hubOwnerUser ?? UserMockFactory.GetNewFullUser();
            var mainAddress = user.Addresses.First();
            var secondAddress = user.Addresses.ElementAt(1);
            var thirdAddress = user.Addresses.ElementAt(2);

            var hub1 = GetEmptyHub()
                .WithNickName("Tisbury")
                .WithAddress(mainAddress)
                .WithSerialNo("10365623")
                .WithAppPassword("AQICAHiNicZoQOIdmOp21Pn4DaTpqXskiTUQnH4bjrjd9W5kYAHfste01y8bErH/iXhVpmHfAAAAZzBlBgkqhkiG9w0BBwagWDBWAgEAMFEGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMbDcZCMEn3wJxFOnaAgEQgCTWMHLtYNh1U+q+6XBLPfl+TBTKBYkGoC5lrvd5QWUNudJiEz0=")
                .WithFirmware("3401S3051")
                .WithRegistrationCode("r5fa6-cdc5")
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-1))
                .WithRegistrationEndDate(DateTime.UtcNow.AddYears(1))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithTimeZoneRegion("Europe/Berlin")
                .WithStatus(0)
                .WithHostServer("s3.myenergi.net")
                .WithLastUpdate();

            var hub2 = GetEmptyHub()
                .WithNickName("garage")
                .WithAddress(secondAddress)
                .WithSerialNo("10159297")
                .WithAppPassword("AQICAHiNicZoQOIdmOp21Pn4DaTpqXskiTUQnH4bjrjd9W5kYAH2ZzK1LFQKJUv95y84p3LZAAAAZjBkBgkqhkiG9w0BBwagVzBVAgEAMFAGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM4GSv1w4KIMS4GRghAgEQgCOynniswbAzQS3H/5Pvu/OoAgXxeCAvCyUybOzcwW5+/o6wEQ==")
                .WithFirmware("3401S3051")
                .WithRegistrationCode("r5f9b-af13")
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-1))
                .WithRegistrationEndDate(DateTime.UtcNow.AddYears(1))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithTimeZoneRegion("Europe/London")
                .WithStatus(0)
                .WithHostServer("s7.myenergi.net")
                .WithLastUpdate();

            var hub3 = GetEmptyHub()
                .WithAddress(thirdAddress)
                .WithNickName("marc@lia.fr")
                .WithSerialNo("12112805")
                .WithAppPassword("AQICAHiNicZoQOIdmOp21Pn4DaTpqXskiTUQnH4bjrjd9W5kYAGs8+1FrK7v0JoZEd3xu7e4AAAAajBoBgkqhkiG9w0BBwagWzBZAgEAMFQGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM+1oj/Msp0C0/YbJMAgEQgCcAzxepCZL4vMv3cLs86lGEHTfdH7jpeaq6eVqOnGIwJ/NjswSQFHI=")
                .WithFirmware("3401S3051")
                .WithRegistrationCode("r5f7d-9b95")
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-1))
                .WithRegistrationEndDate(DateTime.UtcNow.AddYears(1))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithTimeZoneRegion("Europe/Lisbon")
                .WithStatus(0)
                .WithHostServer("s5.myenergi.net")
                .WithLastUpdate();

            return new List<Hub>
            {
                hub1, hub2, hub3
            };
        }

        public static HubData GetHubData()
        {
            return new HubData()
            {
                Id = Guid.Empty,
                Address = new AddressData()
                {
                    AddressLine1 = "36 London Street",
                    City = "Edinburgh",
                    Country = "United Kingdom",
                    CountryCode = "GB",
                    PostalCode = "EH3 6NB",
                    Region = "Edinburgh",
                    SiteName = "Home Hub",
                    Id = Guid.Empty
                },
                SerialNo = "10621741",
                NickName = "Wellbank",
                Password = "TestPassword2000",
                ConfirmPassword = "TestPassword2000",
                Status = Enumerators.DeviceStatus.NewByUser,
                Firmware = "3401S3051",
                TimeZoneRegion = "Europe/London",
                HostServer = "s1.myenergi.net",
                IsGuestUser = false
            };
        }

        public static ICollection<object> GetAccountAccessHubsObject()
        {
            return new List<object>
            {
                new
                {
                    serialNo = "10613333",
                    hubPassword = "12345",
                    relation = "owner",
                    hubName = "Hub NickName",
                    owner = "Gigel Owner",
                    shares = new List<object>
                    {
                        new
                        {
                            email = "share_1@yopmail.com",
                            name = "Share 1",
                            status = "Pending"
                        },
                        new
                        {
                            email = "share_2@yopmail.com",
                            name = "Share 2",
                            status = "Accepted"
                        }
                    }
                },
                new
                {
                    serialNo = "123456765",
                    relation = "invited",
                    hubName = "John's Site",
                    owner = "Gion Hatz"
                },
                new
                {
                    serialNo = "102116765",
                    hubPassword = "98765",
                    relation = "guest",
                    hubName = "Marvel Hub",
                    owner = "Captain America",
                    devices = new List<object>
                    {
                        new
                        {
                            deviceSerialNo = "102188765",
                            validity = new
                            {
                                from = 1620304030553,
                                to = 1620390430554,
                                recurrence = "* * * * *"
                            },
                            features = new List<string> {"status", "commands", "history"}
                        }
                    }
                }
            };
        }

        public static IEnumerable<MigrateHubData> GetMigrateHubsRequestData()
        {
            return new List<MigrateHubData>
            {
                new MigrateHubData
                {
                    SerialNo = "10613333",
                    HubPassword = "12345"
                },
                new MigrateHubData
                {
                    SerialNo = "123456765",
                    HubPassword = "98765"
                },
                new MigrateHubData
                {
                    SerialNo = "102116765",
                    HubPassword = "98765"
                },
                new MigrateHubData
                {
                    SerialNo = "102116769",
                    HubPassword = "wrongPassword"
                }
            };
        }

        public static IEnumerable<object> GetMigrateHubsResponseObject()
        {
            return new List<object>
            {
                new
                {
                    serialNo = "10613333",
                    hubPassword = "12345",
                    relation = "owner"
                },
                new
                {
                    serialNo = "123456765",
                    hubName = "John's Site",
                    relation = "invited",
                    owner = "Gion Gionson"
                },
                new
                {
                    serialNo = "102116765",
                    hubPassword = "98765",
                    relation = "guest",
                    owner = "Alahu Akhbar"
                },
                new
                {
                    serialNo = "102116769",
                    relation = "Access denied"
                }
            };
        }

        public static Hub GetWellbankFullHub(User ownerUser = null)
        {
            var user = ownerUser ?? UserMockFactory.GetNewFullUser();
            var mainAddress = new Address()
            {
                AddressLine1 = "36 London Street",
                City = "Edinburgh",
                Country = "United Kingdom",
                CountryCode = "GB",
                PostalCode = "EH3 6NB",
                Region = "Edinburgh",
                SiteName = "Home Hub"
            };

            return GetEmptyHub()
                .WithAddress(mainAddress)
                .WithNickName("Wellbank")
                .WithSerialNo("10621741")
                .WithAppPassword("AQICAHiNicZoQOIdmOp21Pn4DaTpqXskiTUQnH4bjrjd9W5kYAEauDODdEISIfCxes8N+SqUAAAAZzBlBgkqhkiG9w0BBwagWDBWAgEAMFEGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMfAnkWi9Whk2h4wWMAgEQgCRs9xfgsBtG7ycLZ0f5FWEa4efSJZ2X7/oWj7OEw8Wt84vZHQ8=")
                .WithFirmware("3401S3051")
                .WithRegistrationStartDate(DateTime.UtcNow.AddYears(-1))
                .WithRegistrationEndDate(DateTime.UtcNow.AddYears(1))
                .WithInstallationFeedback(new InstallationFeedback())
                .WithTimeZoneRegion("Europe/London")
                .WithStatus(0)
                .WithHostServer("s1.myenergi.net")
                .WithLastUpdate();
        }
    }
}
