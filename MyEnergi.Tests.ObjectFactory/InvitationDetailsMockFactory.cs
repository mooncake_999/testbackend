﻿using MyEnergi.Data.Entity;
using System;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class InvitationDetailsMockFactory
    {
        public static InvitationDetails GetEmptyInvitationDetails()
        {
            return new InvitationDetails();
        }

        public static InvitationDetails WithId(this InvitationDetails invitationDetails, int id)
        {
            invitationDetails.Id = id;
            return invitationDetails;
        }

        public static InvitationDetails WithGuid(this InvitationDetails invitationDetails, Guid guid)
        {
            invitationDetails.Guid = guid;
            return invitationDetails;
        }

        public static InvitationDetails WithEmail(this InvitationDetails invitationDetails, string email)
        {
            invitationDetails.Email = email;
            return invitationDetails;
        }

        public static InvitationDetails WithFirstName(this InvitationDetails invitationDetails, string firstName)
        {
            invitationDetails.FirstName = firstName;
            return invitationDetails;
        }

        public static InvitationDetails WithLastName(this InvitationDetails invitationDetails, string lastName)
        {
            invitationDetails.LastName = lastName;
            return invitationDetails;
        }

        public static InvitationDetails WithAppAccess(this InvitationDetails invitationDetails, bool appAccess)
        {
            invitationDetails.AppAccess = appAccess;
            return invitationDetails;
        }

        public static InvitationDetails WithAdministratorAccess(this InvitationDetails invitationDetails,
            bool administratorAccess)
        {
            invitationDetails.AdministratorAccess = administratorAccess;
            return invitationDetails;
        }

        public static InvitationDetails WithAllDevicesAccess(this InvitationDetails invitationDetails,
            bool allDevicesAccess)
        {
            invitationDetails.AllDevicesAccess = allDevicesAccess;
            return invitationDetails;
        }

        public static InvitationDetails WithLastUpdate(this InvitationDetails invitationDetails,
            DateTime? lastUpdate = null)
        {
            invitationDetails.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return invitationDetails;
        }

        public static InvitationDetails GetNewFullInvitationDetails(string email, string firstName, string lastName)
        {
            var id = new Random().Next(0, int.MaxValue);

            return GetEmptyInvitationDetails()
                .WithId(id)
                .WithGuid(new Guid())
                .WithEmail(email)
                .WithFirstName(firstName)
                .WithLastName(lastName)
                .WithAppAccess(true)
                .WithAdministratorAccess(true)
                .WithAllDevicesAccess(true)
                .WithLastUpdate();
        }
    }
}
