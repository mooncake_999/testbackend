﻿using MyEnergi.Common;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class InvitationMockFactory
    {
        public static Invitation GetEmptyInvitation()
        {
            return new Invitation();
        }

        public static Invitation WithId(this Invitation invitation, int id)
        {
            invitation.Id = id;
            return invitation;
        }

        public static Invitation WithGuid(this Invitation invitation, Guid guid)
        {
            invitation.Guid = guid;
            return invitation;
        }

        public static Invitation WithGuestUser(this Invitation invitation, User guestUser)
        {
            invitation.GuestUser = guestUser;
            return invitation;
        }

        public static Invitation WithOwnerUser(this Invitation invitation, User ownerUser)
        {
            invitation.OwnerUser = ownerUser;
            return invitation;
        }
        
        public static Invitation WithInvitationStatus(this Invitation invitation, InvitationStatus invitationStatus)
        {
            invitation.InvitationStatus = invitationStatus;
            return invitation;
        }

        public static Invitation WithInvitationDetails(this Invitation invitation, InvitationDetails invitationDetails)
        {
            invitation.InvitationDetails = invitationDetails;
            return invitation;
        }

        public static Invitation WithStartDate(this Invitation invitation, DateTime? startDate)
        {
            invitation.StartDate = startDate;
            return invitation;
        }

        public static Invitation WithEndDate(this Invitation invitation, DateTime? endDate)
        {
            invitation.EndDate = endDate;
            return invitation;
        }

        public static Invitation WithExpirationDate(this Invitation invitation, DateTime? expirationDate)
        {
            invitation.ExpirationDate = expirationDate;
            return invitation;
        }

        public static Invitation WithCreationDate(this Invitation invitation, DateTime creationDate)
        {
            invitation.CreationDate = creationDate;
            return invitation;
        }

        public static Invitation WithCronExpression(this Invitation invitation, string cronExpression)
        {
            invitation.CronExpressionRecurrence = cronExpression;
            return invitation;
        }

        public static Invitation WithUserFeaturePermissions(this Invitation invitation,
            IList<UserFeaturePermission> userFeaturePermissions)
        {
            invitation.UserFeaturePermissions = userFeaturePermissions;
            return invitation;
        }

        public static Invitation WithHistoryUserFeaturePermissions(this Invitation invitation,
            IList<UserFeaturePermissionHistory> userFeaturePermissionHistories)
        {
            invitation.UserFeaturePermissionHistories = userFeaturePermissionHistories;
            return invitation;
        }

        public static Invitation WithLastUpdate(this Invitation invitation, DateTime? lastUpdate = null)
        {
            invitation.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return invitation;
        }

        public static Invitation GetNewFullInvitationForNotExistingGuestUser(User ownerUser, string email,
            string firstName, string lastName, Enumerators.InvitationStatus invitationStatus, IList<Device> devices)
        {
            var invitationStatusEntity = InvitationStatusMockFactory.GetInvitationsWithStatus(invitationStatus);

            var invitationDetails = InvitationDetailsMockFactory
                .GetNewFullInvitationDetails(email, firstName, lastName);

            var id = new Random().Next(0, int.MaxValue);
            var invitation = GetEmptyInvitation()
                .WithId(id)
                .WithGuid(Guid.NewGuid())
                .WithOwnerUser(ownerUser)
                .WithGuestUser(null)
                .WithOwnerUser(ownerUser)
                .WithInvitationStatus(invitationStatusEntity)
                .WithInvitationDetails(invitationDetails)
                .WithLastUpdate();

            var appFeatures = AppFeatureMockFactory.GetAppFeatures();
            var userFeaturePermissions = new List<UserFeaturePermission>();
            foreach (var device in devices)
            {
                userFeaturePermissions.AddRange(appFeatures.Select(appFeature =>
                    UserFeaturePermissionMockFactory
                        .GetFullUserFeaturePermission(ownerUser, invitation, device, appFeature)));
            }

            invitation.WithUserFeaturePermissions(userFeaturePermissions);

            return invitation;
        }

        public static Invitation GetNewFullInvitationForExistingGuestUser(User ownerUser,
            User guestUser,
            Enumerators.InvitationStatus invitationStatus,
            IList<Device> devices)
        {
            return GetNewFullInvitationForNotExistingGuestUser(ownerUser,
                guestUser.Email, guestUser.UserDetails.FirstName, guestUser.UserDetails.LastName, invitationStatus,
                devices)
                .WithGuestUser(guestUser)
                .WithOwnerUser(ownerUser);
        }
    }
}
