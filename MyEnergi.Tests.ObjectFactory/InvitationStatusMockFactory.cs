﻿using MyEnergi.Common;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class InvitationStatusMockFactory
    {
        public static InvitationStatus GetNewInvitationStatus()
        {
            return new InvitationStatus();
        }

        public static InvitationStatus WithId(this InvitationStatus invitationStatus, int id)
        {
            invitationStatus.Id = id;
            return invitationStatus;
        }

        public static InvitationStatus WithGuid(this InvitationStatus invitationStatus, Guid guid)
        {
            invitationStatus.Guid = guid;
            return invitationStatus;
        }

        public static InvitationStatus WithStatus(this InvitationStatus invitationStatus, string status)
        {
            invitationStatus.Status = status;
            return invitationStatus;
        }

        public static InvitationStatus WithInvitations(this InvitationStatus invitationStatus,
            IEnumerable<Invitation> invitations)
        {
            invitationStatus.Invitations = invitations;
            return invitationStatus;
        }

        public static InvitationStatus WithLastUpdate(this InvitationStatus invitationStatus,
            DateTime? lastUpdate = null)
        {
            invitationStatus.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return invitationStatus;
        }

        public static IList<InvitationStatus> GetInvitationStatuses()
        {
            var statuses = Enum.GetNames(typeof(Enumerators.InvitationStatus))
                .Select(s => s.ToLower());

            var invitationStatuses = new List<InvitationStatus>();

            var id = 1;
            foreach (var status in statuses)
            {
                var invitationStatus = GetNewInvitationStatus()
                    .WithId(id++)
                    .WithGuid(Guid.NewGuid())
                    .WithStatus(status)
                    .WithLastUpdate();

                invitationStatuses.Add(invitationStatus);
            }

            return invitationStatuses;
        }

        public static InvitationStatus GetInvitationsWithStatus(Enumerators.InvitationStatus invitationStatus)
        {
            var id = new Random().Next(0, int.MaxValue);
            return GetNewInvitationStatus()
                .WithId(id)
                .WithGuid(Guid.NewGuid())
                .WithStatus(invitationStatus.ToString())
                .WithLastUpdate();
        }
    }
}
