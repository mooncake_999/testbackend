﻿namespace MyEnergi.Tests.ObjectFactory
{
    public class MockConstants
    {
        public const string CognitoIdFromToken = "76fb8e83-489d-4bad-b308-10191d961f21";

        public const string AcceptedLanguage = "en-GB";
    }
}
