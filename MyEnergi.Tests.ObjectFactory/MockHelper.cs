﻿using Microsoft.IdentityModel.Tokens;
using MyEnergi.Data.Entity;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using MyEnergi.AppServer.Model;
using System.Net;
using System.Collections.Generic;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class MockHelper
    {
        private const string MockClientSecret = "23ko3gf1cb7c5rfik4e6pk9k0rjcgc88j3vs2jl8slacnttrirh";
        private const string CognitoIdClaim = "sub";
            
        public static string GenerateJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(MockClientSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim(CognitoIdClaim, user.CognitoId) }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static ApiCallResponse GetOkApiCallResponseHubNickname(string nickname)
        {
            return new ApiCallResponse()
            {
                HttpStatusCode = HttpStatusCode.OK,
                HostServer = "s1.myenergi.com",
                HttpMessage = "OK Server: WSWIDB V1.600",
                Content = nickname
            };
        }

        public static ApiCallResponse GetOkApiCallResponse()
        {
            return new ApiCallResponse()
            {
                HttpStatusCode = HttpStatusCode.OK,
                HostServer = "",
                HttpMessage = "",
                Content = null
            };
        }

        public static ApiCallResponse GetOkApiCallResponseTuple(Tuple<string, List<DeviceData>> tuple)
        {
            return new ApiCallResponse()
            {
                HttpStatusCode = HttpStatusCode.OK,
                HostServer = "s1.myenergi.com",
                HttpMessage = "OK Server: WSWIDB V1.600",
                Content = tuple
            };
        }
    }
}
