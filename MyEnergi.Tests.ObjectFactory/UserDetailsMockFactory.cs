﻿using MyEnergi.Data.Entity;
using System;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class UserDetailsMockFactory
    {
        public static UserDetails GetEmptyUserDetails()
        {
            return new UserDetails();
        }

        public static UserDetails WithId(this UserDetails userDetails, int id)
        {
            userDetails.Id = id;
            return userDetails;
        }

        public static UserDetails WithGuid(this UserDetails userDetails, Guid guid)
        {
            userDetails.Guid = guid;
            return userDetails;
        }

        public static UserDetails WithTitle(this UserDetails userDetails, string title)
        {
            userDetails.Title = title;
            return userDetails;
        }

        public static UserDetails WithFirstName(this UserDetails userDetails, string firstName)
        {
            userDetails.FirstName = firstName;
            return userDetails;
        }

        public static UserDetails WithLastName(this UserDetails userDetails, string lastName)
        {
            userDetails.LastName = lastName;
            return userDetails;
        }

        public static UserDetails WithMobileCode(this UserDetails userDetails, string mobileCode)
        {
            userDetails.MobileCode = mobileCode;
            return userDetails;
        }

        public static UserDetails WithMobileNumber(this UserDetails userDetails, string mobileNumber)
        {
            userDetails.MobileNumber = mobileNumber;
            return userDetails;
        }

        public static UserDetails WithLandlineCode(this UserDetails userDetails, string landlineCode)
        {
            userDetails.LandlineCode = landlineCode;
            return userDetails;
        }

        public static UserDetails WithLandlineNumber(this UserDetails userDetails, string landlineNumber)
        {
            userDetails.LandlineNumber = landlineNumber;
            return userDetails;
        }

        public static UserDetails WithDateOfBirth(this UserDetails userDetails, DateTime? dateOfBirth)
        {
            userDetails.DateOfBirth = dateOfBirth;
            return userDetails;
        }

        public static UserDetails WithLastUpdate(this UserDetails userDetails, DateTime? lastUpdate = null)
        {
            userDetails.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return userDetails;
        }

        public static UserDetails GetNewFullUserDetails()
        {
            var id = new Random().Next(0, int.MaxValue);

            return GetEmptyUserDetails()
                .WithId(id)
                .WithGuid(Guid.NewGuid())
                .WithTitle("Mrs")
                .WithFirstName("Unit " + id)
                .WithLastName("Test " + id)
                .WithMobileNumber("1234567890")
                .WithLandlineNumber("1234567890")
                .WithDateOfBirth(DateTime.Parse("1985-10-15"))
                .WithLandlineCode("+44")
                .WithMobileCode("+44")
                .WithLastUpdate();
        }
    }
}
