﻿using MyEnergi.Data.Entity;
using System;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class UserFeaturePermissionMockFactory
    {
        public static UserFeaturePermission GetEmptyUserFeaturePermission()
        {
            return new UserFeaturePermission();
        }

        public static UserFeaturePermission WithId(this UserFeaturePermission userFeaturePermission, int id)
        {
            userFeaturePermission.Id = id;
            return userFeaturePermission;
        }

        public static UserFeaturePermission WithGuid(this UserFeaturePermission userFeaturePermission, Guid guid)
        {
            userFeaturePermission.Guid = guid;
            return userFeaturePermission;
        }

        public static UserFeaturePermission WithOwnerUser(this UserFeaturePermission userFeaturePermission,
            User ownerUser)
        {
            userFeaturePermission.OwnerUser = ownerUser;
            return userFeaturePermission;
        }

        public static UserFeaturePermission WithInvitation(this UserFeaturePermission userFeaturePermission,
            Invitation invitation)
        {
            userFeaturePermission.Invitation = invitation;
            return userFeaturePermission;
        }

        public static UserFeaturePermission WithDevice(this UserFeaturePermission userFeaturePermission,
            Device device)
        {
            userFeaturePermission.Device = device;
            return userFeaturePermission;
        }

        public static UserFeaturePermission WithAppFeature(this UserFeaturePermission userFeaturePermission,
            AppFeature appFeature)
        {
            userFeaturePermission.AppFeature = appFeature;
            return userFeaturePermission;
        }

        public static UserFeaturePermission WithLastUpdate(this UserFeaturePermission userFeaturePermission, DateTime? lastUpdate = null)
        {
            userFeaturePermission.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return userFeaturePermission;
        }

        public static UserFeaturePermission GetFullUserFeaturePermission(User ownerUser, Invitation invitedUser,
            Device device, AppFeature appFeature)
        {
            return GetEmptyUserFeaturePermission()
                .WithOwnerUser(ownerUser)
                .WithInvitation(invitedUser)
                .WithDevice(device)
                .WithAppFeature(appFeature)
                .WithLastUpdate();
        }
    }
}
