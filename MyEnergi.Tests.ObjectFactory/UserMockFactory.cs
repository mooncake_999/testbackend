﻿using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Entity;
using System;
using System.Collections.Generic;
using UserPreferenceData = MyEnergi.Business.Model.Models.UserPreferenceData;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class UserMockFactory
    {
        public static User GetEmptyUser()
        {
            return new User();
        }

        public static User WithId(this User user, int id)
        {
            user.Id = id;
            return user;
        }

        public static User WithGuid(this User user, Guid guid)
        {
            user.Guid = guid;
            return user;
        }

        public static User WithEmail(this User user, string email)
        {
            user.Email = email;
            return user;
        }

        public static User WithPrivacyAcceptance(this User user, bool privacyAcceptance)
        {
            user.PrivacyAcceptance = privacyAcceptance;
            return user;
        }

        public static User WithMarketingOptIn(this User user, bool marketingOptIn)
        {
            user.MarketingOptIn = marketingOptIn;
            return user;
        }

        public static User WithThirdPartyMarketingOptIn(this User user, bool thirdPartyMarketingOptIn)
        {
            user.ThirdPartyMarketingOptIn = thirdPartyMarketingOptIn;
            return user;
        }

        public static User WithCognitoId(this User user, string cognitoId)
        {
            user.CognitoId = cognitoId;
            return user;
        }

        public static User WithUserDetails(this User user, UserDetails userDetails)
        {
            user.UserDetails = userDetails;
            return user;
        }

        public static User WithUserPreferences(this User user, UserPreference userPreference)
        {
            user.UserPreference = userPreference;
            return user;
        }

        public static User WithUserVehicles(this User user, IList<UserVehicle> userVehicles)
        {
            user.UserVehicles = userVehicles;
            return new User();
        }

        public static User WithAddresses(this User user, IList<Address> addresses)
        {
            user.Addresses = addresses;
            return user;
        }

        public static User WithLastUpdate(this User user, DateTime? lastUpdate = null)
        {
            user.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return user;
        }

        public static User GetNewFullUser(string cognitoId = "76fb8e83-489d-4bad-b308-10191d961f21")
        {
            var id = new Random().Next(0, int.MaxValue);
            var userDetails = UserDetailsMockFactory.GetNewFullUserDetails();
            var userPreference = UserPreferenceMockFactory.GetDefaultUserPreference();

            var user = GetEmptyUser()
                .WithId(id)
                .WithGuid(Guid.NewGuid())
                .WithEmail("testUser_" + id + "@yopmail.com")
                .WithCognitoId(cognitoId)
                .WithUserDetails(userDetails)
                .WithUserPreferences(userPreference)
                .WithLastUpdate();

            var addresses = AddressMockFactory.GetListOfAddressesForUser(user);

            return user.WithAddresses(addresses);
        }

        public static UserDetailsData GetUserDetailsData()
        {
            return new UserDetailsData
            {
                Title = "Ms",
                FirstName = "Mary",
                LastName = "Jane",
                MobileCode = "+44",
                MobileNumber = "1234567890",
                LandlineNumber = "1234567890",
                LandlineCode = "+44",
                DateOfBirth = "10/2/1984",
                Address = new List<AddressData>
                {
                    new AddressData
                    {
                        AddressLine1 = "36 London Street",
                        City = "Edinburgh",
                        Country = "United Kingdom",
                        CountryCode = "GB",
                        PostalCode = "EH3 6NB",
                        Region = "Edinburgh",
                        SiteName = "Home Hub"
                    },
                    new AddressData
                    {
                        AddressLine1 = "Mandeville Road",
                        City = "Aylesbury",
                        Country = "United Kingdom",
                        CountryCode = "GB",
                        PostalCode = "HP21 8AL",
                        Region = "Buckinghamshire",
                        SiteName = "Home Hub 2"
                    }

                },
                UserVehicles = new List<VehicleData>
                {
                    new VehicleData
                    {
                        Manufacturer = "Mercedes",
                        ManufacturerOfficialEVRange = 3,
                        ManufacturingYear = 2021,
                        Model = "Maybach S-Class",
                        RealEVRange = 3
                    },
                    new VehicleData
                    {
                        Manufacturer = "Audi",
                        ManufacturerOfficialEVRange = 3,
                        ManufacturingYear = 2019,
                        Model = "225xe iPerformance Active Tourer (2018-2019)",
                        RealEVRange = 3
                    }
                },
                UserPreference = UserPreferenceMockFactory.GetDefaultUserPreferenceData()
            };
        }

        public static UserPersonalDetailsData GetUserPersonalDetailsData()
        {
            return new UserPersonalDetailsData
            {
                Title = "Ms",
                FirstName = "Mary",
                LastName = "Jane",
                MobileCode = "+44",
                MobileNumber = "1234567890",
                LandlineNumber = "1234567890",
                LandlineCode = "+44",
                DateOfBirth = "10/2/1984",
                Address = new List<AddressData>
                {
                    new AddressData
                    {
                        AddressLine1 = "36 London Street",
                        City = "Edinburgh",
                        Country = "United Kingdom",
                        CountryCode = "GB",
                        PostalCode = "EH3 6NB",
                        Region = "Edinburgh",
                        SiteName = "Home Hub"
                    },
                    new AddressData
                    {
                        AddressLine1 = "Mandeville Road",
                        City = "Aylesbury",
                        Country = "United Kingdom",
                        CountryCode = "GB",
                        PostalCode = "HP21 8AL",
                        Region = "Buckinghamshire",
                        SiteName = "Home Hub 2"
                    }

                },
                UserVehicles = new List<VehicleData>
                {
                    new VehicleData
                    {
                        Manufacturer = "Mercedes",
                        ManufacturerOfficialEVRange = 3,
                        ManufacturingYear = 2021,
                        Model = "Maybach S-Class",
                        RealEVRange = 3
                    },
                    new VehicleData
                    {
                        Manufacturer = "Audi",
                        ManufacturerOfficialEVRange = 3,
                        ManufacturingYear = 2019,
                        Model = "225xe iPerformance Active Tourer (2018-2019)",
                        RealEVRange = 3
                    }
                }
            };
        }

        public static IList<string> GetManufacturers()
        {
            return new List<string>(new[] { "Audi", "Mercedes", "BMW" });
        }

        public static IList<KeyValuePair<Guid, string>> GetVehicles()
        {
            return new List<KeyValuePair<Guid, string>>(){
                new KeyValuePair<Guid, string>(new Guid(), "Cayenne Coupe E-Hybrid (2020-)"),
                new KeyValuePair<Guid, string>(new Guid(), "Cayenne E-Hybrid (2020-)"),
                new KeyValuePair<Guid, string>(new Guid(), "Panamera Sport Turismo 4 E-Hybrid (2020-)"),
                new KeyValuePair<Guid, string>(new Guid(), "Taycan Plus (2021-)"),
                new KeyValuePair<Guid, string>(new Guid(), "Cayenne Coupe Turbo S E-Hybrid (2020-)")
            };
        }

        public static IList<AddressData> GetAddresses()
        {
            return new List<AddressData>
            {
                new AddressData
                {
                    AddressLine1 = "36 London Street",
                    City = "Edinburgh",
                    Country = "United Kingdom",
                    CountryCode = "GB",
                    PostalCode = "EH3 6NB",
                    Region = "Edinburgh",
                    SiteName = "Home Hub"
                }
            };
        }

        public static List<VehicleData> GetVehicleData()
        {
            return new List<VehicleData>
            {
                new VehicleData
                {
                    Manufacturer = "Mercedes",
                    ManufacturerOfficialEVRange = 3,
                    ManufacturingYear = 2021,
                    Model = "Maybach S-Class",
                    RealEVRange = 3
                }
            };
        }

        public static List<object> GetUserAccountResponses()
        {
            return new List<object>
            {
                new
                {
                    FirstName = "Current",
                    LastName = "User",
                    Email = "current_user@yopmail.com",
                    IsGuestUser = false
                },
                new
                {
                    InvitationId = Guid.NewGuid(),
                    FirstName = "UserWith",
                    LastName = "AdminRights",
                    Email = "UserWith_AdminRights@yopmal.com",
                    IsSelectedAccount = false,
                    IsGuestUser = true
                }
            };
        }
    }
}
