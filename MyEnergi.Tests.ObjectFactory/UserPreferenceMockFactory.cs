﻿using System;
using MyEnergi.Business.Model.Models;
using MyEnergi.Data.Entity;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class UserPreferenceMockFactory
    {
        public static UserPreference GetEmptyUserPreference()
        {
            return new UserPreference();
        }

        public static UserPreference WithId(this UserPreference userPreference, int id)
        {
            userPreference.Id = id;
            return userPreference;
        }

        public static UserPreference WithGuid(this UserPreference userPreference, Guid guid)
        {
            userPreference.Guid = guid;
            return userPreference;
        }

        public static UserPreference WithDateFormat(this UserPreference userPreference, string dateFormat)
        {
            userPreference.DateFormat = dateFormat;
            return userPreference;
        }

        public static UserPreference WithIsMetric(this UserPreference userPreference, bool isMetric)
        {
            userPreference.IsMetric = isMetric;
            return userPreference;
        }

        public static UserPreference WithLanguage(this UserPreference userPreference, string language)
        {
            userPreference.Language = language;
            return userPreference;
        }

        public static UserPreference WithCurrency(this UserPreference userPreference, string currency)
        {
            userPreference.Currency = currency;
            return userPreference;
        }
        
        public static UserPreference WithLastUpdate(this UserPreference userPreference, DateTime? lastUpdate = null)
        {
            userPreference.LastUpdate = lastUpdate ?? DateTime.UtcNow;
            return userPreference;
        }
        
        public static UserPreference GetDefaultUserPreference()
        {
            var id = new Random().Next(0, int.MaxValue);

            return GetEmptyUserPreference()
                .WithId(id)
                .WithGuid(Guid.NewGuid())
                .WithCurrency("GBP")
                .WithDateFormat("dd/MM/yyyy")
                .WithIsMetric(false)
                .WithLanguage("en-GB")
                .WithLastUpdate();
        }

        public static UserPreferenceData GetDefaultUserPreferenceData()
        {
            return new UserPreferenceData
            {
                Currency = "GBP",
                DateFormat = "dd/MM/yyyy",
                IsMetric = false,
                Language = "en-GB"
            };
        }
    }
}
