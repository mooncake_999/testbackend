﻿using MyEnergi.AppServer.Model.Email;
using MyEnergi.Business.Model.Models;
using System;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class UserRegistrationMockFactory
    {
        public static MailConfigurationData GetEmailConfiguration()
        {
            var baseDirectory = AppContext.BaseDirectory.Substring(0,
                AppContext.BaseDirectory.IndexOf("MyEnergi", StringComparison.Ordinal));
            return new MailConfigurationData
            {
                FilePath = baseDirectory + "Documents\\registrationConfirmation.html",
                MailSubject = "Registration completed successfully!!",
                MailAddress = "noreply@myenergi.com",
                Sender = "myenergi Customer Account"
            };
        }

        public static UserRegistrationData GetDefaultUserRegistrationData()
        {
            var user = UserMockFactory.GetNewFullUser();
            var token = MockHelper.GenerateJwtToken(user);
            return new UserRegistrationData()
            {
                Email = "testUser@gmail.com",
                UserCognitoId = token,
                PrivacyAcceptance = true,
                MarketingOptIn = false,
                ThirdPartyMarketingOptIn = false,
                Password = "TestPassword2000"
            };
        }

        public static UserRegistrationData GetModifiedUserPreferencesData()
        {
            return new UserRegistrationData
            {
                PrivacyAcceptance = true,
                MarketingOptIn = true,
                ThirdPartyMarketingOptIn = true
            };
        }

        public static UserRegistrationData GetModifiedEmailUserPreferencesData()
        {
            return new UserRegistrationData
            {
                Email = "testUser2@gmail.com"
            };
        }
    }
}
