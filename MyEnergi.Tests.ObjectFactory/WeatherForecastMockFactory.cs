﻿using MyEnergi.AppServer.Model.Weather;
using System;
using System.Collections.Generic;

namespace MyEnergi.Tests.ObjectFactory
{
    public static class WeatherForecastMockFactory
    {
        public static WeatherForecastData WithDateTime(this WeatherForecastData weatherForecast, DateTime dateTime)
        {
            weatherForecast.DateTime = dateTime;
            return weatherForecast;
        }

        public static WeatherForecastData WithWindDirection(this WeatherForecastData weatherForecast,
            string windDirection)
        {
            weatherForecast.WindDirection = windDirection;
            return weatherForecast;
        }

        public static WeatherForecastData WithWindSpeed(this WeatherForecastData weatherForecast, double? windSpeed)
        {
            weatherForecast.WindSpeed = windSpeed;
            return weatherForecast;
        }

        public static WeatherForecastData WithWeatherIconCode(this WeatherForecastData weatherForecast,
            string weatherIconCode)
        {
            weatherForecast.WeatherIconCode = weatherIconCode;
            return weatherForecast;
        }

        public static WeatherForecastData WithWeatherDescription(this WeatherForecastData weatherForecast,
            string weatherDescription)
        {
            weatherForecast.WeatherDescription = weatherDescription;
            return weatherForecast;
        }

        public static WeatherForecastData WithTemperature(this WeatherForecastData weatherForecast, double? temperature)
        {
            weatherForecast.Temperature = temperature;
            return weatherForecast;
        }

        public static DailyWeatherForecastData GetNewDailyWeatherForecast()
        {
            return new DailyWeatherForecastData();
        }

        public static DailyWeatherForecastData WithDateToDisplay(this DailyWeatherForecastData dailyWeather,
            string dateToDisplay)
        {
            dailyWeather.DateToDisplay = dateToDisplay;
            return dailyWeather;
        }

        public static DailyWeatherForecastData WithDailyMinTemp(this DailyWeatherForecastData dailyWeather,
            double? dailyMinTemp)
        {
            dailyWeather.DailyMinTemp = dailyMinTemp;
            return dailyWeather;
        }

        public static DailyWeatherForecastData WithDailyMaxTemp(this DailyWeatherForecastData dailyWeather,
            double? dailyMaxTemp)
        {
            dailyWeather.DailyMaxTemp = dailyMaxTemp;
            return dailyWeather;
        }

        public static HourlyWeatherForecastData GetNewHourlyWeatherForecastData()
        {
            return new HourlyWeatherForecastData();
        }

        public static HourlyWeatherForecastData WithSolarRadiation(this HourlyWeatherForecastData hourlyWeather,
            double? solarRadiation)
        {
            hourlyWeather.SolarRadiation = solarRadiation;
            return hourlyWeather;
        }

        public static DailyWeatherForecastData GetDailyWeatherForecastData()
        {
            return (DailyWeatherForecastData)GetNewDailyWeatherForecast()
                .WithDateToDisplay("Tue 13th")
                .WithDailyMinTemp(1.1d)
                .WithDailyMaxTemp(11.5d)
                .WithDateTime(new DateTime(2021, 04, 13))
                .WithWindDirection("SW")
                .WithWindSpeed(1.74045d)
                .WithWeatherIconCode("804")
                .WithWeatherDescription("Overcast clouds")
                .WithTemperature(8.2d);
        }

        public static HourlyWeatherForecastData GetHourlyWeatherForecastData(int hour)
        {
            return (HourlyWeatherForecastData)GetNewHourlyWeatherForecastData()
                .WithSolarRadiation(165.695d)
                .WithDateTime(new DateTime(2021, 04, 13, hour, 0, 0))
                .WithWindDirection("NW")
                .WithWindSpeed(2.82797d)
                .WithWeatherIconCode("804")
                .WithWeatherDescription("Overcast clouds")
                .WithTemperature(11.5d);
        }

        public static ICollection<object> GetForecastData()
        {
            var dailyForecastData = GetDailyWeatherForecastData();
            var hourlyForecasts = GetHourlyWeatherForecastForOneDay();

            var result = new List<object>
            {
                new
                {
                    dailyForecastData,
                    hourlyForecastData = hourlyForecasts
                }
            };

            return result;
        }

        public static List<HourlyWeatherForecastData> GetHourlyWeatherForecastForOneDay()
        {
            var hourlyForecasts = new List<HourlyWeatherForecastData>();
            for (var hour = 0; hour < 24; hour++)
            {
                var hourlyForecast = GetHourlyWeatherForecastData(hour);
                hourlyForecasts.Add(hourlyForecast);
            }

            return hourlyForecasts;
        }
    }
}
