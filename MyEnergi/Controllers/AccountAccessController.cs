﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using MyEnergi.Business.Model.Models.AccountAccess;
using Newtonsoft.Json;
using Serilog;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class AccountAccessController : ControllerBase
    {
        private readonly IAccountAccessShareBL _accountAccessShareBl;
        private readonly IAccountAccessHubBL _accountAccessHubBl;

        public AccountAccessController(IAccountAccessShareBL accountAccessShareBl,
            IAccountAccessHubBL accountAccessHubBl)
        {
            _accountAccessShareBl = accountAccessShareBl;
            _accountAccessHubBl = accountAccessHubBl;
        }

        [HttpGet("Hubs")]
        public async Task<IActionResult> GetHubs()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called AccountAccess/hubs");

            var hubs = await _accountAccessHubBl.GetHubs(userCognitoId);

            var responseModel = new ResponseModel(new { hubs });
            return Ok(responseModel);
        }

        [HttpPut("Hubs/TransferOwnership")]
        public async Task<IActionResult> TransferOwnership(TransferOwnershipRequest request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called TransferOwnership with {JsonConvert.SerializeObject(request)}");

            await _accountAccessHubBl.TransferHubOwnership(request.SerialNo, request.Email, userCognitoId);

            var response = new ResponseModel("Ownership was successfully transferred!");
            return Ok(response);
        }

        [HttpPut("Hubs/Migrate")]
        public async Task<IActionResult> MigrateHubs(MigrateHubsRequest request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called MigrateHubs with {JsonConvert.SerializeObject(request)}");

            var hubs = await _accountAccessHubBl.MigrateHubs(request.Hubs, userCognitoId);

            var responseModel = new ResponseModel(new { hubs });
            return Ok(responseModel);
        }

        [HttpPost("Hubs/Register")]
        public async Task<IActionResult> RegisterNewHub(RegisterNewHubRequest request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());

            Log.Information(
                $"User {userCognitoId} called RegisterNewHub with params: {JsonConvert.SerializeObject(request)}.");

            var responseModel =
                await _accountAccessHubBl.RegisterNewHub(request.SerialNo, request.RegistrationCode, userCognitoId);

            return Ok(responseModel);
        }

        [HttpPost("Hubs/Shares/Create")]
        public async Task<IActionResult> ShareHub(ShareHubRequest request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called ShareHub with params: {JsonConvert.SerializeObject(request)}.");

            await _accountAccessShareBl.ShareHub(userCognitoId, request.SerialNo, request.Email);

            var response = new ResponseModel("Successful sharing.");
            return Ok(response);
        }

        [HttpPut("Hubs/Shares/Revoke")]
        public async Task<IActionResult> RevokeShare(ShareHubRequest request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called StopShare with params: {request.SerialNo}, {request.Email}.");

            await _accountAccessShareBl.RevokeShare(userCognitoId, request.Email, request.SerialNo);

            var responseModel = new ResponseModel($"Hubs sharing successfully revoked for {request.Email}.");
            return Ok(responseModel);
        }

        [HttpPut("Hubs/Shares/Accept")]
        public async Task<IActionResult> AcceptInvite(InvitationShareRequest request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called AcceptInvite with params: {request.SerialNo}.");

            var result = await _accountAccessShareBl.AcceptInvitation(userCognitoId, request.SerialNo);

            var responseModel = new ResponseModel(result);
            return Ok(responseModel);
        }

        [HttpPut("Hubs/Shares/Reject")]
        public async Task<IActionResult> RejectInvitation(InvitationShareRequest request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called RejectInvitation/{request.SerialNo}");

            await _accountAccessShareBl.RejectInvitation(userCognitoId, request.SerialNo);

            var response = new ResponseModel("Invitation was successfully rejected!");
            return Ok(response);
        }
    }
}
