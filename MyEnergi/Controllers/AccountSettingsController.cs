﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class AccountSettingsController : ControllerBase
    {
        private readonly IAccountSettingsDeviceBL _accountSettingsDeviceBl;
        private readonly IAccountSettingsShareBL _accountSettingsShareBl;
        private readonly IHubBL _hubBl;
        private readonly IDeviceBL _deviceBl;

        public AccountSettingsController(IAccountSettingsDeviceBL accountSettingsDeviceBl,
            IAccountSettingsShareBL accountSettingsShareBl, IHubBL hubBL, IDeviceBL deviceBL)
        {
            _accountSettingsDeviceBl = accountSettingsDeviceBl;
            _accountSettingsShareBl = accountSettingsShareBl;
            _hubBl = hubBL;
            _deviceBl = deviceBL;
        }

        [HttpGet("GetDeviceStatus")]
        public async Task<IActionResult> GetDeviceStatus()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetDeviceStatus.");

            var deviceStatus = await _accountSettingsDeviceBl.GetAllDeviceStatus(userCognitoId);

            var response = new ResponseModel(deviceStatus);
            return Ok(response);
        }

        [HttpGet("GetDeviceStatusOfHub")]
        public async Task<IActionResult> GetDeviceStatusOfHub(Guid hubId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetDeviceStatusOfHub for hub {hubId}.");

            var deviceStatus = hubId == default
                ? await _accountSettingsDeviceBl.GetPhantomDeviceStatus(userCognitoId)
                : await _accountSettingsDeviceBl.GetDeviceStatusOfHub(userCognitoId, hubId);

            var response = new ResponseModel(deviceStatus);
            return Ok(response);
        }

        [HttpPut("SendInvitation")]
        public async Task<IActionResult> SendInvitation(UserPermissionData request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called SendInvitation with: {JsonConvert.SerializeObject(request)}.");

            var invitationId = await _accountSettingsShareBl.SendInvitation(userCognitoId, request);

            var response = new ResponseModel(invitationId, $"Invitation sent successfully to {request.Email}.");
            return Ok(response);
        }

        [HttpPut("ResendInvitationEmail")]
        public async Task<IActionResult> ResendInvitationEmail(ResendInvitationEmailData request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called ResendInvitation with: {JsonConvert.SerializeObject(request)}.");

            await _accountSettingsShareBl.ResendInvitationEmail(userCognitoId, request);

            var response = new ResponseModel($"Invitation resend successfully to {request.Email}.", true);
            return Ok(response);
        }

        [HttpDelete("DeleteInvitation")]
        public async Task<IActionResult> DeleteInvitation(Guid invitationId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called DeleteInvitation with InvitationId={invitationId}");

            await _accountSettingsShareBl.DeleteInvitation(userCognitoId, invitationId);

            var response = new ResponseModel("Invitation successfully deleted!", true);
            return Ok(response);
        }

        [HttpPut("EditPermissions")]
        public async Task<IActionResult> EditPermissions(UserPermissionData request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called EditPermissions with {JsonConvert.SerializeObject(request)}");

            await _accountSettingsShareBl.EditPermissions(userCognitoId, request);

            var response = new ResponseModel("Permissions edited successfully!", true);
            return Ok(response);
        }

        [HttpGet("GetInvitations")]
        public async Task<IActionResult> GetInvitations()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetInvitations");

            var invitations = await _accountSettingsShareBl.GetInvitations(userCognitoId);

            var response = new ResponseModel(invitations);
            return Ok(response);
        }

        [HttpGet("GetUserPermissions")]
        public async Task<IActionResult> GetUserPermissions(Guid invitationId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetUserPermissions for invitationId={invitationId}");

            var invitations = await _accountSettingsShareBl.GetUserPermissions(userCognitoId, invitationId);

            var response = new ResponseModel(invitations);
            return Ok(response);
        }

        [HttpGet("GuestPermissions")]
        public async Task<IActionResult> GetGuestPermissions(Guid invitationId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GuestPermissions");

            var invitations = await _accountSettingsShareBl.GetGuestPermissions(userCognitoId, invitationId);

            var response = new ResponseModel(invitations);
            return Ok(response);
        }

        [HttpPut("SuspendAccess")]
        public async Task<IActionResult> SuspendAccess(Guid invitationId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called SuspendAccess with invitationId={invitationId}");

            await _accountSettingsShareBl.SuspendAccess(userCognitoId, invitationId);

            var response = new ResponseModel("Successfully suspended access for the guest user!", true);
            return Ok(response);
        }

        [HttpPut("AcceptInvitation")]
        public async Task<IActionResult> AcceptInvitation(Guid invitationId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called AcceptInvitation with invitationId={invitationId}");

            await _accountSettingsShareBl.AcceptInvitation(userCognitoId, invitationId);

            var response = new ResponseModel("Invitation accepted successfully!", true);
            return Ok(response);
        }

        [HttpDelete("RejectInvitation")]
        public async Task<IActionResult> RejectInvitation(Guid invitationId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called RejectInvitation with invitationId={invitationId}");

            await _accountSettingsShareBl.RejectInvitation(userCognitoId, invitationId);

            var response = new ResponseModel("Invitation rejected successfully!", true);
            return Ok(response);
        }

        [HttpPost("DeleteHub")]
        public async Task<IActionResult> DeleteHub(Guid hubId)
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);
            
            Log.Information($"User {userCognitoId} called DeleteHub for hub {hubId}.");

            await _hubBl.DeleteHub(hubId, userCognitoId);

            var response = new ResponseModel("Hub deleted successfully!");
            return Ok(response);
        }

        [HttpPost("DeleteDevice")]
        public async Task<IActionResult> DeleteDevice(Guid deviceId)
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);
            
            Log.Information($"User {userCognitoId} called DeleteDevice for Device {deviceId}.");

            await _deviceBl.DeleteDevice(deviceId, userCognitoId);

            var response = new ResponseModel("Device deleted successfully!");
            return Ok(response);
        }
        
        [HttpGet("GenerateAPIKey")]
        public async Task<IActionResult> GenerateApiKey(Guid hubId)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GenerateApiKey for hub {hubId}.");

            var apiKey = await _hubBl.GenerateAPIKey(hubId, userCognitoId);

            var response = new ResponseModel(apiKey);
            return Ok(response);
        }

        [HttpPut("CheckAPIKey")]
        public async Task<IActionResult> CheckAPIKey(CheckApiKeyData checkApiKeyData)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called CheckAPIKey with {JsonConvert.SerializeObject(checkApiKeyData)}.");

            var result = await _hubBl.CheckIsAPIKeyAndLastUpdate(checkApiKeyData.HubIds, userCognitoId);

            var response = new ResponseModel(result);
            return Ok(response);
        }

        [HttpGet("UserAccounts")]
        public async Task<IActionResult> GetUserAccounts()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called UserAccounts.");

            var result = await _accountSettingsShareBl.GetUserAccounts(userCognitoId);

            var response = new ResponseModel(result);
            return Ok(response);
        }

        [HttpPut("SetSelectedAccount")]
        public async Task<IActionResult> SetSelectedAccount(Guid invitationId = default)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called SetSelectedAccount for Invitation {invitationId}.");

            await _accountSettingsShareBl.SetSelectedAccount(userCognitoId, invitationId);
            
            var response = new ResponseModel("Account selected successfully!");
            return Ok(response);
        }
    }
}
