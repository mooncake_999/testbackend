﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Newtonsoft.Json;
using Serilog;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class ChargeSchedulesController : ControllerBase
    {
        private readonly IChargeSchedulesBL _chargeSchedules;
        private readonly IEnergyProviderBL _energyBl;

        public ChargeSchedulesController(IChargeSchedulesBL chargeSchedules, IEnergyProviderBL energyBl)
        {
            _chargeSchedules = chargeSchedules;
            _energyBl = energyBl;
        }

        [HttpGet("GetAllSchedules")]
        public async Task<IActionResult> GetAllSchedules()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetAllSchedules.");

            var allSchedules = await _chargeSchedules.GetAllSchedules(userCognitoId);

            var response = new ResponseModel(allSchedules);
            return Ok(response);
        }

        [HttpPost("SaveSchedules")]
        public async Task<IActionResult> SaveSchedules(ChargeScheduleRequestData chargeSchedule)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called SaveSchedules with params: {JsonConvert.SerializeObject(chargeSchedule)}.");

            var chargeScheduleRequestData = await _chargeSchedules.SaveSchedules(chargeSchedule, userCognitoId);
            await _energyBl.ProgramDevicesOnDemand(chargeSchedule.DeviceId, userCognitoId);

            var response = new ResponseModel(chargeScheduleRequestData);
            return Ok(response);
        }

        [HttpPost("SetIsActiveSchedule")]
        public async Task<IActionResult> SetIsActiveSchedule(ChargeScheduleRequestData requestData)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called SetIsActiveSchedule with params: {JsonConvert.SerializeObject(requestData)}.");

            await _chargeSchedules.SetIsActiveSchedule(requestData.DeviceId, requestData.ScheduleType, requestData.IsActive,
                userCognitoId);
            await _energyBl.ProgramDevicesOnDemand(requestData.DeviceId, userCognitoId);

            var response = new ResponseModel($"Schedules set to {requestData.IsActive}.", true);
            return Ok(response);
        }
    }
}
