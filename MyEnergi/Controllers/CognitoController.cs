﻿using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MyEnergi.Business.Model.Models;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CognitoController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public CognitoController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost("SignIn")]
        public async Task<IActionResult> SignIn(UserRegistrationData data)
        {
            var providerClient = new AmazonCognitoIdentityProviderClient(new Amazon.Runtime.AnonymousAWSCredentials(),
                Amazon.RegionEndpoint.EUWest2);
            var clientId = _configuration["AWSCognito:ClientId"].Split(',').First();

            var authReq = new InitiateAuthRequest
            {
                ClientId = clientId,
                AuthFlow = AuthFlowType.USER_PASSWORD_AUTH,
            };
            authReq.AuthParameters.Add("USERNAME", data.Email);
            authReq.AuthParameters.Add("PASSWORD", data.Password);

            var authResp = await providerClient.InitiateAuthAsync(authReq);
            var token = authResp.AuthenticationResult.IdToken;

            var response = new ResponseModel("User successfully logged in.", true);
            var result = new { response, Token = token };
            return Ok(result);
        }
    }
}
