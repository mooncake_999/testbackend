﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Newtonsoft.Json;
using Serilog;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class DeviceInstallationFeedbackController : ControllerBase
    {
        private readonly IInstallerBL _installerBl;
        private readonly IUserBL _userBl;

        public DeviceInstallationFeedbackController(IInstallerBL installerBl, IUserBL userBl)
        {
            _userBl = userBl;
            _installerBl = installerBl;
        }

        [HttpGet("GetInstallersByCountry")]
        public async Task<IActionResult> GetInstallersByCountry(string country)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetInstallersByCountry with params: {country}.");

            await _userBl.ValidateThatUserExists(userCognitoId);
            var installers = await _installerBl.GetInstallersByCountry(country);

            var response = new ResponseModel(installers);
            return Ok(response);
        }

        [HttpGet("GetInstallerByCode")]
        public async Task<IActionResult> GetInstallerByCode(string code)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetInstallerByCode with params: {code}.");

            await _userBl.ValidateThatUserExists(userCognitoId);
            var installer = await _installerBl.GetInstallerByCode(code);

            var response = new ResponseModel(installer);
            return Ok(response);
        }

        [HttpPost("AddInstallationFeedback")]
        public async Task<IActionResult> AddInstallationFeedback(IList<InstallationFeedbackData> feedbackDatas)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called AddInstallationFeedback with params: {JsonConvert.SerializeObject(feedbackDatas)}.");

            await _installerBl.AddNewInstallationFeedback(feedbackDatas, userCognitoId);

            var response = new ResponseModel("New installation feedback added.", true);
            return Ok(response);
        }
    }
}
