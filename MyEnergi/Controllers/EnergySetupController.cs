﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class EnergySetupController : ControllerBase
    {
        private readonly IEnergyProviderBL _energyBl;
        private readonly IDualTariffEnergyProviderBL _dualTariffEnergyProviderBl;
        private readonly IUserBL _userBl;
        private readonly IAccountSettingsShareBL _accountSettingsShareBl;
        private readonly IConfiguration _configuration;

        public EnergySetupController(IEnergyProviderBL energyBl, IDualTariffEnergyProviderBL dualTariffEnergyProviderBl,
            IUserBL userBl, IAccountSettingsShareBL accountSettingsShareBl, IConfiguration configuration)
        {
            _userBl = userBl;
            _accountSettingsShareBl = accountSettingsShareBl;
            _energyBl = energyBl;
            _dualTariffEnergyProviderBl = dualTariffEnergyProviderBl;
            _configuration = configuration;
        }

        [HttpPost("AddNewEnergySetup")]
        public async Task<IActionResult> AddNewEnergySetup(List<EnergyFormRequestData> energySetupList)
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);

            Log.Information(
                $"User {userCognitoId} called AddNewEnergySetup with params: {JsonConvert.SerializeObject(energySetupList)}.");

            await _energyBl.SaveEnergySetupList(userCognitoId, energySetupList);
            await _energyBl.SaveEnergyPrices(userCognitoId, true);

            var response = new ResponseModel("New energy setup added.", true);
            return Ok(response);
        }

        [HttpGet("GetEnergySetups")]
        public async Task<IActionResult> GetEnergySetups()
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);

            Log.Information($"User {userCognitoId} called GetEnergySetups.");

            var energySetups = await _energyBl.GetEnergySetups(userCognitoId);

            var response = new ResponseModel(energySetups);
            return Ok(response);
        }

        [HttpGet("GetEnergyProviders")]
        public async Task<IActionResult> GetEnergyProviders()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetEnergyProviders.");

            var entsoeIds = _configuration["UserIdEnableEntsoe:Id"].Split(',').Where(x => !string.IsNullOrEmpty(x));
            var userId = await _userBl.ValidateThatUserExistsAndGetId(userCognitoId);
            bool enableEntsoe = entsoeIds.Contains(userId.ToString()) || !entsoeIds.Any();
            var energyProviders = await _energyBl.GetUsedEnergyProviders(enableEntsoe);

            var response = new ResponseModel(energyProviders);
            return Ok(response);
        }

        [HttpGet("GetUserEnergyAddresses")]
        public async Task<IActionResult> GetUserEnergyAddresses()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetUserEnergyAddresses.");

            var addresses = await _energyBl.GetUserEnergyAddresses(userCognitoId);

            var response = new ResponseModel(addresses);
            return Ok(response);
        }

        [HttpGet("GetBestPriceIntervalsGraph")]
        public async Task<IActionResult> GetBestPriceIntervalsGraph(Guid hubId)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetBestPriceIntervalsGraph with params: {hubId}.");

            var energyPriceList = await _energyBl.GetBestPriceIntervalsForGraphs(hubId, userCognitoId);

            var response = new ResponseModel(energyPriceList);
            return Ok(response);
        }

        [HttpPost("ValidateEnergySetupRemoval")]
        public async Task<IActionResult> ValidateEnergySetupRemoval(Guid energySetupId)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called ValidateEnergySetupRemoval with params: {energySetupId}.");

            await _energyBl.ValidateEnergySetupRemoval(energySetupId, userCognitoId);

            var response = new ResponseModel("Energy Setup can be removed.", true);
            return Ok(response);
        }

        [HttpGet("GetDualTariffEnergyPrices")]
        public async Task<IActionResult> GetDualTariffEnergyPrices(Guid hubId)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetDualTariffEnergyPrices.");

            var energyPrices = await _dualTariffEnergyProviderBl.GetDualTariffEnergyPrices(hubId, userCognitoId);

            var response = new ResponseModel(energyPrices);
            return Ok(response);
        }

        [HttpPost("SaveDualTariffEnergyPrices")]
        public async Task<IActionResult> SaveDualTariffEnergyPrices(List<DualTariffRequestData> dualTariffs)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called SaveDualTariffEnergyPrices with params: {JsonConvert.SerializeObject(dualTariffs)}.");

            await _dualTariffEnergyProviderBl.SaveDualTariffEnergyPrices(userCognitoId, dualTariffs);

            var response = new ResponseModel("Dual tariffs saved to DB.", true);
            return Ok(response);
        }
    }
}
