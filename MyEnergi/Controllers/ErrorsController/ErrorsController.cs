﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MyEnergi.Business.Model.Models;
using MyEnergi.Common.CustomExceptions;
using Serilog;
using System;
using System.Net;

namespace MyEnergi.Controllers.ErrorsController
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorsController : ControllerBase
    {
        [Route("Error")]
        public IActionResult Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context.Error;

            var httpStatusCode = exception switch
            {
                BadRequestException _ => HttpStatusCode.BadRequest,
                UnauthorizedAccessException _ => HttpStatusCode.Unauthorized,
                NotFoundException _ => HttpStatusCode.NotFound,
                _ => HttpStatusCode.InternalServerError
            };

            if (httpStatusCode != HttpStatusCode.InternalServerError)
            {
                Response.StatusCode = (int)httpStatusCode;
            }

            if (!exception.Message.Contains("npm ERR!"))
            {
                Log.Error(exception.Message);
            }

            var response = new ResponseModel(exception.Message, false);

            return StatusCode((int)httpStatusCode, response);
        }
    }
}
