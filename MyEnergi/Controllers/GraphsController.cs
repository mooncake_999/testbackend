﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Helpers;
using MyEnergi.Business.Model.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class GraphsController : ControllerBase
    {
        private readonly IGraphBL _graphBl;
        private readonly IHubBL _hubBl;
        private readonly IDeviceBL _deviceBl;

        public GraphsController(IGraphBL graphBl, IHubBL hubBl, IDeviceBL deviceBl)
        {
            _graphBl = graphBl;
            _hubBl = hubBl;
            _deviceBl = deviceBl;
        }

        // aduce date incepand cu timestamp specificat, de pe serverele myenergi
        [HttpGet("GetDataForDeviceGraph")]
        public async Task<IActionResult> GetDataForDeviceGraph(Guid hubId, long lastUpdate, bool isInitialLoad)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetDataForDeviceGraph for hub {hubId}.");

            var hub = await _hubBl.GetHubById(hubId, userCognitoId);
            var devices = await _deviceBl.GetDevicesByHubExceptHarvi(hubId);
            if (hub == null || !devices.Any())
            {
                return Ok(new ResponseModel());
            }

            var lastDateUtc = Helper.AdjustZoneToUtc(lastUpdate, hub.TimeZoneRegion);
            var lastDate = Helper.ConvertTimestampToDate(lastDateUtc).AddMinutes(1);

            var data = _graphBl.GetActiveDataForGraph(devices, lastDate);

            var content = _graphBl.ProcessDataForDeviceGraph(data, hub.TimeZoneRegion);
            var response = new ResponseModel(content);

            return Ok(response);
        }

        [HttpGet("GetDataForTotalGraph")]
        public async Task<IActionResult> GetDataForTotalGraph(Guid hubId, long lastUpdate, bool isInitialLoad)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetDataForTotalGraph for hub {hubId}.");

            var hub = await _hubBl.GetHubById(hubId, userCognitoId);
            var devices = await _deviceBl.GetDevicesByHubExceptHarvi(hubId);
            if (hub == null || !devices.Any())
            {
                return Ok(new ResponseModel());
            }

            var lastDateUtc = Helper.AdjustZoneToUtc(lastUpdate, hub.TimeZoneRegion);
            var lastDate = Helper.ConvertTimestampToDate(lastDateUtc).AddMinutes(1);

            var data = _graphBl.GetActiveDataForGraph(devices, lastDate);

            var content = _graphBl.ProcessDataForTotalGraph(data, hub.TimeZoneRegion);
            var response = new ResponseModel(content);

            return Ok(response);
        }

        [HttpPost("GetSpecificDataForTotalGraph")]
        public async Task<IActionResult> GetSpecificDataForTotalGraph(GraphSpecificDataRequest dataRequest)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetSpecificDataForTotalGraph.");
            var hub = await _hubBl.GetHubById(dataRequest.HubId, userCognitoId);

            var accumulateData = new List<KeyValuePair<long, IList<object>>>();

            dataRequest.DayTimestampsUtc.ForEach(startTimestamp =>
            {
                var startDate = Helper.ConvertTimestampToDate(startTimestamp);
                if (startDate.Hour == 23) startDate = startDate.AddHours(1);
                startTimestamp = Helper.ConvertToTimestamp(startDate);

                var dailyData = _graphBl.GetSpecificDataForGraph(dataRequest.HubId, startTimestamp, hub.TimeZoneRegion).Result;
                var processedData = _graphBl.ProcessDataForTotalGraph(dailyData, hub.TimeZoneRegion);

                accumulateData.Add(new KeyValuePair<long, IList<object>>(startTimestamp, processedData));
            });

            var response = new ResponseModel(accumulateData);

            return Ok(response);
        }

        // aduce date incepand cu lastUpdate pana la data cea mai recenta in Redis
        [HttpPost("GetSpecificDataForDeviceGraph")]
        public async Task<IActionResult> GetSpecificDataForDeviceGraph(GraphSpecificDataRequest dataRequest)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetSpecificDataForDeviceGraph.");
            var hub = await _hubBl.GetHubById(dataRequest.HubId, userCognitoId);

            var accumulateData = new List<KeyValuePair<long, IList<object>>>();

            dataRequest.DayTimestampsUtc.ForEach(startTimestamp =>
            {
                var startDate = Helper.ConvertTimestampToDate(startTimestamp);
                if (startDate.Hour == 23) startDate = startDate.AddHours(1);
                startTimestamp = Helper.ConvertToTimestamp(startDate);

                var dailyData = _graphBl.GetSpecificDataForGraph(dataRequest.HubId, startTimestamp, hub.TimeZoneRegion)
                    .Result;
                var processedData = _graphBl.ProcessDataForDeviceGraph(dailyData, hub.TimeZoneRegion);

                accumulateData.Add(new KeyValuePair<long, IList<object>>(startTimestamp, processedData));
            });

            var response = new ResponseModel(accumulateData);

            return Ok(response);
        }

        [HttpPost("GetSpecificDataForTotalGraphDST")]
        public async Task<IActionResult> GetSpecificDataForTotalGraphDST(GraphSpecificDataRequest dataRequest)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetSpecificDataForTotalGraph.");
            var hub = await _hubBl.GetHubById(dataRequest.HubId, userCognitoId);

            var accumulateData = new List<KeyValuePair<long, IList<object>>>();

            dataRequest.DayTimestampsUtc.ForEach(startTimestamp =>
            {
                var startDate = Helper.ConvertTimestampToDate(startTimestamp);
                if (startDate.Hour == 23) startDate = startDate.AddHours(1);
                startTimestamp = Helper.ConvertToTimestamp(startDate);

                var dailyData = _graphBl
                    .GetSpecificDataForGraphDST(dataRequest.HubId, startTimestamp, hub.TimeZoneRegion).Result;
                var processedData = _graphBl.ProcessDataForTotalGraph(dailyData, hub.TimeZoneRegion);

                accumulateData.Add(new KeyValuePair<long, IList<object>>(startTimestamp, processedData));
            });

            var response = new ResponseModel(accumulateData);

            return Ok(response);
        }

        [HttpPost("GetSpecificDataForTotalGraphDSTSpring")]
        public async Task<IActionResult> GetSpecificDataForTotalGraphDSTSpring(GraphSpecificDataRequest dataRequest)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetSpecificDataForTotalGraph.");
            var hub = await _hubBl.GetHubById(dataRequest.HubId, userCognitoId);

            var accumulateData = new List<KeyValuePair<long, IList<object>>>();

            dataRequest.DayTimestampsUtc.ForEach(startTimestamp =>
            {
                var startDate = Helper.ConvertTimestampToDate(startTimestamp);
                if (startDate.Hour == 23) startDate = startDate.AddHours(1);
                startTimestamp = Helper.ConvertToTimestamp(startDate);

                var dailyData = _graphBl
                    .GetSpecificDataForGraphDSTSpring(dataRequest.HubId, startTimestamp, hub.TimeZoneRegion).Result;
                var processedData = _graphBl.ProcessDataForTotalGraph(dailyData, hub.TimeZoneRegion);

                accumulateData.Add(new KeyValuePair<long, IList<object>>(startTimestamp, processedData));
            });

            var response = new ResponseModel(accumulateData);

            return Ok(response);
        }
    }
}
