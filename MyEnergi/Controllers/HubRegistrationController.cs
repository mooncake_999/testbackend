﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class HubRegistrationController : ControllerBase
    {
        private readonly IHubBL _hubBl;
        private readonly IDeviceBL _deviceBl;
        private readonly IAccountSettingsDeviceBL _deviceAccountSettingsBl;

        public HubRegistrationController(IHubBL hubBl, IDeviceBL deviceBl, IAccountSettingsDeviceBL deviceAccountSettingsBl)
        {
            _hubBl = hubBl;
            _deviceBl = deviceBl;
            _deviceAccountSettingsBl = deviceAccountSettingsBl;
        }

        [HttpPost("AddNewHub")]
        public async Task<IActionResult> AddNewHub(HubData hubData)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called AddNewHub with params: {JsonConvert.SerializeObject(hubData)}.");

            var response = await _hubBl.RegisterNewHub(hubData, userCognitoId);

            return Ok(response);
        }

        [HttpPost("AddNewDevice")]
        public async Task<IActionResult> AddNewDevice(List<DeviceData> deviceData)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called AddNewDevice with params: {JsonConvert.SerializeObject(deviceData)}.");

            deviceData.ForEach(d => { d.TimeZoneRegion = null; });
            await _deviceBl.AddNewDevice(deviceData, userCognitoId, true);

            var response = new ResponseModel("New device successfully added.", true);

            return Ok(response);
        }

        [HttpGet("GetNewDevices")]
        public async Task<IActionResult> GetNewDevices()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetNewDevices.");

            var newDevices = await _deviceAccountSettingsBl.GetNewDevices(userCognitoId);
            var devices = await _deviceBl.GetDevicesWithInstallers(newDevices);

            var response = new ResponseModel(devices);
            return Ok(response);
        }

        [HttpGet("GetUserAddedNewDevices")]
        public async Task<IActionResult> GetUserAddedNewDevices()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetUserAddedNewDevices.");

            var newDevices = (await _deviceBl.GetUserAddedNewDevices(userCognitoId)).ToList();
            var newHubs = await _hubBl.GetUserAddedNewHubs(userCognitoId);

            var devices = (await _deviceBl.GetDevicesWithInstallers(newDevices)).ToList();
            var deviceFeedback = await _hubBl.GetHubsWithInstallers(newHubs);
            devices.AddRange(deviceFeedback);

            var response = new ResponseModel(devices);
            return Ok(response);
        }

        [HttpGet("GetDevicesForInstallationFeedback")]
        public async Task<IActionResult> GetDevicesForInstallationFeedback()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetDevicesForInstallationFeedback.");

            var devices = await _deviceBl.GetDevicesWithInstallers(userCognitoId);

            var response = new ResponseModel(devices);
            return Ok(response);
        }

        [HttpPost("EditHub")]
        public async Task<IActionResult> EditHub(HubData hubData)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called EditHub with params: {JsonConvert.SerializeObject(hubData)}.");

            var response = await _hubBl.EditHub(hubData, userCognitoId);

            return Ok(response);
        }

        [HttpPost("RemoveHub")]
        public async Task<IActionResult> RemoveHub(HubData hubData)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called RemoveHub with params: {JsonConvert.SerializeObject(hubData)}.");

            await _hubBl.RemoveHub(hubData, userCognitoId);

            var response = new ResponseModel("Hub removed successfully!");
            return Ok(response);
        }

        [HttpPost("EditDevice")]
        public async Task<IActionResult> EditDevice(DeviceData deviceData)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called EditDevice with params: {JsonConvert.SerializeObject(deviceData)}.");

            var response = await _deviceBl.EditDevice(deviceData, userCognitoId);

            return Ok(response);
        }

        [HttpGet("GetUsersHubs")]
        public async Task<IActionResult> GetUsersHubs()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetUsersHubs.");

            var hubs = await _hubBl.GetHubsByUserId(userCognitoId);

            var response = new ResponseModel(hubs);
            return Ok(response);
        }

        [HttpGet("GetDeviceInfo")]
        public async Task<IActionResult> GetDeviceInfo(Guid id)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetDeviceInfo with params: {id}.");

            var deviceData = await _deviceBl.GetDeviceById(id, userCognitoId);

            var response = new ResponseModel(deviceData);
            return Ok(response);
        }

        [HttpGet("UpdateDevicesNickname")]
        public async Task<IActionResult> UpdateDevicesNickName()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called UpdateDevicesNickname.");

            await _deviceBl.UpdateDevicesNickname(userCognitoId);

            var response = new ResponseModel("Devices nicknames updated.", true);
            return Ok(response);
        }

        [HttpGet("GetUserHubsAndDevices")]
        public async Task<IActionResult> GetUserHubsAndDevices()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetUserHubsAndDevices.");

            var userHubsAndDevices = await _hubBl.GetUserHubsAndDevices(userCognitoId);

            var response = new ResponseModel(userHubsAndDevices);
            return Ok(response);
        }

        /// <summary>
        /// For ZOOSH Alexa integration MP-630
        /// </summary>
        /// <returns>Hub information based on user cognito id</returns>
        [HttpGet("GetUserHubInfo")]
        public async Task<IActionResult> GetUserHubInfo()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());

            var response = await _hubBl.GetHubSerialNoPasswordByCognitoId(userCognitoId);
            return Ok(response);
        }
    }
}
