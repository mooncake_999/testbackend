﻿using Microsoft.AspNetCore.Mvc;
using MyEnergi.Business.Interfaces;
using MyEnergi.Common;
using MyEnergi.Common.CurrencyConverter;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PatchController : Controller
    {
        private readonly IHubBL _hubBl;
        private readonly IDualTariffEnergyProviderBL _dualTariffEnergyProviderBl;
        private readonly IEnergyProviderBL _energyProviderBL;
        private readonly ICurrencyConverter _currencyConverter;

        public PatchController(IHubBL hubBl, IDualTariffEnergyProviderBL dualTariffEnergyProviderBl, IEnergyProviderBL energyProviderBL, ICurrencyConverter currencyConverter)
        {
            _hubBl = hubBl;
            _dualTariffEnergyProviderBl = dualTariffEnergyProviderBl;
            _energyProviderBL = energyProviderBL;
            _currencyConverter = currencyConverter;
        }

        [HttpGet("TestDualTariff")]
        public void TestDualTariff(int timeToRun)
        {
            _dualTariffEnergyProviderBl.GenerateDualTariff(timeToRun);
        }
        [HttpGet("TestOctopus")]
        public void TestOctopus()
        {
            _energyProviderBL.GenerateOctopus();
        }

        [HttpGet("GetEntsoe")]
        public async Task GetEntsoe()
        {
            await _energyProviderBL.SaveEntsoeEnergyPrices();
        }

        [HttpGet("EncryptAllHubPassword")]
        public bool EncryptAllHubPassword()
        {
            _hubBl.PatchEncryptHubPsw();
            return true;
        }

        [HttpGet("AwsDecryptHubPassword")]
        public string AwsDecryptHubPassword(Psw password)
        {
            return KMSCipher.Decrypt(password.Password).Result;
        }

        [HttpGet("AwsEncryptHubPassword")]
        public string AwsEncryptHubPassword(Psw password)
        {
            return KMSCipher.Encrypt(password.Password).Result;
        }

        [HttpGet("ConvertCurrency")]
        public async Task<double> ConvertCurrency(double amount, string from, string to)
        {
            return await _currencyConverter.ConvertAsync(amount, from, to);
        }
    }
    // to be improved in the FUTURE!!!!
    public class Psw
    {
        public string Password { get; set; }
    }
}
