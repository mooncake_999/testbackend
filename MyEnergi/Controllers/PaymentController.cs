﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Serilog;
using System.Threading.Tasks;
using MyEnergi.Business.Model.Models.Stripe;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IStripePaymentBL _stripePaymentBl;

        public PaymentController(IStripePaymentBL stripePaymentBl)
        {
            _stripePaymentBl = stripePaymentBl;
        }

        [HttpPost("CreateSession")]
        public async Task<IActionResult> CreateSession(ExtendedWarrantyPaymentRequestData request)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called CreateSession");

            var sessionId = await _stripePaymentBl.CreateSession(userCognitoId, request);

            var response = new ResponseModel(new { id = sessionId });
            return Ok(response);
        }

        [HttpGet("Status")]
        public async Task<IActionResult> GetPaymentStatus(string sessionId)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetPaymentStatus");

            var status = await _stripePaymentBl.GetPaymentStatus(userCognitoId, sessionId);

            var response = new ResponseModel(new { isPaymentCompleted = status });
            return Ok(response);
        }
    }
}
