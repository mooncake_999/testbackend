﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class PersonalDetailsController : Controller
    {
        private readonly IUserBL _userBl;
        private readonly IVehicleBL _vehicleBl;
        private readonly IAccountSettingsShareBL _accountSettingsShareBl;

        public PersonalDetailsController(IUserBL userBl, IVehicleBL vehicleBl, IAccountSettingsShareBL accountSettingsShareBl)
        {
            _userBl = userBl;
            _vehicleBl = vehicleBl;
            _accountSettingsShareBl = accountSettingsShareBl;
        }

        [HttpGet("GetPersonalDetails")]
        public async Task<IActionResult> GetPersonalDetails()
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);

            Log.Information($"User {userCognitoId} called GetPersonalDetails.");

            var personalDetails = await _userBl.GetPersonalDetails(userCognitoId);

            var response = new ResponseModel(personalDetails);
            return Ok(response);
        }

        [HttpGet("GetVehicleManufacturers")]
        public async Task<IActionResult> GetVehicleManufacturers()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetVehicleManufacturers.");

            await _userBl.ValidateThatUserExists(userCognitoId);
            var vehicleManufacturers = await _vehicleBl.GetAllManufacturers();

            var response = new ResponseModel(vehicleManufacturers);
            return Ok(response);
        }

        [HttpGet("GetManufacturerVehicleModels")]
        public async Task<IActionResult> GetManufacturerVehicleModels(string manufacturer)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetManufacturerVehicleModels.");

            await _userBl.ValidateThatUserExists(userCognitoId);
            var vehicles = await _vehicleBl.GetVehicleByManufacturer(manufacturer);

            var response = new ResponseModel(vehicles);
            return Ok(response);
        }

        [HttpGet("GetVehicleDetails")]
        public async Task<IActionResult> GetVehicleDetails(Guid vehicleId)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetVehicleDetails.");

            await _userBl.ValidateThatUserExists(userCognitoId);
            var vehicleDetails = await _vehicleBl.GetVehicleDetails(vehicleId);
            
            var response = new ResponseModel(vehicleDetails);
            return Ok(response);
        }

        [HttpPost("SaveUserDetails")]
        public async Task<IActionResult> SaveUserDetails(UserDetailsData userDetailsData)
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);

            Log.Information(
                $"User {userCognitoId} called SaveUserDetails with params: {JsonConvert.SerializeObject(userDetailsData)}.");

            await _userBl.SaveUserDetails(userCognitoId, userDetailsData);

            var response = new ResponseModel("User personal details saved.", true);
            return Ok(response);
        }

        [HttpGet("GetUsersAddresses")]
        public async Task<IActionResult> GetUsersAddresses()
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);

            Log.Information($"User {userCognitoId} called GetUsersAddresses.");

            var addresses = await _userBl.GetUsersAddresses(userCognitoId);

            var response = new ResponseModel(addresses);
            return Ok(response);
        }

        [HttpGet("GetUserVehicles")]
        public async Task<IActionResult> GetUserVehicles()
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);
            
            Log.Information($"User {userCognitoId} called GetUserVehicles.");

            var userVehicles = await _vehicleBl.GetUserVehicles(userCognitoId);
            
            var response = new ResponseModel(userVehicles);
            return Ok(response);
        }

        [HttpGet("GetAllVehicleData")]
        public async Task<IActionResult> GetAllVehicleData()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called GetAllVehicleData.");

            await _userBl.ValidateThatUserExists(userCognitoId);
            var allVehicles = await _vehicleBl.GetAllVehicles();

            var response = new ResponseModel(allVehicles);
            return Ok(response);
        }
    }
}