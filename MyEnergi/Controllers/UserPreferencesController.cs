﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Serilog;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserPreferencesController : ControllerBase
    {
        private readonly IUserPreferencesBL _userPreferencesBl;
        private readonly IAccountSettingsShareBL _accountSettingsShareBl;
        private readonly IConfiguration _configuration;

        public UserPreferencesController(IUserPreferencesBL userPreferencesBl, IAccountSettingsShareBL accountSettingsShareBl, IConfiguration configuration)
        {
            _userPreferencesBl = userPreferencesBl;
            _accountSettingsShareBl = accountSettingsShareBl;
            _configuration = configuration;
        }

        [HttpGet("GetDefaultPreferences")]
        public IActionResult GetDefaultPreferences()
        {
            var defaultUserPreferenceData = GetDefault();
            var response = new ResponseModel(defaultUserPreferenceData);
            return Ok(response);
        }

        [HttpGet("GetUserPreference")]
        public async Task<IActionResult> GetUserPreference()
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);

            Log.Information($"User {userCognitoId} called GetUserPreference.");

            var preference = await _userPreferencesBl.GetPreference(userCognitoId);

            var response = new ResponseModel(preference ?? GetDefault());
            return Ok(response);
        }

        public UserPreferenceData GetDefault()
        {
            return new UserPreferenceData
            {
                Currency = _configuration["DefaultSettings:Currency"],
                DateFormat = _configuration["DefaultSettings:DateFormat"],
                Language = _configuration["DefaultSettings:Language"],
                IsMetric = bool.Parse(_configuration["DefaultSettings:IsMetric"])
            };
        }
    }
}
