﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Newtonsoft.Json;
using Serilog;
using System.IO;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserRegistrationController : ControllerBase
    {
        private readonly IUserBL _userBl;
        private readonly IConfiguration _configuration;
        private readonly IRegistrationEmailSender _registrationEmailSender;

        public UserRegistrationController(IConfiguration configuration, IRegistrationEmailSender registrationEmailSender, IUserBL userBl)
        {
            _userBl = userBl;
            _configuration = configuration;
            _registrationEmailSender = registrationEmailSender;
        }

        [HttpPost("Registration")]
        public async Task<IActionResult> Registration(UserRegistrationData data)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called Registration with params: {JsonConvert.SerializeObject(data)}.");

            await _userBl.SaveUser(data);

            var response = new ResponseModel("User successfully registered.", true);
            return Ok(response);
        }

        [HttpPost("EditUserPreferences")]
        public async Task<IActionResult> EditUserPreferences(UserRegistrationData data)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called EditUserPreferences with params: {JsonConvert.SerializeObject(data)}.");

            await _userBl.EditUserPreferences(data, userCognitoId);

            var response = new ResponseModel("User preferences edited.", true);
            return Ok(response);
        }

        [HttpPost("EditUserMail")]
        public async Task<IActionResult> EditUserMail(UserRegistrationData data)
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information(
                $"User {userCognitoId} called EditUserMail with params: {JsonConvert.SerializeObject(data)}.");

            await _userBl.EditUserMail(data, userCognitoId);

            var response = new ResponseModel("User mail edited.", true);
            return Ok(response);
        }

        [HttpPost("RegistrationMail")]
        public async Task<IActionResult> RegistrationMail()
        {
            var userCognitoId =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            Log.Information($"User {userCognitoId} called RegistrationMail.");

            var filePath = Path.Combine(Directory.GetCurrentDirectory(),
                _configuration["RegistrationMailTemplate:Path"]);
            var sender = _configuration["EmailConfiguration:MailSender"];
            var mailSubject = _configuration["EmailConfiguration:MailSubject"];
            var mailAddress = _configuration["EmailConfiguration:MailAddress"];

            await _registrationEmailSender.SendRegistrationMail(userCognitoId, filePath, sender, mailAddress, mailSubject);

            var response = new ResponseModel("Registration mail successfully sent.", true);
            return Ok(response);
        }
    }
}
