﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using Serilog;
using System.Threading.Tasks;

namespace MyEnergi.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IWeatherBL _weatherBl;
        private readonly IAccountSettingsShareBL _accountSettingsShareBl;

        public WeatherController(IWeatherBL weatherBl, IAccountSettingsShareBL accountSettingsShareBl)
        {
            _weatherBl = weatherBl;
            _accountSettingsShareBl = accountSettingsShareBl;
        }

        [HttpGet("Forecast")]
        public async Task<IActionResult> GetForecast()
        {
            var cognitoIdFromHeader =
                Helpers.GetUserCognitoIdFromToken(Request.Headers[HeaderNames.Authorization].ToString());
            var userCognitoId = await _accountSettingsShareBl.GetInvitationOwnerCognitoId(cognitoIdFromHeader);
            
            Log.Information($"User {userCognitoId} called GetForecast");

            var weatherForecasts = await _weatherBl.GetForecastData(userCognitoId);

            var response = new ResponseModel(weatherForecasts);
            return Ok(response);
        }
    }
}
