﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MyEnergi.Core.Models;

namespace MyEnergi.ExternalAPIs
{
    public abstract class ApiCaller
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public AppApiCallResponse GetResult(string username, string password)
        {
            var url = GenerateApiUrl();

            var uri = new Uri(url);

            var credCache = new CredentialCache
            {
                {
                    new Uri(uri.GetLeftPart(UriPartial.Authority)),
                    "Digest",
                    new NetworkCredential(username, password)
                }
            };
            var httpClient = new HttpClient(new HttpClientHandler { Credentials = credCache });
            var answer = httpClient.GetAsync(uri).Result;

            var result = new AppApiCallResponse
            {
                HttpStatusCode = answer.StatusCode,
                HttpMessage = answer.ReasonPhrase
            };

            if (!answer.IsSuccessStatusCode) return result;

            return DeserializeJson(answer);
        }

        protected abstract string GenerateApiUrl();
        protected abstract AppApiCallResponse DeserializeJson(HttpResponseMessage response);
    }
}
