﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using MyEnergi.Core.Models;
using MyEnergi.ExternalAPIs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyEnergi.ExternalAPIs
{
    public class DeviceEnergyInfoForDay : ApiCaller
    {
        private readonly string _serialNo;
        private readonly int _year;
        private readonly int _month;
        private readonly int _day;

        public DeviceEnergyInfoForDay(string serialNo, string password, int year, int month, int day)
        {
            _serialNo = serialNo;
            _year = year;
            _month = month;
            _day = day;

            Username = serialNo;
            Password = password;
        }

        protected override string GenerateApiUrl()
        {
            var url = $"http://s{_serialNo[^1]}.myenergi.net/cgi-jday-{_year}-{_month}-{_day}";

            return url;
        }

        protected override AppApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            try
            {
                var data = (JObject) JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                //var devices = new List<DeviceData>();

                //foreach (var device in data[0]["eddi"].Value<JArray>())
                //{
                //    devices.Add(new DeviceData
                //    {
                //        DeviceType = "eddi",
                //        SerialNumber = device["sno"].Value<string>()
                //    });
                //}

                //result.Content = devices;

                return new AppApiCallResponse();
            }
            catch (Exception e)
            {
                var data = (JObject) JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new AppApiCallResponse
                {
                    Status = data?["status"].Value<int>() ?? -1,
                    StatusText = data?["statustext"].Value<string>() ?? ""
                };
            }
        }
    }
}
