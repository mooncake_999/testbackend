﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using MyEnergi.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyEnergi.ExternalAPIs
{
    public class ExistingHubStatus : ApiCaller
    {
        private readonly string _serialNo;

        public ExistingHubStatus(string serialNo, string password)
        {
            _serialNo = serialNo;

            Username = serialNo;
            Password = password;
        }

        protected override string GenerateApiUrl()
        {
            var url = $"http://s{_serialNo[^1]}.myenergi.net/cgi-jstatus-*";

            return url;
        }

        protected override AppApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            try
            {
                var data = (JArray) JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                var devices = new List<DeviceData>();

                foreach (var device in data[0]["eddi"].Value<JArray>())
                {
                    devices.Add(new DeviceData
                    {
                        DeviceType = "eddi",
                        SerialNumber = device["sno"].Value<string>()
                    });
                }

                foreach (var device in data[1]["zappi"].Value<JArray>())
                {
                    devices.Add(new DeviceData
                    {
                        DeviceType = "zappi",
                        SerialNumber = device["sno"].Value<string>()
                    });
                }

                foreach (var device in data[2]["harvi"].Value<JArray>())
                {
                    devices.Add(new DeviceData
                    {
                        DeviceType = "harvi",
                        SerialNumber = device["sno"].Value<string>()
                    });
                }

                return new AppApiCallResponse
                {
                    Content = devices,
                    Status = 0
                };
            }
            catch (Exception e)
            {
                var data = (JObject) JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new AppApiCallResponse
                {
                    Status = data?["status"].Value<int>() ?? -1,
                    StatusText = data?["statustext"].Value<string>() ?? ""
                };
            }
        }
    }
}
