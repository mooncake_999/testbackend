﻿using System.Linq;
using System.Net;
using System.Net.Http;
using MyEnergi.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyEnergi.ExternalAPIs
{
    public class KeyValueReader : ApiCaller
    {
        private readonly string _serialNo;
        private readonly string _key;

        public KeyValueReader(string username, string password, string serialNo, string key)
        {
            _serialNo = serialNo;
            _key = key;

            Username = username;
            Password = password;
        }

        protected override string GenerateApiUrl()
        {
            var url = $"https://s{_serialNo[^1]}.myenergi.net/cgi-get-app-key-{_key}";

            return url;
        }

        protected override AppApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            var result = new AppApiCallResponse
            {
                HttpStatusCode = response.StatusCode
            };

            if (!response.IsSuccessStatusCode) return result;

            try
            {
                var allData = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                if (allData == null) return new AppApiCallResponse {Status = -1};
                
                var hubsData = allData[$"H{_serialNo}"].Value<JArray>();
                if(hubsData == null) return new AppApiCallResponse { Status = -1 };

                var value = hubsData.SingleOrDefault(k => k["key"].Value<string>() == _key)["val"].Value<string>();

                return new AppApiCallResponse
                {
                    Content = value
                };
            }
            catch (JsonException e)
            {
                var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                return new AppApiCallResponse
                {
                    Status = data?["status"].Value<int>() ?? -1,
                    StatusText = data?["statustext"].Value<string>() ?? ""
                };
            }
        }
    }
}
