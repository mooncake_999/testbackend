﻿using System.Net;
using System.Net.Http;
using MyEnergi.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyEnergi.ExternalAPIs
{
    public class NewHubRegister : ApiCaller
    {
        private readonly string _serialNo;
        private readonly string _registrationCode;
        private readonly string _password;
        private readonly string _confirmationPassword;

        public NewHubRegister(string serialNo, string registrationCode, string password, string confirmationPassword)
        {
            _serialNo = serialNo;
            _registrationCode = registrationCode;
            _password = password;
            _confirmationPassword = confirmationPassword;

            Username = serialNo;
            Password = registrationCode;
        }

        protected override string GenerateApiUrl()
        {
            var url = $"https://s{_serialNo[^1]}.myenergi.net/cgi-jregister-{_registrationCode}&{_password}&{_confirmationPassword}";

            return url;
        }

        protected override AppApiCallResponse DeserializeJson(HttpResponseMessage response)
        {
            var result = new AppApiCallResponse
            {
                HttpStatusCode = response.StatusCode
            };

            if (!response.IsSuccessStatusCode) return result;

            var data = (JObject)JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

            return new AppApiCallResponse
            {
                Status = data?["status"].Value<int>() ?? -1,
                StatusText = data?["statustext"].Value<string>() ?? ""
            };
        }
    }
}
