﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

namespace MyEnergi
{
    public static class Helpers
    {
        public static string GetSecretHash(string username, string appClientId, string appSecretKey)
        {
            var dataString = username + appClientId;

            var data = Encoding.UTF8.GetBytes(dataString);
            var key = Encoding.UTF8.GetBytes(appSecretKey);

            return Convert.ToBase64String(HmacSHA256(data, key));
        }

        public static byte[] HmacSHA256(byte[] data, byte[] key)
        {
            using (var shaAlgorithm = new System.Security.Cryptography.HMACSHA256(key))
            {
                var result = shaAlgorithm.ComputeHash(data);
                return result;
            }
        }

        public static string GetUserCognitoIdFromToken(string token)
        {
            var accessToken = token.Replace("Bearer ", string.Empty);
            var handler = new JwtSecurityTokenHandler();
            var decodedToken = handler.ReadJwtToken(accessToken);
            return decodedToken.Claims.First(f => f.Type == "sub").Value;
        }
    }
}