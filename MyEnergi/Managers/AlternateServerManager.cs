﻿using System;
using MyEnergi.AppServer;
using MyEnergi.Common;
using MyEnergi.Data.DAL;
using Serilog;

namespace MyEnergi.Managers
{
    public class AlternateServerManager : IManager
    {
        private readonly IHubRepository _hubRepository;

        public AlternateServerManager(IHubRepository hubRepository)
        {
            _hubRepository = hubRepository;
        }

        public void SaveData()
        {
            foreach (var hubData in _hubRepository.GetAllHubs())
            {
                try
                {
                    if (hubData.HostServer == null)
                    {
                        hubData.HostServer = $"s{hubData.SerialNo[^1]}.myenergi.net";
                        _hubRepository.AddUpdateNewHub(hubData);
                        continue;
                    }

                    var result =
                        new HubAlternateServerName(hubData.HostServer).GetResultByDigestAuth(hubData.SerialNo,
                            Cipher.Decrypt(hubData.AppPassword));

                    if (result.Content == null) continue;

                    var alternateServer = result.Content.ToString();

                    if (alternateServer == hubData.HostServer) continue;

                    hubData.HostServer = alternateServer;
                    _hubRepository.AddUpdateNewHub(hubData);

                }
                catch (Exception e)
                {
                    Log.Error($"Something went wrong on updating server for hub {hubData.SerialNo}. {e}" );
                }
            }

            _hubRepository.CommitChanges();
        }
    }
}
