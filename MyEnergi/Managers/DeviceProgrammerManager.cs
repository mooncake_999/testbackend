﻿using Hangfire;
using Microsoft.Extensions.Configuration;
using MyEnergi.Business.Interfaces;
using Serilog;
using System;
using System.Threading.Tasks;

namespace MyEnergi.Managers
{
    public class DeviceProgrammerManager : IManager
    {
        private readonly IEnergyProviderBL _energyProviderBl;
        private IConfiguration Configuration { get; }
        private readonly IRecurringJobManager _recurringJob;

        public DeviceProgrammerManager(IEnergyProviderBL energyProviderBl, IConfiguration configuration, IRecurringJobManager recurringJob)
        {
            _energyProviderBl = energyProviderBl;
            Configuration = configuration;
            _recurringJob = recurringJob;
        }
        
        public async Task SaveData()
        {
            try
            {
                var result = await _energyProviderBl.SaveEnergyPrices(null, false);
                if (!result)
                {
                    var nextRunDate = DateTime.Now.ToUniversalTime().AddMinutes(30);
                    var cronJob = $"{nextRunDate.Minute} {nextRunDate.Hour} {nextRunDate.Day} {nextRunDate.Month} *";
                    _recurringJob.AddOrUpdate<DeviceProgrammerManager>("OCTOPUSAPI", x => x.SaveData(), cronJob);
                }
                else
                {
                    SetDefaultProgram();
                }
            }
            catch(Exception ex)
            {
                Log.Error("OCTOPUSAPI threw an error : " + ex.Message);
                SetDefaultProgram();
            }
        }

        private void SetDefaultProgram()
        {
            var jobConf = Configuration["HangfireJobSchedules:OCTOPUSAPI"];
            _recurringJob.AddOrUpdate<DeviceProgrammerManager>("OCTOPUSAPI", x => x.SaveData(), jobConf);
        }
    }
}
