﻿using MyEnergi.Business.Interfaces;
using System;
using System.Threading.Tasks;

namespace MyEnergi.Managers
{
    public class DualTariffManager : IManager
    {
        private readonly IDualTariffEnergyProviderBL _dualTariffEnergyProvider;

        public DualTariffManager(IDualTariffEnergyProviderBL dualTariffEnergyProvider)
        {
            _dualTariffEnergyProvider = dualTariffEnergyProvider;
        }

        public async Task SaveData()
        {
            var n = (int)DateTime.UtcNow.TimeOfDay.TotalMinutes;
            var a = (n / 15) * 15;
            var b = a + 15;
            var time = (n - a > b - n) ? b : a;
            await _dualTariffEnergyProvider.GenerateDualTariff(time + 15);
        }
    }
}
