﻿using Hangfire;
using Microsoft.Extensions.Configuration;
using MyEnergi.Business.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Managers
{
    public class ENTSOEManager : IManager
    {
        private readonly IEnergyProviderBL _energyProviderBl;
        private IConfiguration Configuration { get; }
        private readonly IRecurringJobManager _recurringJob;

        public ENTSOEManager (IEnergyProviderBL energyProviderBl, IConfiguration configuration, IRecurringJobManager recurringJob)
        {
            _energyProviderBl = energyProviderBl;
            Configuration = configuration;
            _recurringJob = recurringJob;
        }
        public async Task SaveData()
        {
            try
            {
                var result = await _energyProviderBl.SaveEntsoeEnergyPrices();
                if (!result)
                {
                    var nextRunDate = DateTime.Now.ToUniversalTime().AddMinutes(30);
                    var cronJob = $"{nextRunDate.Minute} {nextRunDate.Hour} {nextRunDate.Day} {nextRunDate.Month} *";
                    _recurringJob.AddOrUpdate<ENTSOEManager>("ENTSOEMANAGER", x => x.SaveData(), cronJob);
                }
                else
                {
                    SetDefaultProgram();
                }
            }
            catch (Exception ex)
            {
                Log.Error("ENTSOEMANAGER threw an error : " + ex.Message);
                SetDefaultProgram();
            }
        }

        private void SetDefaultProgram()
        {
            var jobConf = Configuration["HangfireJobSchedules:ENTSOEMANAGER"];
            _recurringJob.AddOrUpdate<ENTSOEManager>("ENTSOEMANAGER", x => x.SaveData(), jobConf);
        }
    }
}
