﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MyEnergi.AppServer.Loaders;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyEnergi.Managers
{
    public class ElectricVehicleManager : IManager
    {
        private readonly IVehicleBL _vehicleBl;
        private readonly IMapper _mapper;
        private readonly string _url;

        public ElectricVehicleManager(IConfiguration configuration, IVehicleBL vehicleBl, IMapper mapper)
        {
            _vehicleBl = vehicleBl;
            _mapper = mapper;
            _url = configuration["EVDatabase:Endpoint"];
        }

        public async Task SaveData()
        {
            var result = new ElectricVehicleLoader(_url).GetResult();
            var castContent = ((IList)result.Content).Cast<AppServer.Model.VehicleData>().ToList();

            var vehicles = _mapper.Map<List<AppServer.Model.VehicleData>, List<VehicleData>>(castContent);

            await _vehicleBl.UpdateVehiclesData(vehicles);

            await Task.FromResult(0);
        }
    }
}