﻿using Microsoft.Extensions.Configuration;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using System.IO;
using System.Threading.Tasks;

namespace MyEnergi.Managers
{
    public class EnergyProviderManager : IManager
    {
        private readonly IEnergyProviderBL _energyBl;
        private readonly string _filePath;

        public EnergyProviderManager(IConfiguration configuration, IEnergyProviderBL energyBl)
        {
            _energyBl = energyBl;
            _filePath = Path.Combine(Directory.GetCurrentDirectory(), configuration["EnergyProviderList:Path"]);
        }

        public async Task SaveData()
        {
            var csvParser = new CsvParser<EnergyProviderCSV>(_filePath);
            await _energyBl.SaveEnergyProviders(csvParser.Parse());
        }
    }
}