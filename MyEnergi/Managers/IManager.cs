﻿using System.Threading.Tasks;

namespace MyEnergi.Managers
{
    public interface IManager
    {
        Task SaveData();
    }
}