﻿using System.Threading.Tasks;
using MyEnergi.Business.Interfaces;

namespace MyEnergi.Managers
{
    public class InfoUpdateManager : IManager
    {
        private readonly IDeviceBL _deviceBl;

        public InfoUpdateManager(IDeviceBL deviceBl)
        {
            _deviceBl = deviceBl;
        }

        public async Task SaveData()
        {
            await _deviceBl.UpdateDeviceInfo();
        }
    }
}
