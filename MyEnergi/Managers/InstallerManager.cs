﻿using Microsoft.Extensions.Configuration;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.Models;
using System.IO;
using System.Threading.Tasks;

namespace MyEnergi.Managers
{
    public class InstallerManager : IManager
    {
        private readonly IInstallerBL _installerBl;
        private readonly string _filePath;

        public InstallerManager(IConfiguration configuration, IInstallerBL installerBl)
        {
            _installerBl = installerBl;
            _filePath = Path.Combine(Directory.GetCurrentDirectory(), configuration["InstallersList:Path"]);
        }

        public async Task SaveData()
        {
            var installers = new CsvParser<InstallerData>(_filePath).Parse();
            await _installerBl.SaveInstallers(installers);
        }
    }
}
