﻿using System.Collections.Generic;
using CsvHelper;
using System.Globalization;
using System.Linq;
using MyEnergi.Parsers;

namespace MyEnergi
{
    public class CsvParser<T> : Parser<T>
    {
        public CsvParser(string filePath) : base(filePath)
        {
        }

        public override IList<T> Parse()
        {
            var reader = GetResourceFromFile();
            var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csvReader.GetRecords<T>().ToList();

            return records;
        }
    }
}
