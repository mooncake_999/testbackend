﻿using System.Collections.Generic;

namespace MyEnergi.Parsers
{
    public interface IParser<T>
    {
        IList<T> Parse();
    }
}