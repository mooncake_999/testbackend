﻿using System.Collections.Generic;

namespace MyEnergi.Parsers
{
    public class JsonParser<T> : Parser<T>
    {
        public JsonParser(string apiUrl) : base(apiUrl, null)
        {   
        }

        public override IList<T> Parse()
        {
            var response = GetResourceFromApi();

            if (!response.IsSuccessStatusCode) return new List<T>();

            // Parse the response body.
            var content = response.Content.ReadAsStringAsync().Result; //Make sure to add a reference to System.Net.Http.Formatting.dll
            //var result = JsonConvert.DeserializeObject<IList<VehicleLookupData>>(JsonConvert.DeserializeObject<string>(content));

            return null;
        }
    }
}