﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace MyEnergi.Parsers
{
    public abstract class Parser<T> : IParser<T>
    {
        protected readonly string ApiUrl;
        protected readonly string Username;
        protected readonly string Password;
        protected readonly string FilePath;

        protected Parser(string filePath)
        {
            FilePath = filePath;
        }

        protected Parser(string apiUrl, string username = null, string password = null)
        {
            ApiUrl = apiUrl;
            Username = username;
            Password = password;
        }

        protected TextReader GetResourceFromFile()
        {
            TextReader reader = new StreamReader(FilePath, Encoding.GetEncoding("iso-8859-1"));
            return reader;
        }

        protected HttpResponseMessage GetResourceFromApi()
        {
            using var client = new HttpClient { BaseAddress = new Uri(ApiUrl) };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage response = client.GetAsync(string.Empty).Result;
                return response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public abstract IList<T> Parse();
    }
}
