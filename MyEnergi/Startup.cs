using AutoMapper;
using Hangfire;
using Hangfire.SqlServer;
using Hangfire.Storage;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MyEnergi.AppServer.EmailService.EmailSender;
using MyEnergi.AppServer.Interfaces;
using MyEnergi.AppServer.OneSignal;
using MyEnergi.Business;
using MyEnergi.Business.ApiCallerHelpers;
using MyEnergi.Business.Interfaces;
using MyEnergi.Business.Model.AutoMapper;
using MyEnergi.Common.CurrencyConverter;
using MyEnergi.Core;
using MyEnergi.Data.Context;
using MyEnergi.Data.Interfaces;
using MyEnergi.Data.Repositories;
using MyEnergi.Managers;
using Newtonsoft.Json;
using Serilog;
using StackExchange.Redis;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;

namespace MyEnergi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var region = Configuration["AWSCognito:Region"];
            var poolId = Configuration["AWSCognito:UserPoolId"];
            var clientsId = Configuration["AWSCognito:ClientId"].Split(',');
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        ;
                    });
            });

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKeyResolver = (s, securityToken, identifier, parameters) =>
                        {
                            // Get JsonWebKeySet from AWS
                            var json = new WebClient().DownloadString(parameters.ValidIssuer + "/.well-known/jwks.json");
                            // Serialize the result
                            return JsonConvert.DeserializeObject<JsonWebKeySet>(json).Keys;
                        },
                        ValidateIssuer = true,
                        ValidIssuer = $"https://cognito-idp.{region}.amazonaws.com/{poolId}",
                        ValidateLifetime = true,
                        LifetimeValidator = (before, expires, token, param) => expires > DateTime.UtcNow,
                        ValidateAudience = true,
                        AudienceValidator = (audience, token, param) =>
                        {
                            var castToken = (JwtSecurityToken)token;
                            if (castToken != null)
                            {
                                foreach (string clientId in clientsId)
                                    if (castToken.Audiences.Contains(clientId))
                                        return true;
                                var clientIdClaim = castToken.Claims.FirstOrDefault(c => c.Type.ToLower() == "client_id")?.Value;
                                if (clientsId.Contains(clientIdClaim))
                                    return true;
                            }
                            return false;
                        }
                    };
                });
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build());
            });

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
                mc.AddProfile(new Data.Entity.AutoMapper.MappingProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddControllers();

            services.AddDbContextPool<MyEnergiDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("MyEnergiDb"), sqlServerOptionsAction: sqlOptions =>
                sqlOptions.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null)));

            var redisConnectionInfo = Configuration.GetSection("ConnectionStrings:Redis");
            var redisConnectionString = redisConnectionInfo["Host"] + ":" + redisConnectionInfo["Port"] +
                                        ",allowAdmin=" + redisConnectionInfo["AllowAdmin"];
            ConnectionMultiplexer connectionMultiplexer = null;
            try
            {
                connectionMultiplexer = ConnectionMultiplexer.Connect(redisConnectionString);
            }
            catch (Exception ex)
            {
                Log.Warning(ex, "Unable to connect to REDIS!!!");
            }

            services.AddSingleton<IConnectionMultiplexer>(x => connectionMultiplexer);
            if (connectionMultiplexer != null)
                services.AddSingleton<IServer>(x =>
                    connectionMultiplexer.GetServer(redisConnectionInfo["Host"] + ":" +
                                                    redisConnectionInfo["Port"]));

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IVehicleRepository, VehicleRepository>();
            services.AddScoped<IEnergyProviderRepository, EnergyProviderRepository>();
            services.AddScoped<IHubRepository, HubRepository>();
            services.AddScoped<IInstallerRepository, InstallerRepository>();
            services.AddScoped<IGraphRepository, GraphRepository>();
            services.AddScoped<IChargeScheduleRepository, ChargeScheduleRepository>();
            services.AddScoped<IInvitationRepository, InvitationRepository>();
            services.AddScoped<IUserFeaturePermissionRepository, UserFeaturePermissionRepository>();
            services.AddScoped<IUserFeaturePermissionHistoryRepository, UserFeaturePermissionHistoryRepository>();
            services.AddScoped<IAppFeatureRepository, AppFeatureRepository>();
            services.AddScoped<IInvitationStatusRepository, InvitationStatusRepository>();
            services.AddScoped<IDeviceRepository, DeviceRepository>();
            services.AddScoped<IDeviceHistoryRepository, DeviceHistoryRepository>();
            services.AddScoped<IHubHistoryRepository, HubHistoryRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IUserVehicleRepository, UserVehicleRepository>();

            services.AddScoped<IUserBL, UserBL>();
            services.AddScoped<IVehicleBL, VehicleBL>();
            services.AddScoped<IEnergyProviderBL, EnergyProviderBL>();
            services.AddScoped<IDualTariffEnergyProviderBL, DualTariffEnergyProviderBL>();
            services.AddScoped<IHubBL, HubBL>();
            services.AddScoped<IDeviceBL, DeviceBL>();
            services.AddScoped<IInstallerBL, InstallerBL>();
            services.AddScoped<IGraphBL, GraphBL>();
            services.AddScoped<IAccountSettingsDeviceBL, AccountSettingsDeviceBL>();
            services.AddScoped<IAccountSettingsShareBL, AccountSettingsShareBL>();
            services.AddScoped<IChargeSchedulesBL, ChargeSchedulesBL>();
            services.AddScoped<IAccountAccessShareBL, AccountAccessShareBL>();
            services.AddScoped<IAccountAccessHubBL, AccountAccessHubBL>();
            services.AddScoped<IUserPreferencesBL, UserPreferencesBL>();
            services.AddScoped<IWeatherBL, WeatherBL>();
            services.AddScoped<IStripePaymentBL, StripePaymentBL>();

            services.AddScoped<IApiCallerHelper, ApiCallerHelper>();
            services.AddScoped<IOneSignal, OneSignal>();
            services.AddScoped<IInvitationEmailSender, InvitationEmailSender>();
            services.AddScoped<IRegistrationEmailSender, RegistrationEmailSender>();
            services.AddScoped<IDeleteDeviceRequestEmailSender, DeleteDeviceRequestEmailSender>();
            services.AddScoped<IGuestAccessRemovalEmailSender, GuestAccessRemovalEmailSender>();
            
            services.AddScoped<ICurrencyConverter, CurrencyConverter>();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            // Add Hangfire services.
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("HangfireDb"), new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                }));

            // Add the processing server as IHostedService
            services.AddHangfireServer();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IRecurringJobManager recurringJob, IWebHostEnvironment env)
        {
            app.UseExceptionHandler("/Error");

            if (!env.IsDevelopment())
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            using (var connection = JobStorage.Current.GetConnection())
            {
                foreach (var job in StorageConnectionExtensions.GetRecurringJobs(connection))
                {
                    recurringJob.RemoveIfExists(job.Id);
                }
            }

            recurringJob.AddOrUpdate<ElectricVehicleManager>("EVAPI", x => x.SaveData(), Configuration["HangfireJobSchedules:EVAPI"]);
            recurringJob.AddOrUpdate<EnergyProviderManager>("ENERGYPROVCSV", x => x.SaveData(), Configuration["HangfireJobSchedules:ENERGYPROVCSV"]);
            recurringJob.AddOrUpdate<InstallerManager>("INSTALLERCSV", x => x.SaveData(), Configuration["HangfireJobSchedules:INSTALLERCSV"]);
            string jobConf = Configuration["HangfireJobSchedules:OCTOPUSAPI"];
            if (!string.IsNullOrEmpty(jobConf))
                recurringJob.AddOrUpdate<DeviceProgrammerManager>("OCTOPUSAPI", x => x.SaveData(), jobConf);
            recurringJob.AddOrUpdate<InfoUpdateManager>("INFOMANAGER", x => x.SaveData(), Configuration["HangfireJobSchedules:INFOMANAGER"]);
            recurringJob.AddOrUpdate<DualTariffManager>("DUALTARIFF", x => x.SaveData(), Configuration["HangfireJobSchedules:DUALTARIFF"]);
            recurringJob.AddOrUpdate<ENTSOEManager>("ENTSOEMANAGER", x => x.SaveData(), Configuration["HangfireJobSchedules:ENTSOEMANAGER"]);

            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
            Path.Combine(Directory.GetCurrentDirectory(), "img")),
                RequestPath = "/img"
            });

            app.UseRouting();

            app.UseCors("AllowAll");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
